#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Excitation for RF KO extraction with feedback & optimizer
# Author: Philipp Niedermayer
# Copyright: Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from PyQt5.QtCore import QObject, pyqtSlot
from exciter_signals import exciter_signals  # grc-generated hier_block
from gnuradio import beam_exciter
from gnuradio.beam_exciter import ensure_pmt
from gnuradio import uhd
import pmt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
import datetime
import sip
import time
import threading


def snipfcn_snippet_0(self):
    # Make a combo box widget to help adding variables
    # to the list of optimisation variables and bounds

    def get(p, var):
        try:
            if "." in var:
                pre, var = var.split(".", maxsplit=1)
                return get(getattr(p, pre), var)
            return getattr(p, f"get_{var}")()
        except:
            return None

    items = []
    for i in range(1, 6):
        for desc, var in (("tune", f"qex{i}"),("width", f"dqex{i}"), ("amplitude", f"aex{i}")):
            if get(self, f"sig.{var}") is not None:
                items.append(f"Signal {i} {desc}   (sig.{var})")
    items.extend([
        "Controller Kp   (kp_set)",
        "Controller Ki   (ki_set)",
        "Controller Kd   (kd_set)",
        "Controller Ta   (ta_set)",
    ])

    picker = Qt.QComboBox()
    picker.addItem("Add...")
    picker.addItems(items)
    picker.addItem("Clear all")

    def picked(i):
        if i:
            val = picker.currentText().rsplit(" (")[-1].strip("()")
            picker.setCurrentIndex(0)
            if val == "Clear all":
                self.set_opti_variables([])
                self.set_opti_bounds([])
            else:
                self.set_opti_variables(list(self.get_opti_variables()) + [val])
                bounds = (0,0)
                v = get(self, val)
                if v is not None:
                    bounds = (float(f"{0.95*float(v):g}"), float(f"{1.05*float(v):g}"))
                self.set_opti_bounds(list(self.get_opti_bounds()) + [bounds])

    picker.currentIndexChanged.connect(picked)
    picker.setFixedWidth(75)
    self.tab_excitation_grid_layout_2.addWidget(picker, 2, 7, 1, 1)

def snipfcn_snippet_1(self):
    # Handle an uncought exception
    def handle_exception(exc_type, exc_value, exc_traceback):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        print("Troubleshooting help at https://git.gsi.de/p.niedermayer/exciter/-/wikis/Troubleshooting")
        # Inform user by flashing window in red
        w = self # the top block, i.e. main qt window
        w.setAttribute(Qt.Qt.WA_StyledBackground, True)
        w.setStyleSheet('background-color: red;')
        from PyQt5 import QtCore
        QtCore.QTimer.singleShot(200, lambda: w.setStyleSheet(""))

    sys.excepthook = handle_exception


def snippets_main_after_init(tb):
    snipfcn_snippet_0(tb)

def snippets_init_before_blocks(tb):
    snipfcn_snippet_1(tb)

class exciter_extraction_feedback_optimize(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Excitation for RF KO extraction with feedback & optimizer", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Excitation for RF KO extraction with feedback & optimizer")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "exciter_extraction_feedback_optimize")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.opti_repeat_wait = opti_repeat_wait = 0.5
        self.opti_dt_eval = opti_dt_eval = 0
        self.opti_dt_count = opti_dt_count = 500e-6
        self.fs_adc = fs_adc = 200e6
        self.decim = decim = 1000
        self.trg_count = trg_count = 0
        self.target_rate = target_rate = 1e6
        self.ta_set = ta_set = 0.1
        self.repeat = repeat = 25
        self.output_duration_set = output_duration_set = 5
        self.output_duration_gated = output_duration_gated = True
        self.opti_value = opti_value = 0
        self.opti_tol = opti_tol = 0.1
        self.opti_t_start = opti_t_start = 0.5
        self.opti_steps = opti_steps = 50
        self.opti_repeat_stop = opti_repeat_stop = 0
        self.opti_repeat_every = opti_repeat_every = (max(opti_dt_count, opti_dt_eval)+opti_repeat_wait)
        self.opti_repeat = opti_repeat = 0
        self.opti_bounds = opti_bounds = [(0.32, 0.36), (0.005, 0.02), (0.63, 0.68), (1.63, 1.68)]
        self.nparticles = nparticles = 1e7
        self.kp_set = kp_set = 0.14
        self.kp_init_set = kp_init_set = 0
        self.ki_set = ki_set = 0.14/0.002
        self.ki_init_set = ki_init_set = 0
        self.kd_set = kd_set = 0
        self.kd_init_set = kd_init_set = 0
        self.fs_detector = fs_detector = fs_adc/decim
        self.feedforward = feedforward = 1
        self.count_rate_probe = count_rate_probe = 0
        self.controller_value = controller_value = 0
        self.y_pre = y_pre = float(target_rate)/float(nparticles) if feedforward else 0
        self.upsample = upsample = 20

        self.rfnoc_graph = uhd_rfnoc_graph = uhd.rfnoc_graph(uhd.device_addr(",fpga=HG_EXFB"))
        self.slot = slot = '1'
        self.rise_time_ms = rise_time_ms = 0
        self.qtgui_label_trg_count = qtgui_label_trg_count = trg_count
        self.qtgui_label_objective = qtgui_label_objective = "{:g}".format(opti_value)
        self.output_on = output_on = 0
        self.output_duration = output_duration = 0 if output_duration_gated else output_duration_set
        self.out_max_init = out_max_init = 2
        self.out_max = out_max = 2
        self.out_label = out_label = controller_value
        self.opti_variables = opti_variables = ['sig.qex1', 'sig.dqex1', 'sig.qex2', 'sig.qex3']
        self.opti_subtract_target = opti_subtract_target = 0
        self.opti_repeat_count = opti_repeat_count = int(1 if not opti_repeat else 0 if not opti_repeat_stop else max((opti_repeat_stop-opti_t_start+opti_repeat_wait)/opti_repeat_every, 1))
        self.opti_options_scan = opti_options_scan = {"bounds":[list(b) for b in zip(*opti_bounds)], "steps":opti_steps}
        self.opti_options = opti_options = {"bounds":[list(b) for b in zip(*opti_bounds)], "user_params":{"model.abs_tol":opti_tol}, "scaling_within_bounds":True, "objfun_has_noise": True, "seek_global_minimum": True}
        self.opti_method = opti_method = 4
        self.opti_logfile = opti_logfile = 'data/%y%m%d/%H%M%S_optimisation.log'
        self.opti_enable = opti_enable = False
        self.opti_algorithm = opti_algorithm = 'PyBOBYQA'
        self.norm = norm = 5
        self.mode = mode = 0
        self.maxoutbuf = maxoutbuf = 64
        self.manual_trigger = manual_trigger = 0
        self.inp_thr_low = inp_thr_low = -0.5
        self.inp_thr_high = inp_thr_high = -0.3
        self.inp_factor = inp_factor = 1
        self.inp_coupling = inp_coupling = False
        self.info = info = 'Description'
        self.fs_signal = fs_signal = fs_detector*repeat
        self.fs_gpio = fs_gpio = 1000
        self.feedback_init_threshold_smoothing = feedback_init_threshold_smoothing = 100
        self.feedback_init_threshold = feedback_init_threshold = 0.8
        self.feedback_init = feedback_init = False
        self.feedback = feedback = 0
        self.enable_ext_trg = enable_ext_trg = False
        self.duration = duration = (float(nparticles)/float(target_rate))
        self.delay = delay = 1e-3
        self.daq_save_enabled = daq_save_enabled = False
        self.daq_filename = daq_filename = ""+f"data/%y%m%d/%H%M%S_tmp.{fs_detector/1e3:g}kSps.complex64"
        self.count_rate_label = count_rate_label = count_rate_probe
        self.abort_on_max_limit = abort_on_max_limit = False
        self.Ta = Ta = float(ta_set)*1e-3
        self.Kp_init = Kp_init = float(kp_init_set)/float(nparticles)
        self.Kp = Kp = float(kp_set)/float(nparticles)
        self.Ki_init = Ki_init = float(ki_init_set)/float(nparticles)
        self.Ki = Ki = float(ki_set)/float(nparticles)
        self.Kd_init = Kd_init = float(kd_init_set)/float(nparticles)
        self.Kd = Kd = float(kd_set)/float(nparticles)

        ##################################################
        # Blocks
        ##################################################
        snippets_init_before_blocks(self)
        self.tab_excitation = Qt.QTabWidget()
        self.tab_excitation_widget_0 = Qt.QWidget()
        self.tab_excitation_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_excitation_widget_0)
        self.tab_excitation_grid_layout_0 = Qt.QGridLayout()
        self.tab_excitation_layout_0.addLayout(self.tab_excitation_grid_layout_0)
        self.tab_excitation.addTab(self.tab_excitation_widget_0, 'Excitation Signal')
        self.tab_excitation_widget_1 = Qt.QWidget()
        self.tab_excitation_layout_1 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_excitation_widget_1)
        self.tab_excitation_grid_layout_1 = Qt.QGridLayout()
        self.tab_excitation_layout_1.addLayout(self.tab_excitation_grid_layout_1)
        self.tab_excitation.addTab(self.tab_excitation_widget_1, 'Level Control')
        self.tab_excitation_widget_2 = Qt.QWidget()
        self.tab_excitation_layout_2 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_excitation_widget_2)
        self.tab_excitation_grid_layout_2 = Qt.QGridLayout()
        self.tab_excitation_layout_2.addLayout(self.tab_excitation_grid_layout_2)
        self.tab_excitation.addTab(self.tab_excitation_widget_2, 'Automatic Optimizer')
        self.tab_excitation_widget_3 = Qt.QWidget()
        self.tab_excitation_layout_3 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_excitation_widget_3)
        self.tab_excitation_grid_layout_3 = Qt.QGridLayout()
        self.tab_excitation_layout_3.addLayout(self.tab_excitation_grid_layout_3)
        self.tab_excitation.addTab(self.tab_excitation_widget_3, 'Expert')
        self.top_grid_layout.addWidget(self.tab_excitation, 2, 0, 1, 4)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 4):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.tab_output = Qt.QTabWidget()
        self.tab_output_widget_0 = Qt.QWidget()
        self.tab_output_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_output_widget_0)
        self.tab_output_grid_layout_0 = Qt.QGridLayout()
        self.tab_output_layout_0.addLayout(self.tab_output_grid_layout_0)
        self.tab_output.addTab(self.tab_output_widget_0, 'Output')
        self.top_grid_layout.addWidget(self.tab_output, 10, 0, 1, 4)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 4):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.tab_input = Qt.QTabWidget()
        self.tab_input_widget_0 = Qt.QWidget()
        self.tab_input_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_input_widget_0)
        self.tab_input_grid_layout_0 = Qt.QGridLayout()
        self.tab_input_layout_0.addLayout(self.tab_input_grid_layout_0)
        self.tab_input.addTab(self.tab_input_widget_0, 'Detector Signal')
        self.tab_input_widget_1 = Qt.QWidget()
        self.tab_input_layout_1 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_input_widget_1)
        self.tab_input_grid_layout_1 = Qt.QGridLayout()
        self.tab_input_layout_1.addLayout(self.tab_input_grid_layout_1)
        self.tab_input.addTab(self.tab_input_widget_1, 'Config')
        self.top_grid_layout.addWidget(self.tab_input, 0, 0, 1, 4)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 4):
            self.top_grid_layout.setColumnStretch(c, 1)
        _opti_repeat_check_box = Qt.QCheckBox("Repeat in spill")
        self._opti_repeat_choices = {True: 1, False: 0}
        self._opti_repeat_choices_inv = dict((v,k) for k,v in self._opti_repeat_choices.items())
        self._opti_repeat_callback = lambda i: Qt.QMetaObject.invokeMethod(_opti_repeat_check_box, "setChecked", Qt.Q_ARG("bool", self._opti_repeat_choices_inv[i]))
        self._opti_repeat_callback(self.opti_repeat)
        _opti_repeat_check_box.stateChanged.connect(lambda i: self.set_opti_repeat(self._opti_repeat_choices[bool(i)]))
        self.tab_excitation_grid_layout_2.addWidget(_opti_repeat_check_box, 5, 4, 1, 2)
        for r in range(5, 6):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(4, 6):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)

        self._opti_dt_count_tool_bar = beam_exciter.NumericEntry(self.set_opti_dt_count, 'Resolution', 500e-6, 50e-6, 's', 'Resolution for objective evaluation', 10, True)
        self._opti_dt_count_tool_bar.set_limits(1/fs_detector, 0.1)
        self.tab_excitation_grid_layout_2.addWidget(self._opti_dt_count_tool_bar, 6, 4, 1, 2)
        for r in range(6, 7):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(4, 6):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)

        self._target_rate_tool_bar = beam_exciter.NumericEntry(self.set_target_rate, 'Target rate', 1e6, 1e6, 'particles/s', 'Desired spill rate', 3, True)
        self._target_rate_tool_bar.set_limits(1, float("inf"))
        self.tab_excitation_grid_layout_1.addWidget(self._target_rate_tool_bar, 0, 10, 1, 10)
        for r in range(0, 1):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(10, 20):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)
        # Create the options list
        self._slot_options = ['1', '2', '3', '4', '5']
        # Create the labels list
        self._slot_labels = ['1', '2', '3', '4', '5']
        # Create the combo box
        self._slot_tool_bar = Qt.QToolBar(self)
        self._slot_tool_bar.addWidget(Qt.QLabel("Configuration" + ": "))
        self._slot_combo_box = Qt.QComboBox()
        self._slot_tool_bar.addWidget(self._slot_combo_box)
        for _label in self._slot_labels: self._slot_combo_box.addItem(_label)
        self._slot_callback = lambda i: Qt.QMetaObject.invokeMethod(self._slot_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._slot_options.index(i)))
        self._slot_callback(self.slot)
        self._slot_combo_box.currentIndexChanged.connect(
            lambda i: self.set_slot(self._slot_options[i]))
        # Create the radio buttons
        self.top_grid_layout.addWidget(self._slot_tool_bar, 100, 0, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)

        self._rise_time_ms_tool_bar = beam_exciter.NumericEntry(self.set_rise_time_ms, 'Rise time', 0, 10, 'ms', 'Feedback controller initial rise time from 0 to target_rate', 5, True)
        self._rise_time_ms_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_1.addWidget(self._rise_time_ms_tool_bar, 30, 25, 1, 5)
        for r in range(30, 31):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(25, 30):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)
        _output_duration_gated_check_box = Qt.QCheckBox("Auto duration (trigger gate)")
        self._output_duration_gated_choices = {True: True, False: False}
        self._output_duration_gated_choices_inv = dict((v,k) for k,v in self._output_duration_gated_choices.items())
        self._output_duration_gated_callback = lambda i: Qt.QMetaObject.invokeMethod(_output_duration_gated_check_box, "setChecked", Qt.Q_ARG("bool", self._output_duration_gated_choices_inv[i]))
        self._output_duration_gated_callback(self.output_duration_gated)
        _output_duration_gated_check_box.stateChanged.connect(lambda i: self.set_output_duration_gated(self._output_duration_gated_choices[bool(i)]))
        self.tab_output_grid_layout_0.addWidget(_output_duration_gated_check_box, 1, 1, 1, 1)
        for r in range(1, 2):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)

        self._out_max_init_tool_bar = beam_exciter.NumericEntry(self.set_out_max_init, 'Controller limit init', 2, 0.1, '', 'Controller value limit (to prevent overload)', 10, True)
        self._out_max_init_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_3.addWidget(self._out_max_init_tool_bar, 12, 1, 1, 1)
        for r in range(12, 13):
            self.tab_excitation_grid_layout_3.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_excitation_grid_layout_3.setColumnStretch(c, 1)

        self._out_max_tool_bar = beam_exciter.NumericEntry(self.set_out_max, 'Limit', 2, 0.1, '', 'Controller value limit (to prevent overload)', 10, True)
        self._out_max_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_1.addWidget(self._out_max_tool_bar, 30, 10, 1, 5)
        for r in range(30, 31):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(10, 15):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)
        self._opti_variables_tool_bar = Qt.QToolBar(self)
        self._opti_variables_tool_bar.addWidget(Qt.QLabel("Variables" + ": "))
        self._opti_variables_line_edit = Qt.QLineEdit(str(self.opti_variables))
        self._opti_variables_tool_bar.addWidget(self._opti_variables_line_edit)
        self._opti_variables_line_edit.editingFinished.connect(
            lambda: self.set_opti_variables(eval(str(self._opti_variables_line_edit.text()))))
        self.tab_excitation_grid_layout_2.addWidget(self._opti_variables_tool_bar, 2, 0, 1, 7)
        for r in range(2, 3):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(0, 7):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)

        self._opti_t_start_tool_bar = beam_exciter.NumericEntry(self.set_opti_t_start, 'Start', 0.5, 0.1, 's', 'Start after trigger for objective evaluation', 10, True)
        self._opti_t_start_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_2.addWidget(self._opti_t_start_tool_bar, 5, 0, 1, 2)
        for r in range(5, 6):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(0, 2):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)
        _opti_subtract_target_check_box = Qt.QCheckBox("W.r.t. target rate")
        self._opti_subtract_target_choices = {True: 1, False: 0}
        self._opti_subtract_target_choices_inv = dict((v,k) for k,v in self._opti_subtract_target_choices.items())
        self._opti_subtract_target_callback = lambda i: Qt.QMetaObject.invokeMethod(_opti_subtract_target_check_box, "setChecked", Qt.Q_ARG("bool", self._opti_subtract_target_choices_inv[i]))
        self._opti_subtract_target_callback(self.opti_subtract_target)
        _opti_subtract_target_check_box.stateChanged.connect(lambda i: self.set_opti_subtract_target(self._opti_subtract_target_choices[bool(i)]))
        self.tab_excitation_grid_layout_2.addWidget(_opti_subtract_target_check_box, 6, 6, 1, 2)
        for r in range(6, 7):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(6, 8):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)
        # Create the options list
        self._opti_method_options = [0, 1, 2, 3, 4]
        # Create the labels list
        self._opti_method_labels = ['RMS (Root mean square)', 'STD (Standard deviation)', 'MEAN (Mean value)', 'MABS (Mean absolute value)', 'CV (Coefficient of variation)']
        # Create the combo box
        self._opti_method_tool_bar = Qt.QToolBar(self)
        self._opti_method_tool_bar.addWidget(Qt.QLabel("Objective" + ": "))
        self._opti_method_combo_box = Qt.QComboBox()
        self._opti_method_tool_bar.addWidget(self._opti_method_combo_box)
        for _label in self._opti_method_labels: self._opti_method_combo_box.addItem(_label)
        self._opti_method_callback = lambda i: Qt.QMetaObject.invokeMethod(self._opti_method_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._opti_method_options.index(i)))
        self._opti_method_callback(self.opti_method)
        self._opti_method_combo_box.currentIndexChanged.connect(
            lambda i: self.set_opti_method(self._opti_method_options[i]))
        # Create the radio buttons
        self.tab_excitation_grid_layout_2.addWidget(self._opti_method_tool_bar, 6, 0, 1, 4)
        for r in range(6, 7):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(0, 4):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)
        self._opti_logfile_tool_bar = Qt.QToolBar(self)
        self._opti_logfile_tool_bar.addWidget(Qt.QLabel("Optimizer: Logfile" + ": "))
        self._opti_logfile_line_edit = Qt.QLineEdit(str(self.opti_logfile))
        self._opti_logfile_tool_bar.addWidget(self._opti_logfile_line_edit)
        self._opti_logfile_line_edit.editingFinished.connect(
            lambda: self.set_opti_logfile(str(str(self._opti_logfile_line_edit.text()))))
        self.tab_excitation_grid_layout_3.addWidget(self._opti_logfile_tool_bar, 1, 1, 1, 2)
        for r in range(1, 2):
            self.tab_excitation_grid_layout_3.setRowStretch(r, 1)
        for c in range(1, 3):
            self.tab_excitation_grid_layout_3.setColumnStretch(c, 1)
        self._opti_enable_choices = {'Pressed': bool(True), 'Released': bool(False)}

        _opti_enable_toggle_button = qtgui.ToggleButton(self.set_opti_enable, 'Optimize', self._opti_enable_choices, False, 'value')
        _opti_enable_toggle_button.setColors("default", "default", "yellow", "default")
        self.opti_enable = _opti_enable_toggle_button

        self.tab_excitation_grid_layout_2.addWidget(_opti_enable_toggle_button, 20, 4, 1, 4)
        for r in range(20, 21):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(4, 8):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)

        self._opti_dt_eval_tool_bar = beam_exciter.NumericEntry(self.set_opti_dt_eval, 'Duration', 0, 0.1, 's', 'Duration for objective evaluation (0 = until end of spill)', 10, True)
        self._opti_dt_eval_tool_bar.set_limits(opti_dt_count*100 if opti_repeat else 0, float("inf"))
        self.tab_excitation_grid_layout_2.addWidget(self._opti_dt_eval_tool_bar, 5, 2, 1, 2)
        for r in range(5, 6):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(2, 4):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)
        # Create the options list
        self._opti_algorithm_options = ['PyBOBYQA', 'RasterScan']
        # Create the labels list
        self._opti_algorithm_labels = ['PyBOBYQA', 'RasterScan']
        # Create the combo box
        self._opti_algorithm_tool_bar = Qt.QToolBar(self)
        self._opti_algorithm_tool_bar.addWidget(Qt.QLabel("Optimizer: Algorithm" + ": "))
        self._opti_algorithm_combo_box = Qt.QComboBox()
        self._opti_algorithm_tool_bar.addWidget(self._opti_algorithm_combo_box)
        for _label in self._opti_algorithm_labels: self._opti_algorithm_combo_box.addItem(_label)
        self._opti_algorithm_callback = lambda i: Qt.QMetaObject.invokeMethod(self._opti_algorithm_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._opti_algorithm_options.index(i)))
        self._opti_algorithm_callback(self.opti_algorithm)
        self._opti_algorithm_combo_box.currentIndexChanged.connect(
            lambda i: self.set_opti_algorithm(self._opti_algorithm_options[i]))
        # Create the radio buttons
        self.tab_excitation_grid_layout_3.addWidget(self._opti_algorithm_tool_bar, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_excitation_grid_layout_3.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_excitation_grid_layout_3.setColumnStretch(c, 1)

        self._norm_tool_bar = beam_exciter.NumericEntry(self.set_norm, 'Global level normalisation', 5, 0.1, '', 'Factor to account for beam rigidity etc.', 10, True)
        self._norm_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_1.addWidget(self._norm_tool_bar, 40, 0, 1, 10)
        for r in range(40, 41):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(0, 10):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)
        # Create the options list
        self._mode_options = [0, 1]
        # Create the labels list
        self._mode_labels = ['Spill [particles/s per Hz]', 'DCCT [particles per Hz]']
        # Create the combo box
        self._mode_tool_bar = Qt.QToolBar(self)
        self._mode_tool_bar.addWidget(Qt.QLabel("Detector" + ": "))
        self._mode_combo_box = Qt.QComboBox()
        self._mode_tool_bar.addWidget(self._mode_combo_box)
        for _label in self._mode_labels: self._mode_combo_box.addItem(_label)
        self._mode_callback = lambda i: Qt.QMetaObject.invokeMethod(self._mode_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._mode_options.index(i)))
        self._mode_callback(self.mode)
        self._mode_combo_box.currentIndexChanged.connect(
            lambda i: self.set_mode(self._mode_options[i]))
        # Create the radio buttons
        self.tab_input_grid_layout_0.addWidget(self._mode_tool_bar, 1, 2, 1, 2)
        for r in range(1, 2):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 4):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        self._manual_trigger_choices = {'Pressed': 1, 'Released': 0}

        _manual_trigger_toggle_button = qtgui.ToggleButton(self.set_manual_trigger, 'Manual trigger', self._manual_trigger_choices, False, 'value')
        _manual_trigger_toggle_button.setColors("default", "default", "default", "default")
        self.manual_trigger = _manual_trigger_toggle_button

        self.tab_output_grid_layout_0.addWidget(_manual_trigger_toggle_button, 0, 2, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)

        self._inp_thr_low_tool_bar = beam_exciter.NumericEntry(self.set_inp_thr_low, 'Threshold low', -0.5, 0.1, 'V', 'Threshold for pulse detection (Schmitt trigger)', 10, True)
        self._inp_thr_low_tool_bar.set_limits(-1, 1)
        self.tab_input_grid_layout_1.addWidget(self._inp_thr_low_tool_bar, 0, 0, 1, 2)
        for r in range(0, 1):
            self.tab_input_grid_layout_1.setRowStretch(r, 1)
        for c in range(0, 2):
            self.tab_input_grid_layout_1.setColumnStretch(c, 1)

        self._inp_thr_high_tool_bar = beam_exciter.NumericEntry(self.set_inp_thr_high, 'Threshold high', -0.3, 0.1, 'V', 'Threshold for pulse detection (Schmitt trigger)', 10, True)
        self._inp_thr_high_tool_bar.set_limits(-1, +1)
        self.tab_input_grid_layout_1.addWidget(self._inp_thr_high_tool_bar, 0, 2, 1, 2)
        for r in range(0, 1):
            self.tab_input_grid_layout_1.setRowStretch(r, 1)
        for c in range(2, 4):
            self.tab_input_grid_layout_1.setColumnStretch(c, 1)

        self._inp_factor_tool_bar = beam_exciter.NumericEntry(self.set_inp_factor, "Calibration", 1, 1, '', 'Number of particles to produce a detector pulse', 10, True)
        self._inp_factor_tool_bar.set_limits(1, float("inf"))
        self.tab_input_grid_layout_0.addWidget(self._inp_factor_tool_bar, 1, 4, 1, 1)
        for r in range(1, 2):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(4, 5):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        _inp_coupling_push_button = Qt.QPushButton('Calibrate Offset')
        _inp_coupling_push_button = Qt.QPushButton('Calibrate Offset')
        self._inp_coupling_choices = {'Pressed': True, 'Released': False}
        _inp_coupling_push_button.pressed.connect(lambda: self.set_inp_coupling(self._inp_coupling_choices['Pressed']))
        _inp_coupling_push_button.released.connect(lambda: self.set_inp_coupling(self._inp_coupling_choices['Released']))
        self.tab_input_grid_layout_1.addWidget(_inp_coupling_push_button, 0, 4, 1, 1)
        for r in range(0, 1):
            self.tab_input_grid_layout_1.setRowStretch(r, 1)
        for c in range(4, 5):
            self.tab_input_grid_layout_1.setColumnStretch(c, 1)
        # Create the options list
        self._feedforward_options = [0, 1, 2, 3]
        # Create the labels list
        self._feedforward_labels = ['Off', 'Constant', 'Memory record', 'Memory playback']
        # Create the combo box
        self._feedforward_tool_bar = Qt.QToolBar(self)
        self._feedforward_tool_bar.addWidget(Qt.QLabel("Feedforward" + ": "))
        self._feedforward_combo_box = Qt.QComboBox()
        self._feedforward_tool_bar.addWidget(self._feedforward_combo_box)
        for _label in self._feedforward_labels: self._feedforward_combo_box.addItem(_label)
        self._feedforward_callback = lambda i: Qt.QMetaObject.invokeMethod(self._feedforward_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._feedforward_options.index(i)))
        self._feedforward_callback(self.feedforward)
        self._feedforward_combo_box.currentIndexChanged.connect(
            lambda i: self.set_feedforward(self._feedforward_options[i]))
        # Create the radio buttons
        self.tab_excitation_grid_layout_1.addWidget(self._feedforward_tool_bar, 10, 0, 1, 10)
        for r in range(10, 11):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(0, 10):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)

        self._feedback_init_threshold_smoothing_tool_bar = beam_exciter.NumericEntry(self.set_feedback_init_threshold_smoothing, 'Init threshold smoothing', 100, 1, '', 'Number of Ta to smooth over for determination of init treshhold', 10, True)
        self._feedback_init_threshold_smoothing_tool_bar.set_limits(1, float("inf"))
        self.tab_excitation_grid_layout_3.addWidget(self._feedback_init_threshold_smoothing_tool_bar, 11, 1, 1, 1)
        for r in range(11, 12):
            self.tab_excitation_grid_layout_3.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_excitation_grid_layout_3.setColumnStretch(c, 1)

        self._feedback_init_threshold_tool_bar = beam_exciter.NumericEntry(self.set_feedback_init_threshold, 'Feedback init threshold', 0.8, 0.01, '', "Fraction of target rate after which 'init' ends", 10, True)
        self._feedback_init_threshold_tool_bar.set_limits(0, 10)
        self.tab_excitation_grid_layout_3.addWidget(self._feedback_init_threshold_tool_bar, 10, 1, 1, 1)
        for r in range(10, 11):
            self.tab_excitation_grid_layout_3.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_excitation_grid_layout_3.setColumnStretch(c, 1)
        _feedback_init_check_box = Qt.QCheckBox("Feedback: Use different init params")
        self._feedback_init_choices = {True: True, False: False}
        self._feedback_init_choices_inv = dict((v,k) for k,v in self._feedback_init_choices.items())
        self._feedback_init_callback = lambda i: Qt.QMetaObject.invokeMethod(_feedback_init_check_box, "setChecked", Qt.Q_ARG("bool", self._feedback_init_choices_inv[i]))
        self._feedback_init_callback(self.feedback_init)
        _feedback_init_check_box.stateChanged.connect(lambda i: self.set_feedback_init(self._feedback_init_choices[bool(i)]))
        self.tab_excitation_grid_layout_3.addWidget(_feedback_init_check_box, 10, 0, 1, 1)
        for r in range(10, 11):
            self.tab_excitation_grid_layout_3.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_excitation_grid_layout_3.setColumnStretch(c, 1)
        # Create the options list
        self._feedback_options = [0, 1]
        # Create the labels list
        self._feedback_labels = ['Off', 'On']
        # Create the combo box
        self._feedback_tool_bar = Qt.QToolBar(self)
        self._feedback_tool_bar.addWidget(Qt.QLabel("Feedback control" + ": "))
        self._feedback_combo_box = Qt.QComboBox()
        self._feedback_tool_bar.addWidget(self._feedback_combo_box)
        for _label in self._feedback_labels: self._feedback_combo_box.addItem(_label)
        self._feedback_callback = lambda i: Qt.QMetaObject.invokeMethod(self._feedback_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._feedback_options.index(i)))
        self._feedback_callback(self.feedback)
        self._feedback_combo_box.currentIndexChanged.connect(
            lambda i: self.set_feedback(self._feedback_options[i]))
        # Create the radio buttons
        self.tab_excitation_grid_layout_1.addWidget(self._feedback_tool_bar, 20, 0, 1, 10)
        for r in range(20, 21):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(0, 10):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)
        _enable_ext_trg_check_box = Qt.QCheckBox("Enable external trigger")
        self._enable_ext_trg_choices = {True: True, False: False}
        self._enable_ext_trg_choices_inv = dict((v,k) for k,v in self._enable_ext_trg_choices.items())
        self._enable_ext_trg_callback = lambda i: Qt.QMetaObject.invokeMethod(_enable_ext_trg_check_box, "setChecked", Qt.Q_ARG("bool", self._enable_ext_trg_choices_inv[i]))
        self._enable_ext_trg_callback(self.enable_ext_trg)
        _enable_ext_trg_check_box.stateChanged.connect(lambda i: self.set_enable_ext_trg(self._enable_ext_trg_choices[bool(i)]))
        self.tab_output_grid_layout_0.addWidget(_enable_ext_trg_check_box, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._daq_save_enabled_choices = {'Pressed': bool(True), 'Released': bool(False)}

        _daq_save_enabled_toggle_button = qtgui.ToggleButton(self.set_daq_save_enabled, 'Data saving', self._daq_save_enabled_choices, False, 'value')
        _daq_save_enabled_toggle_button.setColors("default", "default", "green", "default")
        self.daq_save_enabled = _daq_save_enabled_toggle_button

        self.tab_input_grid_layout_0.addWidget(_daq_save_enabled_toggle_button, 10, 0, 1, 2)
        for r in range(10, 11):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 2):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        self._daq_filename_tool_bar = Qt.QToolBar(self)
        self._daq_filename_tool_bar.addWidget(Qt.QLabel("Filename" + ": "))
        self._daq_filename_line_edit = Qt.QLineEdit(str(self.daq_filename))
        self._daq_filename_tool_bar.addWidget(self._daq_filename_line_edit)
        self._daq_filename_line_edit.editingFinished.connect(
            lambda: self.set_daq_filename(str(str(self._daq_filename_line_edit.text()))))
        self.tab_input_grid_layout_0.addWidget(self._daq_filename_tool_bar, 10, 2, 1, 3)
        for r in range(10, 11):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 5):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        self.blocks_probe_count_rate = blocks.probe_signal_f()
        self.blocks_probe_controller_output = blocks.probe_signal_f()
        _abort_on_max_limit_check_box = Qt.QCheckBox("Abort burst at limit")
        self._abort_on_max_limit_choices = {True: True, False: False}
        self._abort_on_max_limit_choices_inv = dict((v,k) for k,v in self._abort_on_max_limit_choices.items())
        self._abort_on_max_limit_callback = lambda i: Qt.QMetaObject.invokeMethod(_abort_on_max_limit_check_box, "setChecked", Qt.Q_ARG("bool", self._abort_on_max_limit_choices_inv[i]))
        self._abort_on_max_limit_callback(self.abort_on_max_limit)
        _abort_on_max_limit_check_box.stateChanged.connect(lambda i: self.set_abort_on_max_limit(self._abort_on_max_limit_choices[bool(i)]))
        self.tab_excitation_grid_layout_1.addWidget(_abort_on_max_limit_check_box, 30, 15, 1, 10)
        for r in range(30, 31):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(15, 25):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)
        self.uhd_rfnoc_tx_streamer_0 = uhd.rfnoc_tx_streamer(
            self.rfnoc_graph,
            1,
            uhd.stream_args(
                cpu_format="fc32",
                otw_format="sc16",
                channels=[],
                args='',
            ),
            1
        )
        self.uhd_rfnoc_tx_radio_0_0 = uhd.rfnoc_tx_radio(
            self.rfnoc_graph,
            uhd.device_addr(''),
            (-1),
            1)
        self.uhd_rfnoc_tx_radio_0_0.set_rate(fs_adc)
        self.uhd_rfnoc_tx_radio_0_0.set_antenna('AB', 0)
        self.uhd_rfnoc_tx_radio_0_0.set_gain(0, 0)
        self.uhd_rfnoc_tx_radio_0_0.set_bandwidth(0, 0)
        self.uhd_rfnoc_tx_radio_0_0.set_frequency(0, 0)
        self.uhd_rfnoc_rx_streamer_0 = uhd.rfnoc_rx_streamer(
            self.rfnoc_graph,
            1,
            uhd.stream_args(
                cpu_format="fc32",
                otw_format="sc16",
                channels=[],
                args='',
            ),
            1,
            True,
            False,
            uhd.time_spec(0),
        )
        self.uhd_rfnoc_rx_streamer_0.set_max_output_buffer(maxoutbuf)
        self.uhd_rfnoc_rx_radio_0 = uhd.rfnoc_rx_radio(
            self.rfnoc_graph,
            uhd.device_addr('spp=64'),
            (-1),
            0)
        self.uhd_rfnoc_rx_radio_0.set_rate(fs_adc)
        self.uhd_rfnoc_rx_radio_0.set_antenna('AB', 0)
        self.uhd_rfnoc_rx_radio_0.set_gain(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_bandwidth(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_frequency(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_dc_offset(inp_coupling, 0)
        self.uhd_rfnoc_rx_radio_0.set_iq_balance(False, 0)
        self.uhd_rfnoc_rx_radio_0.enable_rx_timestamps(True, 0)
        self.uhd_rfnoc_duc_0_0 = uhd.rfnoc_duc(
            self.rfnoc_graph,
            uhd.device_addr(''),
            (-1),
            0)
        self.uhd_rfnoc_duc_0_0.set_freq(0, 0)
        self.uhd_rfnoc_duc_0_0.set_input_rate(fs_signal, 0)

        self._ta_set_tool_bar = beam_exciter.NumericEntry(self.set_ta_set, 'Ta', 0.1, 0.01, 'ms', 'Feedback controller averaging time', 10, True)
        self._ta_set_tool_bar.set_limits(2*1e3/fs_detector, 100)
        self.tab_excitation_grid_layout_1.addWidget(self._ta_set_tool_bar, 20, 25, 1, 5)
        for r in range(20, 21):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(25, 30):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)
        self.sig = exciter_signals(
            samp_rate=fs_signal,
            upsample=upsample,
        )

        self.tab_excitation_grid_layout_0.addWidget(self.sig, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_excitation_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_excitation_grid_layout_0.setColumnStretch(c, 1)
        self.save = _save_toggle_button = qtgui.MsgPushButton('Save config', 'pressed',1,"default","default")
        self.save = _save_toggle_button

        self.top_grid_layout.addWidget(_save_toggle_button, 100, 2, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.restore = _restore_toggle_button = qtgui.MsgPushButton('Restore config', 'pressed',1,"default","default")
        self.restore = _restore_toggle_button

        self.top_grid_layout.addWidget(_restore_toggle_button, 100, 3, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(3, 4):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_rms = qtgui.number_sink(
            gr.sizeof_float,
            1,
            qtgui.NUM_GRAPH_HORIZ,
            1,
            None # parent
        )
        self.qtgui_rms.set_update_time(0.10)
        self.qtgui_rms.set_title("")

        labels = ['Output RMS:', '', '', '', '',
            '', '', '', '', '']
        units = ['V', '', '', '', '',
            '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
            ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]

        for i in range(1):
            self.qtgui_rms.set_min(i, 0)
            self.qtgui_rms.set_max(i, 1)
            self.qtgui_rms.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_rms.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_rms.set_label(i, labels[i])
            self.qtgui_rms.set_unit(i, units[i])
            self.qtgui_rms.set_factor(i, factor[i])

        self.qtgui_rms.enable_autoscale(False)
        self._qtgui_rms_win = sip.wrapinstance(self.qtgui_rms.qwidget(), Qt.QWidget)
        self.tab_output_grid_layout_0.addWidget(self._qtgui_rms_win, 10, 1, 1, 1)
        for r in range(10, 11):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_overload = qtgui.number_sink(
            gr.sizeof_float,
            1,
            qtgui.NUM_GRAPH_HORIZ,
            1,
            None # parent
        )
        self.qtgui_overload.set_update_time(0.10)
        self.qtgui_overload.set_title("")

        labels = ['Overload:', '', '', '', '',
            '', '', '', '', '']
        units = ['%', '', '', '', '',
            '', '', '', '', '']
        colors = [("black", "red"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
            ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]

        for i in range(1):
            self.qtgui_overload.set_min(i, 0)
            self.qtgui_overload.set_max(i, 40)
            self.qtgui_overload.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_overload.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_overload.set_label(i, labels[i])
            self.qtgui_overload.set_unit(i, units[i])
            self.qtgui_overload.set_factor(i, factor[i])

        self.qtgui_overload.enable_autoscale(False)
        self._qtgui_overload_win = sip.wrapinstance(self.qtgui_overload.qwidget(), Qt.QWidget)
        self.tab_output_grid_layout_0.addWidget(self._qtgui_overload_win, 10, 2, 1, 1)
        for r in range(10, 11):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_ledindicator_0_0_0_0 = self._qtgui_ledindicator_0_0_0_0_win = qtgui.GrLEDIndicator('Output active', "lime", "black", output_on, 35, 1, 1, 1, self)
        self.qtgui_ledindicator_0_0_0_0 = self._qtgui_ledindicator_0_0_0_0_win
        self.tab_output_grid_layout_0.addWidget(self._qtgui_ledindicator_0_0_0_0_win, 10, 0, 1, 1)
        for r in range(10, 11):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._qtgui_label_trg_count_tool_bar = Qt.QToolBar(self)

        if None:
            self._qtgui_label_trg_count_formatter = None
        else:
            self._qtgui_label_trg_count_formatter = lambda x: str(x)

        self._qtgui_label_trg_count_tool_bar.addWidget(Qt.QLabel("Trigger count: "))
        self._qtgui_label_trg_count_label = Qt.QLabel(str(self._qtgui_label_trg_count_formatter(self.qtgui_label_trg_count)))
        self._qtgui_label_trg_count_tool_bar.addWidget(self._qtgui_label_trg_count_label)
        self.tab_output_grid_layout_0.addWidget(self._qtgui_label_trg_count_tool_bar, 0, 1, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._qtgui_label_objective_tool_bar = Qt.QToolBar(self)

        if None:
            self._qtgui_label_objective_formatter = None
        else:
            self._qtgui_label_objective_formatter = lambda x: str(x)

        self._qtgui_label_objective_tool_bar.addWidget(Qt.QLabel("Value: "))
        self._qtgui_label_objective_label = Qt.QLabel(str(self._qtgui_label_objective_formatter(self.qtgui_label_objective)))
        self._qtgui_label_objective_tool_bar.addWidget(self._qtgui_label_objective_label)
        self.tab_excitation_grid_layout_2.addWidget(self._qtgui_label_objective_tool_bar, 20, 0, 1, 2)
        for r in range(20, 21):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(0, 2):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)

        self._output_duration_set_tool_bar = beam_exciter.NumericEntry(self.set_output_duration_set, 'Spill duration', 5, 1, 's', 'Spill duration', 10, not output_duration_gated)
        self._output_duration_set_tool_bar.set_limits(0.1, 1000)
        self.tab_output_grid_layout_0.addWidget(self._output_duration_set_tool_bar, 1, 0, 1, 1)
        for r in range(1, 2):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._out_label_tool_bar = Qt.QToolBar(self)

        if lambda x: f'{x:.6f}':
            self._out_label_formatter = lambda x: f'{x:.6f}'
        else:
            self._out_label_formatter = lambda x: eng_notation.num_to_str(x)

        self._out_label_tool_bar.addWidget(Qt.QLabel("Controller value:  "))
        self._out_label_label = Qt.QLabel(str(self._out_label_formatter(self.out_label)))
        self._out_label_tool_bar.addWidget(self._out_label_label)
        self.tab_excitation_grid_layout_1.addWidget(self._out_label_tool_bar, 30, 0, 1, 10)
        for r in range(30, 31):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(0, 10):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)

        self._opti_tol_tool_bar = beam_exciter.NumericEntry(self.set_opti_tol, 'Desired value', 0.1, 0.1, '', 'Target objective value (optimizer stops when it is reached)', 10, True)
        self._opti_tol_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_2.addWidget(self._opti_tol_tool_bar, 20, 2, 1, 2)
        for r in range(20, 21):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(2, 4):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)

        self._opti_steps_tool_bar = beam_exciter.NumericEntry(self.set_opti_steps, 'Optimizer: RasterScan: Steps', 50, 1, '', 'Number of steps to scan for each variable', 10, True)
        self._opti_steps_tool_bar.set_limits(1, 1000)
        self.tab_excitation_grid_layout_3.addWidget(self._opti_steps_tool_bar, 0, 1, 1, 1)
        for r in range(0, 1):
            self.tab_excitation_grid_layout_3.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_excitation_grid_layout_3.setColumnStretch(c, 1)

        self._opti_repeat_stop_tool_bar = beam_exciter.NumericEntry(self.set_opti_repeat_stop, 'Repeat until', 0, 0.1, 's', 'End of repeated objective evaluation (0 = until end of spill)', 10, opti_repeat)
        self._opti_repeat_stop_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_2.addWidget(self._opti_repeat_stop_tool_bar, 5, 6, 1, 2)
        for r in range(5, 6):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(6, 8):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)
        self._opti_bounds_tool_bar = Qt.QToolBar(self)
        self._opti_bounds_tool_bar.addWidget(Qt.QLabel("Bounds" + ": "))
        self._opti_bounds_line_edit = Qt.QLineEdit(str(self.opti_bounds))
        self._opti_bounds_tool_bar.addWidget(self._opti_bounds_line_edit)
        self._opti_bounds_line_edit.editingFinished.connect(
            lambda: self.set_opti_bounds(eval(str(self._opti_bounds_line_edit.text()))))
        self.tab_excitation_grid_layout_2.addWidget(self._opti_bounds_tool_bar, 4, 0, 1, 8)
        for r in range(4, 5):
            self.tab_excitation_grid_layout_2.setRowStretch(r, 1)
        for c in range(0, 8):
            self.tab_excitation_grid_layout_2.setColumnStretch(c, 1)

        self._nparticles_tool_bar = beam_exciter.NumericEntry(self.set_nparticles, 'Particles stored', 1e7, 1e6, '', 'Particles stored in ring (normalizes level)', 3, True)
        self._nparticles_tool_bar.set_limits(1, float("inf"))
        self.tab_excitation_grid_layout_1.addWidget(self._nparticles_tool_bar, 0, 0, 1, 10)
        for r in range(0, 1):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(0, 10):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)

        self._kp_set_tool_bar = beam_exciter.NumericEntry(self.set_kp_set, 'Kp', 0.14, 0.1, '', 'Feedback controller proportional gain', 10, True)
        self._kp_set_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_1.addWidget(self._kp_set_tool_bar, 20, 10, 1, 5)
        for r in range(20, 21):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(10, 15):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)

        self._kp_init_set_tool_bar = beam_exciter.NumericEntry(self.set_kp_init_set, 'Feedback Kp init', 0, 0.1, '', 'Feedback controller proportional gain', 10, True)
        self._kp_init_set_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_3.addWidget(self._kp_init_set_tool_bar, 10, 2, 1, 1)
        for r in range(10, 11):
            self.tab_excitation_grid_layout_3.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_excitation_grid_layout_3.setColumnStretch(c, 1)

        self._ki_set_tool_bar = beam_exciter.NumericEntry(self.set_ki_set, 'Ki', 0.14/0.002, 1, '/s', 'Feedback controller integration gain', 10, True)
        self._ki_set_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_1.addWidget(self._ki_set_tool_bar, 20, 15, 1, 5)
        for r in range(20, 21):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(15, 20):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)

        self._ki_init_set_tool_bar = beam_exciter.NumericEntry(self.set_ki_init_set, 'Feedback Ki init', 0, 1, '/s', 'Feedback controller integration gain', 10, True)
        self._ki_init_set_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_3.addWidget(self._ki_init_set_tool_bar, 11, 2, 1, 1)
        for r in range(11, 12):
            self.tab_excitation_grid_layout_3.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_excitation_grid_layout_3.setColumnStretch(c, 1)

        self._kd_set_tool_bar = beam_exciter.NumericEntry(self.set_kd_set, 'Kd', 0, 0.1, 's', 'Feedback controller differential gain', 10, True)
        self._kd_set_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_1.addWidget(self._kd_set_tool_bar, 20, 20, 1, 5)
        for r in range(20, 21):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(20, 25):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)

        self._kd_init_set_tool_bar = beam_exciter.NumericEntry(self.set_kd_init_set, 'Feedback Kd init', 0, 0.1, 's', 'Feedback controller differential gain', 10, True)
        self._kd_init_set_tool_bar.set_limits(0, float("inf"))
        self.tab_excitation_grid_layout_3.addWidget(self._kd_init_set_tool_bar, 12, 2, 1, 1)
        for r in range(12, 13):
            self.tab_excitation_grid_layout_3.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_excitation_grid_layout_3.setColumnStretch(c, 1)
        self._info_tool_bar = Qt.QToolBar(self)
        self._info_tool_bar.addWidget(Qt.QLabel("Info" + ": "))
        self._info_line_edit = Qt.QLineEdit(str(self.info))
        self._info_tool_bar.addWidget(self._info_line_edit)
        self._info_line_edit.editingFinished.connect(
            lambda: self.set_info(str(str(self._info_line_edit.text()))))
        self.top_grid_layout.addWidget(self._info_tool_bar, 100, 1, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._duration_tool_bar = Qt.QToolBar(self)

        if lambda x: f"{x:.3f} s":
            self._duration_formatter = lambda x: f"{x:.3f} s"
        else:
            self._duration_formatter = lambda x: eng_notation.num_to_str(x)

        self._duration_tool_bar.addWidget(Qt.QLabel("Expected spill duration:  "))
        self._duration_label = Qt.QLabel(str(self._duration_formatter(self.duration)))
        self._duration_tool_bar.addWidget(self._duration_label)
        self.tab_excitation_grid_layout_1.addWidget(self._duration_tool_bar, 0, 20, 1, 10)
        for r in range(0, 1):
            self.tab_excitation_grid_layout_1.setRowStretch(r, 1)
        for c in range(20, 30):
            self.tab_excitation_grid_layout_1.setColumnStretch(c, 1)
        def _count_rate_probe_probe():
          while True:

            val = self.blocks_probe_count_rate.level()
            try:
              try:
                self.doc.add_next_tick_callback(functools.partial(self.set_count_rate_probe,val))
              except AttributeError:
                self.set_count_rate_probe(val)
            except AttributeError:
              pass
            time.sleep(1.0 / (10))
        _count_rate_probe_thread = threading.Thread(target=_count_rate_probe_probe)
        _count_rate_probe_thread.daemon = True
        _count_rate_probe_thread.start()
        self._count_rate_label_tool_bar = Qt.QToolBar(self)

        if lambda x: (f'{x:.3e} particles/s' if self.mode==0 else f'{x:.3e} particles'):
            self._count_rate_label_formatter = lambda x: (f'{x:.3e} particles/s' if self.mode==0 else f'{x:.3e} particles')
        else:
            self._count_rate_label_formatter = lambda x: eng_notation.num_to_str(x)

        self._count_rate_label_tool_bar.addWidget(Qt.QLabel("Value:  "))
        self._count_rate_label_label = Qt.QLabel(str(self._count_rate_label_formatter(self.count_rate_label)))
        self._count_rate_label_tool_bar.addWidget(self._count_rate_label_label)
        self.tab_input_grid_layout_0.addWidget(self._count_rate_label_tool_bar, 1, 0, 1, 2)
        for r in range(1, 2):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 2):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        def _controller_value_probe():
          while True:

            val = self.blocks_probe_controller_output.level()
            try:
              try:
                self.doc.add_next_tick_callback(functools.partial(self.set_controller_value,val))
              except AttributeError:
                self.set_controller_value(val)
            except AttributeError:
              pass
            time.sleep(1.0 / (10))
        _controller_value_thread = threading.Thread(target=_controller_value_probe)
        _controller_value_thread.daemon = True
        _controller_value_thread.start()
        self.blocks_repeat_0 = blocks.repeat(gr.sizeof_float*1, repeat)
        self.blocks_multiply_xx_0 = blocks.multiply_vff(1)
        self.blocks_multiply_const_vxx_1_0_0 = blocks.multiply_const_ff(100)
        self.blocks_multiply_const_vxx_0_0_0 = blocks.multiply_const_ff(((-1/Ta) if mode==1 else 1))
        self.blocks_multiply_const_vxx_0_0 = blocks.multiply_const_ff((32767*fs_detector*float(inp_factor)))
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_ff(float(norm))
        self.blocks_msgpair_to_var_2 = blocks.msg_pair_to_var(self.set_output_on)
        self.blocks_msgpair_to_var_1 = blocks.msg_pair_to_var(self.set_opti_value)
        self.blocks_msgpair_to_var_0_0 = blocks.msg_pair_to_var(self.set_trg_count)
        self.blocks_moving_average_xx_0_0 = blocks.moving_average_ff((int(fs_detector*100e-3)), (1/int(fs_detector*100e-3)), 4000, 1)
        self.blocks_float_to_complex_1 = blocks.float_to_complex(1)
        self.blocks_float_to_complex_0 = blocks.float_to_complex(1)
        self.blocks_float_to_complex_0.set_max_output_buffer(maxoutbuf)
        self.blocks_complex_to_real_0 = blocks.complex_to_real(1)
        self.blocks_add_const_vxx_0 = blocks.add_const_ff((-opti_subtract_target*float(target_rate)))
        self.beam_exciter_variableFileDump_0 = beam_exciter.variableFileDump(daq_filename + ".yml", daq_save_enabled)
        self.beam_exciter_switchBlock_0 = beam_exciter.switchBlock(gr.sizeof_float, 0, 0, 0, (int(delay*fs_signal)), ensure_pmt("SOB"), ensure_pmt("EOB"), ensure_pmt("START"))
        self.beam_exciter_saveRestoreVariables_1 = beam_exciter.saveRestoreVariables(self, slot, """
        inp_thr_low,inp_thr_high,inp_factor,mode,
        daq_filename,

        sig.frev,
        sig.ex1_enabled,sig.qex1,sig.dqex1,sig.aex1,
        sig.ex2_enabled,sig.qex2,sig.aex2,
        sig.ex3_enabled,sig.qex3,sig.aex3,
        sig.ex4_enabled,sig.qex4,sig.dqex4,sig.aex4,

        nparticles,feedforward,ta_set,
        target_rate,feedback,kp_set,
        out_max,ki_set,
        norm,kd_set,
        abort_on_max_limit,rise_time_ms,

        opti_variables,opti_bounds,
        opti_t_start,opti_dt_eval,opti_repeat,opti_repeat_stop,
        opti_method,opti_dt_count,opti_subtract_target,
        opti_tol,

        opti_logfile, opti_algorithm, opti_steps,
        feedback_init,feedback_init_threshold,
        kp_init_set,ki_init_set,kd_init_set,out_max_init,

        output_duration_set,
        output_duration_gated,
        """, "variable_to_trigger_save", "variable_to_trigger_restore", "info")
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(""),
            "DiscriminatingPulseCounter",
            -1, # device_select
            -1, # instance_index
        )
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_low', float(min(-inp_thr_low, -inp_thr_high)))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_high', float(max(-inp_thr_low, -inp_thr_high)))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('decimation', decim)
        self.beam_exciter_gpioTrigger_0 = beam_exciter.gpioTrigger(
            itemsize=gr.sizeof_float,
            enable_polling=enable_ext_trg,
            poll_every_samples=(int(fs_detector/fs_gpio)),
            duration_samples=(int(output_duration * fs_detector)),
            tag_key_start=ensure_pmt("SOB"),
            tag_key_stop=ensure_pmt("EOB"),
            manual_trigger=manual_trigger,
            ptr_graph=getattr(self, "rfnoc_graph", None),
            device_addr='',
            device_index=0,
            radio_index=0,
            gpio_bank='RX',
            gpio_pin=0,
            inverted=False,
        )
        self.beam_exciter_feedbackController_0 = beam_exciter.feedbackController(feedback, feedforward, mode, target_rate, (int(rise_time_ms*1e-3*fs_detector)), (int(max(1, Ta*fs_detector))), Kp, Ki*Ta, Kd/Ta, 0, out_max/norm if norm else 0, feedback_init, Kp_init, Ki_init*Ta, Kd_init/Ta, feedback_init_threshold, feedback_init_threshold_smoothing, out_max_init/norm if norm else 0, abort_on_max_limit, y_pre, 0, True)
        self.beam_exciter_burstFileSink_0 = beam_exciter.burstFileSink(gr.sizeof_gr_complex, daq_filename, ensure_pmt(pmt.intern("SOB")), ensure_pmt(pmt.intern("EOB")), (int(60*fs_detector)), daq_save_enabled)
        self.beam_exciter_burstEvaluate_1 = beam_exciter.burstEvaluate(pmt.intern("SOB"), (int(opti_t_start*fs_detector)), pmt.intern("EOB"), (int(opti_dt_eval*fs_detector)), opti_method, (max(1, int(opti_dt_count*fs_detector))), True, (int(opti_repeat_every*fs_detector)), opti_repeat_count, not opti_repeat, not opti_repeat)
        self.beam_exciter_Optimizer_0 = beam_exciter.Optimizer(self, opti_enable, opti_variables, opti_options_scan if opti_algorithm=="RasterScan" else opti_options, "opti_enable", opti_algorithm, opti_logfile)
        self.beam_exciter_LevelProbe_0_1_0 = beam_exciter.LevelProbe(1, 10000)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.beam_exciter_burstEvaluate_1, 'msg'), (self.beam_exciter_Optimizer_0, 'obj'))
        self.msg_connect((self.beam_exciter_burstEvaluate_1, 'msg'), (self.blocks_msgpair_to_var_1, 'inpair'))
        self.msg_connect((self.beam_exciter_gpioTrigger_0, 'count'), (self.blocks_msgpair_to_var_0_0, 'inpair'))
        self.msg_connect((self.beam_exciter_switchBlock_0, 'active'), (self.blocks_msgpair_to_var_2, 'inpair'))
        self.msg_connect((self.restore, 'pressed'), (self.beam_exciter_saveRestoreVariables_1, 'restore'))
        self.msg_connect((self.save, 'pressed'), (self.beam_exciter_saveRestoreVariables_1, 'save'))
        self.rfnoc_graph.connect(self.beam_exciter_rfnoc_discriminating_pulse_counter_0.get_unique_id(), 0, self.uhd_rfnoc_rx_streamer_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_duc_0_0.get_unique_id(), 0, self.uhd_rfnoc_tx_radio_0_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_rx_radio_0.get_unique_id(), 0, self.beam_exciter_rfnoc_discriminating_pulse_counter_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_tx_streamer_0.get_unique_id(), 0, self.uhd_rfnoc_duc_0_0.get_unique_id(), 0, False)
        self.connect((self.beam_exciter_LevelProbe_0_1_0, 1), (self.blocks_multiply_const_vxx_1_0_0, 0))
        self.connect((self.beam_exciter_LevelProbe_0_1_0, 0), (self.qtgui_rms, 0))
        self.connect((self.beam_exciter_feedbackController_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.beam_exciter_gpioTrigger_0, 0), (self.blocks_add_const_vxx_0, 0))
        self.connect((self.beam_exciter_gpioTrigger_0, 0), (self.blocks_float_to_complex_1, 0))
        self.connect((self.beam_exciter_gpioTrigger_0, 0), (self.blocks_moving_average_xx_0_0, 0))
        self.connect((self.beam_exciter_gpioTrigger_0, 0), (self.blocks_multiply_const_vxx_0_0_0, 0))
        self.connect((self.beam_exciter_switchBlock_0, 0), (self.blocks_float_to_complex_0, 0))
        self.connect((self.blocks_add_const_vxx_0, 0), (self.beam_exciter_burstEvaluate_1, 0))
        self.connect((self.blocks_complex_to_real_0, 0), (self.blocks_multiply_const_vxx_0_0, 0))
        self.connect((self.blocks_float_to_complex_0, 0), (self.uhd_rfnoc_tx_streamer_0, 0))
        self.connect((self.blocks_float_to_complex_1, 0), (self.beam_exciter_burstFileSink_0, 0))
        self.connect((self.blocks_moving_average_xx_0_0, 0), (self.blocks_probe_count_rate, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_float_to_complex_1, 1))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_probe_controller_output, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_repeat_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.beam_exciter_gpioTrigger_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0_0, 0), (self.beam_exciter_feedbackController_0, 0))
        self.connect((self.blocks_multiply_const_vxx_1_0_0, 0), (self.qtgui_overload, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.beam_exciter_LevelProbe_0_1_0, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.beam_exciter_switchBlock_0, 0))
        self.connect((self.blocks_repeat_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.sig, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.uhd_rfnoc_rx_streamer_0, 0), (self.blocks_complex_to_real_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "exciter_extraction_feedback_optimize")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_opti_repeat_wait(self):
        return self.opti_repeat_wait

    def set_opti_repeat_wait(self, opti_repeat_wait):
        self.opti_repeat_wait = opti_repeat_wait
        self.set_opti_repeat_count(int(1 if not self.opti_repeat else 0 if not self.opti_repeat_stop else max((self.opti_repeat_stop-self.opti_t_start+self.opti_repeat_wait)/self.opti_repeat_every, 1)))
        self.set_opti_repeat_every((max(self.opti_dt_count, self.opti_dt_eval)+self.opti_repeat_wait))

    def get_opti_dt_eval(self):
        return self.opti_dt_eval

    def set_opti_dt_eval(self, opti_dt_eval):
        self.opti_dt_eval = opti_dt_eval
        self._opti_dt_eval_tool_bar.set_value(self.opti_dt_eval)
        self.set_opti_repeat_every((max(self.opti_dt_count, self.opti_dt_eval)+self.opti_repeat_wait))
        self.beam_exciter_burstEvaluate_1.set_eval_samp((int(self.opti_dt_eval*self.fs_detector)))

    def get_opti_dt_count(self):
        return self.opti_dt_count

    def set_opti_dt_count(self, opti_dt_count):
        self.opti_dt_count = opti_dt_count
        self._opti_dt_count_tool_bar.set_value(self.opti_dt_count)
        self._opti_dt_eval_tool_bar.set_limits(self.opti_dt_count*100 if self.opti_repeat else 0, float("inf"))
        self.set_opti_repeat_every((max(self.opti_dt_count, self.opti_dt_eval)+self.opti_repeat_wait))
        self.beam_exciter_burstEvaluate_1.set_pre_avg_samp((max(1, int(self.opti_dt_count*self.fs_detector))))

    def get_fs_adc(self):
        return self.fs_adc

    def set_fs_adc(self, fs_adc):
        self.fs_adc = fs_adc
        self.set_fs_detector(self.fs_adc/self.decim)
        self.uhd_rfnoc_rx_radio_0.set_rate(self.fs_adc)
        self.uhd_rfnoc_tx_radio_0_0.set_rate(self.fs_adc)

    def get_decim(self):
        return self.decim

    def set_decim(self, decim):
        self.decim = decim
        self.set_fs_detector(self.fs_adc/self.decim)
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('decimation', self.decim)

    def get_trg_count(self):
        return self.trg_count

    def set_trg_count(self, trg_count):
        self.trg_count = trg_count
        self.set_qtgui_label_trg_count(self.trg_count)

    def get_target_rate(self):
        return self.target_rate

    def set_target_rate(self, target_rate):
        self.target_rate = target_rate
        self.set_duration((float(self.nparticles)/float(self.target_rate)))
        self._target_rate_tool_bar.set_value(self.target_rate)
        self.set_y_pre(float(self.target_rate)/float(self.nparticles) if self.feedforward else 0)
        self.beam_exciter_feedbackController_0.set_target(self.target_rate)
        self.blocks_add_const_vxx_0.set_k((-self.opti_subtract_target*float(self.target_rate)))

    def get_ta_set(self):
        return self.ta_set

    def set_ta_set(self, ta_set):
        self.ta_set = ta_set
        self.set_Ta(float(self.ta_set)*1e-3)
        self._ta_set_tool_bar.set_value(self.ta_set)

    def get_repeat(self):
        return self.repeat

    def set_repeat(self, repeat):
        self.repeat = repeat
        self.set_fs_signal(self.fs_detector*self.repeat)
        self.blocks_repeat_0.set_interpolation(self.repeat)

    def get_output_duration_set(self):
        return self.output_duration_set

    def set_output_duration_set(self, output_duration_set):
        self.output_duration_set = output_duration_set
        self.set_output_duration(0 if self.output_duration_gated else self.output_duration_set)
        self._output_duration_set_tool_bar.set_value(self.output_duration_set)

    def get_output_duration_gated(self):
        return self.output_duration_gated

    def set_output_duration_gated(self, output_duration_gated):
        self.output_duration_gated = output_duration_gated
        self.set_output_duration(0 if self.output_duration_gated else self.output_duration_set)
        self._output_duration_gated_callback(self.output_duration_gated)
        self._output_duration_set_tool_bar.set_enabled(not self.output_duration_gated)

    def get_opti_value(self):
        return self.opti_value

    def set_opti_value(self, opti_value):
        self.opti_value = opti_value
        self.set_qtgui_label_objective("{:g}".format(self.opti_value))

    def get_opti_tol(self):
        return self.opti_tol

    def set_opti_tol(self, opti_tol):
        self.opti_tol = opti_tol
        self.set_opti_options({"bounds":[list(b) for b in zip(*self.opti_bounds)], "user_params":{"model.abs_tol":self.opti_tol}, "scaling_within_bounds":True, "objfun_has_noise": True, "seek_global_minimum": True})
        self._opti_tol_tool_bar.set_value(self.opti_tol)

    def get_opti_t_start(self):
        return self.opti_t_start

    def set_opti_t_start(self, opti_t_start):
        self.opti_t_start = opti_t_start
        self.set_opti_repeat_count(int(1 if not self.opti_repeat else 0 if not self.opti_repeat_stop else max((self.opti_repeat_stop-self.opti_t_start+self.opti_repeat_wait)/self.opti_repeat_every, 1)))
        self._opti_t_start_tool_bar.set_value(self.opti_t_start)
        self.beam_exciter_burstEvaluate_1.set_start_offset_samp((int(self.opti_t_start*self.fs_detector)))

    def get_opti_steps(self):
        return self.opti_steps

    def set_opti_steps(self, opti_steps):
        self.opti_steps = opti_steps
        self.set_opti_options_scan({"bounds":[list(b) for b in zip(*self.opti_bounds)], "steps":self.opti_steps})
        self._opti_steps_tool_bar.set_value(self.opti_steps)

    def get_opti_repeat_stop(self):
        return self.opti_repeat_stop

    def set_opti_repeat_stop(self, opti_repeat_stop):
        self.opti_repeat_stop = opti_repeat_stop
        self.set_opti_repeat_count(int(1 if not self.opti_repeat else 0 if not self.opti_repeat_stop else max((self.opti_repeat_stop-self.opti_t_start+self.opti_repeat_wait)/self.opti_repeat_every, 1)))
        self._opti_repeat_stop_tool_bar.set_value(self.opti_repeat_stop)

    def get_opti_repeat_every(self):
        return self.opti_repeat_every

    def set_opti_repeat_every(self, opti_repeat_every):
        self.opti_repeat_every = opti_repeat_every
        self.set_opti_repeat_count(int(1 if not self.opti_repeat else 0 if not self.opti_repeat_stop else max((self.opti_repeat_stop-self.opti_t_start+self.opti_repeat_wait)/self.opti_repeat_every, 1)))
        self.beam_exciter_burstEvaluate_1.set_repeat_every_samp((int(self.opti_repeat_every*self.fs_detector)))

    def get_opti_repeat(self):
        return self.opti_repeat

    def set_opti_repeat(self, opti_repeat):
        self.opti_repeat = opti_repeat
        self._opti_dt_eval_tool_bar.set_limits(self.opti_dt_count*100 if self.opti_repeat else 0, float("inf"))
        self._opti_repeat_callback(self.opti_repeat)
        self.set_opti_repeat_count(int(1 if not self.opti_repeat else 0 if not self.opti_repeat_stop else max((self.opti_repeat_stop-self.opti_t_start+self.opti_repeat_wait)/self.opti_repeat_every, 1)))
        self._opti_repeat_stop_tool_bar.set_enabled(self.opti_repeat)
        self.beam_exciter_burstEvaluate_1.set_repeat_msg_avg(not self.opti_repeat)
        self.beam_exciter_burstEvaluate_1.set_delay_msg_eob(not self.opti_repeat)

    def get_opti_bounds(self):
        return self.opti_bounds

    def set_opti_bounds(self, opti_bounds):
        self.opti_bounds = opti_bounds
        Qt.QMetaObject.invokeMethod(self._opti_bounds_line_edit, "setText", Qt.Q_ARG("QString", repr(self.opti_bounds)))
        self.set_opti_options({"bounds":[list(b) for b in zip(*self.opti_bounds)], "user_params":{"model.abs_tol":self.opti_tol}, "scaling_within_bounds":True, "objfun_has_noise": True, "seek_global_minimum": True})
        self.set_opti_options_scan({"bounds":[list(b) for b in zip(*self.opti_bounds)], "steps":self.opti_steps})

    def get_nparticles(self):
        return self.nparticles

    def set_nparticles(self, nparticles):
        self.nparticles = nparticles
        self.set_Kd(float(self.kd_set)/float(self.nparticles))
        self.set_Kd_init(float(self.kd_init_set)/float(self.nparticles))
        self.set_Ki(float(self.ki_set)/float(self.nparticles))
        self.set_Ki_init(float(self.ki_init_set)/float(self.nparticles))
        self.set_Kp(float(self.kp_set)/float(self.nparticles))
        self.set_Kp_init(float(self.kp_init_set)/float(self.nparticles))
        self.set_duration((float(self.nparticles)/float(self.target_rate)))
        self._nparticles_tool_bar.set_value(self.nparticles)
        self.set_y_pre(float(self.target_rate)/float(self.nparticles) if self.feedforward else 0)

    def get_kp_set(self):
        return self.kp_set

    def set_kp_set(self, kp_set):
        self.kp_set = kp_set
        self.set_Kp(float(self.kp_set)/float(self.nparticles))
        self._kp_set_tool_bar.set_value(self.kp_set)

    def get_kp_init_set(self):
        return self.kp_init_set

    def set_kp_init_set(self, kp_init_set):
        self.kp_init_set = kp_init_set
        self.set_Kp_init(float(self.kp_init_set)/float(self.nparticles))
        self._kp_init_set_tool_bar.set_value(self.kp_init_set)

    def get_ki_set(self):
        return self.ki_set

    def set_ki_set(self, ki_set):
        self.ki_set = ki_set
        self.set_Ki(float(self.ki_set)/float(self.nparticles))
        self._ki_set_tool_bar.set_value(self.ki_set)

    def get_ki_init_set(self):
        return self.ki_init_set

    def set_ki_init_set(self, ki_init_set):
        self.ki_init_set = ki_init_set
        self.set_Ki_init(float(self.ki_init_set)/float(self.nparticles))
        self._ki_init_set_tool_bar.set_value(self.ki_init_set)

    def get_kd_set(self):
        return self.kd_set

    def set_kd_set(self, kd_set):
        self.kd_set = kd_set
        self.set_Kd(float(self.kd_set)/float(self.nparticles))
        self._kd_set_tool_bar.set_value(self.kd_set)

    def get_kd_init_set(self):
        return self.kd_init_set

    def set_kd_init_set(self, kd_init_set):
        self.kd_init_set = kd_init_set
        self.set_Kd_init(float(self.kd_init_set)/float(self.nparticles))
        self._kd_init_set_tool_bar.set_value(self.kd_init_set)

    def get_fs_detector(self):
        return self.fs_detector

    def set_fs_detector(self, fs_detector):
        self.fs_detector = fs_detector
        self.set_fs_signal(self.fs_detector*self.repeat)
        self._opti_dt_count_tool_bar.set_limits(1/self.fs_detector, 0.1)
        self._ta_set_tool_bar.set_limits(2*1e3/self.fs_detector, 100)
        self.beam_exciter_burstEvaluate_1.set_start_offset_samp((int(self.opti_t_start*self.fs_detector)))
        self.beam_exciter_burstEvaluate_1.set_eval_samp((int(self.opti_dt_eval*self.fs_detector)))
        self.beam_exciter_burstEvaluate_1.set_pre_avg_samp((max(1, int(self.opti_dt_count*self.fs_detector))))
        self.beam_exciter_burstEvaluate_1.set_repeat_every_samp((int(self.opti_repeat_every*self.fs_detector)))
        self.beam_exciter_burstFileSink_0.set_length((int(60*self.fs_detector)))
        self.beam_exciter_feedbackController_0.set_rise_time((int(self.rise_time_ms*1e-3*self.fs_detector)))
        self.beam_exciter_feedbackController_0.set_ta_samples((int(max(1, self.Ta*self.fs_detector))))
        self.beam_exciter_gpioTrigger_0.set_duration_samples((int(self.output_duration * self.fs_detector)))
        self.blocks_moving_average_xx_0_0.set_length_and_scale((int(self.fs_detector*100e-3)), (1/int(self.fs_detector*100e-3)))
        self.blocks_multiply_const_vxx_0_0.set_k((32767*self.fs_detector*float(self.inp_factor)))

    def get_feedforward(self):
        return self.feedforward

    def set_feedforward(self, feedforward):
        self.feedforward = feedforward
        self._feedforward_callback(self.feedforward)
        self.set_y_pre(float(self.target_rate)/float(self.nparticles) if self.feedforward else 0)
        self.beam_exciter_feedbackController_0.set_feedforward(self.feedforward)

    def get_count_rate_probe(self):
        return self.count_rate_probe

    def set_count_rate_probe(self, count_rate_probe):
        self.count_rate_probe = count_rate_probe
        self.set_count_rate_label(self.count_rate_probe)

    def get_controller_value(self):
        return self.controller_value

    def set_controller_value(self, controller_value):
        self.controller_value = controller_value
        self.set_out_label(self.controller_value)

    def get_y_pre(self):
        return self.y_pre

    def set_y_pre(self, y_pre):
        self.y_pre = y_pre
        self.beam_exciter_feedbackController_0.set_feedforward_const(self.y_pre)

    def get_upsample(self):
        return self.upsample

    def set_upsample(self, upsample):
        self.upsample = upsample
        self.sig.set_upsample(self.upsample)

    def get_uhd_rfnoc_graph(self):
        return self.uhd_rfnoc_graph

    def set_uhd_rfnoc_graph(self, uhd_rfnoc_graph):
        self.uhd_rfnoc_graph = uhd_rfnoc_graph

    def get_slot(self):
        return self.slot

    def set_slot(self, slot):
        self.slot = slot
        self._slot_callback(self.slot)
        self.beam_exciter_saveRestoreVariables_1.set_slot(self.slot)

    def get_rise_time_ms(self):
        return self.rise_time_ms

    def set_rise_time_ms(self, rise_time_ms):
        self.rise_time_ms = rise_time_ms
        self._rise_time_ms_tool_bar.set_value(self.rise_time_ms)
        self.beam_exciter_feedbackController_0.set_rise_time((int(self.rise_time_ms*1e-3*self.fs_detector)))

    def get_qtgui_label_trg_count(self):
        return self.qtgui_label_trg_count

    def set_qtgui_label_trg_count(self, qtgui_label_trg_count):
        self.qtgui_label_trg_count = qtgui_label_trg_count
        Qt.QMetaObject.invokeMethod(self._qtgui_label_trg_count_label, "setText", Qt.Q_ARG("QString", str(self._qtgui_label_trg_count_formatter(self.qtgui_label_trg_count))))

    def get_qtgui_label_objective(self):
        return self.qtgui_label_objective

    def set_qtgui_label_objective(self, qtgui_label_objective):
        self.qtgui_label_objective = qtgui_label_objective
        Qt.QMetaObject.invokeMethod(self._qtgui_label_objective_label, "setText", Qt.Q_ARG("QString", str(self._qtgui_label_objective_formatter(self.qtgui_label_objective))))

    def get_output_on(self):
        return self.output_on

    def set_output_on(self, output_on):
        self.output_on = output_on
        self.beam_exciter_variableFileDump_0.save(self.output_on, (f"""
        # Metadata
        timestamp: {datetime.datetime.now()}

        # Sample rates
        fs_adc: {self.fs_adc} # Hz
        decim: {self.decim}
        fs_detector: {self.fs_detector} # Hz
        repeat: {self.repeat}
        fs_signal: {self.fs_signal} # Hz
        fs_gpio: {self.fs_gpio} # Hz
        upsample: {self.upsample}

        # Pulse counter
        inp_thr_low: {self.inp_thr_low} # V
        inp_thr_high: {self.inp_thr_high} # V
        mode: {self.mode}
        inp_factor: {self.inp_factor} # particles/s per Hz

        """+\
        ((f"""
        # Excitation signal
        sig:
          frev: {self.sig.frev} # Hz
          ex1_source: {list(filter(lambda v: v.startswith("ex1_source"), vars(self.sig)))}
          ex1_enabled: {getattr(self.sig, "ex1_enabled",   "")}
          qex1: {getattr(self.sig, "qex1",  "")}
          dqex1: {getattr(self.sig, "dqex1", "")}
          dtex1: {getattr(self.sig, "dtex1",  "")} # s
          aex1: {getattr(self.sig, "aex1",  "")}
          ex2_source: {list(filter(lambda v: v.startswith("ex2_source"), vars(self.sig)))}
          ex2_enabled: {getattr(self.sig, "ex2_enabled",   "")}
          qex2: {getattr(self.sig, "qex2",  "")}
          dqex2: {getattr(self.sig, "dqex2", "")}
          dtex2: {getattr(self.sig, "dtex2",  "")} # s
          aex2: {getattr(self.sig, "aex2",  "")}
          ex3_source: {list(filter(lambda v: v.startswith("ex3_source"), vars(self.sig)))}
          ex3_enabled: {getattr(self.sig, "ex3_enabled",   "")}
          qex3: {getattr(self.sig, "qex3",  "")}
          dqex3: {getattr(self.sig, "dqex3", "")}
          dtex3: {getattr(self.sig, "dtex3",  "")} # s
          aex3: {getattr(self.sig, "aex3",  "")}
          ex4_source: {list(filter(lambda v: v.startswith("ex4_source"), vars(self.sig)))}
          ex4_enabled: {getattr(self.sig, "ex4_enabled",   "")}
          qex4: {getattr(self.sig, "qex4",  "")}
          dqex4: {getattr(self.sig, "dqex4", "")}
          dtex4: {getattr(self.sig, "dtex4",  "")} # s
          aex4: {getattr(self.sig, "aex4",  "")}
          ex5_source: {list(filter(lambda v: v.startswith("ex5_source"), vars(self.sig)))}
          ex5_enabled: {getattr(self.sig, "ex5_enabled",   "")}
          qex5: {getattr(self.sig, "qex5",  "")}
          dqex5: {getattr(self.sig, "dqex5", "")}
          dtex5: {getattr(self.sig, "dtex5",  "")} # s
          aex5: {getattr(self.sig, "aex5",  "")}
          ex6_source: {list(filter(lambda v: v.startswith("ex6_source"), vars(self.sig)))}
          ex6_enabled: {getattr(self.sig, "ex6_enabled",   "")}
          qex6: {getattr(self.sig, "qex6",  "")}
          dqex6: {getattr(self.sig, "dqex6", "")}
          dtex6: {getattr(self.sig, "dtex6",  "")} # s
          aex6: {getattr(self.sig, "aex6",  "")}

        """) if hasattr(self, "sig") else "")+\
        f"""
        # Feedback controller
        nparticles: {self.nparticles}
        target_rate: {self.target_rate} # particles/s
        norm: {self.norm}
        feedforward: {self.feedforward}
        feedback: {self.feedback}
        ta_set: {self.ta_set} # ms
        kp_set: {self.kp_set}
        ki_set: {self.ki_set} # 1/s
        kd_set: {self.kd_set} # s
        out_max: {self.out_max}
        abort_on_max_limit: {self.abort_on_max_limit}
        rise_time_ms: {self.rise_time_ms} # ms

        # Optimizer
        opti_enable: {self.opti_enable and isinstance(self.opti_enable, bool)}
        opti_variables: {self.opti_variables}
        opti_bounds: {[list(b) for b in self.opti_bounds]}
        opti_tol: {self.opti_tol}
        opti_method: {self.opti_method}
        opti_dt_count: {self.opti_dt_count} # s
        opti_subtract_target: {self.opti_subtract_target}
        opti_t_start: {self.opti_t_start} # s
        opti_dt_eval: {self.opti_dt_eval} # s
        opti_repeat: {self.opti_repeat}
        opti_repeat_stop: {self.opti_repeat_stop}
        opti_repeat_every: {self.opti_repeat_every} # s
        opti_repeat_count: {self.opti_repeat_count}
        _opti_options: {self.opti_options}

        # Output
        enable_ext_trg: {self.enable_ext_trg}
        trg_count: {self.trg_count}
        output_duration: {self.output_duration} # s
        delay: {self.delay} # s

        """))
        self.qtgui_ledindicator_0_0_0_0.setState(self.output_on)

    def get_output_duration(self):
        return self.output_duration

    def set_output_duration(self, output_duration):
        self.output_duration = output_duration
        self.beam_exciter_gpioTrigger_0.set_duration_samples((int(self.output_duration * self.fs_detector)))

    def get_out_max_init(self):
        return self.out_max_init

    def set_out_max_init(self, out_max_init):
        self.out_max_init = out_max_init
        self._out_max_init_tool_bar.set_value(self.out_max_init)
        self.beam_exciter_feedbackController_0.set_output_max_init(self.out_max_init/self.norm if self.norm else 0)

    def get_out_max(self):
        return self.out_max

    def set_out_max(self, out_max):
        self.out_max = out_max
        self._out_max_tool_bar.set_value(self.out_max)
        self.beam_exciter_feedbackController_0.set_output_max(self.out_max/self.norm if self.norm else 0)

    def get_out_label(self):
        return self.out_label

    def set_out_label(self, out_label):
        self.out_label = out_label
        Qt.QMetaObject.invokeMethod(self._out_label_label, "setText", Qt.Q_ARG("QString", str(self._out_label_formatter(self.out_label))))

    def get_opti_variables(self):
        return self.opti_variables

    def set_opti_variables(self, opti_variables):
        self.opti_variables = opti_variables
        Qt.QMetaObject.invokeMethod(self._opti_variables_line_edit, "setText", Qt.Q_ARG("QString", repr(self.opti_variables)))
        self.beam_exciter_Optimizer_0.set_variables(self.opti_variables)

    def get_opti_subtract_target(self):
        return self.opti_subtract_target

    def set_opti_subtract_target(self, opti_subtract_target):
        self.opti_subtract_target = opti_subtract_target
        self._opti_subtract_target_callback(self.opti_subtract_target)
        self.blocks_add_const_vxx_0.set_k((-self.opti_subtract_target*float(self.target_rate)))

    def get_opti_repeat_count(self):
        return self.opti_repeat_count

    def set_opti_repeat_count(self, opti_repeat_count):
        self.opti_repeat_count = opti_repeat_count
        self.beam_exciter_burstEvaluate_1.set_repeat_n_times(self.opti_repeat_count)

    def get_opti_options_scan(self):
        return self.opti_options_scan

    def set_opti_options_scan(self, opti_options_scan):
        self.opti_options_scan = opti_options_scan
        self.beam_exciter_Optimizer_0.set_options(self.opti_options_scan if self.opti_algorithm=="RasterScan" else self.opti_options)

    def get_opti_options(self):
        return self.opti_options

    def set_opti_options(self, opti_options):
        self.opti_options = opti_options
        self.beam_exciter_Optimizer_0.set_options(self.opti_options_scan if self.opti_algorithm=="RasterScan" else self.opti_options)

    def get_opti_method(self):
        return self.opti_method

    def set_opti_method(self, opti_method):
        self.opti_method = opti_method
        self._opti_method_callback(self.opti_method)
        self.beam_exciter_burstEvaluate_1.set_function(self.opti_method)

    def get_opti_logfile(self):
        return self.opti_logfile

    def set_opti_logfile(self, opti_logfile):
        self.opti_logfile = opti_logfile
        Qt.QMetaObject.invokeMethod(self._opti_logfile_line_edit, "setText", Qt.Q_ARG("QString", str(self.opti_logfile)))
        self.beam_exciter_Optimizer_0.set_logfile(self.opti_logfile)

    def get_opti_enable(self):
        return self.opti_enable

    def set_opti_enable(self, opti_enable):
        self.opti_enable = opti_enable
        self.beam_exciter_Optimizer_0.set_enable(self.opti_enable)

    def get_opti_algorithm(self):
        return self.opti_algorithm

    def set_opti_algorithm(self, opti_algorithm):
        self.opti_algorithm = opti_algorithm
        self._opti_algorithm_callback(self.opti_algorithm)
        self.beam_exciter_Optimizer_0.set_options(self.opti_options_scan if self.opti_algorithm=="RasterScan" else self.opti_options)
        self.beam_exciter_Optimizer_0.set_method(self.opti_algorithm)

    def get_norm(self):
        return self.norm

    def set_norm(self, norm):
        self.norm = norm
        self._norm_tool_bar.set_value(self.norm)
        self.beam_exciter_feedbackController_0.set_output_max(self.out_max/self.norm if self.norm else 0)
        self.beam_exciter_feedbackController_0.set_output_max_init(self.out_max_init/self.norm if self.norm else 0)
        self.blocks_multiply_const_vxx_0.set_k(float(self.norm))

    def get_mode(self):
        return self.mode

    def set_mode(self, mode):
        self.mode = mode
        self._mode_callback(self.mode)
        self.beam_exciter_feedbackController_0.set_target_mode(self.mode)
        self.blocks_multiply_const_vxx_0_0_0.set_k(((-1/self.Ta) if self.mode==1 else 1))

    def get_maxoutbuf(self):
        return self.maxoutbuf

    def set_maxoutbuf(self, maxoutbuf):
        self.maxoutbuf = maxoutbuf

    def get_manual_trigger(self):
        return self.manual_trigger

    def set_manual_trigger(self, manual_trigger):
        self.manual_trigger = manual_trigger
        self.beam_exciter_gpioTrigger_0.set_manual_trigger(self.manual_trigger)

    def get_inp_thr_low(self):
        return self.inp_thr_low

    def set_inp_thr_low(self, inp_thr_low):
        self.inp_thr_low = inp_thr_low
        self._inp_thr_low_tool_bar.set_value(self.inp_thr_low)
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_low', float(min(-self.inp_thr_low, -self.inp_thr_high)))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_high', float(max(-self.inp_thr_low, -self.inp_thr_high)))

    def get_inp_thr_high(self):
        return self.inp_thr_high

    def set_inp_thr_high(self, inp_thr_high):
        self.inp_thr_high = inp_thr_high
        self._inp_thr_high_tool_bar.set_value(self.inp_thr_high)
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_low', float(min(-self.inp_thr_low, -self.inp_thr_high)))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_high', float(max(-self.inp_thr_low, -self.inp_thr_high)))

    def get_inp_factor(self):
        return self.inp_factor

    def set_inp_factor(self, inp_factor):
        self.inp_factor = inp_factor
        self._inp_factor_tool_bar.set_value(self.inp_factor)
        self.blocks_multiply_const_vxx_0_0.set_k((32767*self.fs_detector*float(self.inp_factor)))

    def get_inp_coupling(self):
        return self.inp_coupling

    def set_inp_coupling(self, inp_coupling):
        self.inp_coupling = inp_coupling
        self.uhd_rfnoc_rx_radio_0.set_dc_offset(self.inp_coupling, 0)

    def get_info(self):
        return self.info

    def set_info(self, info):
        self.info = info
        Qt.QMetaObject.invokeMethod(self._info_line_edit, "setText", Qt.Q_ARG("QString", str(self.info)))

    def get_fs_signal(self):
        return self.fs_signal

    def set_fs_signal(self, fs_signal):
        self.fs_signal = fs_signal
        self.beam_exciter_switchBlock_0.set_insert_samples((int(self.delay*self.fs_signal)))
        self.sig.set_samp_rate(self.fs_signal)
        self.uhd_rfnoc_duc_0_0.set_input_rate(self.fs_signal, 0)

    def get_fs_gpio(self):
        return self.fs_gpio

    def set_fs_gpio(self, fs_gpio):
        self.fs_gpio = fs_gpio

    def get_feedback_init_threshold_smoothing(self):
        return self.feedback_init_threshold_smoothing

    def set_feedback_init_threshold_smoothing(self, feedback_init_threshold_smoothing):
        self.feedback_init_threshold_smoothing = feedback_init_threshold_smoothing
        self._feedback_init_threshold_smoothing_tool_bar.set_value(self.feedback_init_threshold_smoothing)
        self.beam_exciter_feedbackController_0.set_smoothing(self.feedback_init_threshold_smoothing)

    def get_feedback_init_threshold(self):
        return self.feedback_init_threshold

    def set_feedback_init_threshold(self, feedback_init_threshold):
        self.feedback_init_threshold = feedback_init_threshold
        self._feedback_init_threshold_tool_bar.set_value(self.feedback_init_threshold)
        self.beam_exciter_feedbackController_0.set_init_threshold(self.feedback_init_threshold)

    def get_feedback_init(self):
        return self.feedback_init

    def set_feedback_init(self, feedback_init):
        self.feedback_init = feedback_init
        self._feedback_init_callback(self.feedback_init)
        self.beam_exciter_feedbackController_0.set_init(self.feedback_init)

    def get_feedback(self):
        return self.feedback

    def set_feedback(self, feedback):
        self.feedback = feedback
        self._feedback_callback(self.feedback)
        self.beam_exciter_feedbackController_0.set_feedback(self.feedback)

    def get_enable_ext_trg(self):
        return self.enable_ext_trg

    def set_enable_ext_trg(self, enable_ext_trg):
        self.enable_ext_trg = enable_ext_trg
        self._enable_ext_trg_callback(self.enable_ext_trg)
        self.beam_exciter_gpioTrigger_0.set_enable_polling(self.enable_ext_trg)

    def get_duration(self):
        return self.duration

    def set_duration(self, duration):
        self.duration = duration
        Qt.QMetaObject.invokeMethod(self._duration_label, "setText", Qt.Q_ARG("QString", str(self._duration_formatter(self.duration))))

    def get_delay(self):
        return self.delay

    def set_delay(self, delay):
        self.delay = delay
        self.beam_exciter_switchBlock_0.set_insert_samples((int(self.delay*self.fs_signal)))

    def get_daq_save_enabled(self):
        return self.daq_save_enabled

    def set_daq_save_enabled(self, daq_save_enabled):
        self.daq_save_enabled = daq_save_enabled
        self.beam_exciter_burstFileSink_0.set_enable(self.daq_save_enabled)
        self.beam_exciter_variableFileDump_0.set_enable(self.daq_save_enabled)

    def get_daq_filename(self):
        return self.daq_filename

    def set_daq_filename(self, daq_filename):
        self.daq_filename = daq_filename
        Qt.QMetaObject.invokeMethod(self._daq_filename_line_edit, "setText", Qt.Q_ARG("QString", str(self.daq_filename)))
        self.beam_exciter_burstFileSink_0.set_filename(self.daq_filename)
        self.beam_exciter_variableFileDump_0.set_filename(self.daq_filename + ".yml")

    def get_count_rate_label(self):
        return self.count_rate_label

    def set_count_rate_label(self, count_rate_label):
        self.count_rate_label = count_rate_label
        Qt.QMetaObject.invokeMethod(self._count_rate_label_label, "setText", Qt.Q_ARG("QString", str(self._count_rate_label_formatter(self.count_rate_label))))

    def get_abort_on_max_limit(self):
        return self.abort_on_max_limit

    def set_abort_on_max_limit(self, abort_on_max_limit):
        self.abort_on_max_limit = abort_on_max_limit
        self._abort_on_max_limit_callback(self.abort_on_max_limit)
        self.beam_exciter_feedbackController_0.set_abort_on_max(self.abort_on_max_limit)

    def get_Ta(self):
        return self.Ta

    def set_Ta(self, Ta):
        self.Ta = Ta
        self.beam_exciter_feedbackController_0.set_ta_samples((int(max(1, self.Ta*self.fs_detector))))
        self.beam_exciter_feedbackController_0.set_ki_times_ta(self.Ki*self.Ta)
        self.beam_exciter_feedbackController_0.set_kd_over_ta(self.Kd/self.Ta)
        self.beam_exciter_feedbackController_0.set_ki_times_ta_init(self.Ki_init*self.Ta)
        self.beam_exciter_feedbackController_0.set_kd_over_ta_init(self.Kd_init/self.Ta)
        self.blocks_multiply_const_vxx_0_0_0.set_k(((-1/self.Ta) if self.mode==1 else 1))

    def get_Kp_init(self):
        return self.Kp_init

    def set_Kp_init(self, Kp_init):
        self.Kp_init = Kp_init
        self.beam_exciter_feedbackController_0.set_kp_init(self.Kp_init)

    def get_Kp(self):
        return self.Kp

    def set_Kp(self, Kp):
        self.Kp = Kp
        self.beam_exciter_feedbackController_0.set_kp(self.Kp)

    def get_Ki_init(self):
        return self.Ki_init

    def set_Ki_init(self, Ki_init):
        self.Ki_init = Ki_init
        self.beam_exciter_feedbackController_0.set_ki_times_ta_init(self.Ki_init*self.Ta)

    def get_Ki(self):
        return self.Ki

    def set_Ki(self, Ki):
        self.Ki = Ki
        self.beam_exciter_feedbackController_0.set_ki_times_ta(self.Ki*self.Ta)

    def get_Kd_init(self):
        return self.Kd_init

    def set_Kd_init(self, Kd_init):
        self.Kd_init = Kd_init
        self.beam_exciter_feedbackController_0.set_kd_over_ta_init(self.Kd_init/self.Ta)

    def get_Kd(self):
        return self.Kd

    def set_Kd(self, Kd):
        self.Kd = Kd
        self.beam_exciter_feedbackController_0.set_kd_over_ta(self.Kd/self.Ta)




def main(top_block_cls=exciter_extraction_feedback_optimize, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        gr.logger("realtime").warning("Error: failed to enable real-time scheduling.")

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    snippets_main_after_init(tb)
    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
