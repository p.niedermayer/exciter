#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Excitation for tune measurement
# Author: Philipp Niedermayer
# Copyright: Copyright 2022 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# Description: USRP driven signal generator for beam excitation
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from PyQt5.QtCore import QObject, pyqtSlot
from gnuradio import analog
from gnuradio import beam_exciter
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio.qtgui import Range, RangeWidget
from PyQt5 import QtCore
from source_noise_band import source_noise_band  # grc-generated hier_block
from triggered_output import triggered_output  # grc-generated hier_block
from usrp_sink import usrp_sink  # grc-generated hier_block
from usrp_source import usrp_source  # grc-generated hier_block
import sip
import time
import threading


def snipfcn_snippet_1(self):
    # Handle an uncought exception
    def handle_exception(exc_type, exc_value, exc_traceback):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        print("Troubleshooting help at https://git.gsi.de/p.niedermayer/exciter/-/wikis/Troubleshooting")
        # Inform user by flashing window in red
        w = self # the top block, i.e. main qt window
        w.setAttribute(Qt.Qt.WA_StyledBackground, True)
        w.setStyleSheet('background-color: red;')
        from PyQt5 import QtCore
        QtCore.QTimer.singleShot(200, lambda: w.setStyleSheet(""))

    sys.excepthook = handle_exception


def snippets_init_before_blocks(tb):
    snipfcn_snippet_1(tb)

class exciter_tune(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Excitation for tune measurement", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Excitation for tune measurement")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "exciter_tune")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.repeat = repeat = 40
        self.fs_adc = fs_adc = 200e6
        self.width_khz = width_khz = 50
        self.upsample = upsample = 20
        self.fs_signal = fs_signal = fs_adc/repeat
        self.frev_probe = frev_probe = 0
        self.width_label = width_label = ((width_khz*1e3/frev_probe) if frev_probe>0 else -1)
        self.q2 = q2 = 0.3
        self.q1 = q1 = 0.2
        self.pll_mi = pll_mi = 100
        self.pll_ma = pll_ma = 2000
        self.pll_bw = pll_bw = 1/200
        self.pi = pi = 3.141592653589793
        self.output22 = output22 = 0.8
        self.output21 = output21 = 0
        self.output12 = output12 = 0
        self.output11 = output11 = 1.2
        self.maxoutbuf = maxoutbuf = 64
        self.fs_signal_baseband = fs_signal_baseband = fs_signal/upsample
        self.fs_gpio = fs_gpio = 1000
        self.frev_user_khz = frev_user_khz = 300
        self.frev_select = frev_select = True
        self.frev_label = frev_label = frev_probe
        self.display_outp = display_outp = 0
        self.delay = delay = 1e-3

        ##################################################
        # Blocks
        ##################################################
        snippets_init_before_blocks(self)
        self.frev_tab = Qt.QTabWidget()
        self.frev_tab_widget_0 = Qt.QWidget()
        self.frev_tab_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.frev_tab_widget_0)
        self.frev_tab_grid_layout_0 = Qt.QGridLayout()
        self.frev_tab_layout_0.addLayout(self.frev_tab_grid_layout_0)
        self.frev_tab.addTab(self.frev_tab_widget_0, 'Revolution frequency')
        self.frev_tab_widget_1 = Qt.QWidget()
        self.frev_tab_layout_1 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.frev_tab_widget_1)
        self.frev_tab_grid_layout_1 = Qt.QGridLayout()
        self.frev_tab_layout_1.addLayout(self.frev_tab_grid_layout_1)
        self.frev_tab.addTab(self.frev_tab_widget_1, 'Expert settings')
        self.top_grid_layout.addWidget(self.frev_tab, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.tune_tab = Qt.QTabWidget()
        self.tune_tab_widget_0 = Qt.QWidget()
        self.tune_tab_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tune_tab_widget_0)
        self.tune_tab_grid_layout_0 = Qt.QGridLayout()
        self.tune_tab_layout_0.addLayout(self.tune_tab_grid_layout_0)
        self.tune_tab.addTab(self.tune_tab_widget_0, 'Tune excitation signal')
        self.tune_tab_widget_1 = Qt.QWidget()
        self.tune_tab_layout_1 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tune_tab_widget_1)
        self.tune_tab_grid_layout_1 = Qt.QGridLayout()
        self.tune_tab_layout_1.addLayout(self.tune_tab_grid_layout_1)
        self.tune_tab.addTab(self.tune_tab_widget_1, 'Expert settings')
        self.top_grid_layout.addWidget(self.tune_tab, 3, 0, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.tab_output = Qt.QTabWidget()
        self.tab_output_widget_0 = Qt.QWidget()
        self.tab_output_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_output_widget_0)
        self.tab_output_grid_layout_0 = Qt.QGridLayout()
        self.tab_output_layout_0.addLayout(self.tab_output_grid_layout_0)
        self.tab_output.addTab(self.tab_output_widget_0, 'Output')
        self.top_grid_layout.addWidget(self.tab_output, 10, 0, 1, 1)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        _frev_select_check_box = Qt.QCheckBox("Lock to RF reference")
        self._frev_select_choices = {True: True, False: False}
        self._frev_select_choices_inv = dict((v,k) for k,v in self._frev_select_choices.items())
        self._frev_select_callback = lambda i: Qt.QMetaObject.invokeMethod(_frev_select_check_box, "setChecked", Qt.Q_ARG("bool", self._frev_select_choices_inv[i]))
        self._frev_select_callback(self.frev_select)
        _frev_select_check_box.stateChanged.connect(lambda i: self.set_frev_select(self._frev_select_choices[bool(i)]))
        self.frev_tab_grid_layout_0.addWidget(_frev_select_check_box, 0, 1, 1, 1)
        for r in range(0, 1):
            self.frev_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.frev_tab_grid_layout_0.setColumnStretch(c, 1)

        self._width_khz_tool_bar = beam_exciter.NumericEntry(self.set_width_khz, 'Bandwidth', 50, 0.1, 'kHz', '', 10, True)
        self._width_khz_tool_bar.set_limits(1, 249)
        self.tune_tab_grid_layout_0.addWidget(self._width_khz_tool_bar, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tune_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tune_tab_grid_layout_0.setColumnStretch(c, 1)

        self._q2_tool_bar = beam_exciter.NumericEntry(self.set_q2, 'Tune qy', 0.3, 0.001, '', '', 4, True)
        self._q2_tool_bar.set_limits(0, 5)
        self.tune_tab_grid_layout_0.addWidget(self._q2_tool_bar, 2, 0, 1, 1)
        for r in range(2, 3):
            self.tune_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tune_tab_grid_layout_0.setColumnStretch(c, 1)

        self._q1_tool_bar = beam_exciter.NumericEntry(self.set_q1, 'Tune qx', 0.2, 0.001, '', '', 4, True)
        self._q1_tool_bar.set_limits(0, 5)
        self.tune_tab_grid_layout_0.addWidget(self._q1_tool_bar, 1, 0, 1, 1)
        for r in range(1, 2):
            self.tune_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tune_tab_grid_layout_0.setColumnStretch(c, 1)
        self._pll_mi_range = Range(0, 2000, 1, 100, 200)
        self._pll_mi_win = RangeWidget(self._pll_mi_range, self.set_pll_mi, "PLL min freq [kHz]", "counter", float, QtCore.Qt.Horizontal)
        self.frev_tab_grid_layout_1.addWidget(self._pll_mi_win, 0, 0, 1, 1)
        for r in range(0, 1):
            self.frev_tab_grid_layout_1.setRowStretch(r, 1)
        for c in range(0, 1):
            self.frev_tab_grid_layout_1.setColumnStretch(c, 1)
        self._pll_ma_range = Range(0, 2000, 1, 2000, 200)
        self._pll_ma_win = RangeWidget(self._pll_ma_range, self.set_pll_ma, "PLL max freq [kHz]", "counter", float, QtCore.Qt.Horizontal)
        self.frev_tab_grid_layout_1.addWidget(self._pll_ma_win, 0, 1, 1, 1)
        for r in range(0, 1):
            self.frev_tab_grid_layout_1.setRowStretch(r, 1)
        for c in range(1, 2):
            self.frev_tab_grid_layout_1.setColumnStretch(c, 1)
        self._pll_bw_range = Range(1/200, 2/100, 1/1000, 1/200, 200)
        self._pll_bw_win = RangeWidget(self._pll_bw_range, self.set_pll_bw, "PLL bandwidth [pi/sample]", "counter", float, QtCore.Qt.Horizontal)
        self.frev_tab_grid_layout_1.addWidget(self._pll_bw_win, 0, 2, 1, 1)
        for r in range(0, 1):
            self.frev_tab_grid_layout_1.setRowStretch(r, 1)
        for c in range(2, 3):
            self.frev_tab_grid_layout_1.setColumnStretch(c, 1)

        self._output22_tool_bar = beam_exciter.NumericEntry(self.set_output22, 'Level on Y', 0.8, 0.1, '', '', 3, True)
        self._output22_tool_bar.set_limits(0, 10)
        self.tune_tab_grid_layout_0.addWidget(self._output22_tool_bar, 2, 1, 1, 1)
        for r in range(2, 3):
            self.tune_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tune_tab_grid_layout_0.setColumnStretch(c, 1)

        self._output21_tool_bar = beam_exciter.NumericEntry(self.set_output21, 'Level qy on X (crosstalk)', 0, 0.1, '', '', 3, True)
        self._output21_tool_bar.set_limits(0, 10)
        self.tune_tab_grid_layout_1.addWidget(self._output21_tool_bar, 2, 0, 1, 1)
        for r in range(2, 3):
            self.tune_tab_grid_layout_1.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tune_tab_grid_layout_1.setColumnStretch(c, 1)

        self._output12_tool_bar = beam_exciter.NumericEntry(self.set_output12, 'Level qx on Y (crosstalk)', 0, 0.1, '', '', 3, True)
        self._output12_tool_bar.set_limits(0, 10)
        self.tune_tab_grid_layout_1.addWidget(self._output12_tool_bar, 1, 0, 1, 1)
        for r in range(1, 2):
            self.tune_tab_grid_layout_1.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tune_tab_grid_layout_1.setColumnStretch(c, 1)

        self._output11_tool_bar = beam_exciter.NumericEntry(self.set_output11, 'Level on X', 1.2, 0.1, '', '', 3, True)
        self._output11_tool_bar.set_limits(0, 10)
        self.tune_tab_grid_layout_0.addWidget(self._output11_tool_bar, 1, 1, 1, 1)
        for r in range(1, 2):
            self.tune_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tune_tab_grid_layout_0.setColumnStretch(c, 1)

        self._frev_user_khz_tool_bar = beam_exciter.NumericEntry(self.set_frev_user_khz, 'Set to', 300, 0.01, 'kHz', '', 10, not frev_select)
        self._frev_user_khz_tool_bar.set_limits(100, fs_signal/2000)
        self.frev_tab_grid_layout_0.addWidget(self._frev_user_khz_tool_bar, 0, 2, 1, 1)
        for r in range(0, 1):
            self.frev_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.frev_tab_grid_layout_0.setColumnStretch(c, 1)
        # Create the options list
        self._display_outp_options = [0, 1]
        # Create the labels list
        self._display_outp_labels = ['X', 'Y']
        # Create the combo box
        self._display_outp_tool_bar = Qt.QToolBar(self)
        self._display_outp_tool_bar.addWidget(Qt.QLabel("Show output signal" + ": "))
        self._display_outp_combo_box = Qt.QComboBox()
        self._display_outp_tool_bar.addWidget(self._display_outp_combo_box)
        for _label in self._display_outp_labels: self._display_outp_combo_box.addItem(_label)
        self._display_outp_callback = lambda i: Qt.QMetaObject.invokeMethod(self._display_outp_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._display_outp_options.index(i)))
        self._display_outp_callback(self.display_outp)
        self._display_outp_combo_box.currentIndexChanged.connect(
            lambda i: self.set_display_outp(self._display_outp_options[i]))
        # Create the radio buttons
        self.tab_output_grid_layout_0.addWidget(self._display_outp_tool_bar, 40, 0, 1, 1)
        for r in range(40, 41):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.blocks_probe_frev = blocks.probe_signal_f()
        self._width_label_tool_bar = Qt.QToolBar(self)

        if lambda x: f'{x:.5f}':
            self._width_label_formatter = lambda x: f'{x:.5f}'
        else:
            self._width_label_formatter = lambda x: eng_notation.num_to_str(x)

        self._width_label_tool_bar.addWidget(Qt.QLabel("Bandwidth in tune units:"))
        self._width_label_label = Qt.QLabel(str(self._width_label_formatter(self.width_label)))
        self._width_label_tool_bar.addWidget(self._width_label_label)
        self.tune_tab_grid_layout_0.addWidget(self._width_label_tool_bar, 0, 1, 1, 1)
        for r in range(0, 1):
            self.tune_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tune_tab_grid_layout_0.setColumnStretch(c, 1)
        self.usrp_source_0 = usrp_source(
            fs_signal=fs_signal,
        )
        self.usrp_sink_0 = usrp_sink(
            fs_signal=fs_signal,
        )
        self.source_noise_band_0 = source_noise_band(
            samp_rate=fs_signal,
            upsample=upsample,
            width=(width_khz*1e3),
        )
        self.qtgui_waterfall_sink_x_0_0_0 = qtgui.waterfall_sink_f(
            8192, #size
            window.WIN_BLACKMAN_hARRIS, #wintype
            0, #fc
            fs_signal, #bw
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_waterfall_sink_x_0_0_0.set_update_time(0.1)
        self.qtgui_waterfall_sink_x_0_0_0.enable_grid(True)
        self.qtgui_waterfall_sink_x_0_0_0.enable_axis_labels(True)


        self.qtgui_waterfall_sink_x_0_0_0.set_plot_pos_half(not False)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        colors = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_waterfall_sink_x_0_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_waterfall_sink_x_0_0_0.set_line_label(i, labels[i])
            self.qtgui_waterfall_sink_x_0_0_0.set_color_map(i, colors[i])
            self.qtgui_waterfall_sink_x_0_0_0.set_line_alpha(i, alphas[i])

        self.qtgui_waterfall_sink_x_0_0_0.set_intensity_range(-80, 10)

        self._qtgui_waterfall_sink_x_0_0_0_win = sip.wrapinstance(self.qtgui_waterfall_sink_x_0_0_0.qwidget(), Qt.QWidget)

        self.top_grid_layout.addWidget(self._qtgui_waterfall_sink_x_0_0_0_win, 50, 0, 1, 1)
        for r in range(50, 51):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.output = triggered_output(
            delay=delay,
            fs_gpio=fs_gpio,
            fs_signal=fs_signal,
            open_mode=False,
            rfnoc_graph=None,
            show_output_real_imag=("r" if display_outp==0 else "i"),
        )

        self.tab_output_grid_layout_0.addWidget(self.output, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.hilbert_fc_0 = filter.hilbert_fc(65, window.WIN_HAMMING, 6.76)
        def _frev_probe_probe():
          while True:

            val = self.blocks_probe_frev.level()
            try:
              try:
                self.doc.add_next_tick_callback(functools.partial(self.set_frev_probe,val))
              except AttributeError:
                self.set_frev_probe(val)
            except AttributeError:
              pass
            time.sleep(1.0 / (10))
        _frev_probe_thread = threading.Thread(target=_frev_probe_probe)
        _frev_probe_thread.daemon = True
        _frev_probe_thread.start()
        self._frev_label_tool_bar = Qt.QToolBar(self)

        if lambda x: f'{x/1e3:.3f} kHz':
            self._frev_label_formatter = lambda x: f'{x/1e3:.3f} kHz'
        else:
            self._frev_label_formatter = lambda x: eng_notation.num_to_str(x)

        self._frev_label_tool_bar.addWidget(Qt.QLabel("Frequency:  "))
        self._frev_label_label = Qt.QLabel(str(self._frev_label_formatter(self.frev_label)))
        self._frev_label_tool_bar.addWidget(self._frev_label_label)
        self.frev_tab_grid_layout_0.addWidget(self._frev_label_tool_bar, 0, 0, 1, 1)
        for r in range(0, 1):
            self.frev_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.frev_tab_grid_layout_0.setColumnStretch(c, 1)
        self.blocks_vco_c_0_2_0 = blocks.vco_c(fs_signal, (2*pi), 1)
        self.blocks_vco_c_0_2 = blocks.vco_c(fs_signal, (2*pi), 1)
        self.blocks_selector_1_0_0 = blocks.selector(gr.sizeof_float*1,display_outp,0)
        self.blocks_selector_1_0_0.set_enabled(True)
        self.blocks_selector_0 = blocks.selector(gr.sizeof_float*1,frev_select,0)
        self.blocks_selector_0.set_enabled(True)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_multiply_xx_0_0_0 = blocks.multiply_vcc(1)
        self.blocks_multiply_xx_0_0 = blocks.multiply_vcc(1)
        self.blocks_multiply_matrix_xx_0_0 = blocks.multiply_matrix_ff(((output11, output21), (output12, output22)), gr.TPP_CUSTOM)
        self.blocks_multiply_const_vxx_0_0_0_0 = blocks.multiply_const_ff(q2)
        self.blocks_multiply_const_vxx_0_0_0 = blocks.multiply_const_ff(q1)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_ff((fs_signal/(2*pi)))
        self.blocks_float_to_complex_0_0 = blocks.float_to_complex(1)
        self.blocks_float_to_complex_0_0.set_max_output_buffer(maxoutbuf)
        self.blocks_complex_to_real_0_0_1_0 = blocks.complex_to_real(1)
        self.blocks_complex_to_real_0_0_1 = blocks.complex_to_real(1)
        self.blocks_complex_to_float_1 = blocks.complex_to_float(1)
        self.blocks_complex_to_float_0 = blocks.complex_to_float(1)
        self.analog_rail_ff_0 = analog.rail_ff((-1), 1)
        self.analog_pll_freqdet_cf_0_0 = analog.pll_freqdet_cf((pi*pll_bw), (2*pi*pll_ma*1e3/fs_signal), (2*pi*pll_mi*1e3/fs_signal))
        self.analog_const_source_x_1 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, (float(frev_user_khz)*1e3))


        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_const_source_x_1, 0), (self.blocks_selector_0, 0))
        self.connect((self.analog_pll_freqdet_cf_0_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.analog_rail_ff_0, 0), (self.qtgui_waterfall_sink_x_0_0_0, 0))
        self.connect((self.blocks_complex_to_float_0, 1), (self.blocks_selector_1_0_0, 1))
        self.connect((self.blocks_complex_to_float_0, 0), (self.blocks_selector_1_0_0, 0))
        self.connect((self.blocks_complex_to_float_1, 0), (self.blocks_null_sink_0, 0))
        self.connect((self.blocks_complex_to_float_1, 1), (self.hilbert_fc_0, 0))
        self.connect((self.blocks_complex_to_real_0_0_1, 0), (self.blocks_multiply_matrix_xx_0_0, 0))
        self.connect((self.blocks_complex_to_real_0_0_1_0, 0), (self.blocks_multiply_matrix_xx_0_0, 1))
        self.connect((self.blocks_float_to_complex_0_0, 0), (self.output, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_selector_0, 1))
        self.connect((self.blocks_multiply_const_vxx_0_0_0, 0), (self.blocks_vco_c_0_2, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0_0_0, 0), (self.blocks_vco_c_0_2_0, 0))
        self.connect((self.blocks_multiply_matrix_xx_0_0, 1), (self.blocks_float_to_complex_0_0, 1))
        self.connect((self.blocks_multiply_matrix_xx_0_0, 0), (self.blocks_float_to_complex_0_0, 0))
        self.connect((self.blocks_multiply_xx_0_0, 0), (self.blocks_complex_to_real_0_0_1, 0))
        self.connect((self.blocks_multiply_xx_0_0_0, 0), (self.blocks_complex_to_real_0_0_1_0, 0))
        self.connect((self.blocks_selector_0, 0), (self.blocks_multiply_const_vxx_0_0_0, 0))
        self.connect((self.blocks_selector_0, 0), (self.blocks_multiply_const_vxx_0_0_0_0, 0))
        self.connect((self.blocks_selector_0, 0), (self.blocks_probe_frev, 0))
        self.connect((self.blocks_selector_1_0_0, 0), (self.analog_rail_ff_0, 0))
        self.connect((self.blocks_vco_c_0_2, 0), (self.blocks_multiply_xx_0_0, 0))
        self.connect((self.blocks_vco_c_0_2_0, 0), (self.blocks_multiply_xx_0_0_0, 1))
        self.connect((self.hilbert_fc_0, 0), (self.analog_pll_freqdet_cf_0_0, 0))
        self.connect((self.output, 0), (self.blocks_complex_to_float_0, 0))
        self.connect((self.output, 0), (self.usrp_sink_0, 0))
        self.connect((self.source_noise_band_0, 0), (self.blocks_multiply_xx_0_0, 1))
        self.connect((self.source_noise_band_0, 0), (self.blocks_multiply_xx_0_0_0, 0))
        self.connect((self.usrp_source_0, 0), (self.blocks_complex_to_float_1, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "exciter_tune")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_repeat(self):
        return self.repeat

    def set_repeat(self, repeat):
        self.repeat = repeat
        self.set_fs_signal(self.fs_adc/self.repeat)

    def get_fs_adc(self):
        return self.fs_adc

    def set_fs_adc(self, fs_adc):
        self.fs_adc = fs_adc
        self.set_fs_signal(self.fs_adc/self.repeat)

    def get_width_khz(self):
        return self.width_khz

    def set_width_khz(self, width_khz):
        self.width_khz = width_khz
        self._width_khz_tool_bar.set_value(self.width_khz)
        self.set_width_label(((self.width_khz*1e3/self.frev_probe) if self.frev_probe>0 else -1))
        self.source_noise_band_0.set_width((self.width_khz*1e3))

    def get_upsample(self):
        return self.upsample

    def set_upsample(self, upsample):
        self.upsample = upsample
        self.set_fs_signal_baseband(self.fs_signal/self.upsample)
        self.source_noise_band_0.set_upsample(self.upsample)

    def get_fs_signal(self):
        return self.fs_signal

    def set_fs_signal(self, fs_signal):
        self.fs_signal = fs_signal
        self._frev_user_khz_tool_bar.set_limits(100, self.fs_signal/2000)
        self.set_fs_signal_baseband(self.fs_signal/self.upsample)
        self.analog_pll_freqdet_cf_0_0.set_max_freq((2*self.pi*self.pll_ma*1e3/self.fs_signal))
        self.analog_pll_freqdet_cf_0_0.set_min_freq((2*self.pi*self.pll_mi*1e3/self.fs_signal))
        self.blocks_multiply_const_vxx_0.set_k((self.fs_signal/(2*self.pi)))
        self.output.set_fs_signal(self.fs_signal)
        self.qtgui_waterfall_sink_x_0_0_0.set_frequency_range(0, self.fs_signal)
        self.source_noise_band_0.set_samp_rate(self.fs_signal)
        self.usrp_sink_0.set_fs_signal(self.fs_signal)
        self.usrp_source_0.set_fs_signal(self.fs_signal)

    def get_frev_probe(self):
        return self.frev_probe

    def set_frev_probe(self, frev_probe):
        self.frev_probe = frev_probe
        self.set_frev_label(self.frev_probe)
        self.set_width_label(((self.width_khz*1e3/self.frev_probe) if self.frev_probe>0 else -1))

    def get_width_label(self):
        return self.width_label

    def set_width_label(self, width_label):
        self.width_label = width_label
        Qt.QMetaObject.invokeMethod(self._width_label_label, "setText", Qt.Q_ARG("QString", str(self._width_label_formatter(self.width_label))))

    def get_q2(self):
        return self.q2

    def set_q2(self, q2):
        self.q2 = q2
        self._q2_tool_bar.set_value(self.q2)
        self.blocks_multiply_const_vxx_0_0_0_0.set_k(self.q2)

    def get_q1(self):
        return self.q1

    def set_q1(self, q1):
        self.q1 = q1
        self._q1_tool_bar.set_value(self.q1)
        self.blocks_multiply_const_vxx_0_0_0.set_k(self.q1)

    def get_pll_mi(self):
        return self.pll_mi

    def set_pll_mi(self, pll_mi):
        self.pll_mi = pll_mi
        self.analog_pll_freqdet_cf_0_0.set_min_freq((2*self.pi*self.pll_mi*1e3/self.fs_signal))

    def get_pll_ma(self):
        return self.pll_ma

    def set_pll_ma(self, pll_ma):
        self.pll_ma = pll_ma
        self.analog_pll_freqdet_cf_0_0.set_max_freq((2*self.pi*self.pll_ma*1e3/self.fs_signal))

    def get_pll_bw(self):
        return self.pll_bw

    def set_pll_bw(self, pll_bw):
        self.pll_bw = pll_bw
        self.analog_pll_freqdet_cf_0_0.set_loop_bandwidth((self.pi*self.pll_bw))

    def get_pi(self):
        return self.pi

    def set_pi(self, pi):
        self.pi = pi
        self.analog_pll_freqdet_cf_0_0.set_loop_bandwidth((self.pi*self.pll_bw))
        self.analog_pll_freqdet_cf_0_0.set_max_freq((2*self.pi*self.pll_ma*1e3/self.fs_signal))
        self.analog_pll_freqdet_cf_0_0.set_min_freq((2*self.pi*self.pll_mi*1e3/self.fs_signal))
        self.blocks_multiply_const_vxx_0.set_k((self.fs_signal/(2*self.pi)))

    def get_output22(self):
        return self.output22

    def set_output22(self, output22):
        self.output22 = output22
        self._output22_tool_bar.set_value(self.output22)
        self.blocks_multiply_matrix_xx_0_0.set_A(((self.output11, self.output21), (self.output12, self.output22)))

    def get_output21(self):
        return self.output21

    def set_output21(self, output21):
        self.output21 = output21
        self._output21_tool_bar.set_value(self.output21)
        self.blocks_multiply_matrix_xx_0_0.set_A(((self.output11, self.output21), (self.output12, self.output22)))

    def get_output12(self):
        return self.output12

    def set_output12(self, output12):
        self.output12 = output12
        self._output12_tool_bar.set_value(self.output12)
        self.blocks_multiply_matrix_xx_0_0.set_A(((self.output11, self.output21), (self.output12, self.output22)))

    def get_output11(self):
        return self.output11

    def set_output11(self, output11):
        self.output11 = output11
        self._output11_tool_bar.set_value(self.output11)
        self.blocks_multiply_matrix_xx_0_0.set_A(((self.output11, self.output21), (self.output12, self.output22)))

    def get_maxoutbuf(self):
        return self.maxoutbuf

    def set_maxoutbuf(self, maxoutbuf):
        self.maxoutbuf = maxoutbuf

    def get_fs_signal_baseband(self):
        return self.fs_signal_baseband

    def set_fs_signal_baseband(self, fs_signal_baseband):
        self.fs_signal_baseband = fs_signal_baseband

    def get_fs_gpio(self):
        return self.fs_gpio

    def set_fs_gpio(self, fs_gpio):
        self.fs_gpio = fs_gpio
        self.output.set_fs_gpio(self.fs_gpio)

    def get_frev_user_khz(self):
        return self.frev_user_khz

    def set_frev_user_khz(self, frev_user_khz):
        self.frev_user_khz = frev_user_khz
        self._frev_user_khz_tool_bar.set_value(self.frev_user_khz)
        self.analog_const_source_x_1.set_offset((float(self.frev_user_khz)*1e3))

    def get_frev_select(self):
        return self.frev_select

    def set_frev_select(self, frev_select):
        self.frev_select = frev_select
        self._frev_select_callback(self.frev_select)
        self._frev_user_khz_tool_bar.set_enabled(not self.frev_select)
        self.blocks_selector_0.set_input_index(self.frev_select)

    def get_frev_label(self):
        return self.frev_label

    def set_frev_label(self, frev_label):
        self.frev_label = frev_label
        Qt.QMetaObject.invokeMethod(self._frev_label_label, "setText", Qt.Q_ARG("QString", str(self._frev_label_formatter(self.frev_label))))

    def get_display_outp(self):
        return self.display_outp

    def set_display_outp(self, display_outp):
        self.display_outp = display_outp
        self._display_outp_callback(self.display_outp)
        self.blocks_selector_1_0_0.set_input_index(self.display_outp)
        self.output.set_show_output_real_imag(("r" if self.display_outp==0 else "i"))

    def get_delay(self):
        return self.delay

    def set_delay(self, delay):
        self.delay = delay
        self.output.set_delay(self.delay)




def main(top_block_cls=exciter_tune, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        gr.logger("realtime").warning("Error: failed to enable real-time scheduling.")

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
