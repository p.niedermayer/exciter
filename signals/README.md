# Signal files for playback

Signals can be played upon trigger using the `exciter_from_file` flowgraph.
The file format is raw binary.
The file can be created using GNU Radio's *File Sink* block or with Python Numpy's `np.save` function on an array of dtype `np.float32`.

It is not required but good practice to indicate the sampling rate and data type in the filename, e.g. `mysignal.10MSps.float32`.

