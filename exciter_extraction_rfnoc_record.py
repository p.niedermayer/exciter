#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Excitation for RF KO extraction with fast feedback (record)
# Author: Rahul Singh
# Copyright: Copyright 2023 Rahul Singh, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from PyQt5.QtCore import QObject, pyqtSlot
from gnuradio import beam_exciter
from gnuradio import blocks
import pmt
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import uhd
from source_chirp_gui import source_chirp_gui  # grc-generated hier_block
from source_dual_fm_gui import source_dual_fm_gui  # grc-generated hier_block
from source_noise_band_gui import source_noise_band_gui  # grc-generated hier_block
from source_rbpsk_gui import source_rbpsk_gui  # grc-generated hier_block
from source_sine_gui import source_sine_gui  # grc-generated hier_block
import sip



class exciter_extraction_rfnoc_record(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Excitation for RF KO extraction with fast feedback (record)", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Excitation for RF KO extraction with fast feedback (record)")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "exciter_extraction_rfnoc_record")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.upsample = upsample = 20
        self.fs_signal = fs_signal = 25e6
        self.frev = frev = 1e6
        self.variable_qtgui_label_0 = variable_qtgui_label_0 = ''

        self.rfnoc_graph = uhd_rfnoc_graph = uhd.rfnoc_graph(uhd.device_addr(",fpga=HG_EXFB"))
        self.slot = slot = '1'
        self.replay_duration = replay_duration = 5
        self.info = info = 'Description'
        self.fs_adc = fs_adc = 200e6
        self.check2 = check2 = round(fs_signal/upsample/float(frev), 5)
        self.check1 = check1 = round(fs_signal/float(frev)/2, 5)

        ##################################################
        # Blocks
        ##################################################

        self.t0 = Qt.QTabWidget()
        self.t0_widget_0 = Qt.QWidget()
        self.t0_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.t0_widget_0)
        self.t0_grid_layout_0 = Qt.QGridLayout()
        self.t0_layout_0.addLayout(self.t0_grid_layout_0)
        self.t0.addTab(self.t0_widget_0, 'Excitation signal')
        self.top_grid_layout.addWidget(self.t0, 1, 0, 1, 4)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 4):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.t1 = Qt.QTabWidget()
        self.t1_widget_0 = Qt.QWidget()
        self.t1_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.t1_widget_0)
        self.t1_grid_layout_0 = Qt.QGridLayout()
        self.t1_layout_0.addLayout(self.t1_grid_layout_0)
        self.t1.addTab(self.t1_widget_0, 'Replay on device RAM')
        self.top_grid_layout.addWidget(self.t1, 2, 0, 1, 4)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 4):
            self.top_grid_layout.setColumnStretch(c, 1)
        # Create the options list
        self._slot_options = ['1', '2', '3', '4', '5']
        # Create the labels list
        self._slot_labels = ['1', '2', '3', '4', '5']
        # Create the combo box
        self._slot_tool_bar = Qt.QToolBar(self)
        self._slot_tool_bar.addWidget(Qt.QLabel("Configuration" + ": "))
        self._slot_combo_box = Qt.QComboBox()
        self._slot_tool_bar.addWidget(self._slot_combo_box)
        for _label in self._slot_labels: self._slot_combo_box.addItem(_label)
        self._slot_callback = lambda i: Qt.QMetaObject.invokeMethod(self._slot_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._slot_options.index(i)))
        self._slot_callback(self.slot)
        self._slot_combo_box.currentIndexChanged.connect(
            lambda i: self.set_slot(self._slot_options[i]))
        # Create the radio buttons
        self.top_grid_layout.addWidget(self._slot_tool_bar, 100, 0, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._frev_tool_bar = Qt.QToolBar(self)
        self._frev_tool_bar.addWidget(Qt.QLabel("frev [Hz]" + ": "))
        self._frev_line_edit = Qt.QLineEdit(str(self.frev))
        self._frev_tool_bar.addWidget(self._frev_line_edit)
        self._frev_line_edit.editingFinished.connect(
            lambda: self.set_frev(eval(str(self._frev_line_edit.text()))))
        self.t0_grid_layout_0.addWidget(self._frev_tool_bar, 0, 0, 1, 1)
        for r in range(0, 1):
            self.t0_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.t0_grid_layout_0.setColumnStretch(c, 1)
        self._variable_qtgui_label_0_tool_bar = Qt.QToolBar(self)

        if None:
            self._variable_qtgui_label_0_formatter = None
        else:
            self._variable_qtgui_label_0_formatter = lambda x: str(x)

        self._variable_qtgui_label_0_tool_bar.addWidget(Qt.QLabel("Never close the window while either indicator is lit! \nFailure to comply will cause the device to deadlock \nand require it to be power cycled twice!\nIf in doubt, try play, then stop."))
        self._variable_qtgui_label_0_label = Qt.QLabel(str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0)))
        self._variable_qtgui_label_0_tool_bar.addWidget(self._variable_qtgui_label_0_label)
        self.t1_grid_layout_0.addWidget(self._variable_qtgui_label_0_tool_bar, 2, 2, 1, 4)
        for r in range(2, 3):
            self.t1_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 6):
            self.t1_grid_layout_0.setColumnStretch(c, 1)
        self.uhd_rfnoc_tx_streamer_0 = uhd.rfnoc_tx_streamer(
            self.rfnoc_graph,
            1,
            uhd.stream_args(
                cpu_format="fc32",
                otw_format="sc16",
                channels=[],
                args='',
            ),
            1
        )
        self.uhd_rfnoc_tx_radio_0_0 = uhd.rfnoc_tx_radio(
            self.rfnoc_graph,
            uhd.device_addr(''),
            (-1),
            1)
        self.uhd_rfnoc_tx_radio_0_0.set_rate(fs_adc)
        self.uhd_rfnoc_tx_radio_0_0.set_antenna('AB', 0)
        self.uhd_rfnoc_tx_radio_0_0.set_gain(0, 0)
        self.uhd_rfnoc_tx_radio_0_0.set_bandwidth(0, 0)
        self.uhd_rfnoc_tx_radio_0_0.set_frequency(0, 0)
        self.uhd_rfnoc_replay_0 = uhd.rfnoc_replay(
            self.rfnoc_graph,
            uhd.device_addr(''),
            (-1),
            (-1))
        self.uhd_rfnoc_replay_0.set_record_type("sc16", 0)
        self.uhd_rfnoc_replay_0.set_property('record_offset',0, 0, typename='uint64_t')
        self.uhd_rfnoc_replay_0.set_property('record_size',(int(4*replay_duration*fs_signal)), 0, typename='uint64_t')
        self.uhd_rfnoc_replay_0.set_play_type("sc16", 0)
        self.uhd_rfnoc_replay_0.set_property('play_offset',0, 0, typename='uint64_t')
        self.uhd_rfnoc_replay_0.set_property('play_size',(int(4*replay_duration*fs_signal)), 0, typename='uint64_t')
        self.uhd_rfnoc_duc_0_0 = uhd.rfnoc_duc(
            self.rfnoc_graph,
            uhd.device_addr(''),
            (-1),
            0)
        self.uhd_rfnoc_duc_0_0.set_freq(0, 0)
        self.uhd_rfnoc_duc_0_0.set_input_rate(fs_signal, 0)
        self.uhd_msg_push_button_2 = _uhd_msg_push_button_2_toggle_button = uhd.ReplayMsgPushButton('Stop',"default","default","stop",0,(-1),(-1),(-1),False)
        self.uhd_msg_push_button_2 = _uhd_msg_push_button_2_toggle_button

        self.t1_grid_layout_0.addWidget(_uhd_msg_push_button_2_toggle_button, 0, 2, 1, 2)
        for r in range(0, 1):
            self.t1_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 4):
            self.t1_grid_layout_0.setColumnStretch(c, 1)
        self.uhd_msg_push_button_1 = _uhd_msg_push_button_1_toggle_button = uhd.ReplayMsgPushButton('Play',"default","default","play",0,(-1),(-1),(-1),False)
        self.uhd_msg_push_button_1 = _uhd_msg_push_button_1_toggle_button

        self.t1_grid_layout_0.addWidget(_uhd_msg_push_button_1_toggle_button, 0, 4, 1, 2)
        for r in range(0, 1):
            self.t1_grid_layout_0.setRowStretch(r, 1)
        for c in range(4, 6):
            self.t1_grid_layout_0.setColumnStretch(c, 1)
        self.uhd_msg_push_button_0 = _uhd_msg_push_button_0_toggle_button = uhd.ReplayMsgPushButton('Record',"default","default","record",0,(-1),(-1),(-1),True)
        self.uhd_msg_push_button_0 = _uhd_msg_push_button_0_toggle_button

        self.t1_grid_layout_0.addWidget(_uhd_msg_push_button_0_toggle_button, 0, 0, 1, 2)
        for r in range(0, 1):
            self.t1_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 2):
            self.t1_grid_layout_0.setColumnStretch(c, 1)
        self.sig6 = source_dual_fm_gui(
            frev=frev,
            label_prefix='6) ',
            samp_rate=fs_signal,
        )

        self.t0_grid_layout_0.addWidget(self.sig6, 16, 0, 1, 5)
        for r in range(16, 17):
            self.t0_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 5):
            self.t0_grid_layout_0.setColumnStretch(c, 1)
        self.sig5 = source_chirp_gui(
            frev=frev,
            label_prefix='5) ',
            samp_rate=fs_signal,
        )

        self.t0_grid_layout_0.addWidget(self.sig5, 15, 0, 1, 5)
        for r in range(15, 16):
            self.t0_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 5):
            self.t0_grid_layout_0.setColumnStretch(c, 1)
        self.sig4 = source_rbpsk_gui(
            frev=frev,
            label_prefix='4) ',
            samp_rate=fs_signal,
        )

        self.t0_grid_layout_0.addWidget(self.sig4, 14, 0, 1, 5)
        for r in range(14, 15):
            self.t0_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 5):
            self.t0_grid_layout_0.setColumnStretch(c, 1)
        self.sig3 = source_sine_gui(
            frev=frev,
            label_prefix='3) ',
            samp_rate=fs_signal,
        )

        self.t0_grid_layout_0.addWidget(self.sig3, 13, 0, 1, 5)
        for r in range(13, 14):
            self.t0_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 5):
            self.t0_grid_layout_0.setColumnStretch(c, 1)
        self.sig2 = source_sine_gui(
            frev=frev,
            label_prefix='2) ',
            samp_rate=fs_signal,
        )

        self.t0_grid_layout_0.addWidget(self.sig2, 12, 0, 1, 5)
        for r in range(12, 13):
            self.t0_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 5):
            self.t0_grid_layout_0.setColumnStretch(c, 1)
        self.sig1 = source_noise_band_gui(
            frev=frev,
            label_prefix='1) ',
            samp_rate=fs_signal,
            upsample=upsample,
        )

        self.t0_grid_layout_0.addWidget(self.sig1, 11, 0, 1, 5)
        for r in range(11, 12):
            self.t0_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 5):
            self.t0_grid_layout_0.setColumnStretch(c, 1)
        self.save = _save_toggle_button = qtgui.MsgPushButton('Save config', 'pressed',1,"default","default")
        self.save = _save_toggle_button

        self.top_grid_layout.addWidget(_save_toggle_button, 100, 2, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.restore = _restore_toggle_button = qtgui.MsgPushButton('Restore config', 'pressed',1,"default","default")
        self.restore = _restore_toggle_button

        self.top_grid_layout.addWidget(_restore_toggle_button, 100, 3, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(3, 4):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_rms = qtgui.number_sink(
            gr.sizeof_float,
            1,
            qtgui.NUM_GRAPH_HORIZ,
            1,
            None # parent
        )
        self.qtgui_rms.set_update_time(0.10)
        self.qtgui_rms.set_title("")

        labels = ['Recorded signal RMS:', '', '', '', '',
            '', '', '', '', '']
        units = ['V', '', '', '', '',
            '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
            ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]

        for i in range(1):
            self.qtgui_rms.set_min(i, 0)
            self.qtgui_rms.set_max(i, 1)
            self.qtgui_rms.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_rms.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_rms.set_label(i, labels[i])
            self.qtgui_rms.set_unit(i, units[i])
            self.qtgui_rms.set_factor(i, factor[i])

        self.qtgui_rms.enable_autoscale(False)
        self._qtgui_rms_win = sip.wrapinstance(self.qtgui_rms.qwidget(), Qt.QWidget)
        self.t1_grid_layout_0.addWidget(self._qtgui_rms_win, 100, 0, 1, 3)
        for r in range(100, 101):
            self.t1_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 3):
            self.t1_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_overload = qtgui.number_sink(
            gr.sizeof_float,
            1,
            qtgui.NUM_GRAPH_HORIZ,
            1,
            None # parent
        )
        self.qtgui_overload.set_update_time(0.10)
        self.qtgui_overload.set_title("")

        labels = ['Recorded signal overload:', '', '', '', '',
            '', '', '', '', '']
        units = ['%', '', '', '', '',
            '', '', '', '', '']
        colors = [("black", "red"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
            ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]

        for i in range(1):
            self.qtgui_overload.set_min(i, 0)
            self.qtgui_overload.set_max(i, 40)
            self.qtgui_overload.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_overload.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_overload.set_label(i, labels[i])
            self.qtgui_overload.set_unit(i, units[i])
            self.qtgui_overload.set_factor(i, factor[i])

        self.qtgui_overload.enable_autoscale(False)
        self._qtgui_overload_win = sip.wrapinstance(self.qtgui_overload.qwidget(), Qt.QWidget)
        self.t1_grid_layout_0.addWidget(self._qtgui_overload_win, 100, 3, 1, 3)
        for r in range(100, 101):
            self.t1_grid_layout_0.setRowStretch(r, 1)
        for c in range(3, 6):
            self.t1_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_ledindicator_0_0_0_1 = self._qtgui_ledindicator_0_0_0_1_win = qtgui.GrLEDIndicator('Recording', "yellow", "black", False, 40, 1, 1, 1, self)
        self.qtgui_ledindicator_0_0_0_1 = self._qtgui_ledindicator_0_0_0_1_win
        self.t1_grid_layout_0.addWidget(self._qtgui_ledindicator_0_0_0_1_win, 2, 0, 1, 1)
        for r in range(2, 3):
            self.t1_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.t1_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_ledindicator_0_0_0_0_0 = self._qtgui_ledindicator_0_0_0_0_0_win = qtgui.GrLEDIndicator("Don't close", "red", "black", True, 40, 1, 1, 1, self)
        self.qtgui_ledindicator_0_0_0_0_0 = self._qtgui_ledindicator_0_0_0_0_0_win
        self.t1_grid_layout_0.addWidget(self._qtgui_ledindicator_0_0_0_0_0_win, 2, 1, 1, 1)
        for r in range(2, 3):
            self.t1_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.t1_grid_layout_0.setColumnStretch(c, 1)
        self._info_tool_bar = Qt.QToolBar(self)
        self._info_tool_bar.addWidget(Qt.QLabel("Info" + ": "))
        self._info_line_edit = Qt.QLineEdit(str(self.info))
        self._info_tool_bar.addWidget(self._info_line_edit)
        self._info_line_edit.editingFinished.connect(
            lambda: self.set_info(str(str(self._info_line_edit.text()))))
        self.top_grid_layout.addWidget(self._info_tool_bar, 100, 1, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._check2_tool_bar = Qt.QToolBar(self)

        if None:
            self._check2_formatter = None
        else:
            self._check2_formatter = lambda x: repr(x)

        self._check2_tool_bar.addWidget(Qt.QLabel("Max width:"))
        self._check2_label = Qt.QLabel(str(self._check2_formatter(self.check2)))
        self._check2_tool_bar.addWidget(self._check2_label)
        self.t0_grid_layout_0.addWidget(self._check2_tool_bar, 0, 2, 1, 1)
        for r in range(0, 1):
            self.t0_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.t0_grid_layout_0.setColumnStretch(c, 1)
        self._check1_tool_bar = Qt.QToolBar(self)

        if None:
            self._check1_formatter = None
        else:
            self._check1_formatter = lambda x: repr(x)

        self._check1_tool_bar.addWidget(Qt.QLabel("Max tune:"))
        self._check1_label = Qt.QLabel(str(self._check1_formatter(self.check1)))
        self._check1_tool_bar.addWidget(self._check1_label)
        self.t0_grid_layout_0.addWidget(self._check1_tool_bar, 0, 1, 1, 1)
        for r in range(0, 1):
            self.t0_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.t0_grid_layout_0.setColumnStretch(c, 1)
        self.blocks_throttle2_1 = blocks.throttle( gr.sizeof_float*1, 10e6, True, 0 if "auto" == "auto" else max( int(float(0.1) * 10e6) if "auto" == "time" else int(0.1), 1) )
        self.blocks_probe_rate_0 = blocks.probe_rate(gr.sizeof_float*1, 500.0, 0.15, '')
        self.blocks_multiply_const_vxx_1_0_0 = blocks.multiply_const_ff(100)
        self.blocks_message_strobe_0 = blocks.message_strobe(pmt.intern("tick"), 1000)
        self.blocks_float_to_complex_1_0 = blocks.float_to_complex(1)
        self.blocks_add_xx_0 = blocks.add_vff(1)
        self.beam_exciter_saveRestoreVariables_1 = beam_exciter.saveRestoreVariables(self, slot, """
        frev,
        sig1.ex_enabled,sig1.qex,sig1.dqex,sig1.aex,
        sig2.ex_enabled,sig2.qex,sig2.aex,
        sig3.ex_enabled,sig3.qex,sig3.aex,
        sig4.ex_enabled,sig4.qex,sig4.dqex,sig4.aex,
        sig5.ex_enabled,sig5.qex,sig5.dqex,sig5.aex,sig5.dtex,
        sig6.ex_enabled,sig6.qex,sig6.dqex,sig6.aex,sig6.dtex,
        """, "variable_to_trigger_save", "variable_to_trigger_restore", "info")
        self.beam_exciter_ReplaySafeguard_0 = beam_exciter.ReplaySafeguard()
        self.beam_exciter_LevelProbe_0 = beam_exciter.LevelProbe(1, 10000)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.beam_exciter_ReplaySafeguard_0, 'dont_close'), (self.qtgui_ledindicator_0_0_0_0_0, 'state'))
        self.msg_connect((self.beam_exciter_ReplaySafeguard_0, 'busy'), (self.qtgui_ledindicator_0_0_0_1, 'state'))
        self.msg_connect((self.blocks_message_strobe_0, 'strobe'), (self.beam_exciter_ReplaySafeguard_0, 'tick'))
        self.msg_connect((self.blocks_probe_rate_0, 'rate'), (self.beam_exciter_ReplaySafeguard_0, 'rate'))
        self.msg_connect((self.restore, 'pressed'), (self.beam_exciter_saveRestoreVariables_1, 'restore'))
        self.msg_connect((self.save, 'pressed'), (self.beam_exciter_saveRestoreVariables_1, 'save'))
        self.msg_connect((self.uhd_msg_push_button_0, 'pressed'), (self.beam_exciter_ReplaySafeguard_0, 'cmd'))
        self.msg_connect((self.uhd_msg_push_button_0, 'pressed'), (self.uhd_rfnoc_replay_0, 'command'))
        self.msg_connect((self.uhd_msg_push_button_1, 'pressed'), (self.beam_exciter_ReplaySafeguard_0, 'cmd'))
        self.msg_connect((self.uhd_msg_push_button_1, 'pressed'), (self.uhd_rfnoc_replay_0, 'command'))
        self.msg_connect((self.uhd_msg_push_button_2, 'pressed'), (self.beam_exciter_ReplaySafeguard_0, 'cmd'))
        self.msg_connect((self.uhd_msg_push_button_2, 'pressed'), (self.uhd_rfnoc_replay_0, 'command'))
        self.rfnoc_graph.connect(self.uhd_rfnoc_duc_0_0.get_unique_id(), 0, self.uhd_rfnoc_tx_radio_0_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_replay_0.get_unique_id(), 0, self.uhd_rfnoc_duc_0_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_tx_streamer_0.get_unique_id(), 0, self.uhd_rfnoc_replay_0.get_unique_id(), 0, False)
        self.connect((self.beam_exciter_LevelProbe_0, 1), (self.blocks_multiply_const_vxx_1_0_0, 0))
        self.connect((self.beam_exciter_LevelProbe_0, 0), (self.qtgui_rms, 0))
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_throttle2_1, 0))
        self.connect((self.blocks_float_to_complex_1_0, 0), (self.uhd_rfnoc_tx_streamer_0, 0))
        self.connect((self.blocks_multiply_const_vxx_1_0_0, 0), (self.qtgui_overload, 0))
        self.connect((self.blocks_throttle2_1, 0), (self.beam_exciter_LevelProbe_0, 0))
        self.connect((self.blocks_throttle2_1, 0), (self.blocks_float_to_complex_1_0, 0))
        self.connect((self.blocks_throttle2_1, 0), (self.blocks_probe_rate_0, 0))
        self.connect((self.sig1, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.sig2, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.sig3, 0), (self.blocks_add_xx_0, 2))
        self.connect((self.sig4, 0), (self.blocks_add_xx_0, 3))
        self.connect((self.sig5, 0), (self.blocks_add_xx_0, 4))
        self.connect((self.sig6, 0), (self.blocks_add_xx_0, 5))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "exciter_extraction_rfnoc_record")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_upsample(self):
        return self.upsample

    def set_upsample(self, upsample):
        self.upsample = upsample
        self.set_check2(round(self.fs_signal/self.upsample/float(self.frev), 5))
        self.sig1.set_upsample(self.upsample)

    def get_fs_signal(self):
        return self.fs_signal

    def set_fs_signal(self, fs_signal):
        self.fs_signal = fs_signal
        self.set_check1(round(self.fs_signal/float(self.frev)/2, 5))
        self.set_check2(round(self.fs_signal/self.upsample/float(self.frev), 5))
        self.sig1.set_samp_rate(self.fs_signal)
        self.sig2.set_samp_rate(self.fs_signal)
        self.sig3.set_samp_rate(self.fs_signal)
        self.sig4.set_samp_rate(self.fs_signal)
        self.sig5.set_samp_rate(self.fs_signal)
        self.sig6.set_samp_rate(self.fs_signal)
        self.uhd_rfnoc_duc_0_0.set_input_rate(self.fs_signal, 0)
        self.uhd_rfnoc_replay_0.set_property('record_size',(int(4*self.replay_duration*self.fs_signal)), 0, typename='uint64_t')
        self.uhd_rfnoc_replay_0.set_property('play_size',(int(4*self.replay_duration*self.fs_signal)), 0, typename='uint64_t')

    def get_frev(self):
        return self.frev

    def set_frev(self, frev):
        self.frev = frev
        self.set_check1(round(self.fs_signal/float(self.frev)/2, 5))
        self.set_check2(round(self.fs_signal/self.upsample/float(self.frev), 5))
        Qt.QMetaObject.invokeMethod(self._frev_line_edit, "setText", Qt.Q_ARG("QString", repr(self.frev)))
        self.sig1.set_frev(self.frev)
        self.sig2.set_frev(self.frev)
        self.sig3.set_frev(self.frev)
        self.sig4.set_frev(self.frev)
        self.sig5.set_frev(self.frev)
        self.sig6.set_frev(self.frev)

    def get_variable_qtgui_label_0(self):
        return self.variable_qtgui_label_0

    def set_variable_qtgui_label_0(self, variable_qtgui_label_0):
        self.variable_qtgui_label_0 = variable_qtgui_label_0
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_0_label, "setText", Qt.Q_ARG("QString", str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0))))

    def get_uhd_rfnoc_graph(self):
        return self.uhd_rfnoc_graph

    def set_uhd_rfnoc_graph(self, uhd_rfnoc_graph):
        self.uhd_rfnoc_graph = uhd_rfnoc_graph

    def get_slot(self):
        return self.slot

    def set_slot(self, slot):
        self.slot = slot
        self._slot_callback(self.slot)
        self.beam_exciter_saveRestoreVariables_1.set_slot(self.slot)

    def get_replay_duration(self):
        return self.replay_duration

    def set_replay_duration(self, replay_duration):
        self.replay_duration = replay_duration
        self.uhd_rfnoc_replay_0.set_property('record_size',(int(4*self.replay_duration*self.fs_signal)), 0, typename='uint64_t')
        self.uhd_rfnoc_replay_0.set_property('play_size',(int(4*self.replay_duration*self.fs_signal)), 0, typename='uint64_t')

    def get_info(self):
        return self.info

    def set_info(self, info):
        self.info = info
        Qt.QMetaObject.invokeMethod(self._info_line_edit, "setText", Qt.Q_ARG("QString", str(self.info)))

    def get_fs_adc(self):
        return self.fs_adc

    def set_fs_adc(self, fs_adc):
        self.fs_adc = fs_adc
        self.uhd_rfnoc_tx_radio_0_0.set_rate(self.fs_adc)

    def get_check2(self):
        return self.check2

    def set_check2(self, check2):
        self.check2 = check2
        Qt.QMetaObject.invokeMethod(self._check2_label, "setText", Qt.Q_ARG("QString", str(self._check2_formatter(self.check2))))

    def get_check1(self):
        return self.check1

    def set_check1(self, check1):
        self.check1 = check1
        Qt.QMetaObject.invokeMethod(self._check1_label, "setText", Qt.Q_ARG("QString", str(self._check1_formatter(self.check1))))




def main(top_block_cls=exciter_extraction_rfnoc_record, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        gr.logger("realtime").warning("Error: failed to enable real-time scheduling.")

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
