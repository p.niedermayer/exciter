title: The gr-beam_exciter OOT Module
brief: Blocks for beam excitation
tags: # Tags are arbitrary, but look at CGRAN what other authors are using
  - sdr
author:
  - Philipp Niedermayer <p.niedermayer@gsi.de>
copyright_owner:
  - Philipp Niedermayer
  - GSI Helmholtzzentrum für Schwerionenforschung
license: GPL-3.0-or-later
gr_supported_version: 3.10.7.0 # Put a comma separated list of supported GR versions here
repo: https://git.gsi.de/p.niedermayer/exciter # Put the URL of the repository here, or leave blank for default
#website: <module_website> # If you have a separate project website, put it here
#icon: <icon_url> # Put a URL to a square image here that will be used as an icon on CGRAN
---
#A longer, multi-line description of gr-beam_exciter.
#You may use some *basic* Markdown here.
#If left empty, it will try to find a README file instead.
