#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Trigger switch example
# Author: Philipp Niedermayer
# Copyright: Copyright 2022 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio import analog
from gnuradio import beam_exciter
from gnuradio import uhd
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio.qtgui import Range, RangeWidget
from PyQt5 import QtCore
import sip



class triggerSwitchExample(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Trigger switch example", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Trigger switch example")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "triggerSwitchExample")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.trg_count = trg_count = 0
        self.variable_qtgui_label_1 = variable_qtgui_label_1 = 0
        self.variable_qtgui_label_0 = variable_qtgui_label_0 = trg_count

        self.rfnoc_graph = uhd_rfnoc_graph = uhd.rfnoc_graph(uhd.device_addr(""))
        self.samp_rate_gpio = samp_rate_gpio = 1e3
        self.samp_rate = samp_rate = 5000000
        self.output_duration = output_duration = 7
        self.output_delay = output_delay = 0
        self.outp_enable_manual = outp_enable_manual = True
        self.manual_trigger = manual_trigger = 0
        self.enable_polling = enable_polling = True

        ##################################################
        # Blocks
        ##################################################

        self._output_duration_range = Range(0, int((2**31-1)/samp_rate), 0.1, 7, 200)
        self._output_duration_win = RangeWidget(self._output_duration_range, self.set_output_duration, "Burst duration* [s]", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._output_duration_win, 1, 0, 1, 2)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        _outp_enable_manual_check_box = Qt.QCheckBox("Enable output")
        self._outp_enable_manual_choices = {True: True, False: False}
        self._outp_enable_manual_choices_inv = dict((v,k) for k,v in self._outp_enable_manual_choices.items())
        self._outp_enable_manual_callback = lambda i: Qt.QMetaObject.invokeMethod(_outp_enable_manual_check_box, "setChecked", Qt.Q_ARG("bool", self._outp_enable_manual_choices_inv[i]))
        self._outp_enable_manual_callback(self.outp_enable_manual)
        _outp_enable_manual_check_box.stateChanged.connect(lambda i: self.set_outp_enable_manual(self._outp_enable_manual_choices[bool(i)]))
        self.top_layout.addWidget(_outp_enable_manual_check_box)
        self._manual_trigger_choices = {'Pressed': 1, 'Released': 0}

        _manual_trigger_toggle_button = qtgui.ToggleButton(self.set_manual_trigger, 'Manual trigger', self._manual_trigger_choices, False, 'value')
        _manual_trigger_toggle_button.setColors("default", "default", "default", "default")
        self.manual_trigger = _manual_trigger_toggle_button

        self.top_grid_layout.addWidget(_manual_trigger_toggle_button, 0, 2, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        _enable_polling_check_box = Qt.QCheckBox("Enable external trigger")
        self._enable_polling_choices = {True: True, False: False}
        self._enable_polling_choices_inv = dict((v,k) for k,v in self._enable_polling_choices.items())
        self._enable_polling_callback = lambda i: Qt.QMetaObject.invokeMethod(_enable_polling_check_box, "setChecked", Qt.Q_ARG("bool", self._enable_polling_choices_inv[i]))
        self._enable_polling_callback(self.enable_polling)
        _enable_polling_check_box.stateChanged.connect(lambda i: self.set_enable_polling(self._enable_polling_choices[bool(i)]))
        self.top_grid_layout.addWidget(_enable_polling_check_box, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._variable_qtgui_label_1_tool_bar = Qt.QToolBar(self)

        if None:
            self._variable_qtgui_label_1_formatter = None
        else:
            self._variable_qtgui_label_1_formatter = lambda x: str(x)

        self._variable_qtgui_label_1_tool_bar.addWidget(Qt.QLabel("*for gated mode use duration ="))
        self._variable_qtgui_label_1_label = Qt.QLabel(str(self._variable_qtgui_label_1_formatter(self.variable_qtgui_label_1)))
        self._variable_qtgui_label_1_tool_bar.addWidget(self._variable_qtgui_label_1_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_1_tool_bar, 1, 2, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._variable_qtgui_label_0_tool_bar = Qt.QToolBar(self)

        if None:
            self._variable_qtgui_label_0_formatter = None
        else:
            self._variable_qtgui_label_0_formatter = lambda x: str(x)

        self._variable_qtgui_label_0_tool_bar.addWidget(Qt.QLabel("Trigger count: "))
        self._variable_qtgui_label_0_label = Qt.QLabel(str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0)))
        self._variable_qtgui_label_0_tool_bar.addWidget(self._variable_qtgui_label_0_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_0_tool_bar, 0, 1, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_waterfall_sink_x_0_0_0 = qtgui.waterfall_sink_f(
            1024, #size
            window.WIN_BLACKMAN_hARRIS, #wintype
            0, #fc
            samp_rate, #bw
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_waterfall_sink_x_0_0_0.set_update_time(0.10)
        self.qtgui_waterfall_sink_x_0_0_0.enable_grid(False)
        self.qtgui_waterfall_sink_x_0_0_0.enable_axis_labels(True)


        self.qtgui_waterfall_sink_x_0_0_0.set_plot_pos_half(not False)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        colors = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_waterfall_sink_x_0_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_waterfall_sink_x_0_0_0.set_line_label(i, labels[i])
            self.qtgui_waterfall_sink_x_0_0_0.set_color_map(i, colors[i])
            self.qtgui_waterfall_sink_x_0_0_0.set_line_alpha(i, alphas[i])

        self.qtgui_waterfall_sink_x_0_0_0.set_intensity_range(-100, 10)

        self._qtgui_waterfall_sink_x_0_0_0_win = sip.wrapinstance(self.qtgui_waterfall_sink_x_0_0_0.qwidget(), Qt.QWidget)

        self.top_grid_layout.addWidget(self._qtgui_waterfall_sink_x_0_0_0_win, 98, 0, 1, 3)
        for r in range(98, 99):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_ledindicator_0_0_0 = self._qtgui_ledindicator_0_0_0_win = qtgui.GrLEDIndicator('Output', "lime", "black", False, 35, 1, 1, 1, self)
        self.qtgui_ledindicator_0_0_0 = self._qtgui_ledindicator_0_0_0_win
        self.top_grid_layout.addWidget(self._qtgui_ledindicator_0_0_0_win, 10, 0, 2, 3)
        for r in range(10, 12):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_vco_c_0 = blocks.vco_c(samp_rate, (samp_rate/5*2*3.1415), 1)
        self.blocks_throttle2_0 = blocks.throttle( gr.sizeof_gr_complex*1, samp_rate, True, 0 if "auto" == "auto" else max( int(float(0.1) * samp_rate) if "auto" == "time" else int(0.1), 1) )
        self.blocks_msgpair_to_var_0 = blocks.msg_pair_to_var(self.set_trg_count)
        self.blocks_copy_0 = blocks.copy(gr.sizeof_gr_complex*1)
        self.blocks_copy_0.set_enabled(outp_enable_manual)
        self.blocks_complex_to_float_0 = blocks.complex_to_float(1)
        self.beam_exciter_triggerSwitchV2_0 = beam_exciter.triggerSwitchV2(
            enable_polling=enable_polling,
            poll_every_samples=(int(samp_rate/samp_rate_gpio)),
            duration_samples=(int(output_duration * samp_rate)),
            delay_samples=(int(output_delay * samp_rate)),
            manual_trigger=manual_trigger,
            ptr_graph=getattr(self, "rfnoc_graph", None),
            device_addr='',
            device_index=0,
            radio_index=0,
            gpio_bank='RXA',
            gpio_pin=0,
            input_mode=0,
            stop_output=True,
            add_stream_tags=True,
        )
        self.analog_sig_source_x_0 = analog.sig_source_f(samp_rate, analog.GR_COS_WAVE, 0.2, 0.2, 1, 0)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.beam_exciter_triggerSwitchV2_0, 'trg'), (self.blocks_msgpair_to_var_0, 'inpair'))
        self.msg_connect((self.beam_exciter_triggerSwitchV2_0, 'outp'), (self.qtgui_ledindicator_0_0_0, 'state'))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_vco_c_0, 0))
        self.connect((self.beam_exciter_triggerSwitchV2_0, 0), (self.blocks_copy_0, 0))
        self.connect((self.blocks_complex_to_float_0, 0), (self.qtgui_waterfall_sink_x_0_0_0, 0))
        self.connect((self.blocks_copy_0, 0), (self.blocks_complex_to_float_0, 0))
        self.connect((self.blocks_throttle2_0, 0), (self.beam_exciter_triggerSwitchV2_0, 0))
        self.connect((self.blocks_vco_c_0, 0), (self.blocks_throttle2_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "triggerSwitchExample")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_trg_count(self):
        return self.trg_count

    def set_trg_count(self, trg_count):
        self.trg_count = trg_count
        self.set_variable_qtgui_label_0(self.trg_count)

    def get_variable_qtgui_label_1(self):
        return self.variable_qtgui_label_1

    def set_variable_qtgui_label_1(self, variable_qtgui_label_1):
        self.variable_qtgui_label_1 = variable_qtgui_label_1
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_1_label, "setText", Qt.Q_ARG("QString", str(self._variable_qtgui_label_1_formatter(self.variable_qtgui_label_1))))

    def get_variable_qtgui_label_0(self):
        return self.variable_qtgui_label_0

    def set_variable_qtgui_label_0(self, variable_qtgui_label_0):
        self.variable_qtgui_label_0 = variable_qtgui_label_0
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_0_label, "setText", Qt.Q_ARG("QString", str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0))))

    def get_uhd_rfnoc_graph(self):
        return self.uhd_rfnoc_graph

    def set_uhd_rfnoc_graph(self, uhd_rfnoc_graph):
        self.uhd_rfnoc_graph = uhd_rfnoc_graph

    def get_samp_rate_gpio(self):
        return self.samp_rate_gpio

    def set_samp_rate_gpio(self, samp_rate_gpio):
        self.samp_rate_gpio = samp_rate_gpio

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.beam_exciter_triggerSwitchV2_0.set_duration_samples((int(self.output_duration * self.samp_rate)))
        self.beam_exciter_triggerSwitchV2_0.set_delay_samples((int(self.output_delay * self.samp_rate)))
        self.blocks_throttle2_0.set_sample_rate(self.samp_rate)
        self.qtgui_waterfall_sink_x_0_0_0.set_frequency_range(0, self.samp_rate)

    def get_output_duration(self):
        return self.output_duration

    def set_output_duration(self, output_duration):
        self.output_duration = output_duration
        self.beam_exciter_triggerSwitchV2_0.set_duration_samples((int(self.output_duration * self.samp_rate)))

    def get_output_delay(self):
        return self.output_delay

    def set_output_delay(self, output_delay):
        self.output_delay = output_delay
        self.beam_exciter_triggerSwitchV2_0.set_delay_samples((int(self.output_delay * self.samp_rate)))

    def get_outp_enable_manual(self):
        return self.outp_enable_manual

    def set_outp_enable_manual(self, outp_enable_manual):
        self.outp_enable_manual = outp_enable_manual
        self._outp_enable_manual_callback(self.outp_enable_manual)
        self.blocks_copy_0.set_enabled(self.outp_enable_manual)

    def get_manual_trigger(self):
        return self.manual_trigger

    def set_manual_trigger(self, manual_trigger):
        self.manual_trigger = manual_trigger
        self.beam_exciter_triggerSwitchV2_0.set_manual_trigger(self.manual_trigger)

    def get_enable_polling(self):
        return self.enable_polling

    def set_enable_polling(self, enable_polling):
        self.enable_polling = enable_polling
        self._enable_polling_callback(self.enable_polling)
        self.beam_exciter_triggerSwitchV2_0.set_enable_polling(self.enable_polling)




def main(top_block_cls=triggerSwitchExample, options=None):

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
