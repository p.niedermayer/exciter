#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: QT GUI Numeric Entry Example
# Author: Philipp Niedermayer
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio import beam_exciter
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx



class qtGuiNumericEntry(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "QT GUI Numeric Entry Example", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("QT GUI Numeric Entry Example")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "qtGuiNumericEntry")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.freq = freq = 1
        self.variable_qtgui_label_0 = variable_qtgui_label_0 = freq
        self.samp_rate = samp_rate = 32000
        self.limit_min = limit_min = 0
        self.limit_max = limit_max = 100
        self.enabled = enabled = True

        ##################################################
        # Blocks
        ##################################################

        self._limit_min_tool_bar = Qt.QToolBar(self)
        self._limit_min_tool_bar.addWidget(Qt.QLabel("Min value" + ": "))
        self._limit_min_line_edit = Qt.QLineEdit(str(self.limit_min))
        self._limit_min_tool_bar.addWidget(self._limit_min_line_edit)
        self._limit_min_line_edit.editingFinished.connect(
            lambda: self.set_limit_min(eval(str(self._limit_min_line_edit.text()))))
        self.top_grid_layout.addWidget(self._limit_min_tool_bar, 4, 0, 1, 1)
        for r in range(4, 5):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._limit_max_tool_bar = Qt.QToolBar(self)
        self._limit_max_tool_bar.addWidget(Qt.QLabel("Max value" + ": "))
        self._limit_max_line_edit = Qt.QLineEdit(str(self.limit_max))
        self._limit_max_tool_bar.addWidget(self._limit_max_line_edit)
        self._limit_max_line_edit.editingFinished.connect(
            lambda: self.set_limit_max(eval(str(self._limit_max_line_edit.text()))))
        self.top_grid_layout.addWidget(self._limit_max_tool_bar, 5, 0, 1, 1)
        for r in range(5, 6):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        _enabled_check_box = Qt.QCheckBox("Enabled")
        self._enabled_choices = {True: True, False: False}
        self._enabled_choices_inv = dict((v,k) for k,v in self._enabled_choices.items())
        self._enabled_callback = lambda i: Qt.QMetaObject.invokeMethod(_enabled_check_box, "setChecked", Qt.Q_ARG("bool", self._enabled_choices_inv[i]))
        self._enabled_callback(self.enabled)
        _enabled_check_box.stateChanged.connect(lambda i: self.set_enabled(self._enabled_choices[bool(i)]))
        self.top_layout.addWidget(_enabled_check_box)
        self._variable_qtgui_label_0_tool_bar = Qt.QToolBar(self)

        if None:
            self._variable_qtgui_label_0_formatter = None
        else:
            self._variable_qtgui_label_0_formatter = lambda x: repr(x)

        self._variable_qtgui_label_0_tool_bar.addWidget(Qt.QLabel("Actual value:"))
        self._variable_qtgui_label_0_label = Qt.QLabel(str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0)))
        self._variable_qtgui_label_0_tool_bar.addWidget(self._variable_qtgui_label_0_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_0_tool_bar, 1, 0, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)

        self._freq_tool_bar = beam_exciter.NumericEntry(self.set_freq, 'Frequency', 1, 0.1, 'kHz', 'Example frequency value for testing', 5, enabled)
        self._freq_tool_bar.set_limits(limit_min, limit_max)
        self.top_grid_layout.addWidget(self._freq_tool_bar, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)



    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "qtGuiNumericEntry")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self._freq_tool_bar.set_value(self.freq)
        self.set_variable_qtgui_label_0(self.freq)

    def get_variable_qtgui_label_0(self):
        return self.variable_qtgui_label_0

    def set_variable_qtgui_label_0(self, variable_qtgui_label_0):
        self.variable_qtgui_label_0 = variable_qtgui_label_0
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_0_label, "setText", Qt.Q_ARG("QString", str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0))))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_limit_min(self):
        return self.limit_min

    def set_limit_min(self, limit_min):
        self.limit_min = limit_min
        self._freq_tool_bar.set_limits(self.limit_min, self.limit_max)
        Qt.QMetaObject.invokeMethod(self._limit_min_line_edit, "setText", Qt.Q_ARG("QString", repr(self.limit_min)))

    def get_limit_max(self):
        return self.limit_max

    def set_limit_max(self, limit_max):
        self.limit_max = limit_max
        self._freq_tool_bar.set_limits(self.limit_min, self.limit_max)
        Qt.QMetaObject.invokeMethod(self._limit_max_line_edit, "setText", Qt.Q_ARG("QString", repr(self.limit_max)))

    def get_enabled(self):
        return self.enabled

    def set_enabled(self, enabled):
        self.enabled = enabled
        self._enabled_callback(self.enabled)
        self._freq_tool_bar.set_enabled(self.enabled)




def main(top_block_cls=qtGuiNumericEntry, options=None):

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
