#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Varialbe Save Restore example
# Author: Philipp Niedermayer
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
from PyQt5.QtCore import QObject, pyqtSlot
from gnuradio import beam_exciter
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx



class saveRestoreVariables(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Varialbe Save Restore example", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Varialbe Save Restore example")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "saveRestoreVariables")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.variable_entry_1 = variable_entry_1 = 3.1415
        self.variable_0 = variable_0 = 456
        self.samp_rate = samp_rate = 32000
        self.variable_qtgui_label_0_0 = variable_qtgui_label_0_0 = (2*variable_entry_1)
        self.variable_qtgui_label_0 = variable_qtgui_label_0 = variable_0
        self.variable_entry_2 = variable_entry_2 = 'hello'
        self.variable_entry_0 = variable_entry_0 = samp_rate
        self.variable_chooser_0 = variable_chooser_0 = 0
        self.variable_check_0 = variable_check_0 = True
        self.slot = slot = '1'
        self.info = info = '?'

        ##################################################
        # Blocks
        ##################################################

        # Create the options list
        self._slot_options = ['1', '2', '3', 'TEST', 'private']
        # Create the labels list
        self._slot_labels = ['1', '2', '3', 'TEST', 'private']
        # Create the combo box
        self._slot_tool_bar = Qt.QToolBar(self)
        self._slot_tool_bar.addWidget(Qt.QLabel("Slot" + ": "))
        self._slot_combo_box = Qt.QComboBox()
        self._slot_tool_bar.addWidget(self._slot_combo_box)
        for _label in self._slot_labels: self._slot_combo_box.addItem(_label)
        self._slot_callback = lambda i: Qt.QMetaObject.invokeMethod(self._slot_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._slot_options.index(i)))
        self._slot_callback(self.slot)
        self._slot_combo_box.currentIndexChanged.connect(
            lambda i: self.set_slot(self._slot_options[i]))
        # Create the radio buttons
        self.top_grid_layout.addWidget(self._slot_tool_bar, 100, 0, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._variable_qtgui_label_0_0_tool_bar = Qt.QToolBar(self)

        if None:
            self._variable_qtgui_label_0_0_formatter = None
        else:
            self._variable_qtgui_label_0_0_formatter = lambda x: eng_notation.num_to_str(x)

        self._variable_qtgui_label_0_0_tool_bar.addWidget(Qt.QLabel("Double of variable_1:"))
        self._variable_qtgui_label_0_0_label = Qt.QLabel(str(self._variable_qtgui_label_0_0_formatter(self.variable_qtgui_label_0_0)))
        self._variable_qtgui_label_0_0_tool_bar.addWidget(self._variable_qtgui_label_0_0_label)
        self.top_layout.addWidget(self._variable_qtgui_label_0_0_tool_bar)
        self._variable_qtgui_label_0_tool_bar = Qt.QToolBar(self)

        if None:
            self._variable_qtgui_label_0_formatter = None
        else:
            self._variable_qtgui_label_0_formatter = lambda x: eng_notation.num_to_str(x)

        self._variable_qtgui_label_0_tool_bar.addWidget(Qt.QLabel("Value of variable_0:"))
        self._variable_qtgui_label_0_label = Qt.QLabel(str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0)))
        self._variable_qtgui_label_0_tool_bar.addWidget(self._variable_qtgui_label_0_label)
        self.top_layout.addWidget(self._variable_qtgui_label_0_tool_bar)
        self._variable_entry_2_tool_bar = Qt.QToolBar(self)
        self._variable_entry_2_tool_bar.addWidget(Qt.QLabel("String" + ": "))
        self._variable_entry_2_line_edit = Qt.QLineEdit(str(self.variable_entry_2))
        self._variable_entry_2_tool_bar.addWidget(self._variable_entry_2_line_edit)
        self._variable_entry_2_line_edit.editingFinished.connect(
            lambda: self.set_variable_entry_2(str(str(self._variable_entry_2_line_edit.text()))))
        self.top_layout.addWidget(self._variable_entry_2_tool_bar)
        self._variable_entry_1_tool_bar = Qt.QToolBar(self)
        self._variable_entry_1_tool_bar.addWidget(Qt.QLabel("Float" + ": "))
        self._variable_entry_1_line_edit = Qt.QLineEdit(str(self.variable_entry_1))
        self._variable_entry_1_tool_bar.addWidget(self._variable_entry_1_line_edit)
        self._variable_entry_1_line_edit.editingFinished.connect(
            lambda: self.set_variable_entry_1(eng_notation.str_to_num(str(self._variable_entry_1_line_edit.text()))))
        self.top_layout.addWidget(self._variable_entry_1_tool_bar)
        self._variable_entry_0_tool_bar = Qt.QToolBar(self)
        self._variable_entry_0_tool_bar.addWidget(Qt.QLabel("Raw:" + ": "))
        self._variable_entry_0_line_edit = Qt.QLineEdit(str(self.variable_entry_0))
        self._variable_entry_0_tool_bar.addWidget(self._variable_entry_0_line_edit)
        self._variable_entry_0_line_edit.editingFinished.connect(
            lambda: self.set_variable_entry_0(eval(str(self._variable_entry_0_line_edit.text()))))
        self.top_layout.addWidget(self._variable_entry_0_tool_bar)
        # Create the options list
        self._variable_chooser_0_options = [0, 1, 2]
        # Create the labels list
        self._variable_chooser_0_labels = ['A', 'B', 'C']
        # Create the combo box
        self._variable_chooser_0_tool_bar = Qt.QToolBar(self)
        self._variable_chooser_0_tool_bar.addWidget(Qt.QLabel("Choice" + ": "))
        self._variable_chooser_0_combo_box = Qt.QComboBox()
        self._variable_chooser_0_tool_bar.addWidget(self._variable_chooser_0_combo_box)
        for _label in self._variable_chooser_0_labels: self._variable_chooser_0_combo_box.addItem(_label)
        self._variable_chooser_0_callback = lambda i: Qt.QMetaObject.invokeMethod(self._variable_chooser_0_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._variable_chooser_0_options.index(i)))
        self._variable_chooser_0_callback(self.variable_chooser_0)
        self._variable_chooser_0_combo_box.currentIndexChanged.connect(
            lambda i: self.set_variable_chooser_0(self._variable_chooser_0_options[i]))
        # Create the radio buttons
        self.top_layout.addWidget(self._variable_chooser_0_tool_bar)
        _variable_check_0_check_box = Qt.QCheckBox("Check")
        self._variable_check_0_choices = {True: True, False: False}
        self._variable_check_0_choices_inv = dict((v,k) for k,v in self._variable_check_0_choices.items())
        self._variable_check_0_callback = lambda i: Qt.QMetaObject.invokeMethod(_variable_check_0_check_box, "setChecked", Qt.Q_ARG("bool", self._variable_check_0_choices_inv[i]))
        self._variable_check_0_callback(self.variable_check_0)
        _variable_check_0_check_box.stateChanged.connect(lambda i: self.set_variable_check_0(self._variable_check_0_choices[bool(i)]))
        self.top_layout.addWidget(_variable_check_0_check_box)
        self.save = _save_toggle_button = qtgui.MsgPushButton('Save', 'pressed',1,"default","default")
        self.save = _save_toggle_button

        self.top_grid_layout.addWidget(_save_toggle_button, 102, 0, 1, 1)
        for r in range(102, 103):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.restore = _restore_toggle_button = qtgui.MsgPushButton('Restore', 'pressed',1,"default","default")
        self.restore = _restore_toggle_button

        self.top_grid_layout.addWidget(_restore_toggle_button, 103, 0, 1, 1)
        for r in range(103, 104):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._info_tool_bar = Qt.QToolBar(self)
        self._info_tool_bar.addWidget(Qt.QLabel("Slot description" + ": "))
        self._info_line_edit = Qt.QLineEdit(str(self.info))
        self._info_tool_bar.addWidget(self._info_line_edit)
        self._info_line_edit.editingFinished.connect(
            lambda: self.set_info(str(str(self._info_line_edit.text()))))
        self.top_grid_layout.addWidget(self._info_tool_bar, 101, 0, 1, 1)
        for r in range(101, 102):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.beam_exciter_saveRestoreVariables_1 = beam_exciter.saveRestoreVariables(self, slot, """
        samp_rate,variable_0,variable_entry_0,variable_entry_1,
        variable_entry_2,
        variable_chooser_0,variable_check_0,
        """, "variable_to_trigger_save", "variable_to_trigger_restore", "info")


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.restore, 'pressed'), (self.beam_exciter_saveRestoreVariables_1, 'restore'))
        self.msg_connect((self.save, 'pressed'), (self.beam_exciter_saveRestoreVariables_1, 'save'))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "saveRestoreVariables")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_variable_entry_1(self):
        return self.variable_entry_1

    def set_variable_entry_1(self, variable_entry_1):
        self.variable_entry_1 = variable_entry_1
        Qt.QMetaObject.invokeMethod(self._variable_entry_1_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.variable_entry_1)))
        self.set_variable_qtgui_label_0_0((2*self.variable_entry_1))

    def get_variable_0(self):
        return self.variable_0

    def set_variable_0(self, variable_0):
        self.variable_0 = variable_0
        self.set_variable_qtgui_label_0(self.variable_0)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_variable_entry_0(self.samp_rate)

    def get_variable_qtgui_label_0_0(self):
        return self.variable_qtgui_label_0_0

    def set_variable_qtgui_label_0_0(self, variable_qtgui_label_0_0):
        self.variable_qtgui_label_0_0 = variable_qtgui_label_0_0
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_0_0_label, "setText", Qt.Q_ARG("QString", str(self._variable_qtgui_label_0_0_formatter(self.variable_qtgui_label_0_0))))

    def get_variable_qtgui_label_0(self):
        return self.variable_qtgui_label_0

    def set_variable_qtgui_label_0(self, variable_qtgui_label_0):
        self.variable_qtgui_label_0 = variable_qtgui_label_0
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_0_label, "setText", Qt.Q_ARG("QString", str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0))))

    def get_variable_entry_2(self):
        return self.variable_entry_2

    def set_variable_entry_2(self, variable_entry_2):
        self.variable_entry_2 = variable_entry_2
        Qt.QMetaObject.invokeMethod(self._variable_entry_2_line_edit, "setText", Qt.Q_ARG("QString", str(self.variable_entry_2)))

    def get_variable_entry_0(self):
        return self.variable_entry_0

    def set_variable_entry_0(self, variable_entry_0):
        self.variable_entry_0 = variable_entry_0
        Qt.QMetaObject.invokeMethod(self._variable_entry_0_line_edit, "setText", Qt.Q_ARG("QString", repr(self.variable_entry_0)))

    def get_variable_chooser_0(self):
        return self.variable_chooser_0

    def set_variable_chooser_0(self, variable_chooser_0):
        self.variable_chooser_0 = variable_chooser_0
        self._variable_chooser_0_callback(self.variable_chooser_0)

    def get_variable_check_0(self):
        return self.variable_check_0

    def set_variable_check_0(self, variable_check_0):
        self.variable_check_0 = variable_check_0
        self._variable_check_0_callback(self.variable_check_0)

    def get_slot(self):
        return self.slot

    def set_slot(self, slot):
        self.slot = slot
        self._slot_callback(self.slot)
        self.beam_exciter_saveRestoreVariables_1.set_slot(self.slot)

    def get_info(self):
        return self.info

    def set_info(self, info):
        self.info = info
        Qt.QMetaObject.invokeMethod(self._info_line_edit, "setText", Qt.Q_ARG("QString", str(self.info)))




def main(top_block_cls=saveRestoreVariables, options=None):

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
