#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Example for burstFileSink
# Author: Philipp Niedermayer
# Copyright: Copyright 2022 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio import analog
from gnuradio import beam_exciter
from gnuradio.beam_exciter import ensure_pmt
import pmt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
import datetime



class burstFileSinkExample(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Example for burstFileSink", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Example for burstFileSink")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "burstFileSinkExample")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 32000
        self.daq_filename = daq_filename = ""+f"/tmp/signal_%H%M%S.{samp_rate/1e3:g}kSps.float32"
        self.burst_button = burst_button = False
        self.burst = burst = 0

        ##################################################
        # Blocks
        ##################################################

        self._daq_filename_tool_bar = Qt.QToolBar(self)
        self._daq_filename_tool_bar.addWidget(Qt.QLabel("Filename" + ": "))
        self._daq_filename_line_edit = Qt.QLineEdit(str(self.daq_filename))
        self._daq_filename_tool_bar.addWidget(self._daq_filename_line_edit)
        self._daq_filename_line_edit.editingFinished.connect(
            lambda: self.set_daq_filename(str(str(self._daq_filename_line_edit.text()))))
        self.top_layout.addWidget(self._daq_filename_tool_bar)
        self._burst_button_choices = {'Pressed': bool(True), 'Released': bool(False)}

        _burst_button_toggle_button = qtgui.ToggleButton(self.set_burst_button, 'Burst', self._burst_button_choices, False, 'burst')
        _burst_button_toggle_button.setColors("default", "default", "default", "default")
        self.burst_button = _burst_button_toggle_button

        self.top_grid_layout.addWidget(_burst_button_toggle_button, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_throttle2_0 = blocks.throttle( gr.sizeof_float*1, samp_rate, True, 0 if "auto" == "auto" else max( int(float(0.1) * samp_rate) if "auto" == "time" else int(0.1), 1) )
        self.blocks_msgpair_to_var_0 = blocks.msg_pair_to_var(self.set_burst)
        self.blocks_burst_tagger_0 = blocks.burst_tagger(gr.sizeof_float)
        self.blocks_burst_tagger_0.set_true_tag('SOB',True)
        self.blocks_burst_tagger_0.set_false_tag('EOB',True)
        self.beam_exciter_burstFileSink_0 = beam_exciter.burstFileSink(gr.sizeof_float, daq_filename, ensure_pmt(pmt.intern("SOB")), ensure_pmt(pmt.intern("EOB")), (int(5*samp_rate)), True)
        self.analog_sig_source_x_0 = analog.sig_source_f(samp_rate, analog.GR_COS_WAVE, 1000, 1, 0, 0)
        self.analog_const_source_x_0_1 = analog.sig_source_s(0, analog.GR_CONST_WAVE, 0, 0, burst)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.burst_button, 'state'), (self.blocks_msgpair_to_var_0, 'inpair'))
        self.connect((self.analog_const_source_x_0_1, 0), (self.blocks_burst_tagger_0, 1))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_throttle2_0, 0))
        self.connect((self.blocks_burst_tagger_0, 0), (self.beam_exciter_burstFileSink_0, 0))
        self.connect((self.blocks_throttle2_0, 0), (self.blocks_burst_tagger_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "burstFileSinkExample")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.beam_exciter_burstFileSink_0.set_length((int(5*self.samp_rate)))
        self.blocks_throttle2_0.set_sample_rate(self.samp_rate)

    def get_daq_filename(self):
        return self.daq_filename

    def set_daq_filename(self, daq_filename):
        self.daq_filename = daq_filename
        Qt.QMetaObject.invokeMethod(self._daq_filename_line_edit, "setText", Qt.Q_ARG("QString", str(self.daq_filename)))
        self.beam_exciter_burstFileSink_0.set_filename(self.daq_filename)

    def get_burst_button(self):
        return self.burst_button

    def set_burst_button(self, burst_button):
        self.burst_button = burst_button

    def get_burst(self):
        return self.burst

    def set_burst(self, burst):
        self.burst = burst
        self.analog_const_source_x_0_1.set_offset(self.burst)




def main(top_block_cls=burstFileSinkExample, options=None):

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
