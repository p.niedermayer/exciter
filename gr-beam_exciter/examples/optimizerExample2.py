#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Optimizer Block Example
# Author: Philipp Niedermayer
# Copyright: Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
from PyQt5.QtCore import QObject, pyqtSlot
from gnuradio import analog
from gnuradio import beam_exciter
import pmt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
import sip



class optimizerExample2(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Optimizer Block Example", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Optimizer Block Example")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "optimizerExample2")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.objective_tol = objective_tol = 0.01
        self.bounds_low = bounds_low = [-5,0]
        self.bounds_high = bounds_high = [5,10]
        self.variables = variables = ['offset', 'amplitude']
        self.samp_rate = samp_rate = 32000*10
        self.pre_avg = pre_avg = 1
        self.options = options = {"bounds":(bounds_low,bounds_high), "user_params":{"model.abs_tol":objective_tol}, "scaling_within_bounds":True}
        self.offset = offset = 2
        self.method = method = 0
        self.enable = enable = False
        self.delay_msg = delay_msg = False
        self.amplitude = amplitude = 1

        ##################################################
        # Blocks
        ##################################################

        self.var = Qt.QTabWidget()
        self.var_widget_0 = Qt.QWidget()
        self.var_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.var_widget_0)
        self.var_grid_layout_0 = Qt.QGridLayout()
        self.var_layout_0.addLayout(self.var_grid_layout_0)
        self.var.addTab(self.var_widget_0, 'Variables')
        self.top_grid_layout.addWidget(self.var, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._variables_tool_bar = Qt.QToolBar(self)
        self._variables_tool_bar.addWidget(Qt.QLabel("Variables to optimize" + ": "))
        self._variables_line_edit = Qt.QLineEdit(str(self.variables))
        self._variables_tool_bar.addWidget(self._variables_line_edit)
        self._variables_line_edit.editingFinished.connect(
            lambda: self.set_variables(eval(str(self._variables_line_edit.text()))))
        self.top_grid_layout.addWidget(self._variables_tool_bar, 1, 0, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._pre_avg_tool_bar = Qt.QToolBar(self)
        self._pre_avg_tool_bar.addWidget(Qt.QLabel("Objective pre_avg" + ": "))
        self._pre_avg_line_edit = Qt.QLineEdit(str(self.pre_avg))
        self._pre_avg_tool_bar.addWidget(self._pre_avg_line_edit)
        self._pre_avg_line_edit.editingFinished.connect(
            lambda: self.set_pre_avg(int(str(self._pre_avg_line_edit.text()))))
        self.top_grid_layout.addWidget(self._pre_avg_tool_bar, 7, 0, 1, 1)
        for r in range(7, 8):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._offset_tool_bar = Qt.QToolBar(self)
        self._offset_tool_bar.addWidget(Qt.QLabel("offset" + ": "))
        self._offset_line_edit = Qt.QLineEdit(str(self.offset))
        self._offset_tool_bar.addWidget(self._offset_line_edit)
        self._offset_line_edit.editingFinished.connect(
            lambda: self.set_offset(eval(str(self._offset_line_edit.text()))))
        self.var_grid_layout_0.addWidget(self._offset_tool_bar, 1, 0, 1, 1)
        for r in range(1, 2):
            self.var_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.var_grid_layout_0.setColumnStretch(c, 1)
        # Create the options list
        self._method_options = [0, 1, 2, 3, 4]
        # Create the labels list
        self._method_labels = ['0 RMS (Root mean square)', '1 STD (Standard deviation)', '2 MEAN (Mean value)', '3 MABS (Mean absolute value)', '4 CV (Coefficient of variation)']
        # Create the combo box
        self._method_tool_bar = Qt.QToolBar(self)
        self._method_tool_bar.addWidget(Qt.QLabel("Objective method" + ": "))
        self._method_combo_box = Qt.QComboBox()
        self._method_tool_bar.addWidget(self._method_combo_box)
        for _label in self._method_labels: self._method_combo_box.addItem(_label)
        self._method_callback = lambda i: Qt.QMetaObject.invokeMethod(self._method_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._method_options.index(i)))
        self._method_callback(self.method)
        self._method_combo_box.currentIndexChanged.connect(
            lambda i: self.set_method(self._method_options[i]))
        # Create the radio buttons
        self.top_grid_layout.addWidget(self._method_tool_bar, 6, 0, 1, 1)
        for r in range(6, 7):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._enable_choices = {'Pressed': bool(True), 'Released': bool(False)}

        _enable_toggle_button = qtgui.ToggleButton(self.set_enable, 'Enable optimizer', self._enable_choices, False, 'value')
        _enable_toggle_button.setColors("default", "default", "yellow", "default")
        self.enable = _enable_toggle_button

        self.top_grid_layout.addWidget(_enable_toggle_button, 10, 0, 1, 1)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        _delay_msg_check_box = Qt.QCheckBox("Delay until EOB")
        self._delay_msg_choices = {True: True, False: False}
        self._delay_msg_choices_inv = dict((v,k) for k,v in self._delay_msg_choices.items())
        self._delay_msg_callback = lambda i: Qt.QMetaObject.invokeMethod(_delay_msg_check_box, "setChecked", Qt.Q_ARG("bool", self._delay_msg_choices_inv[i]))
        self._delay_msg_callback(self.delay_msg)
        _delay_msg_check_box.stateChanged.connect(lambda i: self.set_delay_msg(self._delay_msg_choices[bool(i)]))
        self.top_layout.addWidget(_delay_msg_check_box)
        self._amplitude_tool_bar = Qt.QToolBar(self)
        self._amplitude_tool_bar.addWidget(Qt.QLabel("amplitude" + ": "))
        self._amplitude_line_edit = Qt.QLineEdit(str(self.amplitude))
        self._amplitude_tool_bar.addWidget(self._amplitude_line_edit)
        self._amplitude_line_edit.editingFinished.connect(
            lambda: self.set_amplitude(eval(str(self._amplitude_line_edit.text()))))
        self.var_grid_layout_0.addWidget(self._amplitude_tool_bar, 2, 0, 1, 1)
        for r in range(2, 3):
            self.var_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.var_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_time_sink_x_0 = qtgui.time_sink_f(
            (int(samp_rate*1.2)), #size
            samp_rate, #samp_rate
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_0.set_update_time(0.1)
        self.qtgui_time_sink_x_0.set_y_axis(-5, 5)

        self.qtgui_time_sink_x_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0.enable_tags(True)
        self.qtgui_time_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0.1, 0, "SOB")
        self.qtgui_time_sink_x_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0.enable_grid(False)
        self.qtgui_time_sink_x_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0.enable_stem_plot(False)


        labels = ['Signal 1', 'Signal 2', 'Signal 3', 'Signal 4', 'Signal 5',
            'Signal 6', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'red', 'green', 'black', 'cyan',
            'magenta', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0.qwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_time_sink_x_0_win)
        self.qtgui_edit_box_msg_0 = qtgui.edit_box_msg(qtgui.DOUBLE, '', 'Objective value:', True, True, 'msg', None)
        self._qtgui_edit_box_msg_0_win = sip.wrapinstance(self.qtgui_edit_box_msg_0.qwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_edit_box_msg_0_win, 9, 0, 1, 1)
        for r in range(9, 10):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._objective_tol_tool_bar = Qt.QToolBar(self)
        self._objective_tol_tool_bar.addWidget(Qt.QLabel("Objective tolerance" + ": "))
        self._objective_tol_line_edit = Qt.QLineEdit(str(self.objective_tol))
        self._objective_tol_tool_bar.addWidget(self._objective_tol_line_edit)
        self._objective_tol_line_edit.editingFinished.connect(
            lambda: self.set_objective_tol(eval(str(self._objective_tol_line_edit.text()))))
        self.top_grid_layout.addWidget(self._objective_tol_tool_bar, 8, 0, 1, 1)
        for r in range(8, 9):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._bounds_low_tool_bar = Qt.QToolBar(self)
        self._bounds_low_tool_bar.addWidget(Qt.QLabel("Lower boundaries" + ": "))
        self._bounds_low_line_edit = Qt.QLineEdit(str(self.bounds_low))
        self._bounds_low_tool_bar.addWidget(self._bounds_low_line_edit)
        self._bounds_low_line_edit.editingFinished.connect(
            lambda: self.set_bounds_low(eval(str(self._bounds_low_line_edit.text()))))
        self.top_grid_layout.addWidget(self._bounds_low_tool_bar, 2, 0, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._bounds_high_tool_bar = Qt.QToolBar(self)
        self._bounds_high_tool_bar.addWidget(Qt.QLabel("Upper boundaries" + ": "))
        self._bounds_high_line_edit = Qt.QLineEdit(str(self.bounds_high))
        self._bounds_high_tool_bar.addWidget(self._bounds_high_line_edit)
        self._bounds_high_line_edit.editingFinished.connect(
            lambda: self.set_bounds_high(eval(str(self._bounds_high_line_edit.text()))))
        self.top_grid_layout.addWidget(self._bounds_high_tool_bar, 3, 0, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_throttle2_0 = blocks.throttle( gr.sizeof_float*1, samp_rate, True, 0 if "auto" == "auto" else max( int(float(0.1) * samp_rate) if "auto" == "time" else int(0.1), 1) )
        self.blocks_burst_tagger_0_0 = blocks.burst_tagger(gr.sizeof_float)
        self.blocks_burst_tagger_0_0.set_true_tag('SOB',True)
        self.blocks_burst_tagger_0_0.set_false_tag('EOB',True)
        self.beam_exciter_burstEvaluate_0 = beam_exciter.burstEvaluate(pmt.intern("SOB"), 0, pmt.intern("EOB"), (int(samp_rate*0.3)), method, pre_avg, True, 0, 1, False, delay_msg)
        self.beam_exciter_Optimizer_0 = beam_exciter.Optimizer(self, enable, variables, options, "enable", 'PyBOBYQA', '')
        self.analog_sig_source_x_0_0 = analog.sig_source_s(samp_rate, analog.GR_SQR_WAVE, 1, 1, 0, 0)
        self.analog_sig_source_x_0 = analog.sig_source_f(samp_rate, analog.GR_COS_WAVE, 50, float(amplitude), float(offset), 0)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.beam_exciter_burstEvaluate_0, 'msg'), (self.beam_exciter_Optimizer_0, 'obj'))
        self.msg_connect((self.beam_exciter_burstEvaluate_0, 'msg'), (self.qtgui_edit_box_msg_0, 'val'))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_throttle2_0, 0))
        self.connect((self.analog_sig_source_x_0_0, 0), (self.blocks_burst_tagger_0_0, 1))
        self.connect((self.blocks_burst_tagger_0_0, 0), (self.beam_exciter_burstEvaluate_0, 0))
        self.connect((self.blocks_burst_tagger_0_0, 0), (self.qtgui_time_sink_x_0, 0))
        self.connect((self.blocks_throttle2_0, 0), (self.blocks_burst_tagger_0_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "optimizerExample2")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_objective_tol(self):
        return self.objective_tol

    def set_objective_tol(self, objective_tol):
        self.objective_tol = objective_tol
        Qt.QMetaObject.invokeMethod(self._objective_tol_line_edit, "setText", Qt.Q_ARG("QString", repr(self.objective_tol)))
        self.set_options({"bounds":(self.bounds_low,self.bounds_high), "user_params":{"model.abs_tol":self.objective_tol}, "scaling_within_bounds":True})

    def get_bounds_low(self):
        return self.bounds_low

    def set_bounds_low(self, bounds_low):
        self.bounds_low = bounds_low
        Qt.QMetaObject.invokeMethod(self._bounds_low_line_edit, "setText", Qt.Q_ARG("QString", repr(self.bounds_low)))
        self.set_options({"bounds":(self.bounds_low,self.bounds_high), "user_params":{"model.abs_tol":self.objective_tol}, "scaling_within_bounds":True})

    def get_bounds_high(self):
        return self.bounds_high

    def set_bounds_high(self, bounds_high):
        self.bounds_high = bounds_high
        Qt.QMetaObject.invokeMethod(self._bounds_high_line_edit, "setText", Qt.Q_ARG("QString", repr(self.bounds_high)))
        self.set_options({"bounds":(self.bounds_low,self.bounds_high), "user_params":{"model.abs_tol":self.objective_tol}, "scaling_within_bounds":True})

    def get_variables(self):
        return self.variables

    def set_variables(self, variables):
        self.variables = variables
        Qt.QMetaObject.invokeMethod(self._variables_line_edit, "setText", Qt.Q_ARG("QString", repr(self.variables)))
        self.beam_exciter_Optimizer_0.set_variables(self.variables)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.analog_sig_source_x_0_0.set_sampling_freq(self.samp_rate)
        self.beam_exciter_burstEvaluate_0.set_eval_samp((int(self.samp_rate*0.3)))
        self.blocks_throttle2_0.set_sample_rate(self.samp_rate)
        self.qtgui_time_sink_x_0.set_samp_rate(self.samp_rate)

    def get_pre_avg(self):
        return self.pre_avg

    def set_pre_avg(self, pre_avg):
        self.pre_avg = pre_avg
        Qt.QMetaObject.invokeMethod(self._pre_avg_line_edit, "setText", Qt.Q_ARG("QString", str(self.pre_avg)))
        self.beam_exciter_burstEvaluate_0.set_pre_avg_samp(self.pre_avg)

    def get_options(self):
        return self.options

    def set_options(self, options):
        self.options = options
        self.beam_exciter_Optimizer_0.set_options(self.options)

    def get_offset(self):
        return self.offset

    def set_offset(self, offset):
        self.offset = offset
        Qt.QMetaObject.invokeMethod(self._offset_line_edit, "setText", Qt.Q_ARG("QString", repr(self.offset)))
        self.analog_sig_source_x_0.set_offset(float(self.offset))

    def get_method(self):
        return self.method

    def set_method(self, method):
        self.method = method
        self._method_callback(self.method)
        self.beam_exciter_burstEvaluate_0.set_function(self.method)

    def get_enable(self):
        return self.enable

    def set_enable(self, enable):
        self.enable = enable
        self.beam_exciter_Optimizer_0.set_enable(self.enable)

    def get_delay_msg(self):
        return self.delay_msg

    def set_delay_msg(self, delay_msg):
        self.delay_msg = delay_msg
        self._delay_msg_callback(self.delay_msg)
        self.beam_exciter_burstEvaluate_0.set_delay_msg_eob(self.delay_msg)

    def get_amplitude(self):
        return self.amplitude

    def set_amplitude(self, amplitude):
        self.amplitude = amplitude
        Qt.QMetaObject.invokeMethod(self._amplitude_line_edit, "setText", Qt.Q_ARG("QString", repr(self.amplitude)))
        self.analog_sig_source_x_0.set_amplitude(float(self.amplitude))




def main(top_block_cls=optimizerExample2, options=None):

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
