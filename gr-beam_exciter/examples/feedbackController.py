#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Feedback Controller - Example Flowgraph
# Author: Philipp Niedermayer
# Copyright: Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
from PyQt5.QtCore import QObject, pyqtSlot
from gnuradio import analog
from gnuradio import beam_exciter
from gnuradio import blocks
from gnuradio import filter
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio.qtgui import Range, RangeWidget
from PyQt5 import QtCore
import scipy.signal
import sip
import time
import threading



class feedbackController(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Feedback Controller - Example Flowgraph", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Feedback Controller - Example Flowgraph")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "feedbackController")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.ti_init = ti_init = 0.1
        self.ti = ti = 0.05
        self.td_init = td_init = 0
        self.td = td = 0
        self.target = target = 1
        self.ta = ta = 0.1
        self.samp_rate = samp_rate = 100000
        self.rise_time = rise_time = 0
        self.maxoutbuf = maxoutbuf = 1024
        self.kp_init = kp_init = 0.1
        self.kp = kp = 0.1
        self.init_th = init_th = 0.8
        self.init_enabled = init_enabled = False
        self.ffw_const = ffw_const = 0.1
        self.feedforward = feedforward = 0
        self.feedback = feedback = True
        self.f_low_pass_hz = f_low_pass_hz = 3
        self.disturbance = disturbance = 0.1
        self.controller = controller = 0
        self.burst = burst = 1

        ##################################################
        # Blocks
        ##################################################

        self._target_range = Range(0, 2, 0.01, 1, 200)
        self._target_win = RangeWidget(self._target_range, self.set_target, "target", "counter_slider", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._target_win, 10, 0, 1, 3)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_probe_signal_x_0 = blocks.probe_signal_f()
        self._ti_init_range = Range(-999, 999, 0.01, 0.1, 200)
        self._ti_init_win = RangeWidget(self._ti_init_range, self.set_ti_init, "Ti (init)", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._ti_init_win, 2, 1, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._ti_range = Range(-999, 999, 0.01, 0.05, 200)
        self._ti_win = RangeWidget(self._ti_range, self.set_ti, "Ti", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._ti_win, 2, 0, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._td_init_range = Range(-999, 999, 0.01, 0, 200)
        self._td_init_win = RangeWidget(self._td_init_range, self.set_td_init, "Td (init)", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._td_init_win, 3, 1, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._td_range = Range(-999, 999, 0.01, 0, 200)
        self._td_win = RangeWidget(self._td_range, self.set_td, "Td", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._td_win, 3, 0, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._rise_time_range = Range(0, 100, 0.1, 0, 200)
        self._rise_time_win = RangeWidget(self._rise_time_range, self.set_rise_time, "Rise time [s]", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._rise_time_win, 4, 0, 1, 1)
        for r in range(4, 5):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._kp_init_range = Range(-999, 999, 0.01, 0.1, 200)
        self._kp_init_win = RangeWidget(self._kp_init_range, self.set_kp_init, "Kp (init)", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._kp_init_win, 1, 1, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._kp_range = Range(-999, 999, 0.01, 0.1, 200)
        self._kp_win = RangeWidget(self._kp_range, self.set_kp, "Kp", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._kp_win, 1, 0, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._init_th_range = Range(0, 2, 0.01, 0.8, 200)
        self._init_th_win = RangeWidget(self._init_th_range, self.set_init_th, "Threshold", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._init_th_win, 4, 1, 1, 1)
        for r in range(4, 5):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        _init_enabled_check_box = Qt.QCheckBox("Use init parameter")
        self._init_enabled_choices = {True: True, False: False}
        self._init_enabled_choices_inv = dict((v,k) for k,v in self._init_enabled_choices.items())
        self._init_enabled_callback = lambda i: Qt.QMetaObject.invokeMethod(_init_enabled_check_box, "setChecked", Qt.Q_ARG("bool", self._init_enabled_choices_inv[i]))
        self._init_enabled_callback(self.init_enabled)
        _init_enabled_check_box.stateChanged.connect(lambda i: self.set_init_enabled(self._init_enabled_choices[bool(i)]))
        self.top_grid_layout.addWidget(_init_enabled_check_box, 0, 1, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._ffw_const_range = Range(-999, 999, 0.01, 0.1, 200)
        self._ffw_const_win = RangeWidget(self._ffw_const_range, self.set_ffw_const, "Feedforward constant", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._ffw_const_win, 1, 2, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        # Create the options list
        self._feedforward_options = [0, 1, 2, 3]
        # Create the labels list
        self._feedforward_labels = ['Off', 'Constant', 'Record mem', 'Playback mem']
        # Create the combo box
        self._feedforward_tool_bar = Qt.QToolBar(self)
        self._feedforward_tool_bar.addWidget(Qt.QLabel("Feedforward" + ": "))
        self._feedforward_combo_box = Qt.QComboBox()
        self._feedforward_tool_bar.addWidget(self._feedforward_combo_box)
        for _label in self._feedforward_labels: self._feedforward_combo_box.addItem(_label)
        self._feedforward_callback = lambda i: Qt.QMetaObject.invokeMethod(self._feedforward_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._feedforward_options.index(i)))
        self._feedforward_callback(self.feedforward)
        self._feedforward_combo_box.currentIndexChanged.connect(
            lambda i: self.set_feedforward(self._feedforward_options[i]))
        # Create the radio buttons
        self.top_grid_layout.addWidget(self._feedforward_tool_bar, 0, 2, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        _feedback_check_box = Qt.QCheckBox("Feedback on")
        self._feedback_choices = {True: True, False: False}
        self._feedback_choices_inv = dict((v,k) for k,v in self._feedback_choices.items())
        self._feedback_callback = lambda i: Qt.QMetaObject.invokeMethod(_feedback_check_box, "setChecked", Qt.Q_ARG("bool", self._feedback_choices_inv[i]))
        self._feedback_callback(self.feedback)
        _feedback_check_box.stateChanged.connect(lambda i: self.set_feedback(self._feedback_choices[bool(i)]))
        self.top_grid_layout.addWidget(_feedback_check_box, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._disturbance_range = Range(0, 2, 0.01, 0.1, 200)
        self._disturbance_win = RangeWidget(self._disturbance_range, self.set_disturbance, "disturbance", "counter_slider", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._disturbance_win, 11, 0, 1, 3)
        for r in range(11, 12):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        def _controller_probe():
          while True:

            val = self.blocks_probe_signal_x_0.level()
            try:
              try:
                self.doc.add_next_tick_callback(functools.partial(self.set_controller,val))
              except AttributeError:
                self.set_controller(val)
            except AttributeError:
              pass
            time.sleep(1.0 / ((1/ta)))
        _controller_thread = threading.Thread(target=_controller_probe)
        _controller_thread.daemon = True
        _controller_thread.start()
        _burst_check_box = Qt.QCheckBox("Burst active")
        self._burst_choices = {True: 1, False: 0}
        self._burst_choices_inv = dict((v,k) for k,v in self._burst_choices.items())
        self._burst_callback = lambda i: Qt.QMetaObject.invokeMethod(_burst_check_box, "setChecked", Qt.Q_ARG("bool", self._burst_choices_inv[i]))
        self._burst_callback(self.burst)
        _burst_check_box.stateChanged.connect(lambda i: self.set_burst(self._burst_choices[bool(i)]))
        self.top_grid_layout.addWidget(_burst_check_box, 20, 0, 1, 1)
        for r in range(20, 21):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_number_sink_0 = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_HORIZ,
            3,
            None # parent
        )
        self.qtgui_number_sink_0.set_update_time(0.10)
        self.qtgui_number_sink_0.set_title("")

        labels = ['Target value', 'Actual value', 'Controller output', '', '',
            '', '', '', '', '']
        units = ['', '', '', '', '',
            '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
            ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]

        for i in range(3):
            self.qtgui_number_sink_0.set_min(i, 0)
            self.qtgui_number_sink_0.set_max(i, 2)
            self.qtgui_number_sink_0.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_number_sink_0.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_number_sink_0.set_label(i, labels[i])
            self.qtgui_number_sink_0.set_unit(i, units[i])
            self.qtgui_number_sink_0.set_factor(i, factor[i])

        self.qtgui_number_sink_0.enable_autoscale(False)
        self._qtgui_number_sink_0_win = sip.wrapinstance(self.qtgui_number_sink_0.qwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_number_sink_0_win, 100, 0, 1, 3)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.iir_filter_xxx_0 = filter.iir_filter_ffd(scipy.signal.butter(3, f_low_pass_hz/samp_rate)[0], scipy.signal.butter(3, f_low_pass_hz/samp_rate)[1], False)
        self.iir_filter_xxx_0.set_max_output_buffer(maxoutbuf)
        self.blocks_throttle2_0 = blocks.throttle( gr.sizeof_float*1, samp_rate, True, 0 if "auto" == "auto" else max( int(float(maxoutbuf) * samp_rate) if "auto" == "time" else int(maxoutbuf), 1) )
        self.blocks_throttle2_0.set_max_output_buffer(maxoutbuf)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_ff(0.8)
        self.blocks_multiply_const_vxx_0.set_max_output_buffer(maxoutbuf)
        self.blocks_burst_tagger_0 = blocks.burst_tagger(gr.sizeof_float)
        self.blocks_burst_tagger_0.set_true_tag('SOB',True)
        self.blocks_burst_tagger_0.set_false_tag('EOB',True)
        self.blocks_burst_tagger_0.set_max_output_buffer(maxoutbuf)
        self.blocks_add_xx_0 = blocks.add_vff(1)
        self.blocks_add_xx_0.set_max_output_buffer(maxoutbuf)
        self.beam_exciter_feedbackController_0 = beam_exciter.feedbackController(feedback, feedforward, 0, target, (int(rise_time*samp_rate)), (int(ta*samp_rate)), kp, (0 if ti==0 else (kp/ti*ta)), (kp*td/ta), 0, 2, init_enabled, kp_init, (0 if ti_init==0 else (kp_init/ti_init*ta)), kp_init*td_init/ta, init_th, 10, 5, True, ffw_const, 0, False)
        self.beam_exciter_feedbackController_0.set_max_output_buffer(maxoutbuf)
        self.analog_const_source_x_0_1 = analog.sig_source_s(0, analog.GR_CONST_WAVE, 0, 0, burst)
        self.analog_const_source_x_0_1.set_max_output_buffer(maxoutbuf)
        self.analog_const_source_x_0_0_0 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, controller)
        self.analog_const_source_x_0_0_0.set_max_output_buffer(maxoutbuf)
        self.analog_const_source_x_0_0 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, disturbance)
        self.analog_const_source_x_0_0.set_max_output_buffer(maxoutbuf)
        self.analog_const_source_x_0 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, target)


        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_const_source_x_0, 0), (self.qtgui_number_sink_0, 0))
        self.connect((self.analog_const_source_x_0_0, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.analog_const_source_x_0_0_0, 0), (self.blocks_throttle2_0, 0))
        self.connect((self.analog_const_source_x_0_1, 0), (self.blocks_burst_tagger_0, 1))
        self.connect((self.beam_exciter_feedbackController_0, 0), (self.blocks_probe_signal_x_0, 0))
        self.connect((self.beam_exciter_feedbackController_0, 0), (self.qtgui_number_sink_0, 2))
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.blocks_burst_tagger_0, 0), (self.beam_exciter_feedbackController_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.iir_filter_xxx_0, 0))
        self.connect((self.blocks_throttle2_0, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.iir_filter_xxx_0, 0), (self.blocks_burst_tagger_0, 0))
        self.connect((self.iir_filter_xxx_0, 0), (self.qtgui_number_sink_0, 1))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "feedbackController")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_ti_init(self):
        return self.ti_init

    def set_ti_init(self, ti_init):
        self.ti_init = ti_init
        self.beam_exciter_feedbackController_0.set_ki_times_ta_init((0 if self.ti_init==0 else (self.kp_init/self.ti_init*self.ta)))

    def get_ti(self):
        return self.ti

    def set_ti(self, ti):
        self.ti = ti
        self.beam_exciter_feedbackController_0.set_ki_times_ta((0 if self.ti==0 else (self.kp/self.ti*self.ta)))

    def get_td_init(self):
        return self.td_init

    def set_td_init(self, td_init):
        self.td_init = td_init
        self.beam_exciter_feedbackController_0.set_kd_over_ta_init(self.kp_init*self.td_init/self.ta)

    def get_td(self):
        return self.td

    def set_td(self, td):
        self.td = td
        self.beam_exciter_feedbackController_0.set_kd_over_ta((self.kp*self.td/self.ta))

    def get_target(self):
        return self.target

    def set_target(self, target):
        self.target = target
        self.analog_const_source_x_0.set_offset(self.target)
        self.beam_exciter_feedbackController_0.set_target(self.target)

    def get_ta(self):
        return self.ta

    def set_ta(self, ta):
        self.ta = ta
        self.beam_exciter_feedbackController_0.set_ta_samples((int(self.ta*self.samp_rate)))
        self.beam_exciter_feedbackController_0.set_ki_times_ta((0 if self.ti==0 else (self.kp/self.ti*self.ta)))
        self.beam_exciter_feedbackController_0.set_kd_over_ta((self.kp*self.td/self.ta))
        self.beam_exciter_feedbackController_0.set_ki_times_ta_init((0 if self.ti_init==0 else (self.kp_init/self.ti_init*self.ta)))
        self.beam_exciter_feedbackController_0.set_kd_over_ta_init(self.kp_init*self.td_init/self.ta)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.beam_exciter_feedbackController_0.set_rise_time((int(self.rise_time*self.samp_rate)))
        self.beam_exciter_feedbackController_0.set_ta_samples((int(self.ta*self.samp_rate)))
        self.blocks_throttle2_0.set_sample_rate(self.samp_rate)
        self.iir_filter_xxx_0.set_taps(scipy.signal.butter(3, self.f_low_pass_hz/self.samp_rate)[0], scipy.signal.butter(3, self.f_low_pass_hz/self.samp_rate)[1])

    def get_rise_time(self):
        return self.rise_time

    def set_rise_time(self, rise_time):
        self.rise_time = rise_time
        self.beam_exciter_feedbackController_0.set_rise_time((int(self.rise_time*self.samp_rate)))

    def get_maxoutbuf(self):
        return self.maxoutbuf

    def set_maxoutbuf(self, maxoutbuf):
        self.maxoutbuf = maxoutbuf

    def get_kp_init(self):
        return self.kp_init

    def set_kp_init(self, kp_init):
        self.kp_init = kp_init
        self.beam_exciter_feedbackController_0.set_kp_init(self.kp_init)
        self.beam_exciter_feedbackController_0.set_ki_times_ta_init((0 if self.ti_init==0 else (self.kp_init/self.ti_init*self.ta)))
        self.beam_exciter_feedbackController_0.set_kd_over_ta_init(self.kp_init*self.td_init/self.ta)

    def get_kp(self):
        return self.kp

    def set_kp(self, kp):
        self.kp = kp
        self.beam_exciter_feedbackController_0.set_kp(self.kp)
        self.beam_exciter_feedbackController_0.set_ki_times_ta((0 if self.ti==0 else (self.kp/self.ti*self.ta)))
        self.beam_exciter_feedbackController_0.set_kd_over_ta((self.kp*self.td/self.ta))

    def get_init_th(self):
        return self.init_th

    def set_init_th(self, init_th):
        self.init_th = init_th
        self.beam_exciter_feedbackController_0.set_init_threshold(self.init_th)

    def get_init_enabled(self):
        return self.init_enabled

    def set_init_enabled(self, init_enabled):
        self.init_enabled = init_enabled
        self._init_enabled_callback(self.init_enabled)
        self.beam_exciter_feedbackController_0.set_init(self.init_enabled)

    def get_ffw_const(self):
        return self.ffw_const

    def set_ffw_const(self, ffw_const):
        self.ffw_const = ffw_const
        self.beam_exciter_feedbackController_0.set_feedforward_const(self.ffw_const)

    def get_feedforward(self):
        return self.feedforward

    def set_feedforward(self, feedforward):
        self.feedforward = feedforward
        self._feedforward_callback(self.feedforward)
        self.beam_exciter_feedbackController_0.set_feedforward(self.feedforward)

    def get_feedback(self):
        return self.feedback

    def set_feedback(self, feedback):
        self.feedback = feedback
        self._feedback_callback(self.feedback)
        self.beam_exciter_feedbackController_0.set_feedback(self.feedback)

    def get_f_low_pass_hz(self):
        return self.f_low_pass_hz

    def set_f_low_pass_hz(self, f_low_pass_hz):
        self.f_low_pass_hz = f_low_pass_hz
        self.iir_filter_xxx_0.set_taps(scipy.signal.butter(3, self.f_low_pass_hz/self.samp_rate)[0], scipy.signal.butter(3, self.f_low_pass_hz/self.samp_rate)[1])

    def get_disturbance(self):
        return self.disturbance

    def set_disturbance(self, disturbance):
        self.disturbance = disturbance
        self.analog_const_source_x_0_0.set_offset(self.disturbance)

    def get_controller(self):
        return self.controller

    def set_controller(self, controller):
        self.controller = controller
        self.analog_const_source_x_0_0_0.set_offset(self.controller)

    def get_burst(self):
        return self.burst

    def set_burst(self, burst):
        self.burst = burst
        self._burst_callback(self.burst)
        self.analog_const_source_x_0_1.set_offset(self.burst)




def main(top_block_cls=feedbackController, options=None):

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
