#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Burst Evaluate Block Example
# Author: Philipp Niedermayer
# Copyright: Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
from PyQt5.QtCore import QObject, pyqtSlot
from gnuradio import analog
from gnuradio import beam_exciter
import pmt
from gnuradio import blocks
from gnuradio import blocks, gr
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import sip



class burstEvaluateExample(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Burst Evaluate Block Example", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Burst Evaluate Block Example")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "burstEvaluateExample")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 100000
        self.method = method = 0
        self.delay_msg = delay_msg = True
        self.burst = burst = 0

        ##################################################
        # Blocks
        ##################################################

        # Create the options list
        self._method_options = [0, 1, 2, 3, 4]
        # Create the labels list
        self._method_labels = ['0 RMS', '1 STD', '2 MEAN', '3 MABS', '4 CV']
        # Create the combo box
        self._method_tool_bar = Qt.QToolBar(self)
        self._method_tool_bar.addWidget(Qt.QLabel("Method" + ": "))
        self._method_combo_box = Qt.QComboBox()
        self._method_tool_bar.addWidget(self._method_combo_box)
        for _label in self._method_labels: self._method_combo_box.addItem(_label)
        self._method_callback = lambda i: Qt.QMetaObject.invokeMethod(self._method_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._method_options.index(i)))
        self._method_callback(self.method)
        self._method_combo_box.currentIndexChanged.connect(
            lambda i: self.set_method(self._method_options[i]))
        # Create the radio buttons
        self.top_layout.addWidget(self._method_tool_bar)
        _delay_msg_check_box = Qt.QCheckBox("delay_msg")
        self._delay_msg_choices = {True: True, False: False}
        self._delay_msg_choices_inv = dict((v,k) for k,v in self._delay_msg_choices.items())
        self._delay_msg_callback = lambda i: Qt.QMetaObject.invokeMethod(_delay_msg_check_box, "setChecked", Qt.Q_ARG("bool", self._delay_msg_choices_inv[i]))
        self._delay_msg_callback(self.delay_msg)
        _delay_msg_check_box.stateChanged.connect(lambda i: self.set_delay_msg(self._delay_msg_choices[bool(i)]))
        self.top_layout.addWidget(_delay_msg_check_box)
        _burst_push_button = Qt.QPushButton('Burst')
        _burst_push_button = Qt.QPushButton('Burst')
        self._burst_choices = {'Pressed': 1, 'Released': 0}
        _burst_push_button.pressed.connect(lambda: self.set_burst(self._burst_choices['Pressed']))
        _burst_push_button.released.connect(lambda: self.set_burst(self._burst_choices['Released']))
        self.top_layout.addWidget(_burst_push_button)
        self.qtgui_time_sink_x_0 = qtgui.time_sink_f(
            (4096*8), #size
            samp_rate, #samp_rate
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_0.set_update_time(0.1)
        self.qtgui_time_sink_x_0.set_y_axis(-1, 2)

        self.qtgui_time_sink_x_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0.enable_tags(True)
        self.qtgui_time_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0.001, 0, "SOB")
        self.qtgui_time_sink_x_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0.enable_grid(False)
        self.qtgui_time_sink_x_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0.enable_stem_plot(False)


        labels = ['Signal 1', 'Signal 2', 'Signal 3', 'Signal 4', 'Signal 5',
            'Signal 6', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'red', 'green', 'black', 'cyan',
            'magenta', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0.qwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_time_sink_x_0_win)
        self.blocks_throttle2_0 = blocks.throttle( gr.sizeof_float*1, samp_rate, True, 0 if "auto" == "auto" else max( int(float(0.1) * samp_rate) if "auto" == "time" else int(0.1), 1) )
        self.blocks_message_debug_0 = blocks.message_debug(True, gr.log_levels.info)
        self.blocks_burst_tagger_0_0 = blocks.burst_tagger(gr.sizeof_float)
        self.blocks_burst_tagger_0_0.set_true_tag('SOB',True)
        self.blocks_burst_tagger_0_0.set_false_tag('EOB',True)
        self.beam_exciter_burstEvaluate_0 = beam_exciter.burstEvaluate(pmt.intern("SOB"), 100, pmt.intern("EOB"), 10000, method, 3, True, 20000, 5, False, delay_msg)
        self.analog_sig_source_x_0 = analog.sig_source_f(samp_rate, analog.GR_COS_WAVE, 1000, 1, 0.5, 0)
        self.analog_const_source_x_0 = analog.sig_source_s(0, analog.GR_CONST_WAVE, 0, 0, burst)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.beam_exciter_burstEvaluate_0, 'msg'), (self.blocks_message_debug_0, 'print'))
        self.connect((self.analog_const_source_x_0, 0), (self.blocks_burst_tagger_0_0, 1))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_throttle2_0, 0))
        self.connect((self.blocks_burst_tagger_0_0, 0), (self.beam_exciter_burstEvaluate_0, 0))
        self.connect((self.blocks_burst_tagger_0_0, 0), (self.qtgui_time_sink_x_0, 0))
        self.connect((self.blocks_throttle2_0, 0), (self.blocks_burst_tagger_0_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "burstEvaluateExample")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.blocks_throttle2_0.set_sample_rate(self.samp_rate)
        self.qtgui_time_sink_x_0.set_samp_rate(self.samp_rate)

    def get_method(self):
        return self.method

    def set_method(self, method):
        self.method = method
        self._method_callback(self.method)
        self.beam_exciter_burstEvaluate_0.set_function(self.method)

    def get_delay_msg(self):
        return self.delay_msg

    def set_delay_msg(self, delay_msg):
        self.delay_msg = delay_msg
        self._delay_msg_callback(self.delay_msg)
        self.beam_exciter_burstEvaluate_0.set_delay_msg_eob(self.delay_msg)

    def get_burst(self):
        return self.burst

    def set_burst(self, burst):
        self.burst = burst
        self.analog_const_source_x_0.set_offset(self.burst)




def main(top_block_cls=burstEvaluateExample, options=None):

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
