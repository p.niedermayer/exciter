#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Optimizer Block Example
# Author: Philipp Niedermayer
# Copyright: Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio import beam_exciter
from gnuradio import blocks
import pmt
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx



class optimizerExample(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Optimizer Block Example", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Optimizer Block Example")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "optimizerExample")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.variable_3 = variable_3 = '3'
        self.variable_2 = variable_2 = '6'
        self.variable_1 = variable_1 = '2'
        self.objective_tol = objective_tol = 0.01
        self.objective = objective = (float(variable_1)-1)**2 + (float(variable_2)-2)**2 + (float(variable_3)-3)**2
        self.bounds_low = bounds_low = [0,0]
        self.bounds_high = bounds_high = [5,20]
        self.variables = variables = ['variable_1', "variable_2"]
        self.samp_rate = samp_rate = 32000
        self.options = options = {"bounds":(bounds_low,bounds_high), "user_params":{"model.abs_tol":objective_tol}, "scaling_within_bounds":True}
        self.objective_label = objective_label = objective
        self.enable = enable = False

        ##################################################
        # Blocks
        ##################################################

        self._variables_tool_bar = Qt.QToolBar(self)
        self._variables_tool_bar.addWidget(Qt.QLabel("Variables to adjust" + ": "))
        self._variables_line_edit = Qt.QLineEdit(str(self.variables))
        self._variables_tool_bar.addWidget(self._variables_line_edit)
        self._variables_line_edit.editingFinished.connect(
            lambda: self.set_variables(eval(str(self._variables_line_edit.text()))))
        self.top_grid_layout.addWidget(self._variables_tool_bar, 1, 0, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.var = Qt.QTabWidget()
        self.var_widget_0 = Qt.QWidget()
        self.var_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.var_widget_0)
        self.var_grid_layout_0 = Qt.QGridLayout()
        self.var_layout_0.addLayout(self.var_grid_layout_0)
        self.var.addTab(self.var_widget_0, 'Variables')
        self.top_grid_layout.addWidget(self.var, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._enable_choices = {'Pressed': bool(True), 'Released': bool(False)}

        _enable_toggle_button = qtgui.ToggleButton(self.set_enable, 'Enable optimizer', self._enable_choices, False, 'value')
        _enable_toggle_button.setColors("default", "default", "orange", "default")
        self.enable = _enable_toggle_button

        self.top_grid_layout.addWidget(_enable_toggle_button, 10, 0, 1, 1)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._variable_3_tool_bar = Qt.QToolBar(self)
        self._variable_3_tool_bar.addWidget(Qt.QLabel("'variable_3'" + ": "))
        self._variable_3_line_edit = Qt.QLineEdit(str(self.variable_3))
        self._variable_3_tool_bar.addWidget(self._variable_3_line_edit)
        self._variable_3_line_edit.editingFinished.connect(
            lambda: self.set_variable_3(str(str(self._variable_3_line_edit.text()))))
        self.var_grid_layout_0.addWidget(self._variable_3_tool_bar, 3, 0, 1, 1)
        for r in range(3, 4):
            self.var_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.var_grid_layout_0.setColumnStretch(c, 1)
        self._variable_2_tool_bar = Qt.QToolBar(self)
        self._variable_2_tool_bar.addWidget(Qt.QLabel("'variable_2'" + ": "))
        self._variable_2_line_edit = Qt.QLineEdit(str(self.variable_2))
        self._variable_2_tool_bar.addWidget(self._variable_2_line_edit)
        self._variable_2_line_edit.editingFinished.connect(
            lambda: self.set_variable_2(str(str(self._variable_2_line_edit.text()))))
        self.var_grid_layout_0.addWidget(self._variable_2_tool_bar, 2, 0, 1, 1)
        for r in range(2, 3):
            self.var_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.var_grid_layout_0.setColumnStretch(c, 1)
        self._variable_1_tool_bar = Qt.QToolBar(self)
        self._variable_1_tool_bar.addWidget(Qt.QLabel("'variable_1'" + ": "))
        self._variable_1_line_edit = Qt.QLineEdit(str(self.variable_1))
        self._variable_1_tool_bar.addWidget(self._variable_1_line_edit)
        self._variable_1_line_edit.editingFinished.connect(
            lambda: self.set_variable_1(str(str(self._variable_1_line_edit.text()))))
        self.var_grid_layout_0.addWidget(self._variable_1_tool_bar, 1, 0, 1, 1)
        for r in range(1, 2):
            self.var_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.var_grid_layout_0.setColumnStretch(c, 1)
        self._objective_tol_tool_bar = Qt.QToolBar(self)
        self._objective_tol_tool_bar.addWidget(Qt.QLabel("Objective tolerance" + ": "))
        self._objective_tol_line_edit = Qt.QLineEdit(str(self.objective_tol))
        self._objective_tol_tool_bar.addWidget(self._objective_tol_line_edit)
        self._objective_tol_line_edit.editingFinished.connect(
            lambda: self.set_objective_tol(eval(str(self._objective_tol_line_edit.text()))))
        self.top_grid_layout.addWidget(self._objective_tol_tool_bar, 5, 0, 1, 1)
        for r in range(5, 6):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._objective_label_tool_bar = Qt.QToolBar(self)

        if None:
            self._objective_label_formatter = None
        else:
            self._objective_label_formatter = lambda x: eng_notation.num_to_str(x)

        self._objective_label_tool_bar.addWidget(Qt.QLabel("Objective: "))
        self._objective_label_label = Qt.QLabel(str(self._objective_label_formatter(self.objective_label)))
        self._objective_label_tool_bar.addWidget(self._objective_label_label)
        self.var_grid_layout_0.addWidget(self._objective_label_tool_bar, 10, 0, 1, 1)
        for r in range(10, 11):
            self.var_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.var_grid_layout_0.setColumnStretch(c, 1)
        self._bounds_low_tool_bar = Qt.QToolBar(self)
        self._bounds_low_tool_bar.addWidget(Qt.QLabel("Lower boundaries" + ": "))
        self._bounds_low_line_edit = Qt.QLineEdit(str(self.bounds_low))
        self._bounds_low_tool_bar.addWidget(self._bounds_low_line_edit)
        self._bounds_low_line_edit.editingFinished.connect(
            lambda: self.set_bounds_low(eval(str(self._bounds_low_line_edit.text()))))
        self.top_grid_layout.addWidget(self._bounds_low_tool_bar, 2, 0, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._bounds_high_tool_bar = Qt.QToolBar(self)
        self._bounds_high_tool_bar.addWidget(Qt.QLabel("Upper boundaries" + ": "))
        self._bounds_high_line_edit = Qt.QLineEdit(str(self.bounds_high))
        self._bounds_high_tool_bar.addWidget(self._bounds_high_line_edit)
        self._bounds_high_line_edit.editingFinished.connect(
            lambda: self.set_bounds_high(eval(str(self._bounds_high_line_edit.text()))))
        self.top_grid_layout.addWidget(self._bounds_high_tool_bar, 3, 0, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_var_to_msg_0 = blocks.var_to_msg_pair('objective')
        self.blocks_message_strobe_0 = blocks.message_strobe(pmt.to_pmt(-1.0), 300)
        self.beam_exciter_Optimizer_0 = beam_exciter.Optimizer(self, enable, variables, options, "enable", 'PyBOBYQA', '../../data/%y%m%d/%H%M%S_example.log')


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_message_strobe_0, 'strobe'), (self.beam_exciter_Optimizer_0, 'obj'))
        self.msg_connect((self.blocks_var_to_msg_0, 'msgout'), (self.blocks_message_strobe_0, 'set_msg'))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "optimizerExample")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_variable_3(self):
        return self.variable_3

    def set_variable_3(self, variable_3):
        self.variable_3 = variable_3
        self.set_objective((float(self.variable_1)-1)**2 + (float(self.variable_2)-2)**2 + (float(self.variable_3)-3)**2)
        Qt.QMetaObject.invokeMethod(self._variable_3_line_edit, "setText", Qt.Q_ARG("QString", str(self.variable_3)))

    def get_variable_2(self):
        return self.variable_2

    def set_variable_2(self, variable_2):
        self.variable_2 = variable_2
        self.set_objective((float(self.variable_1)-1)**2 + (float(self.variable_2)-2)**2 + (float(self.variable_3)-3)**2)
        Qt.QMetaObject.invokeMethod(self._variable_2_line_edit, "setText", Qt.Q_ARG("QString", str(self.variable_2)))

    def get_variable_1(self):
        return self.variable_1

    def set_variable_1(self, variable_1):
        self.variable_1 = variable_1
        self.set_objective((float(self.variable_1)-1)**2 + (float(self.variable_2)-2)**2 + (float(self.variable_3)-3)**2)
        Qt.QMetaObject.invokeMethod(self._variable_1_line_edit, "setText", Qt.Q_ARG("QString", str(self.variable_1)))

    def get_objective_tol(self):
        return self.objective_tol

    def set_objective_tol(self, objective_tol):
        self.objective_tol = objective_tol
        Qt.QMetaObject.invokeMethod(self._objective_tol_line_edit, "setText", Qt.Q_ARG("QString", repr(self.objective_tol)))
        self.set_options({"bounds":(self.bounds_low,self.bounds_high), "user_params":{"model.abs_tol":self.objective_tol}, "scaling_within_bounds":True})

    def get_objective(self):
        return self.objective

    def set_objective(self, objective):
        self.objective = objective
        self.set_objective_label(self.objective)
        self.blocks_var_to_msg_0.variable_changed(self.objective)

    def get_bounds_low(self):
        return self.bounds_low

    def set_bounds_low(self, bounds_low):
        self.bounds_low = bounds_low
        Qt.QMetaObject.invokeMethod(self._bounds_low_line_edit, "setText", Qt.Q_ARG("QString", repr(self.bounds_low)))
        self.set_options({"bounds":(self.bounds_low,self.bounds_high), "user_params":{"model.abs_tol":self.objective_tol}, "scaling_within_bounds":True})

    def get_bounds_high(self):
        return self.bounds_high

    def set_bounds_high(self, bounds_high):
        self.bounds_high = bounds_high
        Qt.QMetaObject.invokeMethod(self._bounds_high_line_edit, "setText", Qt.Q_ARG("QString", repr(self.bounds_high)))
        self.set_options({"bounds":(self.bounds_low,self.bounds_high), "user_params":{"model.abs_tol":self.objective_tol}, "scaling_within_bounds":True})

    def get_variables(self):
        return self.variables

    def set_variables(self, variables):
        self.variables = variables
        Qt.QMetaObject.invokeMethod(self._variables_line_edit, "setText", Qt.Q_ARG("QString", repr(self.variables)))
        self.beam_exciter_Optimizer_0.set_variables(self.variables)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_options(self):
        return self.options

    def set_options(self, options):
        self.options = options
        self.beam_exciter_Optimizer_0.set_options(self.options)

    def get_objective_label(self):
        return self.objective_label

    def set_objective_label(self, objective_label):
        self.objective_label = objective_label
        Qt.QMetaObject.invokeMethod(self._objective_label_label, "setText", Qt.Q_ARG("QString", str(self._objective_label_formatter(self.objective_label))))

    def get_enable(self):
        return self.enable

    def set_enable(self, enable):
        self.enable = enable
        self.beam_exciter_Optimizer_0.set_enable(self.enable)




def main(top_block_cls=optimizerExample, options=None):

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
