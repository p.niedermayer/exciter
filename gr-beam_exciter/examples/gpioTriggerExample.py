#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: GPIO Trigger Block Example
# Author: Philipp Niedermayer
# Copyright: Philipp Niedermayer
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio import analog
from gnuradio import beam_exciter
from gnuradio.beam_exciter import ensure_pmt
from gnuradio import uhd
import pmt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio.qtgui import Range, RangeWidget
from PyQt5 import QtCore
import sip



class gpioTriggerExample(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "GPIO Trigger Block Example", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("GPIO Trigger Block Example")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "gpioTriggerExample")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.trg_count = trg_count = 0
        self.output_duration = output_duration = 6

        self.rfnoc_graph = uhd_rfnoc_graph = uhd.rfnoc_graph(uhd.device_addr(""))
        self.trigger_gate_mode_label = trigger_gate_mode_label = "Gated mode" if output_duration == 0 else ""
        self.samp_rate = samp_rate = 1e5
        self.qtgui_label_trg_count = qtgui_label_trg_count = trg_count
        self.manual_trigger = manual_trigger = 0
        self.fs_gpio = fs_gpio = 1000
        self.enable_ext_trg = enable_ext_trg = False
        self.active = active = 0

        ##################################################
        # Blocks
        ##################################################

        self._output_duration_range = Range(0, int((2**31-1)/samp_rate), 0.01, 6, 200)
        self._output_duration_win = RangeWidget(self._output_duration_range, self.set_output_duration, "Burst duration [s]", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._output_duration_win, 1, 0, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._manual_trigger_choices = {'Pressed': 1, 'Released': 0}

        _manual_trigger_toggle_button = qtgui.ToggleButton(self.set_manual_trigger, 'Manual trigger', self._manual_trigger_choices, False, 'value')
        _manual_trigger_toggle_button.setColors("default", "default", "default", "default")
        self.manual_trigger = _manual_trigger_toggle_button

        self.top_grid_layout.addWidget(_manual_trigger_toggle_button, 0, 2, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        _enable_ext_trg_check_box = Qt.QCheckBox("Enable external trigger")
        self._enable_ext_trg_choices = {True: True, False: False}
        self._enable_ext_trg_choices_inv = dict((v,k) for k,v in self._enable_ext_trg_choices.items())
        self._enable_ext_trg_callback = lambda i: Qt.QMetaObject.invokeMethod(_enable_ext_trg_check_box, "setChecked", Qt.Q_ARG("bool", self._enable_ext_trg_choices_inv[i]))
        self._enable_ext_trg_callback(self.enable_ext_trg)
        _enable_ext_trg_check_box.stateChanged.connect(lambda i: self.set_enable_ext_trg(self._enable_ext_trg_choices[bool(i)]))
        self.top_grid_layout.addWidget(_enable_ext_trg_check_box, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._trigger_gate_mode_label_tool_bar = Qt.QToolBar(self)

        if None:
            self._trigger_gate_mode_label_formatter = None
        else:
            self._trigger_gate_mode_label_formatter = lambda x: str(x)

        self._trigger_gate_mode_label_tool_bar.addWidget(Qt.QLabel(" "))
        self._trigger_gate_mode_label_label = Qt.QLabel(str(self._trigger_gate_mode_label_formatter(self.trigger_gate_mode_label)))
        self._trigger_gate_mode_label_tool_bar.addWidget(self._trigger_gate_mode_label_label)
        self.top_grid_layout.addWidget(self._trigger_gate_mode_label_tool_bar, 1, 1, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_time_sink_x_0 = qtgui.time_sink_f(
            8192, #size
            samp_rate, #samp_rate
            "", #name
            2, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0.enable_tags(True)
        self.qtgui_time_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0.001, 1, "SOB")
        self.qtgui_time_sink_x_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0.enable_grid(False)
        self.qtgui_time_sink_x_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0.enable_stem_plot(False)


        labels = ['Signal 1', 'Signal 2', 'Signal 3', 'Signal 4', 'Signal 5',
            'Signal 6', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'red', 'green', 'black', 'cyan',
            'magenta', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(2):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0.qwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_win, 50, 0, 1, 3)
        for r in range(50, 51):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_ledindicator_0_0_0_0 = self._qtgui_ledindicator_0_0_0_0_win = qtgui.GrLEDIndicator('Burst active', "lime", "black", active, 35, 1, 1, 1, self)
        self.qtgui_ledindicator_0_0_0_0 = self._qtgui_ledindicator_0_0_0_0_win
        self.top_grid_layout.addWidget(self._qtgui_ledindicator_0_0_0_0_win, 10, 0, 1, 1)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._qtgui_label_trg_count_tool_bar = Qt.QToolBar(self)

        if None:
            self._qtgui_label_trg_count_formatter = None
        else:
            self._qtgui_label_trg_count_formatter = lambda x: str(x)

        self._qtgui_label_trg_count_tool_bar.addWidget(Qt.QLabel("Trigger count: "))
        self._qtgui_label_trg_count_label = Qt.QLabel(str(self._qtgui_label_trg_count_formatter(self.qtgui_label_trg_count)))
        self._qtgui_label_trg_count_tool_bar.addWidget(self._qtgui_label_trg_count_label)
        self.top_grid_layout.addWidget(self._qtgui_label_trg_count_tool_bar, 0, 1, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_throttle2_0 = blocks.throttle( gr.sizeof_float*1, samp_rate, True, 0 if "auto" == "auto" else max( int(float(0.1) * samp_rate) if "auto" == "time" else int(0.1), 1) )
        self.blocks_tag_debug_0 = blocks.tag_debug(gr.sizeof_float*1, '', "")
        self.blocks_tag_debug_0.set_display(True)
        self.blocks_msgpair_to_var_0_1 = blocks.msg_pair_to_var(self.set_active)
        self.blocks_msgpair_to_var_0_0 = blocks.msg_pair_to_var(self.set_trg_count)
        self.beam_exciter_gpioTrigger_0 = beam_exciter.gpioTrigger(
            itemsize=gr.sizeof_float,
            enable_polling=enable_ext_trg,
            poll_every_samples=(int(samp_rate / fs_gpio)),
            duration_samples=(int(output_duration * samp_rate)),
            tag_key_start=ensure_pmt(pmt.intern("SOB")),
            tag_key_stop=ensure_pmt(pmt.intern("EOB")),
            manual_trigger=manual_trigger,
            ptr_graph=getattr(self, "rfnoc_graph", None),
            device_addr='addr=192.168.10.2',
            device_index=0,
            radio_index=0,
            gpio_bank='RXA',
            gpio_pin=0,
            inverted=False,
        )
        self.beam_exciter_gpioTrigger_0.set_max_output_buffer((int(samp_rate / fs_gpio)))
        self.analog_sig_source_x_0 = analog.sig_source_f(samp_rate, analog.GR_COS_WAVE, 100, 0.5, 0, 0)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.beam_exciter_gpioTrigger_0, 'count'), (self.blocks_msgpair_to_var_0_0, 'inpair'))
        self.msg_connect((self.beam_exciter_gpioTrigger_0, 'active'), (self.blocks_msgpair_to_var_0_1, 'inpair'))
        self.connect((self.analog_sig_source_x_0, 0), (self.beam_exciter_gpioTrigger_0, 0))
        self.connect((self.analog_sig_source_x_0, 0), (self.qtgui_time_sink_x_0, 0))
        self.connect((self.beam_exciter_gpioTrigger_0, 0), (self.blocks_throttle2_0, 0))
        self.connect((self.blocks_throttle2_0, 0), (self.blocks_tag_debug_0, 0))
        self.connect((self.blocks_throttle2_0, 0), (self.qtgui_time_sink_x_0, 1))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "gpioTriggerExample")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_trg_count(self):
        return self.trg_count

    def set_trg_count(self, trg_count):
        self.trg_count = trg_count
        self.set_qtgui_label_trg_count(self.trg_count)

    def get_output_duration(self):
        return self.output_duration

    def set_output_duration(self, output_duration):
        self.output_duration = output_duration
        self.set_trigger_gate_mode_label("Gated mode" if self.output_duration == 0 else "")
        self.beam_exciter_gpioTrigger_0.set_duration_samples((int(self.output_duration * self.samp_rate)))

    def get_uhd_rfnoc_graph(self):
        return self.uhd_rfnoc_graph

    def set_uhd_rfnoc_graph(self, uhd_rfnoc_graph):
        self.uhd_rfnoc_graph = uhd_rfnoc_graph

    def get_trigger_gate_mode_label(self):
        return self.trigger_gate_mode_label

    def set_trigger_gate_mode_label(self, trigger_gate_mode_label):
        self.trigger_gate_mode_label = trigger_gate_mode_label
        Qt.QMetaObject.invokeMethod(self._trigger_gate_mode_label_label, "setText", Qt.Q_ARG("QString", str(self._trigger_gate_mode_label_formatter(self.trigger_gate_mode_label))))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.beam_exciter_gpioTrigger_0.set_duration_samples((int(self.output_duration * self.samp_rate)))
        self.blocks_throttle2_0.set_sample_rate(self.samp_rate)
        self.qtgui_time_sink_x_0.set_samp_rate(self.samp_rate)

    def get_qtgui_label_trg_count(self):
        return self.qtgui_label_trg_count

    def set_qtgui_label_trg_count(self, qtgui_label_trg_count):
        self.qtgui_label_trg_count = qtgui_label_trg_count
        Qt.QMetaObject.invokeMethod(self._qtgui_label_trg_count_label, "setText", Qt.Q_ARG("QString", str(self._qtgui_label_trg_count_formatter(self.qtgui_label_trg_count))))

    def get_manual_trigger(self):
        return self.manual_trigger

    def set_manual_trigger(self, manual_trigger):
        self.manual_trigger = manual_trigger
        self.beam_exciter_gpioTrigger_0.set_manual_trigger(self.manual_trigger)

    def get_fs_gpio(self):
        return self.fs_gpio

    def set_fs_gpio(self, fs_gpio):
        self.fs_gpio = fs_gpio

    def get_enable_ext_trg(self):
        return self.enable_ext_trg

    def set_enable_ext_trg(self, enable_ext_trg):
        self.enable_ext_trg = enable_ext_trg
        self._enable_ext_trg_callback(self.enable_ext_trg)
        self.beam_exciter_gpioTrigger_0.set_enable_polling(self.enable_ext_trg)

    def get_active(self):
        return self.active

    def set_active(self, active):
        self.active = active
        self.qtgui_ledindicator_0_0_0_0.setState(self.active)




def main(top_block_cls=gpioTriggerExample, options=None):

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
