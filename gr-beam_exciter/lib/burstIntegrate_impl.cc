/* -*- c++ -*- */
/*
 * Copyright 2024 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "burstIntegrate_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace beam_exciter {

burstIntegrate::sptr burstIntegrate::make(pmt::pmt_t tag_key_reset, double initial_value, double gain){
    return gnuradio::make_block_sptr<burstIntegrate_impl>(
        tag_key_reset, initial_value, gain);
}


/*
 * The private constructor
 */
burstIntegrate_impl::burstIntegrate_impl(
    pmt::pmt_t tag_key_reset, double initial_value, double gain
) : gr::sync_block(
    "burstIntegrate",
    gr::io_signature::make(1 /* min inputs */, 1 /* max inputs */, sizeof(float)),
    gr::io_signature::make(1 /* min outputs */, 1 /*max outputs */, sizeof(float))
    )
{
    // settings
    _tag_key_reset = tag_key_reset;
    _initial_value = initial_value;
    _gain = gain;
    
    // internals
    _input_tags.reserve(1);
    _sum = _initial_value / _gain;
    
}

/*
 * Our virtual destructor.
 */
burstIntegrate_impl::~burstIntegrate_impl() {}

int burstIntegrate_impl::work(
    int noutput_items,
    gr_vector_const_void_star& input_items,
    gr_vector_void_star& output_items
){
    
    int n = noutput_items;
    
    
    // ======================
    // check for tags
    // ======================
    
    bool reset = false;
    
    get_tags_in_window(_input_tags, 0, 0, n, _tag_key_reset);
    if (!_input_tags.empty()){
        reset = true;
        n = _input_tags.front().offset - nitems_read(0) + 1; // consume only until tag (including)
    }
    
    
    // =======================
    // input / output handling
    // =======================
    
    auto in = static_cast<const float*>(input_items[0]);
    auto out = static_cast<float*>(output_items[0]);
    
    for (int i = 0; i < n; i++) {
        _sum += in[i];
        out[i] = _sum * _gain;
    }
    
    if (reset){
        _sum = _initial_value / _gain;
    }

    // Tell runtime system how many output items we produced.
    return n;
}

} /* namespace beam_exciter */
} /* namespace gr */
