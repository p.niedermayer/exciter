/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_RAMPLINEAR_IMPL_H
#define INCLUDED_BEAM_EXCITER_RAMPLINEAR_IMPL_H

#include <gnuradio/beam_exciter/rampLinear.h>

namespace gr {
namespace beam_exciter {

class rampLinear_impl : public rampLinear
{
private:
    float d_start;
    float d_stop;
    int d_duration_samples;
    int d_delay_samples;
    
    pmt::pmt_t _tag_value;
    pmt::pmt_t _tag_key;
    bool _keep_last_value;
    std::vector<tag_t> _input_tags;
    std::vector<int> _sob_pos;
    
	bool _reset_message_pending = false;
	uint64_t _offset_start = 0; // absolute sample offset of ramp start (after delay)
    uint64_t _offset_stop = 0; // absolute sample offset of ramp end
    double _value = 0; // current value
    double _value_increment = 0; // value increment per sample
    

public:
      rampLinear_impl(float start, float stop, int duration_samples, int delay_samples, pmt::pmt_t tag_value, pmt::pmt_t tag_key, bool keep_last_value);
      ~rampLinear_impl();
            
      void set_start(float start) override { d_start = start; }
      void set_stop(float stop) override { d_stop = stop; }
      void set_duration_samples(int duration_samples) override { d_duration_samples = duration_samples; }
      void set_delay_samples(int delay_samples) override { d_delay_samples = delay_samples; }

      // Where all the action really happens
      int work(int noutput_items,
               gr_vector_const_void_star& input_items,
               gr_vector_void_star& output_items);
      
	  void fill_ramp(float out[], int count, uint64_t offset);
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_RAMPLINEAR_IMPL_H */
