/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "burstCopy_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace beam_exciter {

burstCopy::sptr burstCopy::make(size_t itemsize, pmt::pmt_t tag_key_start, pmt::pmt_t tag_key_stop){
    return gnuradio::make_block_sptr<burstCopy_impl>(
        itemsize, tag_key_start, tag_key_stop);
}


/*
 * The private constructor
 */
burstCopy_impl::burstCopy_impl(
    size_t itemsize, pmt::pmt_t tag_key_start, pmt::pmt_t tag_key_stop
) : gr::block(
        "burstCopy",
        gr::io_signature::make(1 /* min inputs */, 1 /* max inputs */, itemsize),
        gr::io_signature::make(1 /* min outputs */, 1 /*max outputs */, itemsize)
    )
{
    // settings
    _itemsize = itemsize;
    _tag_key_start = tag_key_start;
    _tag_key_stop = tag_key_stop;
    
	// internals
	_input_tags.reserve(1);
	_copying = false;
    
    
    set_tag_propagation_policy(TPP_CUSTOM); // we handle tags on our own
    
}

/*
 * Our virtual destructor.
 */
burstCopy_impl::~burstCopy_impl() {}

/*
 * Forcast
 */
void burstCopy_impl::forecast(int noutput_items, gr_vector_int& ninput_items_required){
    if (_copying){
        ninput_items_required[0] = noutput_items;
    } else {
        ninput_items_required[0] = noutput_items; // as much as we can possibly get
    }
}

/*
 * Work
 */
int burstCopy_impl::general_work(
    int noutput_items,
    gr_vector_int& ninput_items,
    gr_vector_const_void_star& input_items,
    gr_vector_void_star& output_items
){
    
    int n = ninput_items[0];
    
	
	// ======================
	// check for tags
	// ======================
    
    bool next_copying = _copying;
    
    // check for tags
	pmt::pmt_t key = _copying ? _tag_key_stop : _tag_key_start;
    get_tags_in_window(_input_tags, 0, 0, n, key);
    if (!_input_tags.empty()){
        next_copying = !_copying; // toggle switch
        n = _input_tags.front().offset - nitems_read(0); // consume only until tag
        if (_copying) n += 1; // include EOB sample
    }
    
    
	// =======================
	// input / output handling
	// =======================
    
	const uint8_t* i0ptr = (const uint8_t*) input_items[0];
	uint8_t* optr = (uint8_t*) output_items[0];
    
    if (_copying){
        // copy intput to output
        std::memcpy(optr, i0ptr, n * _itemsize);
        
        // propagate tags from input 0
        std::vector<tag_t> tags;
        get_tags_in_window(tags, 0, 0, n);
        for (auto& tag : tags) {
            tag.offset += nitems_written(0) - nitems_read(0); // correct offset
            add_item_tag(0, tag);
        }
        
        consume(0, n);
        produce(0, n);
        
    } else {
        // drop input
        consume(0, n);
        produce(0, 0);
    }
    
    
    _copying = next_copying;
    
    
    return WORK_CALLED_PRODUCE;
    
}

} /* namespace beam_exciter */
} /* namespace gr */
