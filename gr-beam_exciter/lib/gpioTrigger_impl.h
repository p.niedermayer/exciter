/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_GPIOTRIGGER_IMPL_H
#define INCLUDED_BEAM_EXCITER_GPIOTRIGGER_IMPL_H

#include <gnuradio/beam_exciter/gpioTrigger.h>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/rfnoc_graph.hpp>
#include <uhd/rfnoc/radio_control.hpp>
#include <gnuradio/uhd/api.h>
#include <gnuradio/uhd/usrp_block.h>
#include <gnuradio/uhd/rfnoc_graph.h>
#include <stdexcept>

using namespace uhd;

namespace gr {
namespace beam_exciter {

class gpioTrigger_impl : public gpioTrigger
{
private:
    // settings
    size_t _itemsize;
    bool d_enable_polling;
    int _poll_every_samples;
    int d_duration_samples;
    int d_manual_trigger;    
    std::shared_ptr<rfnoc::radio_control> _radio;
    usrp::multi_usrp::sptr _usrp;
    std::string _device_addr;
    int _device_index;
    int _radio_index;
    std::string _gpio_bank;
    int _gpio_pin;
    bool _inverted;
    pmt::pmt_t _tag_key_start;
    pmt::pmt_t _tag_key_stop;
    
    // internals
    int _counter_until_poll;
    int _counter_active;
    bool _input_received_tag;
    bool _trigger_level;
    int _trigger_counter;
    std::vector<tag_t> _input_tags;
    
    // ports
    const pmt::pmt_t _port_count;
    const pmt::pmt_t _port_active;
    
    

public:
    gpioTrigger_impl(
        size_t itemsize,
        bool enable_polling,
        int poll_every_samples,
        int duration_samples,
        pmt::pmt_t tag_key_start,
        pmt::pmt_t tag_key_stop,
        int manual_trigger,
        ::gr::uhd::rfnoc_graph::sptr ptr_graph,
        std::string device_addr,
        int device_index,
        int radio_index,
        std::string gpio_bank,
        int gpio_pin,
        bool inverted
    );
    ~gpioTrigger_impl();
    
    void set_enable_polling(bool enable_polling) override { d_enable_polling = enable_polling; }
    void set_duration_samples(int duration_samples) override { d_duration_samples = duration_samples; }
    void set_manual_trigger(int manual_trigger) override { d_manual_trigger = manual_trigger; }
    
    // Where all the action really happens
    int work(int noutput_items,
           gr_vector_const_void_star& input_items,
           gr_vector_void_star& output_items);
    
    bool poll_gpio();
    void start_burst();
    void end_burst();

};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_GPIOTRIGGER_IMPL_H */
