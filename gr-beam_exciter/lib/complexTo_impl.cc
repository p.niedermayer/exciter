/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "complexTo_impl.h"
#include <gnuradio/io_signature.h>
#include <volk/volk.h>

namespace gr {
namespace beam_exciter {

    using input_type = gr_complex;
    using output_type = float;
    
    complexTo::sptr
    complexTo::make(bool toreal){
      return gnuradio::make_block_sptr<complexTo_impl>(toreal);
    }


    /*
     * The private constructor
     */
    complexTo_impl::complexTo_impl(bool toreal)
      : gr::sync_block("complexTo",
              gr::io_signature::make(1 /* min inputs */, 1 /*max inputs */, sizeof(input_type)),
              gr::io_signature::make(1 /* min outputs */, 1 /*max outputs */, sizeof(output_type)))
    {
        set_toreal(toreal);
        
        set_tag_propagation_policy(TPP_ALL_TO_ALL);
    }

    complexTo_impl::~complexTo_impl() {}

    int complexTo_impl::work(int noutput_items,
                             gr_vector_const_void_star& input_items,
                             gr_vector_void_star& output_items
    ){
        auto in = static_cast<const input_type*>(input_items[0]);
        auto out = static_cast<output_type*>(output_items[0]);
        int n = noutput_items;
        
        //const gr_complex* in = (const gr_complex*)input_items[0];
        //float* out = (float*)output_items[0];
        
        if (d_toreal) {
            volk_32fc_deinterleave_real_32f(out, in, n);
        } else {
            volk_32fc_deinterleave_imag_32f(out, in, n);
        }

        return n;
    }

    } /* namespace beam_exciter */
    } /* namespace gr */
