/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "burstFileSink_impl.h"
#include <gnuradio/io_signature.h>
#include <ctime>
#include <iomanip>
#include <sstream> 
#include <filesystem>

namespace fs = std::filesystem;

namespace gr {
namespace beam_exciter {

burstFileSink::sptr
burstFileSink::make(size_t itemsize, const char* filename, pmt::pmt_t tag_key_start, pmt::pmt_t tag_key_stop, int length, bool enable)
{
    return gnuradio::make_block_sptr<burstFileSink_impl>(itemsize, filename, tag_key_start, tag_key_stop, length, enable);
}


/*
 * The private constructor
 */
burstFileSink_impl::burstFileSink_impl(size_t itemsize,
                                       const char* filename,
                                       pmt::pmt_t tag_key_start,
                                       pmt::pmt_t tag_key_stop,
                                       int length,
                                       bool enable)
    : gr::sync_block("burstFileSink",
                     gr::io_signature::make(
                         1 /* min inputs */, 1 /* max inputs */, itemsize),
                     gr::io_signature::make(0, 0, 0))
      ,gr::blocks::file_sink_base("/dev/null", /* binary */ true, /* append */ false)
	  , _port_write(pmt::mp("write"))
					 
	{
		
        _itemsize = itemsize;
		_tag_key_start = tag_key_start;
		_tag_key_stop = tag_key_stop;
		set_filename(filename);
		set_length(length);
		set_enable(enable);
		
		message_port_register_out(_port_write);
	
	}

/*
 * Our virtual destructor.
 */
burstFileSink_impl::~burstFileSink_impl() {}

int burstFileSink_impl::work(int noutput_items,
                             gr_vector_const_void_star& input_items,
                             gr_vector_void_star& output_items)
{
    if (!d_enable){
        if (_nsamples_to_write != 0 && d_fp){
            // make sure to close incomplete file
            _nsamples_to_write = 0;
            fflush(d_fp);
            close();
            do_update();
            message_port_pub(_port_write, pmt::cons(_port_write, pmt::PMT_F)); // writing off
            std::cout << "[FileSink] Saving aborted" << std::endl;
        }
        return noutput_items; // drop input
    }
    
    const char* inbuf = static_cast<const char*>(input_items[0]);
    int nconsumed = 0;
    
    // check for start tags
    get_tags_in_window(_input_tags, 0, 0, noutput_items, _tag_key_start);
    if (d_enable && !_input_tags.empty() && pmt::is_true(_input_tags.back().value)){
        // tag received, advance until last start tag
        int seek = _input_tags.back().offset - nitems_read(0);
        inbuf += seek * _itemsize;
        nconsumed += seek;
        
        // build filename (handle optional time placeholders)
        std::time_t t = std::time(nullptr);
        std::tm tm = *std::localtime(&t);
        std::stringstream strbuffer;
        strbuffer << std::put_time(&tm, d_filename.c_str());
        std::string filename = strbuffer.str();
        
        // create directories as necessary        
        fs::path path = fs::current_path();
        path /= filename;
        std::filesystem::create_directories(path.parent_path());
        
        // open file and start writing (will close previous file if any)
        open(filename.c_str());
        do_update();
        _nsamples_to_write = d_length;
        
        message_port_pub(_port_write, pmt::cons(_port_write, pmt::PMT_T)); // writing on
        
        if (d_fp)
            std::cout << "[FileSink] Saving to file " << filename << std::endl; // << " (up to " << _nsamples_to_write << " samples)"
        else
            std::cout << "[FileSink] Failed to open file " << filename << std::endl;
    }
	
    if (_nsamples_to_write == 0 || !d_fp){
        // nothing to do or failed to open file
        _nsamples_to_write = 0;
        return noutput_items; // drop input
    }
    
    // check for stop tags
    get_tags_in_window(_input_tags, 0, nconsumed, noutput_items-nconsumed, _tag_key_stop);
    if (!_input_tags.empty() && pmt::is_true(_input_tags.back().value)){
        // tag received, reduce _nsamples_to_write
        int stop = _input_tags.back().offset - nitems_read(0);
        if (_nsamples_to_write == -1 || _nsamples_to_write > stop - nconsumed)
            _nsamples_to_write = stop - nconsumed;
    }
    
    // write data to file
    while (nconsumed < noutput_items && (_nsamples_to_write > 0 || _nsamples_to_write == -1)) {
        int n_write = noutput_items - nconsumed;
        if (n_write > _nsamples_to_write && _nsamples_to_write > 0)
            n_write = _nsamples_to_write;
        const int n_written = fwrite(inbuf, _itemsize, n_write, d_fp);
        nconsumed += n_written;
        if (_nsamples_to_write > 0) _nsamples_to_write -= n_written;
        inbuf += n_written * _itemsize; // seek
        
        // error handling
        if (n_write > 0 && n_written == 0) {
            if (ferror(d_fp)) {
                std::stringstream s;
                s << "[FileSink] Write failed with error " << fileno(d_fp) << std::endl;
                throw std::runtime_error(s.str());
            } else { // is EOF
                break;
            }
        }
    }
    
    if (d_unbuffered){
        fflush(d_fp);
    }
    
    // check if we are done
    if (_nsamples_to_write == 0){
        fflush(d_fp);
        close();
        do_update();
        
        message_port_pub(_port_write, pmt::cons(_port_write, pmt::PMT_F)); // writing off
        
        std::cout << "[FileSink] Saving completed" << std::endl;
    }
    
    
    return nconsumed;

}

} /* namespace beam_exciter */
} /* namespace gr */
