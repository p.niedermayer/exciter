/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_BURSTFILESINK_IMPL_H
#define INCLUDED_BEAM_EXCITER_BURSTFILESINK_IMPL_H

#include <gnuradio/beam_exciter/burstFileSink.h>

namespace gr {
namespace beam_exciter {

class burstFileSink_impl : public burstFileSink
{
private:
    size_t _itemsize;
    std::string d_filename;
    int d_length;
    bool d_enable;
    pmt::pmt_t _tag_key_start;
    pmt::pmt_t _tag_key_stop;
    std::vector<tag_t> _input_tags;
    int _nsamples_to_write = 0; // can also be -1 for continuous save
    
    const pmt::pmt_t _port_write;
	

public:
    burstFileSink_impl(size_t itemsize, const char* filename, pmt::pmt_t tag_key_start, pmt::pmt_t tag_key_stop, int length, bool enable);
    ~burstFileSink_impl();
	
    void set_filename(const char* filename) override {
        close();
        d_filename = std::string(filename);
    }
    void set_length(int length) override {
        d_length = length;
    }
    void set_enable(bool enable) override {
        d_enable = enable;
    }

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_BURSTFILESINK_IMPL_H */
