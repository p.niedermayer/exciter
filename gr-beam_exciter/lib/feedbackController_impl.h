/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_FEEDBACKCONTROLLER_IMPL_H
#define INCLUDED_BEAM_EXCITER_FEEDBACKCONTROLLER_IMPL_H

#include <gnuradio/beam_exciter/feedbackController.h>

namespace gr {
namespace beam_exciter {

class feedbackController_impl : public feedbackController
{
private:
    long _update_count = 0; // number of updates since SOB
    int _sample_count = 0; // number of samples since last update
    float _input_sum = 0;
    float _last_actual = 0;
    bool _last_actual_valid = false;
    float _output = 0;
    bool _burst_ended = false;
    float _smoothed_actual = 0;

    float _e = 0;
    float _e_previous = 0;
    float _e_sum = 0;
    bool _is_init = false;
    
    std::vector<float> _feedforward_memory_values;
    std::vector<float>::size_type _feedforward_memory_index = 0;
    int _feedforward_memory_recording = 0; // 0 = not recording
    
    // parameters
    int d_ta_samples;
    bool d_feedback;
    int d_feedforward;
    int d_target_mode;
    float d_target;
    int d_rise_time_samples;
    int d_rise_time_updates;
    float d_kp;
    float d_ki_times_ta;
    float d_kd_over_ta;
    float d_output_min;
    float d_output_max;
    bool d_init;
    float d_kp_init;
    float d_ki_times_ta_init;
    float d_kd_over_ta_init;
    float d_init_threshold;
    float d_smoothing;
    float d_output_max_init;
    bool d_abort_on_max;
    float d_feedforward_const;
    float d_feedforward_memory_update; // unused
    
public:
    feedbackController_impl(bool feedback,
                            int feedforward,
                            int target_mode,
                            float target,
                            int rise_time,
                            int ta_samples,
                            float kp,
                            float ki_times_ta,
                            float kd_over_ta,
                            float output_min,
                            float output_max,
                            bool init,
                            float kp_init,
                            float ki_times_ta_init,
                            float kd_over_ta_init,
                            float init_threshold,
                            float smoothing,
                            float output_max_init,
                            bool abort_on_max,
                            float feedforward_const,
                            float feedforward_memory_update,
                            bool wait_first_sob);
    ~feedbackController_impl();

    // parameter updates    
    void set_feedback(bool feedback);
    void set_feedforward(int feedforward);
    void set_target_mode(int target_mode) override { d_target_mode = target_mode; }
    void set_target(float target) override { d_target = target; }
    void set_rise_time(int rise_time) override {
        d_rise_time_samples = rise_time;
        d_rise_time_updates = rise_time/d_ta_samples;
    }
    void set_ta_samples(int ta_samples) override {
        d_ta_samples = std::max(ta_samples, 1);
        set_rise_time(d_rise_time_samples);
    }
    void set_kp(float kp) override { d_kp = kp; }
    void set_ki_times_ta(float ki_times_ta) override {
        if (!_is_init && ki_times_ta != 0) _e_sum = _e_sum * d_ki_times_ta / ki_times_ta;
        d_ki_times_ta = ki_times_ta;
    }
    void set_kd_over_ta(float kd_over_ta) override { d_kd_over_ta = kd_over_ta; }
    void set_output_min(float output_min) override { d_output_min = output_min; }
    void set_output_max(float output_max) override { d_output_max = output_max; }
    void set_init(bool init) override { d_init = init; }
    void set_kp_init(float kp_init) override { d_kp_init = kp_init; }
    void set_ki_times_ta_init(float ki_times_ta_init) override {
        if (_is_init && ki_times_ta_init != 0) _e_sum = _e_sum * d_ki_times_ta_init / ki_times_ta_init;
        d_ki_times_ta_init = ki_times_ta_init;
    }
    void set_kd_over_ta_init(float kd_over_ta_init) override { d_kd_over_ta_init = kd_over_ta_init; }
    void set_init_threshold(float init_threshold) override { d_init_threshold = init_threshold; }
    void set_smoothing(float smoothing) override { d_smoothing = smoothing > 0 ? smoothing : 1; }
    void set_output_max_init(float output_max_init) override { d_output_max_init = output_max_init; }
    void set_abort_on_max(bool abort_on_max) override { d_abort_on_max = abort_on_max; }
    void set_feedforward_const(float feedforward_const) override { d_feedforward_const = feedforward_const; }
    void set_feedforward_memory_update(float feedforward_memory_update) override { d_feedforward_memory_update = feedforward_memory_update; }

    
    void reset_feedback();
    float feedforward_value();


    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_FEEDBACKCONTROLLER_IMPL_H */
