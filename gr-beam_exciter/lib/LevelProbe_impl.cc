/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "LevelProbe_impl.h"
#include <gnuradio/io_signature.h>
#include <cmath>

namespace gr {
namespace beam_exciter {

using input_type = float;
using output_type = float;
LevelProbe::sptr LevelProbe::make(int treshold, int decimation)
{
    return gnuradio::make_block_sptr<LevelProbe_impl>(treshold, decimation);
}


/*
 * The private constructor
 */
LevelProbe_impl::LevelProbe_impl(int treshold, int decimation)
    : gr::sync_decimator("LevelProbe",
                         gr::io_signature::make(1, 1, sizeof(input_type)), // inputs (min, max)
                         gr::io_signature::make(2, 2, sizeof(output_type)), // outputs (min, max)
                         decimation)
{
    _treshold = treshold;
    _decimation = decimation;
}

/*
 * Our virtual destructor.
 */
LevelProbe_impl::~LevelProbe_impl() {}

/*
 * Work function
 * 
 * The following will be true: len(input_items[i]) = len(output_items[j])*decimation
 */
int LevelProbe_impl::work(int noutput_items,
                               gr_vector_const_void_star& input_items,
                               gr_vector_void_star& output_items)
{
    auto in = static_cast<const input_type*>(input_items[0]);
    auto out_rms = static_cast<output_type*>(output_items[0]);
    auto out_over = static_cast<output_type*>(output_items[1]);

    // Do signal processing
    for (int o = 0; o < noutput_items; o++) {
        int count = 0;
        double square_sum = 0;
        for (int i = 0; i < _decimation; i++) {
            float v = in[o * _decimation + i];
            // Count number of samples above treshhold
            if (v >= _treshold) count++;
            // Sum squares
            square_sum += v * v;
        }
        // Set outputs
        out_rms[o] = sqrt(square_sum / _decimation);
        out_over[o] = count * 1.0 / _decimation;
    }

    // Tell runtime system how many output items we produced.
    return noutput_items;
}

} /* namespace beam_exciter */
} /* namespace gr */
