/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "triggerSwitchV2_impl.h"
#include <gnuradio/io_signature.h>
#include <iostream>
#include <iomanip>
#include <ctime>

using namespace uhd;

static const pmt::pmt_t SOB_KEY = pmt::string_to_symbol("tx_sob");
static const pmt::pmt_t EOB_KEY = pmt::string_to_symbol("tx_eob");
static const pmt::pmt_t TIME_KEY = pmt::string_to_symbol("tx_time");
static const pmt::pmt_t FREQ_KEY = pmt::string_to_symbol("tx_freq");
static const pmt::pmt_t COMMAND_KEY = pmt::string_to_symbol("tx_command");

static const pmt::pmt_t INPUT_TAG_KEY = pmt::string_to_symbol("trigger_switch_block");



namespace gr {
namespace beam_exciter {


using input_type = gr_complex;
using output_type = gr_complex;


triggerSwitchV2::sptr triggerSwitchV2::make(bool enable_polling,
                                            int poll_every_samples,
                                            int duration_samples,
                                            int delay_samples,
                                            int manual_trigger,
                                            ::gr::uhd::rfnoc_graph::sptr ptr_graph,
                                            std::string device_addr,
                                            int device_index,
                                            int radio_index,
                                            std::string gpio_bank,
                                            int gpio_pin,
                                            int input_mode,
                                            bool stop_output,
                                            bool add_stream_tags)
{
    return gnuradio::make_block_sptr<triggerSwitchV2_impl>(enable_polling,
                                                           poll_every_samples,
                                                           duration_samples,
                                                           delay_samples,
                                                           manual_trigger,
                                                           ptr_graph,
                                                           device_addr,
                                                           device_index,
                                                           radio_index,
                                                           gpio_bank,
                                                           gpio_pin,
                                                           input_mode,
                                                           stop_output,
                                                           add_stream_tags);
}


/*
 * The private constructor
 */
triggerSwitchV2_impl::triggerSwitchV2_impl(bool enable_polling,
                                           int poll_every_samples,
                                           int duration_samples,
                                           int delay_samples,
                                           int manual_trigger,
                                           ::gr::uhd::rfnoc_graph::sptr ptr_graph,
                                           std::string device_addr,
                                           int device_index,
                                           int radio_index,
                                           std::string gpio_bank,
                                           int gpio_pin,
                                           int input_mode,
                                           bool stop_output,
                                           bool add_stream_tags)
    : gr::block("triggerSwitchV2",
                gr::io_signature::make(1, 1, sizeof(input_type)), // inputs (min, max)
                gr::io_signature::make(1, 1, sizeof(output_type))) // outputs (min, max)
    // init fields
    , _port_outp(pmt::mp("outp"))
    , _port_trg(pmt::mp("trg"))
    , _port_reset(pmt::mp("reset"))
{
    d_enable_polling = enable_polling;
    _poll_every_samples = poll_every_samples;
    d_duration_samples = duration_samples;
    d_delay_samples = delay_samples;
    d_manual_trigger = manual_trigger;
    _device_addr = device_addr;
    _device_index = device_index;
    _radio_index = radio_index;
    _gpio_bank = gpio_bank;
    _gpio_pin = gpio_pin;
    d_input_mode = input_mode;
    d_stop_output = stop_output;
    _add_stream_tags = add_stream_tags;
    
    _counter_polling = 0;
    _counter = -1;
    _input_received_tag = false;
    _trigger_level = false;
    _trigger_counter = 0;
    _input_tags.reserve(1);
    
    
    message_port_register_out(_port_outp);
    message_port_register_out(_port_trg);
    message_port_register_out(_port_reset);
    
    set_tag_propagation_policy(TPP_ALL_TO_ALL);
    
    

    // USRP GPIO Setup
    ////////////////////
    
    if (ptr_graph){
		// RFNoC based GPIO access
		// https://files.ettus.com/manual/classuhd_1_1rfnoc_1_1radio__control.html#ac4e8177679bc40dccc5e532ea9f5e10e
		
		// Get controller
		auto block_id = ptr_graph->get_block_id("Radio", _device_index, _radio_index);
		std::cout << "Accessing GPIOs via RFNoC block " << block_id << std::endl;
		auto block_ref = ptr_graph->get_block_ref(block_id, 99);
		_radio = std::dynamic_pointer_cast<rfnoc::radio_control>(block_ref);
		
		// List available GPIO banks
		std::cout << "Available GPIO banks: " << std::endl;
		auto banks = _radio->get_gpio_banks();
		for (auto& bank : banks) {
			std::cout << "  * " << bank << std::endl;
		}
		std::cout << "Using bank " << _gpio_bank << std::endl;
		
		/*
		auto _graph = ptr_graph->_graph; // ! Error: this is a private field :(
		auto radio_control_blocks = _graph->find_blocks<rfnoc::radio_control>("");
		if (radio_control_blocks.empty()) {
			throw std::runtime_error("No rfnoc radio_control block found.");
		}
		for (auto& block_id : radio_control_blocks) {
			std::cout << "* " << block_id << std::endl;
			
			auto radio_control_block = _graph->get_block<rfnoc::radio_control>(block_id);
			if (radio_control_block) {
				auto banks = radio_control_block->get_gpio_banks();
				for (auto& bank : banks) {
					std::cout << "  * " << bank << std::endl;
				}
			} else {
				std::cout << "  ERROR: Failed to extract radio_control_block!" << std::endl;
			}
		}		
		
		// Get controller
		auto radio_id = radio_control_blocks[_radio_index];
		_radio = _graph->get_block<rfnoc::radio_control>(radio_id);
		*/
		
		
		// Set pin to manual control
		auto bits_ctrl = _radio->get_gpio_attr(_gpio_bank, "CTRL");
		bits_ctrl &= ~(1 << _gpio_pin); // set bit to 0
		_radio->set_gpio_attr(_gpio_bank, "CTRL", bits_ctrl);
		
		// Set pin to input direction
		auto bits_ddr = _radio->get_gpio_attr(_gpio_bank, "DDR");
		bits_ddr &= ~(1 << _gpio_pin); // set bit to 0
		_radio->set_gpio_attr(_gpio_bank, "DDR", bits_ddr);
		
		std::cout << "Pin " << _gpio_pin << " configured as trigger input" << std::endl;
		
		
	} else {
		// USRP based GPIO access
		// https://files.ettus.com/manual/classuhd_1_1usrp_1_1multi__usrp.html#a57f25d118d20311aca261e6dd252625e
		std::cout << "Accessing GPIOs via USRP device " << _device_index << " at " << _device_addr << std::endl;
		
		
		// Get controller
		if (!_usrp){
			_usrp = usrp::multi_usrp::make(_device_addr);
		}
		
		// List available GPIO banks
		std::cout << "Available GPIO banks: " << std::endl;
		auto banks = _usrp->get_gpio_banks(_device_index);
		for (auto& bank : banks) {
			std::cout << "* " << bank << std::endl;
		}
		std::cout << "Using bank " << _gpio_bank << std::endl;
		
		// Set pin to manual control
        _usrp->set_gpio_attr(_gpio_bank, "CTRL", 0, (1 << _gpio_pin), _device_index);
        // Set pin to input direction
		_usrp->set_gpio_attr(_gpio_bank, "DDR", 0, (1 << _gpio_pin), _device_index);
		
		std::cout << "Pin " << _gpio_pin << " configured as trigger input" << std::endl;
		
	}

}



bool triggerSwitchV2_impl::poll_gpio()
{    
    bool trigger_level = false;
    
    if (d_enable_polling){
        // read pin state
        
        if (_radio){
			// RFNoC based GPIO access
			// https://files.ettus.com/manual/classuhd_1_1rfnoc_1_1radio__control.html#ac4e8177679bc40dccc5e532ea9f5e10e    
			int readback = _radio->get_gpio_attr(_gpio_bank, "READBACK");
			trigger_level = (readback >> _gpio_pin) & 1;
		
		
		} else {
			// USRP based GPIO access
			// https://files.ettus.com/manual/classuhd_1_1usrp_1_1multi__usrp.html#a57f25d118d20311aca261e6dd252625e
			int readback = _usrp->get_gpio_attr(_gpio_bank, "READBACK", _device_index);
			trigger_level = (readback >> _gpio_pin) & 1;
		}

        //GR_LOG_DEBUG(d_logger, boost::format("GPIO polled: level = %d, manual = %d") % (trigger_level?1:0) % d_manual_trigger);
        //std::cout << (trigger_level ? 1 : 0);
    }

    return trigger_level || d_manual_trigger > 0;
    
}

void triggerSwitchV2_impl::set_input_mode(int input_mode){
    d_input_mode = input_mode;
    if (d_input_mode == 2 && _counter < 0){
        _input_received_tag = false;
        // send a reset to tell the other blocks we are waiting for a tag
        message_port_pub(_port_reset, pmt::cons(_port_reset, pmt::PMT_NIL));
    }
}

void triggerSwitchV2_impl::set_enable_polling(bool enable_polling){
    d_enable_polling = enable_polling;
    if (!enable_polling){
        // abort burst
        _trigger_level = false;
        reset_counter(0);        
    }
}

/*
 * Our virtual destructor.
 */
triggerSwitchV2_impl::~triggerSwitchV2_impl() {}

void triggerSwitchV2_impl::forecast(int noutput_items,
                                    gr_vector_int& ninput_items_required)
{
    // forecast how many items on each input we need to produce noutput_items
    ninput_items_required[0] = noutput_items;
}

int triggerSwitchV2_impl::general_work(int noutput_items,
                                       gr_vector_int& ninput_items,
                                       gr_vector_const_void_star& input_items,
                                       gr_vector_void_star& output_items)
{
    auto in = static_cast<const input_type*>(input_items[0]);
    auto out = static_cast<output_type*>(output_items[0]);
    int n = noutput_items;
    


    // GPIO polling
    /////////////////
    _counter_polling -= n;
    if (_counter_polling <= 0){
        _counter_polling = 0;

        if (d_duration_samples <= 0 || _counter < 0){ // only poll if timer is not active
            _counter_polling = _poll_every_samples;

            bool trigger_level = poll_gpio(); // read gpio trigger pin level
            //add_item_tag(0, nitems_written(0), pmt::string_to_symbol("trg"), pmt::from_bool(trigger_level));


            if (trigger_level && !_trigger_level){
                // pos flank
                _trigger_counter++;
                std::time_t t = std::time(nullptr);
                std::cout << "[Trigger] trg received " << std::put_time(std::localtime(&t), "%F %T %Z") << std::endl;
                message_port_pub(_port_trg, pmt::cons(_port_trg, pmt::mp(_trigger_counter)));
                // start counter from zero
                _counter = 0;
                // start burst
                if (_add_stream_tags){
                    add_item_tag(0, nitems_written(0), SOB_KEY, pmt::PMT_T); // start-of-burst tag
                }
                message_port_pub(_port_outp, pmt::cons(_port_outp, pmt::PMT_T)); // output on
            }
            _trigger_level = trigger_level;
        }
    }



    // Switch state
    //////////////////////////////
    // Note: the implementation assumes n << delay and n << duration (i.e. switching only at start of items)
    if (_counter < 0){
        // waiting for start

        if (d_input_mode == 0) {
            // drop input
            consume(0, n);

        } else if (d_input_mode == 1) {
            // block input
            consume(0, 0);

        } else if (d_input_mode == 2) {
            // drop until tag, then block
            if (!_input_received_tag) {
                int n_drop = n;
                get_tags_in_window(_input_tags, 0, 0, n, INPUT_TAG_KEY);
                for (auto& tag : _input_tags) {
                    _input_received_tag = true;
                    std::cout << "[Trigger] trg armed" << std::endl;
                    n_drop = tag.offset - nitems_read(0);
                }
                consume(0, n_drop);

            } else {
                consume(0, 0);
            }            
        }
        
        
        if (d_stop_output){
            produce(0, 0);

        } else {
            for(int i = 0; i < n; i++) { out[i] = 0; }
            produce(0, n);
        }
        
        return WORK_CALLED_PRODUCE;
        

    } else if (d_duration_samples <= 0 && !_trigger_level){
        // gate end: end burst with zeros and reset
        for(int i = 0; i < n; i++) {
            out[i] = 0;
        }
        reset_counter(n-1);
        consume(0, n);
        produce(0, n);
        return WORK_CALLED_PRODUCE;

    } else if (_counter < d_delay_samples){
        // initial delay: insert zeros
        for(int i = 0; i < n; i++) {
            out[i] = 0;
        }
        _counter = _counter + n;
        consume(0, 0);
        produce(0, n);
        return WORK_CALLED_PRODUCE;

    } else if (d_duration_samples <= 0){
        // gate active: pass inputs to outputs
        for(int i = 0; i < n; i++) { //memcpy(out, in, noutput_items * sizeof(input_type));
            out[i] = in[i];;
        }
        // no need to increment counter any further in gating mode
        consume(0, n);
        produce(0, n);
        return WORK_CALLED_PRODUCE;

    } else if (_counter < d_delay_samples + d_duration_samples){
        // timer active: pass inputs to outputs
        for(int i = 0; i < n; i++) {
            out[i] = in[i];
        }
        _counter = _counter + n;
        consume(0, n);
        produce(0, n);
        return WORK_CALLED_PRODUCE;

    } else {
        // timer end: end burst with zeros and reset
        for(int i = 0; i < n; i++) {
            out[i] = 0;
        }
        reset_counter(n-1);
        consume(0, n);
        produce(0, n);
        return WORK_CALLED_PRODUCE;
    }
    
}

void triggerSwitchV2_impl::reset_counter(int eob_tag_offset)
{
    // reset internal variables
    _counter = -1;
    _input_received_tag = false;
    // add end-of-burst tag
    if (_add_stream_tags){
        add_item_tag(0, nitems_written(0)+eob_tag_offset, EOB_KEY, pmt::PMT_T);
    }
    // publish message: output off and reset
    message_port_pub(_port_outp, pmt::cons(_port_outp, pmt::PMT_F));
    message_port_pub(_port_reset, pmt::cons(_port_reset, pmt::PMT_NIL));
}

} /* namespace beam_exciter */
} /* namespace gr */
