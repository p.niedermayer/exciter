/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_LEVELPROBE_IMPL_H
#define INCLUDED_BEAM_EXCITER_LEVELPROBE_IMPL_H

#include <gnuradio/beam_exciter/LevelProbe.h>

namespace gr {
namespace beam_exciter {

class LevelProbe_impl : public LevelProbe
{
private:
    int _treshold;
    int _decimation;

public:
    LevelProbe_impl(int treshold, int decimation);
    ~LevelProbe_impl();

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_LEVELPROBE_IMPL_H */
