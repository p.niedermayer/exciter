/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_TRIGGERSWITCHV1_IMPL_H
#define INCLUDED_BEAM_EXCITER_TRIGGERSWITCHV1_IMPL_H

#include <gnuradio/beam_exciter/triggerSwitchV1.h>
#include <uhd/usrp/multi_usrp.hpp>
#include <gnuradio/uhd/api.h>
#include <gnuradio/uhd/usrp_block.h>

namespace gr {
namespace beam_exciter {

class triggerSwitchV1_impl : public triggerSwitchV1
{
private:
    int _poll_every_samples;
    int d_duration_samples;
    int d_delay_samples;
    int d_manual_trigger;
    std::string _device_addr;
    int _gpio_mboard;
    std::string _gpio_bank;
    int _gpio_pin;
    bool d_stop_output;
    bool _add_stream_tags;
    
    int _counter_polling;
    int _counter;
    bool _trigger_level;
    
    ::uhd::usrp::multi_usrp::sptr _usrp;

public:
    triggerSwitchV1_impl(int poll_every_samples,
                         int duration_samples,
                         int delay_samples,
                         int manual_trigger,
                         std::string device_addr,
                         int gpio_mboard,
                         std::string gpio_bank,
                         int gpio_pin,
                         bool stop_output,
                         bool add_stream_tags);
    ~triggerSwitchV1_impl();
    
    void set_duration_samples(int duration_samples) override { d_duration_samples = duration_samples; }
    void set_delay_samples(int delay_samples) override { d_delay_samples = delay_samples; }
    void set_manual_trigger(int manual_trigger) override { d_manual_trigger = manual_trigger; }
    void set_stop_output(bool stop_output) override { d_stop_output = stop_output; }


    // Where all the action really happens
    void forecast(int noutput_items, gr_vector_int& ninput_items_required);

    int general_work(int noutput_items,
                     gr_vector_int& ninput_items,
                     gr_vector_const_void_star& input_items,
                     gr_vector_void_star& output_items);
    
    bool poll_gpio();
    
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_TRIGGERSWITCHV1_IMPL_H */
