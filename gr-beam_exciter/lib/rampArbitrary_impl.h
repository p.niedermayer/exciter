/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_RAMPARBITRARY_IMPL_H
#define INCLUDED_BEAM_EXCITER_RAMPARBITRARY_IMPL_H

#include <gnuradio/beam_exciter/rampArbitrary.h>

namespace gr {
namespace beam_exciter {

class rampArbitrary_impl : public rampArbitrary
{
private:
    std::string d_filename = "";
    float d_offset_scale = 1;
    
    pmt::pmt_t _tag_value;
    pmt::pmt_t _tag_key;
    std::vector<tag_t> _input_tags;
    std::vector<int> _sob_pos;
    
    bool _reset_message_pending = false;
    uint64_t _offset_start = 0; // absolute sample offset of ramp start
    std::vector<uint64_t> _ramp_offsets;
    std::vector<float> _ramp_values;
    std::vector<float>::size_type _ramp_segment = 0; // the segment we are approaching
    uint64_t _offset_next_segment = 0;
    double _ramp_value = 0;
    double _ramp_increment = 0;
    

public:
      rampArbitrary_impl(std::string filename, float offset_scale, pmt::pmt_t tag_value, pmt::pmt_t tag_key);
      ~rampArbitrary_impl();
            
      void set_filename(std::string filename) override { d_filename = filename; readFile(); }
      void set_offset_scale(float offset_scale) override { d_offset_scale = offset_scale; readFile(); }

      // Where all the action really happens
      int work(int noutput_items,
               gr_vector_const_void_star& input_items,
               gr_vector_void_star& output_items);
      
	    void fill_ramp(float out[], int count, uint64_t offset);
      void readFile();
      void initRampSegment(std::vector<float>::size_type segment);
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_RAMPARBITRARY_IMPL_H */
