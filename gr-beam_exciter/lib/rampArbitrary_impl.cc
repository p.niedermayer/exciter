/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "rampArbitrary_impl.h"
#include <gnuradio/io_signature.h>
#include <iostream>
#include <fstream>
#include <sstream>

namespace gr {
namespace beam_exciter {


static const pmt::pmt_t MSG_PORT_RESET = pmt::mp("reset");



    rampArbitrary::sptr
    rampArbitrary::make(std::string filename, float offset_scale, pmt::pmt_t tag_value, pmt::pmt_t tag_key)
    {
      return gnuradio::make_block_sptr<rampArbitrary_impl>(
        filename, offset_scale, tag_value, tag_key);
    }


    /*
     * The private constructor
     */
    rampArbitrary_impl::rampArbitrary_impl(std::string filename, float offset_scale, pmt::pmt_t tag_value, pmt::pmt_t tag_key)
      : gr::sync_block("rampArbitrary",
              gr::io_signature::make(0 /* min inputs */, 1 /*max inputs */, sizeof(float)),
              gr::io_signature::make(1 /* min outputs */, 1 /*max outputs */, sizeof(float)))
    {
        set_filename(filename);
        set_offset_scale(offset_scale);
        
        _tag_key = tag_key;
        _tag_value = tag_value;
        
        // register message handler
        message_port_register_in(MSG_PORT_RESET);
        set_msg_handler(MSG_PORT_RESET, [this](const pmt::pmt_t& msg) {
                // accept any message
                _reset_message_pending = true;
        });
        
        set_tag_propagation_policy(TPP_ALL_TO_ALL);
    }

    rampArbitrary_impl::~rampArbitrary_impl() {}

    int rampArbitrary_impl::work(int noutput_items,
                              gr_vector_const_void_star& input_items,
                              gr_vector_void_star& output_items
    ){
        //auto in = static_cast<const float*>(input_items[0]);
        auto out = static_cast<float*>(output_items[0]);
        int n = noutput_items;
            uint64_t offset = nitems_written(0);
            
            
        // collect sob from message and stream tags
        _sob_pos.clear();
        if (_reset_message_pending){
            _reset_message_pending = false;
            add_item_tag(0, offset, _tag_key, _tag_value);
            _sob_pos.push_back(0);
        }
        get_tags_in_window(_input_tags, 0, 0, n, _tag_key);
        for (auto& tag : _input_tags) {
            _sob_pos.push_back(tag.offset - offset);
        }
        
        // insert ramp after each sob
        int from = 0, to;
        for (auto& sob : _sob_pos) {
            to = sob;
            fill_ramp(&out[from], to-from, offset+from);
            from = to;
            // reset ramp
            _offset_start = offset + from;
            initRampSegment(0);
        }
        to = n;
        fill_ramp(&out[from], to-from, offset+from);
        
        // Tell runtime system how many output items we produced.
        return n;
        
        
    }
    
    
    void rampArbitrary_impl::fill_ramp(float out[], int count, uint64_t offset){
        
        // ramp
        for (int i = 0; i < count; i++){
            while (_offset_next_segment > 0 && offset + i >= _offset_next_segment){
                initRampSegment(_ramp_segment + 1);
            }
            out[i] = _ramp_value;
            _ramp_value += _ramp_increment;
        }
        
    }
    
    
    void rampArbitrary_impl::readFile() {
        _ramp_offsets.clear();
        _ramp_values.clear();
        
        try {
        // read ramp definition from csv file
            std::ifstream file;
        file.open(d_filename);
            if (file.is_open()){
                std::setlocale(LC_NUMERIC,"C"); // use '.' as decimal point for std::stof
                std::string row, element;
                while (getline(file, row)){
                    if (row.find(',') == std::string::npos) continue;
                    std::stringstream str(row);
                    // offset, value
                    getline(str, element, ','); _ramp_offsets.push_back((uint64_t)(std::stof(element) * d_offset_scale));
                    getline(str, element, ','); _ramp_values.push_back(std::stof(element));
                }
                file.close();
                std::cout << "[Ramp] Successfully loaded ramp with " << _ramp_offsets.size() << " points from file " << d_filename << std::endl;
            
           } else {
               std::cerr << "[Ramp] Failed to find ramp file " << d_filename << std::endl;
           }
       } catch (const std::exception &exc) {
           std::cerr << "[Ramp] Failed to load ramp file " << d_filename << ": Error ";
           std::cerr << exc.what() << std::endl;
           //throw;
       }
       
       // freeze current output value until next reset
       _ramp_increment = 0;
       _offset_next_segment = 0;
       
    }
    
    void rampArbitrary_impl::initRampSegment(std::vector<float>::size_type segment) {
        if (_ramp_values.empty()){
            // empty ramp
            std::cout << "[Ramp] No ramp definition loaded! Last filename " << d_filename << std::endl;
            _ramp_value = 0;
            _ramp_increment = 0;
            _offset_next_segment = 0;
            _ramp_segment = 0;
            return;
        }
    
        if (segment > _ramp_values.size()) segment = _ramp_values.size();       
        
        if (segment == 0) {
            // waiting for first segment
            _offset_next_segment = _offset_start + _ramp_offsets.at(0);
            _ramp_value = _ramp_values.at(0);
            _ramp_increment = 0;
        } else if (segment + 1 <= _ramp_values.size()) {
            // linear slope to segment
            _offset_next_segment = _offset_start + _ramp_offsets.at(segment);
            _ramp_value = _ramp_values.at(segment-1);
            _ramp_increment = (_ramp_values.at(segment) - _ramp_values.at(segment-1)) * 1.0 /
                              (_ramp_offsets.at(segment) - _ramp_offsets.at(segment-1));
        } else {
            // last segment reached
            _offset_next_segment = 0;
            _ramp_value = _ramp_values.at(segment-1);
            _ramp_increment = 0;
        }
        
        _ramp_segment = segment;
        
    }


    } /* namespace beam_exciter */
    } /* namespace gr */
