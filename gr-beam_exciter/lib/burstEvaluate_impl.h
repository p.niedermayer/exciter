/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_BURSTEVALUATE_IMPL_H
#define INCLUDED_BEAM_EXCITER_BURSTEVALUATE_IMPL_H

#include <gnuradio/beam_exciter/burstEvaluate.h>

// State machine
#define STATE_WAIT_SOB              0
#define STATE_WAIT_START            1
#define STATE_START_EVAL            2
#define STATE_EVAL                  3
#define STATE_FINISH_EVAL           4
#define STATE_FINISH_EVAL_THEN_END  5
#define STATE_WAIT_EVAL             6
#define STATE_END_BURST             7


/*
 *
 * STATE_WAIT_SOB
 * - if SOB tag received     -> STATE_WAIT_START
 *
 * STATE_WAIT_START
 * - if EOB tag received     -> STATE_WAIT_SOB
 * - after start_offset_samp -> STATE_START_EVAL
 *
 * STATE_START_EVAL
 * - always                  -> STATE_EVAL
 *
 * STATE_EVAL
 * - if eval_samp is infinite
 *   - if EOB tag received   -> STATE_FINISH_EVAL_THEN_END
 * - if eval_samp is given
 *   - if EOB tag received   -> STATE_END_BURST
 *   - after eval_samp       -> STATE_FINISH_EVAL
 *
 * STATE_FINISH_EVAL
 * - always                  -> STATE_WAIT_EVAL
 *
 * STATE_FINISH_EVAL_THEN_END
 * - always                  -> STATE_END_BURST
 *
 * STATE_WAIT_EVAL
 * - if repetitions is infinite
 *   - if EOB tag received   -> STATE_END_BURST
 *   - when sample reached   -> STATE_START_EVAL
 * - if repetitions is given
 *   - after repetitions     -> STATE_END_BURST
 *
 * STATE_END_BURST
 * - always                  -> STATE_WAIT_SOB
 *
 *
 */





namespace gr {
namespace beam_exciter {

class burstEvaluate_impl : public burstEvaluate
{
private:
    bool d_enable;
    int d_function;
    int d_start_offset_samp = 0; // d_delay;
    int d_eval_samp; //d_length;
    int d_pre_avg_samp; //d_window;
    int d_repeat_every_samp;
    int d_repeat_n_times;
    bool d_repeat_msg_avg;
    bool d_delay_msg_eob;
    pmt::pmt_t _tag_key_start;
    pmt::pmt_t _tag_key_stop;
    
    std::vector<tag_t> _input_tags;
    int _state = STATE_WAIT_SOB;
    bool _reset_state = false;
    int _sample_counter = 0;
    int _repeat_counter = 0;
    double _repeat_result_mean = 0;
    
    const pmt::pmt_t _port_write;
    bool _message_pending = false;
    double _message_value = 0;
    

public:
    burstEvaluate_impl(
        pmt::pmt_t tag_key_start,
        int start_offset_samp,
        pmt::pmt_t tag_key_stop,
        int eval_samp,
        int function,
        int pre_avg_samp,
        bool enable,
        int repeat_every_samp,
        int repeat_n_times,
        bool repeat_msg_avg,
        bool delay_msg_eob);
    ~burstEvaluate_impl();
    
    void set_start_offset_samp(int samples) override {
        d_start_offset_samp = samples;
        _reset_state = true;
    }
    void set_eval_samp(int samples) override {
        d_eval_samp = samples;
        _reset_state = true;
    }
    void set_function(int function) override {
        d_function = function;
        _reset_state = true;
    }
    void set_pre_avg_samp(int samples) override {
        d_pre_avg_samp = std::max(1, samples);
        _reset_state = true;
    }
    void set_enable(bool enable) override {
        d_enable = enable;
        _reset_state = true;
    }
    void set_repeat_every_samp(int samples) override {
        d_repeat_every_samp = samples;
        _reset_state = true;
    }
    void set_repeat_n_times(int n) override {
        // values <= 0 mean infinite repeat
        d_repeat_n_times = n;
        _reset_state = true;
    }
    void set_repeat_msg_avg(bool avg) override {
        d_repeat_msg_avg = avg;
        _reset_state = true;
    }
    void set_delay_msg_eob(bool delay) override {
        d_delay_msg_eob = delay;
    }
    
    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);


private:   
    
    double _eval_n = 0;
    double _eval_sqsum = 0;
    double _eval_asum = 0;
    double _eval_m2 = 0;
    double _eval_mean = 0;
    
    void eval_method_start();
    void eval_method_update(double value);
    double eval_method_result();
    
    void message_register(double value);
    void message_publish();
    
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_BURSTEVALUATE_IMPL_H */
