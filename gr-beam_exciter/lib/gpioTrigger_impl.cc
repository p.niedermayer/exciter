/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gpioTrigger_impl.h"
#include <gnuradio/io_signature.h>
#include <iostream>
#include <iomanip>
#include <ctime>

namespace gr {
namespace beam_exciter {


gpioTrigger::sptr gpioTrigger::make(
    size_t itemsize,
    bool enable_polling,
    int poll_every_samples,
    int duration_samples,
    pmt::pmt_t tag_key_start,
    pmt::pmt_t tag_key_stop,
    int manual_trigger,
    ::gr::uhd::rfnoc_graph::sptr ptr_graph,
    std::string device_addr,
    int device_index,
    int radio_index,
    std::string gpio_bank,
    int gpio_pin,
    bool inverted
){
    return gnuradio::make_block_sptr<gpioTrigger_impl>(
        itemsize,
        enable_polling,
        poll_every_samples,
        duration_samples,
        tag_key_start,
        tag_key_stop,
        manual_trigger,
        ptr_graph,
        device_addr,
        device_index,
        radio_index,
        gpio_bank,
        gpio_pin,
        inverted
);
}


/*
 * The private constructor
 */
gpioTrigger_impl::gpioTrigger_impl(
    size_t itemsize,
    bool enable_polling,
    int poll_every_samples,
    int duration_samples,
    pmt::pmt_t tag_key_start,
    pmt::pmt_t tag_key_stop,
    int manual_trigger,
    ::gr::uhd::rfnoc_graph::sptr ptr_graph,
    std::string device_addr,
    int device_index,
    int radio_index,
    std::string gpio_bank,
    int gpio_pin,
    bool inverted
) : gr::sync_block(
        "gpioTrigger",
        gr::io_signature::make(0 /* min inputs */, 1 /* max inputs */, itemsize),
        gr::io_signature::make(1 /* min outputs */, 1 /*max outputs */, itemsize)
    ),
    // ports
    _port_count(pmt::mp("count")),
    _port_active(pmt::mp("active"))
{
    
    message_port_register_out(_port_count);
    message_port_register_out(_port_active);
    
    // settings
    _itemsize = itemsize;
    d_enable_polling = enable_polling;
    _poll_every_samples = poll_every_samples;
    d_duration_samples = duration_samples;
    _tag_key_start = tag_key_start;
    _tag_key_stop = tag_key_stop;
    d_manual_trigger = manual_trigger;
    _device_addr = device_addr;
    _device_index = device_index;
    _radio_index = radio_index;
    _gpio_bank = gpio_bank;
    _gpio_pin = gpio_pin;
    _inverted = inverted;
    
    // internals
    _counter_until_poll = 0;
    _counter_active = -1;
    _input_received_tag = false;
    _trigger_level = false;
    _trigger_counter = 0;
    _input_tags.reserve(1);
    
    
    set_tag_propagation_policy(TPP_ALL_TO_ALL);
    
    

    // USRP GPIO Setup
    ////////////////////
    
    if (ptr_graph){
        // =======================
		// RFNoC based GPIO access
        // =======================
		// https://files.ettus.com/manual/classuhd_1_1rfnoc_1_1radio__control.html#ac4e8177679bc40dccc5e532ea9f5e10e
		
		// Get controller
		auto block_id = ptr_graph->get_block_id("Radio", _device_index, _radio_index);
		std::cout << "Accessing GPIOs via RFNoC block " << block_id << std::endl;
		auto block_ref = ptr_graph->get_block_ref(block_id, 99);
		_radio = std::dynamic_pointer_cast<rfnoc::radio_control>(block_ref);
		
		// List available GPIO banks
		std::cout << "Available GPIO banks: " << std::endl;
		auto banks = _radio->get_gpio_banks();
		for (auto& bank : banks) {
			std::cout << "  * " << bank << std::endl;
		}
		std::cout << "Using bank " << _gpio_bank << std::endl;		
		
		// Set pin to manual control
		auto bits_ctrl = _radio->get_gpio_attr(_gpio_bank, "CTRL");
		bits_ctrl &= ~(1 << _gpio_pin); // set bit to 0 for GPIO mode
		_radio->set_gpio_attr(_gpio_bank, "CTRL", bits_ctrl);
		
		// Set pin to input direction
		auto bits_ddr = _radio->get_gpio_attr(_gpio_bank, "DDR");
		bits_ddr &= ~(1 << _gpio_pin); // set bit to 0 for input
		_radio->set_gpio_attr(_gpio_bank, "DDR", bits_ddr);
		
		std::cout << "Pin " << _gpio_pin << " configured as trigger input" << std::endl;
		
		
	} else {
        // =======================
		// USRP based GPIO access
        // =======================
		// https://files.ettus.com/manual/classuhd_1_1usrp_1_1multi__usrp.html#a57f25d118d20311aca261e6dd252625e
		std::cout << "Accessing GPIOs via USRP device " << _device_index << " at " << _device_addr << std::endl;
		
		
		// Get controller
		if (!_usrp){
			_usrp = usrp::multi_usrp::make(_device_addr);
		}
		
		// List available GPIO banks
		std::cout << "Available GPIO banks: " << std::endl;
		auto banks = _usrp->get_gpio_banks(_device_index);
		for (auto& bank : banks) {
			std::cout << "* " << bank << std::endl;
		}
		std::cout << "Using bank " << _gpio_bank << std::endl;
		
		// Set pin to manual control
        _usrp->set_gpio_attr(_gpio_bank, "CTRL", 0, (1 << _gpio_pin), _device_index);
        // Set pin to input direction
		_usrp->set_gpio_attr(_gpio_bank, "DDR", 0, (1 << _gpio_pin), _device_index);
		
		std::cout << "Pin " << _gpio_pin << " configured as trigger input" << std::endl;
		
	}
    
}

/*
 * Our virtual destructor.
 */
gpioTrigger_impl::~gpioTrigger_impl() {}

/*
 * GPIO polling routine
 */
bool gpioTrigger_impl::poll_gpio(){    
    bool trigger_level = false;
    
    if (d_enable_polling){
        
        if (_radio){
            // =======================
            // RFNoC based GPIO access
            // =======================
            // https://files.ettus.com/manual/classuhd_1_1rfnoc_1_1radio__control.html#ac4e8177679bc40dccc5e532ea9f5e10e    
            int readback = _radio->get_gpio_attr(_gpio_bank, "READBACK");
            trigger_level = (readback >> _gpio_pin) & 1;


        } else {
            // =======================
            // USRP based GPIO access
            // =======================
            // https://files.ettus.com/manual/classuhd_1_1usrp_1_1multi__usrp.html#a57f25d118d20311aca261e6dd252625e
            int readback = _usrp->get_gpio_attr(_gpio_bank, "READBACK", _device_index);
            trigger_level = (readback >> _gpio_pin) & 1;
        }

        if (_inverted){
            trigger_level = !trigger_level;
        }
    
    }    
    
    return trigger_level || d_manual_trigger > 0;
    
}



int gpioTrigger_impl::work(int noutput_items,
                           gr_vector_const_void_star& input_items,
                           gr_vector_void_star& output_items)
{
    
    int n = noutput_items;
    
    
    // Check if the burst ended because the duration was reached
    if (d_duration_samples > 0 && _counter_active >= d_duration_samples){
        end_burst();
    }


    // GPIO polling
    /////////////////
    if (_counter_until_poll <= 0){
        _counter_until_poll = 0;
        
        // only poll outside burst, or if duration undefined
        if (_counter_active < 0 || d_duration_samples <= 0){

            bool trigger_level = poll_gpio(); // read gpio trigger pin level

            if (_counter_active < 0 && trigger_level && !_trigger_level){
                // not active + pos flank -> start burst
                start_burst();
            } else
            if (_counter_active >= 0 && !trigger_level && _trigger_level){
                // active + neg flank -> end burst
                end_burst();
            }
            
            _trigger_level = trigger_level;
            _counter_until_poll = _poll_every_samples; // next polling
        }
    }
    
    
    
    // Process samples
    ////////////////////
    
    // process no more than until next polling is due
    if (_counter_until_poll > 0 && _counter_until_poll < n){
        n = _counter_until_poll;
    }
    
    // process no more than until end of burst
    if (_counter_active >= 0 && d_duration_samples > 0 && d_duration_samples - _counter_active < n){
        n = d_duration_samples - _counter_active;
    }
    
    // copy input to output (or produce zeros if no input)
    uint8_t* optr = (uint8_t*) output_items[0];
    if (input_items.size() > 0){
        const uint8_t* iptr = (const uint8_t*) input_items[0];
        std::memcpy(optr, iptr, n * _itemsize);
    } else {
        memset(optr, 0, n * _itemsize);
    }
    
    _counter_until_poll -= n;
    if (_counter_active >= 0) _counter_active += n;
    
    return n; // how many output items we produced
}


/*
 * Start the burst
 */
void gpioTrigger_impl::start_burst(){
    
    std::time_t t = std::time(nullptr);
    std::cout << "[Trigger] trg received " << std::put_time(std::localtime(&t), "%F %T %Z") << std::endl;
    _trigger_counter++;
    
    // add start-of-burst tag
    add_item_tag(0, nitems_written(0), _tag_key_start, pmt::PMT_T);
    
    // publish messages
    message_port_pub(_port_count, pmt::cons(_port_count, pmt::mp(_trigger_counter)));
    message_port_pub(_port_active, pmt::cons(_port_active, pmt::PMT_T));
    
    // start counter from zero
    _counter_active = 0;
    
}

/*
 * End the burst
 */
void gpioTrigger_impl::end_burst(){
    
    // add end-of-burst tag
    add_item_tag(0, nitems_written(0), _tag_key_stop, pmt::PMT_T);
    
    // publish messages
    message_port_pub(_port_active, pmt::cons(_port_active, pmt::PMT_F));
    
    // reset counter
    _counter_active = -1;
    
}


} /* namespace beam_exciter */
} /* namespace gr */
