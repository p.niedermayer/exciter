/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "feedbackController_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace beam_exciter {

using input_type = float;
using output_type = float;
feedbackController::sptr feedbackController::make(bool feedback,
                                                  int feedforward,
                                                  int target_mode,
                                                  float target,
                                                  int rise_time,
                                                  int ta_samples,
                                                  float kp,
                                                  float ki_times_ta,
                                                  float kd_over_ta,
                                                  float output_min,
                                                  float output_max,
                                                  bool init,
                                                  float kp_init,
                                                  float ki_times_ta_init,
                                                  float kd_over_ta_init,
                                                  float init_threshold,
                                                  float smoothing,
                                                  float output_max_init,
                                                  bool abort_on_max,
                                                  float feedforward_const,
                                                  float feedforward_memory_update,
                                                  bool wait_first_sob)
{
    return gnuradio::make_block_sptr<feedbackController_impl>(feedback,
                                                              feedforward,
                                                              target_mode,
                                                              target,
                                                              rise_time,
                                                              ta_samples,
                                                              kp,
                                                              ki_times_ta,
                                                              kd_over_ta,
                                                              output_min,
                                                              output_max,
                                                              init,
                                                              kp_init,
                                                              ki_times_ta_init,
                                                              kd_over_ta_init,
                                                              init_threshold,
                                                              smoothing,
                                                              output_max_init,
                                                              abort_on_max,
                                                              feedforward_const,
                                                              feedforward_memory_update,
                                                              wait_first_sob);
}

/*
 * Our virtual destructor.
 */
feedbackController_impl::~feedbackController_impl() {}


/*
 * The private constructor
 */
feedbackController_impl::feedbackController_impl(bool feedback,
                                                 int feedforward,
                                                 int target_mode,
                                                 float target,
                                                 int rise_time,
                                                 int ta_samples,
                                                 float kp,
                                                 float ki_times_ta,
                                                 float kd_over_ta,
                                                 float output_min,
                                                 float output_max,
                                                 bool init,
                                                 float kp_init,
                                                 float ki_times_ta_init,
                                                 float kd_over_ta_init,
                                                 float init_threshold,
                                                 float smoothing,
                                                 float output_max_init,
                                                 bool abort_on_max,
                                                 float feedforward_const,
                                                 float feedforward_memory_update,
                                                 bool wait_first_sob)
    : gr::sync_block("feedbackController",
        // stream ports
        gr::io_signature::make(1, 1, sizeof(input_type)), // inputs (min, max)
        gr::io_signature::make(1, 1, sizeof(output_type))) // outputs (min, max)
    // message ports
    // -
{
    // init parameters
    set_feedback(feedback);
    set_feedforward(feedforward);
    set_target_mode(target_mode);
    set_target(target);
    set_rise_time(rise_time);
    set_ta_samples(ta_samples);
    set_kp(kp);
    set_ki_times_ta(ki_times_ta);
    set_kd_over_ta(kd_over_ta);
    set_output_min(output_min);
    set_output_max(output_max);
    set_init(init);
    set_kp_init(kp_init);
    set_ki_times_ta_init(ki_times_ta_init);
    set_kd_over_ta_init(kd_over_ta_init);
    set_init_threshold(init_threshold);
    set_smoothing(smoothing);
    set_output_max_init(output_max_init);
    set_abort_on_max(abort_on_max);
    set_feedforward_const(feedforward_const);
    set_feedforward_memory_update(feedforward_memory_update);
    _burst_ended = wait_first_sob;

    // init internals
    reset_feedback();
}


void feedbackController_impl::set_feedforward(int feedforward){
    d_feedforward = feedforward;
    
    // Stop recording
    if (_feedforward_memory_recording > 0 && d_feedforward != FF_MEMORY_REC){
        _feedforward_memory_recording = 0;               
        std::cout << "[Feedback] recording finished" << std::endl;
    }
}


void feedbackController_impl::set_feedback(bool feedback){
    if (feedback && !d_feedback){ // switched on
        reset_feedback();
    }
    d_feedback = feedback;
}

/**
 * Reset the feedback/feedforward controller
 */
void feedbackController_impl::reset_feedback(){
    _sample_count = 0;
    _input_sum = 0;
    _output = 0;
    _e = 0;
    _e_previous = 0;
    _e_sum = 0;
    _is_init = d_init;
    _last_actual = 0;
    _last_actual_valid = false;
    _smoothed_actual = 0;
    
}

/**
 * Determine the current feedforward value (not updating anything)
 */
float feedbackController_impl::feedforward_value(){
    // feedforward
    if (d_feedforward == FF_CONST){
        return d_feedforward_const;
    }
    if (d_feedforward == FF_MEMORY_PLAY){
        if (_feedforward_memory_index < _feedforward_memory_values.size()){
            return _feedforward_memory_values[_feedforward_memory_index];
        }
    }
    return 0;
}


int feedbackController_impl::work(int noutput_items,
                                  gr_vector_const_void_star& input_items,
                                  gr_vector_void_star& output_items)
{
    auto in = static_cast<const input_type*>(input_items[0]);
    auto out = static_cast<output_type*>(output_items[0]);
    int n = noutput_items;
    std::vector<tag_t> input_tags;


    if (_burst_ended){

        // outside burst
        
        // wait for SOB tag
        pmt::pmt_t key = pmt::intern(std::string("SOB"));
        get_tags_in_window(input_tags, 0, 0, n, key);
        for (auto& tag : input_tags) {
            n = tag.offset - nitems_written(0);
            
            // Burst start
            _burst_ended = false;
            _update_count = 0;
            reset_feedback();
            
            // Initialize output with feed-forward value
            _feedforward_memory_index = 0;
            _output = feedforward_value();
            
            // Start/continue recording
            if (d_feedforward == FF_MEMORY_REC){
                if (_feedforward_memory_recording == 0){
                    _feedforward_memory_values.clear();
                    std::cout << "[Feedback] recording started" << std::endl;                    
                }
                _feedforward_memory_recording += 1;
            }

            break; // process remaining samples until first SOB tag
        }


        // produce zeros and return
        for (int i = 0; i < n; i++) {
            out[i] = 0;
        }
        return n; // number of samples produced




    } else {

        // inside burst
        
        // check for EOB tag
        pmt::pmt_t key = pmt::intern(std::string("EOB"));
        get_tags_in_window(input_tags, 0, 0, n, key);
        for (auto& tag : input_tags) {
            n = tag.offset - nitems_written(0);

            // Burst end
            _burst_ended = true;

            break; // process remaining samples until first EOB tag
        }


        // process only up to next multiple of ta_samples
        n = std::min(n, std::max(d_ta_samples - _sample_count, 0));

        // read input and produce output
        _sample_count += n;
        for (int i = 0; i < n; i++) {
            _input_sum += in[i];
            out[i] = _output;
        }



        // update controller
        if (_sample_count >= d_ta_samples){

            // averaged input
            float actual = _input_sum/_sample_count;
            bool actual_valid = true;
            
            _update_count ++;
            _sample_count = 0;
            _input_sum = 0;
            
            // derivative with respect to ta_samples
            if (d_target_mode == TARGET_SLOPE){
                float slope = actual - _last_actual; 
                bool slope_valid = _last_actual_valid;
                _last_actual = actual;
                _last_actual_valid = true;
                actual = slope;
                actual_valid = slope_valid;
            }

            // feedforward
            _feedforward_memory_index ++;
            _output = feedforward_value();
            
            // feedback
            float kp = (_is_init ? d_kp_init : d_kp),
                  ki_ta = (_is_init ? d_ki_times_ta_init : d_ki_times_ta),
                  kd_ta = (_is_init ? d_kd_over_ta_init : d_kd_over_ta),
                  out_max = (_is_init ? d_output_max_init : d_output_max);
                  
            if (d_feedback && actual_valid){
                
                // smoothing (only for init detection)
                _smoothed_actual = _smoothed_actual * (1-1/d_smoothing) + actual / d_smoothing;

                // init phase
                if (_is_init && _smoothed_actual >= d_init_threshold * d_target){
                    //std::cout << "[Feedback] init phase completed" << std::endl;
                    _is_init = false;
                    // reset error to mitigate overshooting
                    _e_previous = 0;
                    _e = 0;
                    _e_sum = 0;
                }

                // error (control deviation)
                float target = d_target;
                if (_update_count < d_rise_time_updates)
                    target = target * _update_count / d_rise_time_updates; // rise time
                _e_previous = _e;
                _e = target - actual;
                _e_sum += _e;

                // PID controller
                float p = kp * _e;
                float i = ki_ta * _e_sum;
                float d = kd_ta * (_e - _e_previous);
                float output_pid = p + i + d;
                _output += output_pid;
                
                // Update feedforward memory
                if (_feedforward_memory_recording > 0 && _feedforward_memory_index < 999999){
                    if (_feedforward_memory_index >= _feedforward_memory_values.size()){
                        _feedforward_memory_values.resize(_feedforward_memory_index+1, 0);
                    }
                    // Record PID controller output (online algorithm for average)
                    // Note that this assumes that all cycles have equal length
                    _feedforward_memory_values[_feedforward_memory_index] += (output_pid - _feedforward_memory_values[_feedforward_memory_index])/_feedforward_memory_recording;
                    
                }

            }
            

            // output limits
            if (_output < d_output_min){
                _output = d_output_min;

                if (d_feedback && ki_ta != 0 && ki_ta * _e_sum < d_output_min){
                    // prevent integral from growing beyond limit
                    _e_sum = d_output_min / ki_ta;
                }
            }
            if (_output > out_max){
                _output = out_max;

                if (d_feedback && ki_ta != 0 && ki_ta * _e_sum > out_max){
                    // prevent integral from growing beyond limit
                    _e_sum = out_max / ki_ta;
                }
                
                if (d_abort_on_max){
                    // abort burst
                    _output = 0;
                    _burst_ended = true;
                }
            }

            // TODO: log
            //std::cout << "[ELC] " << (_is_init ? "INIT" : "");
            //std::cout << " | target: " << d_target << " | actual: " << actual;
            //std::cout << " | esum: " << _e_sum;
            //std::cout << " | output: " << _output << std::endl;

        }



        return n; // number of samples produced

    }
}

} /* namespace beam_exciter */
} /* namespace gr */
