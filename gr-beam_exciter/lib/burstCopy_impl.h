/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_BURSTCOPY_IMPL_H
#define INCLUDED_BEAM_EXCITER_BURSTCOPY_IMPL_H

#include <gnuradio/beam_exciter/burstCopy.h>

namespace gr {
namespace beam_exciter {

class burstCopy_impl : public burstCopy
{
private:
    // settings
    size_t _itemsize;
    pmt::pmt_t _tag_key_start;
    pmt::pmt_t _tag_key_stop;

	// internals
	std::vector<tag_t> _input_tags;
	bool _copying;

public:
      burstCopy_impl(size_t itemsize, pmt::pmt_t tag_key_start, pmt::pmt_t tag_key_stop);
      ~burstCopy_impl();

      // Where all the action really happens
      void forecast(int noutput_items, gr_vector_int& ninput_items_required);

      int general_work(int noutput_items,
                       gr_vector_int& ninput_items,
                       gr_vector_const_void_star& input_items,
                       gr_vector_void_star& output_items);
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_BURSTCOPY_IMPL_H */
