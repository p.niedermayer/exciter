/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "triggerSwitchV1_impl.h"
#include <gnuradio/io_signature.h>

static const pmt::pmt_t SOB_KEY = pmt::string_to_symbol("tx_sob");
static const pmt::pmt_t EOB_KEY = pmt::string_to_symbol("tx_eob");
static const pmt::pmt_t TIME_KEY = pmt::string_to_symbol("tx_time");
static const pmt::pmt_t FREQ_KEY = pmt::string_to_symbol("tx_freq");
static const pmt::pmt_t COMMAND_KEY = pmt::string_to_symbol("tx_command");


namespace gr {
namespace beam_exciter {


using input_type = gr_complex;
using output0_type = gr_complex;
using output1_type = short;
static int out_sizes[] = { sizeof(output0_type), sizeof(output1_type)};
static std::vector<int> out_signature(out_sizes, out_sizes + sizeof(out_sizes) / sizeof(int));


triggerSwitchV1::sptr triggerSwitchV1::make(int poll_every_samples,
                                            int duration_samples,
                                            int delay_samples,
                                            int manual_trigger,
                                            std::string device_addr,
                                            int gpio_mboard,
                                            std::string gpio_bank,
                                            int gpio_pin,
                                            bool stop_output,
                                            bool add_stream_tags)
{
    return gnuradio::make_block_sptr<triggerSwitchV1_impl>(poll_every_samples,
                                                           duration_samples,
                                                           delay_samples,
                                                           manual_trigger,
                                                           device_addr,
                                                           gpio_mboard,
                                                           gpio_bank,
                                                           gpio_pin,
                                                           stop_output,
                                                           add_stream_tags);
}


/*
 * The private constructor
 */
triggerSwitchV1_impl::triggerSwitchV1_impl(int poll_every_samples,
                                           int duration_samples,
                                           int delay_samples,
                                           int manual_trigger,
                                           std::string device_addr,
                                           int gpio_mboard,
                                           std::string gpio_bank,
                                           int gpio_pin,
                                           bool stop_output,
                                           bool add_stream_tags)
    : gr::block("triggerSwitchV1",
                gr::io_signature::make(1, 1, sizeof(input_type)), // inputs (min, max)
                gr::io_signature::makev(2, 2, out_signature)) // outputs (min, max)
{
    _poll_every_samples = poll_every_samples;
    d_duration_samples = duration_samples;
    d_delay_samples = delay_samples;
    d_manual_trigger = manual_trigger;
    _device_addr = device_addr;
    _gpio_mboard = gpio_mboard;
    _gpio_bank = gpio_bank;
    _gpio_pin = gpio_pin;
    d_stop_output = stop_output;
    _add_stream_tags = add_stream_tags;
    
    _counter_polling = 0;
    _counter = 0;
    _trigger_level = false;
    

    // USRP GPIO Setup
    ////////////////////
    _usrp = ::uhd::usrp::multi_usrp::make(_device_addr);
    
    std::cout << "Available GPIO banks on board " << _gpio_mboard << ": " << std::endl;
    auto banks = _usrp->get_gpio_banks(_gpio_mboard);
    for (auto& bank : banks) {
        std::cout << "* " << bank << std::endl;
    }
        
    // set pin to manual controled input
    // https://files.ettus.com/manual/classuhd_1_1usrp_1_1multi__usrp.html#a57f25d118d20311aca261e6dd252625e
    // CTRL_ATR = DDR_OUT = HIGH = 0xffffffff
    // CTRL_MAN = DDR_INP = LOW = 0
    _usrp->set_gpio_attr(_gpio_bank, "CTRL", 0, (1 << _gpio_pin), _gpio_mboard); // set control to manual
    _usrp->set_gpio_attr(_gpio_bank, "DDR", 0, (1 << _gpio_pin), _gpio_mboard); // set data direction to input
    
    std::cout << "Pin " << _gpio_pin << " on bank " << _gpio_bank << " configured as trigger input" << std::endl;


}



bool triggerSwitchV1_impl::poll_gpio()
{    
    
    // read pin state
    // https://files.ettus.com/manual/classuhd_1_1usrp_1_1multi__usrp.html#a8445587e23f5b18eef863e310351dbad
    int readback = _usrp->get_gpio_attr(_gpio_bank, "READBACK", _gpio_mboard);
    bool trigger_level = (readback & (1 << _gpio_pin)) > 0;

    //GR_LOG_DEBUG(d_logger, boost::format("GPIO polled: level = %d, manual = %d") % (trigger_level?1:0) % d_manual_trigger);
    //std::cout << (trigger_level ? 1 : 0);

    return trigger_level || d_manual_trigger > 0;
    
}

/*
 * Our virtual destructor.
 */
triggerSwitchV1_impl::~triggerSwitchV1_impl() {}

void triggerSwitchV1_impl::forecast(int noutput_items,
                                    gr_vector_int& ninput_items_required)
{
    // forecast how many items on each input we need to produce noutput_items
    ninput_items_required[0] = noutput_items;
}

int triggerSwitchV1_impl::general_work(int noutput_items,
                                       gr_vector_int& ninput_items,
                                       gr_vector_const_void_star& input_items,
                                       gr_vector_void_star& output_items)
{
    auto in = static_cast<const input_type*>(input_items[0]);
    auto out = static_cast<output0_type*>(output_items[0]);
    auto out1 = static_cast<output1_type*>(output_items[1]);
    int n = noutput_items;
    


    // GPIO polling
    /////////////////
    _counter_polling -= n;
    if (_counter_polling <= 0){
        _counter_polling = 0;

        if (d_duration_samples <= 0 || _counter < 0){ // only poll if timer is not active
            _counter_polling = _poll_every_samples;

            bool trigger_level = poll_gpio(); // read gpio trigger pin level
            //add_item_tag(0, nitems_written(0), pmt::string_to_symbol("trg"), pmt::from_bool(trigger_level));


            if (trigger_level && !_trigger_level){
                // pos flank: start burst and counter from zero
                _counter = 0;
                if (_add_stream_tags){
                    add_item_tag(0, nitems_written(0), SOB_KEY, pmt::PMT_T); // start-of-burst tag
                }
            }
            _trigger_level = trigger_level;
        }
    }



    // Switch state
    //////////////////////////////
    // Note: the implementation assumes n << delay and n << duration (i.e. switching only at start of items)
    if (_counter < 0){
        // waiting for start: drop inputs
        consume(0, n);
        if (d_stop_output){
            produce(0, 0);
        } else {
            for(int i = 0; i < n; i++) { 
                out[i] = 0;
            }
            produce(0, n);
        }

    } else if (d_duration_samples <= 0 && !_trigger_level){
        // gate end: end burst with zeros and reset
        consume(0, n);
        for(int i = 0; i < n; i++) {
            out[i] = 0;
        }
        produce(0, n);
        _counter = -1;
        if (_add_stream_tags){
            add_item_tag(0, nitems_written(0)-1, EOB_KEY, pmt::PMT_T); // end-of-burst tag
        }

    } else if (_counter < d_delay_samples){
        // initial delay: insert zeros
        consume(0, 0);
        for(int i = 0; i < n; i++) {
            out[i] = 0;
        }
        produce(0, n);
        _counter = _counter + n;

    } else if (d_duration_samples <= 0){
        // gate active: pass inputs to outputs
        consume(0, n);
        for(int i = 0; i < n; i++) { //memcpy(out, in, noutput_items * sizeof(input_type));
            out[i] = in[i];;
        }
        produce(0, n);
        // no need to increment counter any further in gating mode

    } else if (_counter < d_delay_samples + d_duration_samples){
        // timer active: pass inputs to outputs
        consume(0, n);
        for(int i = 0; i < n; i++) {
            out[i] = in[i];
        }
        produce(0, n);
        _counter = _counter + n;

    } else{
        // timer end: end burst with zeros and reset
        consume(0, n);
        for(int i = 0; i < n; i++) {
            out[i] = 0;
        }
        produce(0, n);
        _counter = -1;
        if (_add_stream_tags){
            add_item_tag(0, nitems_written(0)-1, EOB_KEY, pmt::PMT_T); // end-of-burst tag
        }
    }
    
    
    // Status output
    for(int i = 0; i < n; i++) {
        out1[i] = _trigger_level;
    }
    produce(1, n);


    // Tell runtime system how many output items we produced.
    return WORK_CALLED_PRODUCE;
}

} /* namespace beam_exciter */
} /* namespace gr */
