/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_SWITCHBLOCK_IMPL_H
#define INCLUDED_BEAM_EXCITER_SWITCHBLOCK_IMPL_H

#include <gnuradio/beam_exciter/switchBlock.h>

#define SWITCH_MODE_TAGS_ON_INPUT0 0
#define SWITCH_MODE_TAGS_ON_INPUT1 1
#define SWITCH_MODE_GATE_ON_INPUT1 2

#define SWITCH_INPUT_MODE_DROP           0
#define SWITCH_INPUT_MODE_BLOCK          1
#define SWITCH_INPUT_MODE_BLOCK_ON_TAG   2

#define SWITCH_OUTPUT_MODE_STOP           0
#define SWITCH_OUTPUT_MODE_ZEROS          1

namespace gr {
namespace beam_exciter {

class switchBlock_impl : public switchBlock
{
private:
    // settings
    size_t _itemsize;
	int _mode;
	int d_input_mode;
	int d_output_mode;
	int d_insert_samples;
    pmt::pmt_t _tag_key_start;
    pmt::pmt_t _tag_key_stop;
	pmt::pmt_t _tag_key_ffw;

	// internals
	std::vector<tag_t> _input_tags;
	bool _in_burst;
	bool _copying;
	int _samples_since_sob;
	int _to_drop_until_sob;
	
	// ports
	const pmt::pmt_t _port_active;
	const pmt::pmt_t _port_reset;
	
	
	int drop_input();
	void start_copying(int out_offset);
	void stop_copying(int out_offset);
	
	struct Nitems {int in0; int in1; int out;};
	Nitems tie_nitems(Nitems r);
	

public:
	switchBlock_impl(size_t itemsize, int mode, int input_mode, int output_mode, int insert_samples, pmt::pmt_t tag_key_start, pmt::pmt_t tag_key_stop, pmt::pmt_t tag_key_ffw);
	~switchBlock_impl();

	void set_input_mode(int input_mode);
	void set_output_mode(int output_mode);
	void set_insert_samples(int insert_samples);

	// Where all the action really happens
	void forecast(int noutput_items, gr_vector_int& ninput_items_required);

	int general_work(int noutput_items,
				   gr_vector_int& ninput_items,
				   gr_vector_const_void_star& input_items,
				   gr_vector_void_star& output_items);
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_SWITCHBLOCK_IMPL_H */
