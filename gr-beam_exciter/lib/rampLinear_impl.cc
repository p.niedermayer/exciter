/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "rampLinear_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace beam_exciter {


static const pmt::pmt_t MSG_PORT_RESET = pmt::mp("reset");


using input_type = float;
using output_type = float;
    rampLinear::sptr
    rampLinear::make(float start, float stop, int duration_samples, int delay_samples, pmt::pmt_t tag_value, pmt::pmt_t tag_key, bool keep_last_value)
    {
      return gnuradio::make_block_sptr<rampLinear_impl>(
        start, stop, duration_samples, delay_samples, tag_value, tag_key, keep_last_value);
    }


    /*
     * The private constructor
     */
    rampLinear_impl::rampLinear_impl(float start, float stop, int duration_samples, int delay_samples, pmt::pmt_t tag_value, pmt::pmt_t tag_key, bool keep_last_value)
      : gr::sync_block("rampLinear",
              gr::io_signature::make(0 /* min inputs */, 1 /*max inputs */, sizeof(input_type)),
              gr::io_signature::make(1 /* min outputs */, 1 /*max outputs */, sizeof(output_type)))
    {
        set_start(start);
        set_stop(stop);
        set_duration_samples(duration_samples);
        set_delay_samples(delay_samples);
        
        _tag_key = tag_key;
        _tag_value = tag_value;
        _keep_last_value = keep_last_value;
        
        // init internal variables
        _value = start;
        
        // register message handler
        message_port_register_in(MSG_PORT_RESET);
        set_msg_handler(MSG_PORT_RESET, [this](const pmt::pmt_t msg) {
            // accept any message
            _reset_message_pending = true;
        });
        
        set_tag_propagation_policy(TPP_ALL_TO_ALL);
    }

    rampLinear_impl::~rampLinear_impl() {}

    int rampLinear_impl::work(int noutput_items,
                              gr_vector_const_void_star& input_items,
                              gr_vector_void_star& output_items
    ){
        //auto in = static_cast<const input_type*>(input_items[0]);
        auto out = static_cast<output_type*>(output_items[0]);
        int n = noutput_items;
        uint64_t offset = nitems_written(0);
        
        
        // collect sob from message and stream tags
        _sob_pos.clear();
        if (_reset_message_pending){
            _reset_message_pending = false;
            add_item_tag(0, offset, _tag_key, _tag_value);
            _sob_pos.push_back(0);
        }
        get_tags_in_window(_input_tags, 0, 0, n, _tag_key);
        for (auto& tag : _input_tags) {
            _sob_pos.push_back(tag.offset - offset);
        }
        
        // insert ramp after each sob
        int from = 0, to;
        for (auto& sob : _sob_pos) {
            to = sob;
            fill_ramp(&out[from], to-from, offset+from);
            from = to;
            // reset ramp
            _offset_start = offset + from + d_delay_samples;
            _offset_stop = _offset_start + d_duration_samples;
            _value = d_start;
            _value_increment = (d_stop - d_start) * 1.0 / d_duration_samples;
        }
        to = n;
        fill_ramp(&out[from], to-from, offset+from);
        
        // Tell runtime system how many output items we produced.
        return n;
    }
    
    
    void rampLinear_impl::fill_ramp(float out[], int count, uint64_t offset){
        
        int i = 0;
        // before ramp start
        if (offset < _offset_start){
            int c = _offset_start < offset + count ? _offset_start - offset : count;
            std::fill_n(out, c, _value);
            i += c;
        }
        
        // ramp
        if (offset < _offset_stop){
            int c = _offset_stop < offset + count ? _offset_stop - offset : count;
            for (; i < c; i++){
                out[i] = _value;
                _value += _value_increment;
            }
        }
        
        // after ramp
        if (i < count){
            if (!_keep_last_value) _value = d_start;
            std::fill_n(&out[i], count - i, _value);
        }
        
    }
        

    } /* namespace beam_exciter */
    } /* namespace gr */
