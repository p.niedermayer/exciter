/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "switchBlock_impl.h"
#include <gnuradio/io_signature.h>
#include <cmath>
#include <numeric>


namespace gr {
namespace beam_exciter {


switchBlock::sptr switchBlock::make(size_t itemsize, int mode, int input_mode, int output_mode, int insert_samples, pmt::pmt_t tag_key_start, pmt::pmt_t tag_key_stop, pmt::pmt_t tag_key_ffw){
	return gnuradio::make_block_sptr<switchBlock_impl>(
		itemsize, mode, input_mode, output_mode, insert_samples, tag_key_start, tag_key_stop, tag_key_ffw);
}


/*
 * The private constructor
 */
switchBlock_impl::switchBlock_impl(
	size_t itemsize, int mode, int input_mode, int output_mode, int insert_samples, pmt::pmt_t tag_key_start, pmt::pmt_t tag_key_stop, pmt::pmt_t tag_key_ffw
) : gr::block(
		"switchBlock",
		gr::io_signature::make2(1 /* min inputs */, 2 /* max inputs */, itemsize, sizeof(short)),
		gr::io_signature::make(1 /* min outputs */, 1 /*max outputs */, itemsize)
	),
	// ports
    _port_active(pmt::mp("active")),
    _port_reset(pmt::mp("reset"))
{
	
    message_port_register_out(_port_active);
    message_port_register_out(_port_reset);

	// settings
    _itemsize = itemsize;
	_mode = mode;
	d_input_mode = input_mode;
	d_output_mode = output_mode;
	d_insert_samples = insert_samples;
    _tag_key_start = tag_key_start;
    _tag_key_stop = tag_key_stop;
	_tag_key_ffw = tag_key_ffw;
	
	// internals
	_input_tags.reserve(1);
	_in_burst = false;
	_copying = false;
	_samples_since_sob = 0;	
	_to_drop_until_sob = -1;
	
	
	set_tag_propagation_policy(TPP_CUSTOM); // we handle tags on our own
	
}

/*
 * Our virtual destructor.
 */
switchBlock_impl::~switchBlock_impl() {}

/*
 * Settings
 */
void switchBlock_impl::set_input_mode(int input_mode){
    d_input_mode = input_mode;
	// reset switch
	_in_burst = false;
	_copying = false;
	_to_drop_until_sob = -1;
}

void switchBlock_impl::set_output_mode(int output_mode){
    d_output_mode = output_mode;
}

void switchBlock_impl::set_insert_samples(int insert_samples){
	d_insert_samples = insert_samples;
}

/*
 * Number of samples to drop
 */
int switchBlock_impl::drop_input(){
	if (_mode == SWITCH_MODE_TAGS_ON_INPUT0){
		return -1; // may not block input if waiting for tags on it
	}
	if (d_input_mode == SWITCH_INPUT_MODE_DROP){
		return -1; // drop all
	}
	if (d_input_mode == SWITCH_INPUT_MODE_BLOCK){
		return 0; // block
	}
	if (d_input_mode == SWITCH_INPUT_MODE_BLOCK_ON_TAG){
		return _to_drop_until_sob;
	}
	return -1;
}

/*
 * Forcast
 */
void switchBlock_impl::forecast(int noutput_items, gr_vector_int& ninput_items_required){
	// forecast how many items on each input we need to produce noutput_items
	
	if (_in_burst && _copying){
		ninput_items_required[0] = noutput_items;
	} else {
		int ndrop = drop_input();
		if (ndrop < 0){
			ninput_items_required[0] = noutput_items; // as much as we can possibly get
		} else {
			ninput_items_required[0] = std::min(ndrop, noutput_items);
		}
	}
	
	if (_mode == SWITCH_MODE_TAGS_ON_INPUT0){
		ninput_items_required[1] = 0;
	} else {
		ninput_items_required[1] = noutput_items;
	}
	
}

/*
 * Ensure input and output sample rates are tied according to state and settings
 */
switchBlock_impl::Nitems switchBlock_impl::tie_nitems(Nitems r){
	
	// copy mode: input 0 linked to output
	if (_in_burst && _copying){
		r.out = std::min(r.out, r.in0);
		r.in0 = r.out;
	}
	
	// gate (input 1) not used: ignore it
	if (_mode == SWITCH_MODE_TAGS_ON_INPUT0){
		r.in1 = 0;
	}
	
	// gate (input 1) used: input 1 linked to output
	else {
		r.out = std::min(r.out, r.in1);
		r.in1 = r.out;
		
		// reflect new r.out if input 0 is also linked to output
		if (_in_burst && _copying){
			r.in0 = r.out;
		}
        
	}
	
	return r;
}

/*
 * Start / stop copying
 */
void switchBlock_impl::start_copying(int out_offset){
	_copying = true;
	message_port_pub(_port_active, pmt::cons(_port_active, pmt::PMT_T));
	
	if (_mode != SWITCH_MODE_TAGS_ON_INPUT0){
		// Insert SOB tag manually
		add_item_tag(0, nitems_written(0) + out_offset, _tag_key_start, pmt::PMT_T);
	}
}

void switchBlock_impl::stop_copying(int out_offset){
	_copying = false;
	message_port_pub(_port_active, pmt::cons(_port_active, pmt::PMT_F));
	
	if (_mode != SWITCH_MODE_TAGS_ON_INPUT0){
		// Insert EOB tag manually
		add_item_tag(0, nitems_written(0) + out_offset, _tag_key_stop, pmt::PMT_F);
	}
}

/*
 * Work
 */
int switchBlock_impl::general_work(
	int noutput_items,
	gr_vector_int& ninput_items,
	gr_vector_const_void_star& input_items,
	gr_vector_void_star& output_items
){
	
	// maximum samples to read/produce
	Nitems n = Nitems{ninput_items[0], ninput_items[1], noutput_items};
	n = tie_nitems(n);	
	
	
	// ======================
	// check for tags or gate
	// ======================
	
	bool next_in_burst = _in_burst;	
	
	if (_mode == SWITCH_MODE_TAGS_ON_INPUT0 || _mode == SWITCH_MODE_TAGS_ON_INPUT1){
		// check for tags
		pmt::pmt_t key = _in_burst ? _tag_key_stop : _tag_key_start;
		if (_mode == SWITCH_MODE_TAGS_ON_INPUT0){
			// tags on input 0
			get_tags_in_window(_input_tags, 0, 0, n.in0, key);
			if (!_input_tags.empty()){
				next_in_burst = !_in_burst; // toggle switch
				n.in0 = _input_tags.front().offset - nitems_read(0); // consume only until tag
				if (_in_burst) n.in0 += 1; // include EOB sample
			}
		} else {
			// tags on input 1
			get_tags_in_window(_input_tags, 1, 0, n.in1, key);
			if (!_input_tags.empty()){
				next_in_burst = !_in_burst; // toggle switch
				n.in1 = _input_tags.front().offset - nitems_read(1); // consume only until tag
				if (_in_burst) n.in1 += 1; // include EOB sample
			}
		}
		
	} else
	if (_mode == SWITCH_MODE_GATE_ON_INPUT1){
		auto in1 = static_cast<const short*>(input_items[1]);
		
		// check for flanks on input 1
		bool level = _in_burst ? false : true;
		for (int i = 0; i < n.in1; i++) {
			if ((in1[i] > 0) == level){
				next_in_burst = !_in_burst; // toggle switch
				n.in1 = i; // consume only until flank
				if (_in_burst) n.in1 += 1; // include EOB sample
				break;
			}
		}
		
	}
	
	// handle updates
	n = tie_nitems(n);
	
	
	// =======================
	// input / output handling
	// =======================
	
	const uint8_t* i0ptr = (const uint8_t*) input_items[0];
	uint8_t* optr = (uint8_t*) output_items[0];
	
	if (_in_burst){
		// =============
		// Switch closed
		// =============
		
		if (!_copying){
			
			// insert zeros at beginning
			n.in0 = 0;
			n.out = std::min(n.out, d_insert_samples - _samples_since_sob);
			n = tie_nitems(n);
			memset(optr, 0, n.out * _itemsize);
			_samples_since_sob += n.out;
			
			if (_samples_since_sob >= d_insert_samples){
				start_copying(n.out);
			}
			
		} else {
			
			// copy input to output
			std::memcpy(optr, i0ptr, n.in0 * _itemsize);
			
			// propagate tags from input 0
			std::vector<tag_t> tags;
			get_tags_in_window(tags, 0, 0, n.in0);
			for (auto& tag : tags) {
				tag.offset += nitems_written(0) - nitems_read(0); // correct offset
                add_item_tag(0, tag);
            }
			
		}
		
		
	} else {
		// ===========
		// Switch open
		// ===========
		
		if (d_input_mode == SWITCH_INPUT_MODE_BLOCK_ON_TAG && _to_drop_until_sob < 0){
			// check how much we can still drop on input 0 until SOB
			get_tags_in_window(_input_tags, 0, 0, n.in0, _tag_key_ffw);
			if (!_input_tags.empty()){
				_to_drop_until_sob = _input_tags.front().offset - nitems_read(0);
			}
		}
        
        // output: stop or zeros
        if (d_output_mode == SWITCH_OUTPUT_MODE_STOP){
            // stop output
            n.out = 0;
            // not calling tie_nitems here, because we still consume input
        } else {
            // produce zeros
            memset(optr, 0, n.out * _itemsize);
        }
		
		// input: drop or block
		int nmaxdrop = drop_input();
		if (nmaxdrop >= 0 && nmaxdrop < n.in0){
			n.in0 = nmaxdrop; // maximum we can drop
		}
		
		if (d_input_mode == SWITCH_INPUT_MODE_BLOCK_ON_TAG && _to_drop_until_sob > 0){
			_to_drop_until_sob -= n.in0;
		}
		
	}
	
	
	// ============
	// switch state
	// ============
	
	if (!_in_burst && next_in_burst){
		// switch closed at SOB
		_samples_since_sob = 0;
		if (d_insert_samples <= 0){
			// start copying immediatly
			start_copying(n.out);
		}		
		
	} else if (_in_burst && !next_in_burst){
		// switch opened at EOB
		stop_copying(n.out - 1);
		message_port_pub(_port_reset, pmt::cons(_port_reset, pmt::PMT_NIL));
		_to_drop_until_sob = -1;
	}
	
	_in_burst = next_in_burst;
	
	
	// ===============
	// Consume/produce
	// ===============
	
	consume(0, n.in0);
	consume(1, n.in1);
	produce(0, n.out);
	
	//std::cout << "[Switch] (" << n.in0 << ", " << n.in1 << ") --> " << n.out << std::endl; // DEBUG
	
	return WORK_CALLED_PRODUCE;

}

} /* namespace beam_exciter */
} /* namespace gr */
