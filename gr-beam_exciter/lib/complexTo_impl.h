/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_COMPLEXTO_IMPL_H
#define INCLUDED_BEAM_EXCITER_COMPLEXTO_IMPL_H

#include <gnuradio/beam_exciter/complexTo.h>

namespace gr {
namespace beam_exciter {

class complexTo_impl : public complexTo
{
private:
    bool d_toreal;

public:
            
    complexTo_impl(bool toreal);
    ~complexTo_impl();
    
    void set_toreal(bool toreal) override { d_toreal = toreal; }

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_COMPLEXTO_IMPL_H */
