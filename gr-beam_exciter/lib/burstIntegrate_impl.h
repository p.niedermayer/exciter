/* -*- c++ -*- */
/*
 * Copyright 2024 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_BURSTINTEGRATE_IMPL_H
#define INCLUDED_BEAM_EXCITER_BURSTINTEGRATE_IMPL_H

#include <gnuradio/beam_exciter/burstIntegrate.h>

namespace gr {
namespace beam_exciter {

class burstIntegrate_impl : public burstIntegrate
{
private:
    // settings
    pmt::pmt_t _tag_key_reset;
    double _initial_value;
    double _gain;

    // internals
    std::vector<tag_t> _input_tags;
    double _sum = 0;

public:
    burstIntegrate_impl(pmt::pmt_t tag_key_reset, double initial_value, double gain);
    ~burstIntegrate_impl();

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_BURSTINTEGRATE_IMPL_H */
