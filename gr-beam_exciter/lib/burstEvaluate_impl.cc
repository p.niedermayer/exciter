/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "burstEvaluate_impl.h"
#include <gnuradio/io_signature.h>
#include <cmath>
#include <numeric>

namespace gr {
namespace beam_exciter {

    using input_type = float;
    burstEvaluate::sptr
    burstEvaluate::make(
        pmt::pmt_t tag_key_start,
        int start_offset_samp,
        pmt::pmt_t tag_key_stop,
        int eval_samp,
        int function,
        int pre_avg_samp,
        bool enable,
        int repeat_every_samp,
        int repeat_n_times,
        bool repeat_msg_avg,
        bool delay_msg_eob
    ){
        return gnuradio::make_block_sptr<burstEvaluate_impl>(
            tag_key_start, start_offset_samp, tag_key_stop, eval_samp, function, pre_avg_samp, enable, repeat_every_samp, repeat_n_times, repeat_msg_avg, delay_msg_eob
        );
    }


    /*
     * The private constructor
     */
    burstEvaluate_impl::burstEvaluate_impl(
        pmt::pmt_t tag_key_start,
        int start_offset_samp,
        pmt::pmt_t tag_key_stop,
        int eval_samp,
        int function,
        int pre_avg_samp,
        bool enable,
        int repeat_every_samp,
        int repeat_n_times,
        bool repeat_msg_avg,
        bool delay_msg_eob
    ) : gr::sync_block("burstEvaluate",
              gr::io_signature::make(1 /* min inputs */, 1 /* max inputs */, sizeof(input_type)),
              gr::io_signature::make(0, 0, 0)),
        _port_write(pmt::mp("msg"))
    {
        
        _tag_key_start = tag_key_start;
        set_start_offset_samp(start_offset_samp);
        _tag_key_stop = tag_key_stop;
        set_eval_samp(eval_samp);
        set_function(function);
        set_pre_avg_samp(pre_avg_samp);
        set_enable(enable);
        set_repeat_every_samp(repeat_every_samp);
        set_repeat_n_times(repeat_n_times);
        set_repeat_msg_avg(repeat_msg_avg);
        set_delay_msg_eob(delay_msg_eob);
        
        message_port_register_out(_port_write);

    }
    
    
    /*
     * Evaluation algorithms
     * - RMS = root mean squared
     * - STD = standard deviation
     * - MEAN = mean value
     * - MABS = mean absolute value
     * - CV = coefficient of variation
     */
    
    void burstEvaluate_impl::eval_method_start(){
        // Initialize evaluation
        switch (d_function){
            case FUNCT_RMS:
                _eval_n = 0;
                _eval_sqsum = 0;
                break;
            case FUNCT_STD:
            case FUNCT_MEAN:
            case FUNCT_CV:
                _eval_n = 0;
                _eval_mean = 0;
                _eval_m2 = 0;
                break;
            case FUNCT_MABS:
                _eval_n = 0;
                _eval_asum = 0;
                break;
        }
    }
    
    void burstEvaluate_impl::eval_method_update(double value){
        // Evaluate a single sample
        switch (d_function){
            case FUNCT_RMS:
                // Sum over squared values
                _eval_n++;
                _eval_sqsum += value * value;
                break;
            case FUNCT_STD:
            case FUNCT_MEAN:
            case FUNCT_CV: {
                // Welford's algorithm
                _eval_n++;
                double delta = value - _eval_mean;
                _eval_mean += delta/_eval_n;
                _eval_m2 += delta*(value - _eval_mean);
                } break;
            case FUNCT_MABS:
                // Sum over absolute values
                _eval_n++;
                _eval_asum += abs(value);
                break;
        }
    }
    
    double burstEvaluate_impl::eval_method_result(){
        // Finish evaluation
        switch (d_function){
            case FUNCT_RMS:
                return _eval_n > 0 ? sqrt(_eval_sqsum / _eval_n) : NAN;
            case FUNCT_STD:
                return _eval_n > 0 ? sqrt(_eval_m2 / _eval_n) : NAN;
            case FUNCT_MEAN:
                return _eval_n > 0 ? _eval_mean : NAN;
            case FUNCT_CV:
                return _eval_n > 0 && _eval_mean != 0 ? sqrt(_eval_m2 / _eval_n) / _eval_mean : NAN;
            case FUNCT_MABS:
                return _eval_n > 0 ? _eval_asum / _eval_n : NAN;
        }
        return NAN;        
    }
    

    /*
     * Our virtual destructor.
     */
    burstEvaluate_impl::~burstEvaluate_impl() {}

    int burstEvaluate_impl::work(int nitems,
                                 gr_vector_const_void_star& input_items,
                                 gr_vector_void_star& output_items
    ){
        if (!d_enable){
            // drop everything
            return nitems;
        }
        
        if (_reset_state){
            _reset_state = false;
            _state = STATE_WAIT_SOB;
        }
        
        auto in = static_cast<const input_type*>(input_items[0]);
        int nconsumed = 0;
        bool eob = false;
        int eob_at = nitems;
        
        // check for EOB tags
        get_tags_in_window(_input_tags, 0, nconsumed, nitems-nconsumed, _tag_key_stop);
        if (!_input_tags.empty()){ //  && pmt::is_true(_input_tags.back().value)
            // tag received, make sure to stop at first EOB
            eob = true;
            eob_at = _input_tags.front().offset - nitems_read(0);
            if (nitems > eob_at + 1) nitems = eob_at + 1;
            // DEBUG
            //std::cout << "[BurstEvaluate] Got EOB, processing only " << eob << " of " << nitems << " samples now" << std::endl;
        }
        
        if (_state == STATE_WAIT_SOB){
            // check for SOB tags
            get_tags_in_window(_input_tags, 0, nconsumed, nitems-nconsumed, _tag_key_start);
            if (!_input_tags.empty()){ //  && pmt::is_true(_input_tags.back().value)
                // tag received, seek to first SOB and reset
                int skip = _input_tags.front().offset - nitems_read(0);
                nconsumed = skip + 1; // also consume SOB tagged sample itself
                _sample_counter = 0;
                _repeat_counter = 0;
                _repeat_result_mean = 0;
                _state = STATE_WAIT_START;
                
                // DEBUG
                //std::cout << "[BurstEvaluate] Got SOB, skipping to sample after SOB at abs " << nitems_read(0) + nconsumed << std::endl;
                //std::cout << "[BurstEvaluate] State: STATE_WAIT_SOB -> STATE_WAIT_START" << std::endl;
            } else {
                // drop everything so far
                nconsumed = nitems;
            }
        }
        
        if (!eob){
            // check for additional SOB tags
            get_tags_in_window(_input_tags, 0, nconsumed, nitems-nconsumed, _tag_key_start);
            if (!_input_tags.empty()){ //  && pmt::is_true(_input_tags.back().value)
                // Unexpected second SOB tag without EOB in between, make sure to EOB one sample before it
                eob = true;
                eob_at = _input_tags.front().offset - nitems_read(0) - 1;
                if (nitems > eob_at + 1) nitems = eob_at + 1;
                // DEBUG
                //std::cout << "[BurstEvaluate] Missing EOB, processing only " << eob << " of " << nitems << " samples now" << std::endl;
            }
        }
        
        
        
        
        if (_state == STATE_WAIT_START){
            // skip start offset samples
            if (_sample_counter < d_start_offset_samp){
                int skip;
                if (d_start_offset_samp - _sample_counter > nitems - nconsumed){
                    skip = nitems - nconsumed; // skip all left
                } else {
                    skip = d_start_offset_samp - _sample_counter; // skip until start
                }
                if (eob && nconsumed + skip >= eob_at){
                    skip = eob_at - nconsumed; // don't skip beyond eob
                }
                _sample_counter += skip;
                nconsumed += skip;
                // DEBUG
                //std::cout << "[BurstEvaluate] Wait start skipped " << skip << " samples until abs " << nitems_read(0) + nconsumed << std::endl;
            }
            if (eob && nconsumed == eob_at){
                // got EOB, start over new
                _state = STATE_WAIT_SOB;
                // DEBUG
                //std::cout << "[BurstEvaluate] Wait start completed at EOB. State: STATE_WAIT_START -> STATE_WAIT_SOB" << std::endl;
            } else if (_sample_counter == d_start_offset_samp){ // TODO: use >= to account for user changing d_start_offset_samp
                // completed waiting, start evaluating
                _state = STATE_START_EVAL;
                // DEBUG
                //std::cout << "[BurstEvaluate] Wait start completed at OFFSET abs " << nitems_read(0) + nconsumed << ". State: STATE_WAIT_START -> STATE_START_EVAL" << std::endl;
            }
            if (_sample_counter > d_start_offset_samp){
                // DEBUG : this can happen if user changes d_start_offset_samp during processing!
                //std::cout << "[BurstEvaluate] ERROR this must never happen! a6845ef" << std::endl;  
            }
        }
        
        if (_state == STATE_START_EVAL){
            // make sure to publish any pending messages
            message_publish();
            // initialize evaluation
            _sample_counter = 0;
            eval_method_start();
            _state = STATE_EVAL;
            // DEBUG
            //std::cout << "[BurstEvaluate] Evaluation started. State: STATE_START_EVAL -> STATE_EVAL" << std::endl;
            //std::cout << "[BurstEvaluate] Publishing message" << std::endl;  
        }
        
        if (_state == STATE_EVAL){
            // calculate how many samples to evaluate
            int stop = nitems;
            if (d_eval_samp <= 0){
                // evaluate until EOB
                if (eob && eob_at < stop){
                    stop = eob_at;
                    _state = STATE_FINISH_EVAL_THEN_END;
                    // DEBUG
                    //std::cout << "[BurstEvaluate] Evaluation to be stopped at EOB at abs " << nitems_read(0) + stop << ". State: STATE_EVAL -> STATE_FINISH_EVAL_THEN_END" << std::endl;
                }
            } else {
                // evaluate until d_eval_samp
                if (nconsumed + d_eval_samp - _sample_counter < nitems){
                    stop = nconsumed + d_eval_samp - _sample_counter;
                    _state = STATE_FINISH_EVAL;
                    // DEBUG
                    //std::cout << "[BurstEvaluate] Evaluation to be stopped after d_eval_samp consumed at abs " << nitems_read(0) + stop << ". State: STATE_EVAL -> STATE_FINISH_EVAL" << std::endl;
                }
                // or if EOB comes before, end immediately
                if (eob && eob_at < stop){
                    stop = nconsumed;
                    _state = STATE_END_BURST;
                    // DEBUG
                    //std::cout << "[BurstEvaluate] Evaluation to be aborted due to EOB. State: STATE_EVAL -> STATE_END_BURST" << std::endl;
                }
            }
                
            // do the actual evaluation
            for (int o = nconsumed; o + d_pre_avg_samp < stop; o += d_pre_avg_samp) {
                double window_average = std::accumulate(&in[o], &in[o]+d_pre_avg_samp, 0.0) / d_pre_avg_samp;
                eval_method_update(window_average);
                _sample_counter += d_pre_avg_samp;
                nconsumed += d_pre_avg_samp;
            }
            
            // DEBUG
            //if (_state != STATE_EVAL){
            //    std::cout << "[BurstEvaluate] Processed " << _sample_counter << " samples (of " << d_eval_samp << " requested)" << std::endl;
            //}
            
        }
        
        if (_state == STATE_FINISH_EVAL or _state == STATE_FINISH_EVAL_THEN_END){
            // calculate result
            double result = eval_method_result();
            if (not std::isnan(result)){
                if (not d_repeat_msg_avg){
                    // publish on message port
                    message_register(result);
                }
                _repeat_counter += 1;
                _repeat_result_mean += (result - _repeat_result_mean)/_repeat_counter;
            }
            if (_state == STATE_FINISH_EVAL_THEN_END){
                _state = STATE_END_BURST;                
            } else {
                _state = STATE_WAIT_EVAL;                
            }
            
            // DEBUG
            //std::cout << "[BurstEvaluate] Evaluation completed at abs " << nitems_read(0) + nconsumed << (d_repeat_msg_avg ? ". Result saved." : ". MSG published.") << " State: STATE_FINISH_EVAL -> STATE_WAIT_EVAL" << std::endl;
            
        }
        
        if (_state == STATE_WAIT_EVAL){
            if (d_repeat_n_times > 0 and _repeat_counter >= d_repeat_n_times){
                // repetitions completed
                _state = STATE_END_BURST;
                // DEBUG
                //std::cout << "[BurstEvaluate] Repeat completed (" << _repeat_counter << " of " << d_repeat_n_times << "). State: STATE_WAIT_EVAL -> STATE_END_BURST" << std::endl;            
            } else {
                // wait for next evaluation to start
                int start = nconsumed + std::max(0, d_repeat_every_samp - _sample_counter);
                if (eob && eob_at < start){
                    // EOB before next start, so end
                    _state = STATE_END_BURST;
                    // DEBUG
                    //std::cout << "[BurstEvaluate] EOB while waiting for next evaluation. State: STATE_WAIT_EVAL -> STATE_END_BURST" << std::endl;            
                } else {
                    // drop samples
                    int skip;
                    if (start <= nitems){
                        // drop until start
                        skip = start - nconsumed;
                        _state = STATE_START_EVAL;
                        // DEBUG
                        //std::cout << "[BurstEvaluate] Waiting for next evaluation completed at abs " << nitems_read(0) + nconsumed + skip << ". State: STATE_WAIT_EVAL -> STATE_START_EVAL" << std::endl;            
                    } else {
                        // drop all left
                        skip = nitems - nconsumed;
                    }
                    _sample_counter += skip;
                    nconsumed += skip;                    
                }
            }
        }
        
        if (_state == STATE_END_BURST){
            // end the burst
            if (d_repeat_msg_avg && _repeat_counter > 0){
                // publish on message port
                message_register(_repeat_result_mean);
            }
            // skip until eob or end
            int skip = (eob ? eob_at + 1 : nitems) - nconsumed;
            nconsumed += skip;
            _state = STATE_WAIT_SOB;
            // DEBUG
            //std::cout << "[BurstEvaluate] Burst ended. State: STATE_END_BURST -> STATE_WAIT_SOB" << std::endl;            
                    
        }
        
        
        // publish message
        if (_message_pending && (!d_delay_msg_eob || (eob && eob_at <= nconsumed))){
            message_publish();
            // DEBUG
            //std::cout << "[BurstEvaluate] Publishing message (EOB: " << eob << ")" << std::endl;
        }
        
        
        return nconsumed;
        
    
    }
    
    
    void burstEvaluate_impl::message_register(double value){
        // publish pending message (if any)
        message_publish();
        
        // register pending message
        _message_pending = true;
        _message_value = value;        
    }
    
    void burstEvaluate_impl::message_publish(){
        if (!_message_pending) return;
        
        message_port_pub(_port_write, pmt::cons(_port_write, pmt::from_double(_message_value)));
        _message_pending = false;
    }

} /* namespace beam_exciter */
} /* namespace gr */
