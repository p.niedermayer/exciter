/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_TRIGGERSWITCHV2_IMPL_H
#define INCLUDED_BEAM_EXCITER_TRIGGERSWITCHV2_IMPL_H

#include <gnuradio/beam_exciter/triggerSwitchV2.h>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/rfnoc_graph.hpp>
#include <uhd/rfnoc/radio_control.hpp>
#include <gnuradio/uhd/api.h>
#include <gnuradio/uhd/usrp_block.h>
#include <gnuradio/uhd/rfnoc_graph.h>
#include <stdexcept>

using namespace uhd;

namespace gr {
namespace beam_exciter {

class triggerSwitchV2_impl : public triggerSwitchV2
{
private:
    bool d_enable_polling;
    int _poll_every_samples;
    int d_duration_samples;
    int d_delay_samples;
    int d_manual_trigger;    
    std::shared_ptr<rfnoc::radio_control> _radio;
    usrp::multi_usrp::sptr _usrp;
    std::string _device_addr;
    int _device_index;
    int _radio_index;
    std::string _gpio_bank;
    int _gpio_pin;
    int d_input_mode;
    bool d_stop_output;
    bool _add_stream_tags;
    
    int _counter_polling;
    int _counter;
    bool _input_received_tag;
    bool _trigger_level;
    int _trigger_counter;
    std::vector<tag_t> _input_tags;
    
    const pmt::pmt_t _port_outp;
    const pmt::pmt_t _port_trg;
    const pmt::pmt_t _port_reset;
    
    
    void reset_counter(int eob_tag_offset);

public:
    triggerSwitchV2_impl(bool enable_polling,
                         int poll_every_samples,
                         int duration_samples,
                         int delay_samples,
                         int manual_trigger,
                         ::gr::uhd::rfnoc_graph::sptr ptr_graph,
                         std::string device_addr,
                         int device_index,
                         int radio_index,
                         std::string gpio_bank,
                         int gpio_pin,
                         int input_mode,
                         bool stop_output,
                         bool add_stream_tags);
    ~triggerSwitchV2_impl();
    
    void set_duration_samples(int duration_samples) override { d_duration_samples = duration_samples; }
    void set_delay_samples(int delay_samples) override { d_delay_samples = delay_samples; }
    void set_manual_trigger(int manual_trigger) override { d_manual_trigger = manual_trigger; }
    void set_stop_output(bool stop_output) override { d_stop_output = stop_output; }
    void set_input_mode(int input_mode); // override { d_input_mode = input_mode;  }
    void set_enable_polling(bool enable_polling);


    // Where all the action really happens
    void forecast(int noutput_items, gr_vector_int& ninput_items_required);

    int general_work(int noutput_items,
                     gr_vector_int& ninput_items,
                     gr_vector_const_void_star& input_items,
                     gr_vector_void_star& output_items);
    
    bool poll_gpio();
    
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_TRIGGERSWITCHV2_IMPL_H */
