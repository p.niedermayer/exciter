#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2023 Philipp Niedermayer.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


from gnuradio import gr
import pmt

class replayRestarter(gr.sync_block):
    """
    This block creates a stop command message followed by a start command message
    formatted as a dictionary to pass to the RFNoC Replay block
    """
    def __init__(self, port=0, offset=-1, size=-1, time=-1, repeat=True):
        gr.sync_block.__init__(self, name="replayRestarter",
                               in_sig=None, out_sig=None)
        
        self.message_port_register_out(pmt.intern("cmd"))
        self.logger = gr.logger(self.alias())
        
        # build the dict
        self.dict = {
            'port': port,
            'repeat': repeat,
        }
        # If the user does not specify a command offset, size, or time,
        # don't add to the command. The properties will be used for
        # offset and size, and the command will happen immediately.
        if offset != -1:
            self.dict['offset'] = offset
        if size != -1:
            self.dict['size'] = size
        if time != -1:
            self.dict['time'] = time
    
        self.trigger = None
        
    def set_trigger(self, trigger):
        if trigger != self.trigger:
            self.send_cmd()
        self.trigger = trigger
     
    def send_cmd(self):
        # publish the message
        for cmd in ("stop", "play"):
            self.dict['command'] = cmd
            self.logger.debug("Sending {command} command...".format(**self.dict))
            self.message_port_pub(pmt.intern("cmd"), pmt.to_pmt(self.dict))
        
