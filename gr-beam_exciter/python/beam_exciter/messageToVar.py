#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2024 Philipp Niedermayer.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import numpy
from gnuradio import gr
import pmt

class messageToVar(gr.sync_block):
    """
    Accepts a message pair or dict and sets the flow graph variable(s) accordingly
    """
    def __init__(self, top_block=None):
        gr.sync_block.__init__(self,
            name="messageToVar",
            in_sig=[],
            out_sig=None)
        
        self.top_block = top_block
        
        self.message_port_register_in(pmt.intern("msg"))
        self.set_msg_handler(pmt.intern("msg"), self.handle_msg)
        
    def handle_msg(self, msg):
        if pmt.is_pair(msg):
            name, value = pmt.to_python(msg)
            self.set_variable(name, value)
        elif pmt.is_dict(msg):
            for name, value in pmt.to_python(msg).items():
                self.set_variable(name, value)
        else:
            print("Message ignored as it is neither a pair nor dict:", msg)

            
    def set_variable(self, name, value):
        """Set values of variables using top-glock setters"""
        self._set_variable(self.top_block, name, value)
            
    def _set_variable(self, block, name, val):
        try:
            if "." in name:
                subblock, name = name.split(".", maxsplit=1)
                return self._set_variable(getattr(block, subblock), name, val)
            setter = getattr(block, f"set_{name}")
            setter(val)
            
        except AttributeError as e:
            print("AttributeError:", e)
            print(f"ERROR: Failed to set variable `{name}` to `{val}`")
    
    def work(self, input_items, output_items):
        print("You shall not work!")
        return 0
