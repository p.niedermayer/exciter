#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2023 Philipp Niedermayer.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import numpy
from gnuradio import gr
import datetime
import textwrap
import os

class variableFileDump(gr.sync_block):
    """
    docstring for block variableFileDump
    """
    def __init__(self, filename="", enable=True):
        gr.sync_block.__init__(self,
            name="variableFileDump",
            in_sig=[],
            out_sig=[])
        self.set_filename(filename)
        self.set_enable(enable)
    
    def set_filename(self, filename):
        self.filename = filename
    
    def set_enable(self, enable):
        self.enable = enable
        
    def save(self, save, data=None):
        if self.enable and save:
            # handle optional time placeholders in filename
            fname = datetime.datetime.now().strftime(self.filename)
            # create intermediate folders
            os.makedirs(os.path.dirname(fname), exist_ok=True)
            # save data
            with open(fname, "w") as f:
                f.write(textwrap.dedent(data))

    def work(self, input_items, output_items):
        print("You shall not work!")
        return 0
