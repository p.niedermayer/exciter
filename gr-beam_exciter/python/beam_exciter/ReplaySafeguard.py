#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2023 Philipp Niedermayer.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import numpy as np
from gnuradio import gr
import pmt


class ReplaySafeguard(gr.sync_block):
    """
    Safeguard for Replay block usage
    """

    def __init__(self):
        gr.sync_block.__init__(self,
            name='Replay usage safeguard',
            in_sig=[],
            out_sig=[]
        )
        self.message_port_register_in(pmt.intern("tick"))
        self.set_msg_handler(pmt.intern("tick"), self.handle_tick_msg)
        self.message_port_register_in(pmt.intern("rate"))
        self.set_msg_handler(pmt.intern("rate"), self.handle_rate_msg)
        self.message_port_register_in(pmt.intern("cmd"))
        self.set_msg_handler(pmt.intern("cmd"), self.handle_cmd_msg)
        self.message_port_register_out(pmt.intern("busy"))
        self.message_port_register_out(pmt.intern("dont_close"))
        
        self.is_busy = False
        self.recording_started = False
        self.playback_started = False
        self.is_playing = False
    
    def report_msg(self):
        self.message_port_pub(
            pmt.intern("busy"), 
            pmt.cons(
                pmt.to_pmt("busy"), 
                pmt.from_bool(
                    self.recording_started and self.is_busy
                )
            )
        )
        self.message_port_pub(
            pmt.intern("dont_close"), 
            pmt.cons(
                pmt.to_pmt("dont_close"), 
                pmt.from_bool(
                    (self.recording_started and self.is_busy) or
                    self.is_playing or
                    not (self.recording_started or self.playback_started)
                )
            )
        )
    
    def handle_tick_msg(self, msg):
        self.report_msg()
        self.is_busy = False
    
    def handle_cmd_msg(self, msg):
        d = pmt.to_python(msg)
        if d.get("command", None) == "play":
            self.playback_started = True
            self.is_playing = True
        if d.get("command", None) == "stop":
            self.is_playing = False
        if d.get("command", None) == "record":
            self.recording_started = True
        self.report_msg()
    
    def handle_rate_msg(self, msg):
        print(msg)
        self.is_busy = True
