#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2023 Philipp Niedermayer.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import numpy
from gnuradio import gr
from datetime import datetime
import pmt
import os

class Optimizer(gr.sync_block):
    """
    Optimizer block, see GR YML for details
    """
    def __init__(self, top_block, enable=False, variables=[], options={}, state_widget_name=None, method="pybobyqa", logfile=None):
        gr.sync_block.__init__(self,
            name="Optimizer",
            in_sig=[],
            out_sig=None)
        self.message_port_register_in(pmt.intern("obj"))
        self.set_msg_handler(pmt.intern("obj"), self.handle_objective)
        
        self.top_block = top_block
        self.enable = enable
        self.variables = variables
        self.options = options
        self.method = method
        self.logfilepattern = logfile
        self.logfile = None
        self.solution_callback = lambda: None
        self.error_callback = lambda: None
        self.opti = OptimizerAlgorithm(error_callback=lambda:(self.ui_state(color='red'),self.error_callback()))
        self.state_widget = getattr(top_block, state_widget_name) if state_widget_name is not None else None
    
    def log(self, text=None, data=None):
        if text is not None:
            print("[Optimizer]", text)
        if self.logfile:
            os.makedirs(os.path.dirname(self.logfile), exist_ok=True)
            with open(self.logfile, 'a') as f:
                if text is not None:
                    f.write(f"# {text}\n")
                if data is not None:
                    f.write(",\t".join([str(d) for d in data])+"\n")
    
    def set_enable(self, enable):
        """En- or disables the block, starting or stoping the optimizer if required"""
        self.enable = enable
        if not enable:
            self.stop_optimizer()
        elif not self.opti.is_running():
            self.start_optimizer()
    
    def set_logfile(self, logfile):
        self.logfilepattern = logfile
    
    def set_variables(self, variables):
        self.enable = False
        self.opti.abort()
        self.variables = variables
    
    def set_options(self, options):
        self.enable = False
        self.opti.abort()
        self.options = options
    
    def set_method(self, method):
        self.enable = False
        self.opti.abort()
        self.method = method
    
    def handle_objective(self, msg):
        """Handle the objective value received on the message port"""
        if self.enable:
            # update objective
            f = pmt.to_float(pmt.cdr(msg) if pmt.is_pair(msg) else msg)
            self.opti.answer_objective(f)
            
            self.handle_parameters()
    
    def handle_parameters(self):
        """Handle a parameter update request"""
        if self.enable:
            # set next values (if any)
            x = self.opti.ask_parameters(blocking=True, timeout=0.1)
            if x is not None:
                self.set_variable_values(x)
            elif not self.opti.is_running():
                self.stop_optimizer()
                if self.opti.failed:
                    self.error_callback()
                    self.ui_state(color='red')
                else:
                    self.solution_callback()
                    self.ui_state(color='limegreen')
    
    def start_optimizer(self):
        """Start the optimizer algorithm"""
        
        if self.method.lower() == "pybobyqa":
            Algorithm = PyBobyqaOptimizerAlgorithm
        elif self.method.lower() == "rasterscan":
            Algorithm = RasterScanOptimizerAlgorithm
        else:
            raise ValueError(f"Unknown method '{self.method.lower()}', choices are 'pybobyqa' or 'raster'")
        
        self.logfile = None if self.logfilepattern is None else datetime.now().strftime(self.logfilepattern)
        if self.logfile: print("[Optimizer] Logging to file", os.path.abspath(self.logfile))
        
        self.opti = Algorithm(
            error_callback = lambda: (self.ui_state(color='red'), self.error_callback()),
            log_callback = self.log,
        )
        
        self.log(f"{self.opti}")
        self.log(f"Variables: {self.variables}")
        self.log(f"Options: {self.options}")
        
        try:
            x0 = self.get_variable_values()
        except AttributeError as e:
            print("AttributeError:", e)
            self.log("ERROR: Failed to get initial variable values")
            self.enable = False
            self.error_callback()
            self.ui_state(color='red')
        else:
            self.ui_state(color='yellow')
            self.opti.start(x0, **self.options)
            self.handle_parameters()
    
    def stop_optimizer(self):
        """Stop the optimizer algorithm (aborting if required) and apply best values"""
        self.enable = False
        self.opti.abort()
        # apply best parameters
        best = self.opti.best()
        if best is not None:
            x, f = best
            self.set_variable_values(x, verbose=True)
        self.ui_state(color=None)
    
    def get_variable_values(self):
        """Get values of variables using top-glock getters"""
        return [self._get_variable(self.top_block, name) for name in self.variables]
    
    def _get_variable(self, block, name):
        if "." in name:
            subblock, name = name.split(".", maxsplit=1)
            return self._get_variable(getattr(block, subblock), name)
        return float(getattr(block, f"get_{name}")())
    
    def set_variable_values(self, x, verbose=False):
        """Set values of variables using top-glock setters"""
        for val, name in zip(x, self.variables):
            if verbose: print(f"{name:20s}    {val}")
            self._set_variable(self.top_block, name, val)
    
    def _set_variable(self, block, name, val):
        try:
            if "." in name:
                subblock, name = name.split(".", maxsplit=1)
                return self._set_variable(getattr(block, subblock), name, val)
            # round to 9 significant digits and apply value
            val = float(f"{val:.9g}")
            setter = getattr(block, f"set_{name}")
            setter(val)
            
        except AttributeError as e:
            print("AttributeError:", e)
            self.log("ERROR: Failed to set variable `name` to `val`")
            self.enable = False
            self.error_callback()
            self.ui_state(color='red')
    
    def ui_state(self, color=None):
        """Update UI state with color"""
        if self.state_widget is not None:
            try:
                call = lambda: self._set_widget_color(self.state_widget, color)
                call()
                # delay to make sure it comes *after* the color change due to push button press
                import threading
                threading.Timer(0.1, call).start() # 0.1 s
            except Exception as e:
                print("Warning: Failed to update widget UI state:", e)
                pass # ignore errors, ui state is "nice to have" but not needed
        for name in self.variables:
            self._mark_variable(self.top_block, name, color=color)
        
    def _mark_variable(self, block, name, color=None):
        try:
            if "." in name:
                subblock, name = name.split(".", maxsplit=1)
                return self._mark_variable(getattr(block, subblock), name, color=color)
            
            widget = getattr(block, f"_{name}_tool_bar")
            self._set_widget_color(widget, color, "QLineEdit")            
        except Exception as e:
            print("Warning: Failed to update UI state:", e)
            pass # ignore errors, marking is "nice to have" but not needed
    
    def _set_widget_color(self, widget, color, qclass=None):
        from PyQt5 import Qt
        style = "" if color is None else f"background-color: {color};"
        if qclass is not None: style = f"{qclass} {{ {style} }}"
        Qt.QMetaObject.invokeMethod(widget, "setStyleSheet", Qt.Q_ARG("QString", style))
        #widget.setStyleSheet("" if color is None else f"background-color: {color};")
            

    def work(self, input_items, output_items):
        # Only message ports, nothing to do
        return 0










# pip install Py-BOBYQA
import pybobyqa
import numpy as np
import threading
import itertools


class StuckError(RuntimeError):
    pass


class OptimizerAlgorithm:
    def __init__(self, new_parameter_callback=None, error_callback=None, log_callback=lambda a, **kwargs: print(a)):
        self._thread = None
        self._lock = threading.Condition() # threading.RLock()
        self._x_lock = threading.Condition(self._lock)
        self._obj_lock = threading.Condition(self._lock)
        self.state = None
        self._abort = False
        self.new_parameter_callback = new_parameter_callback
        self.error_callback = error_callback
        self.log_callback = log_callback
        self.x = None
        self.obj = None
        self.failed = False
        self.history = []       
    
    def start(self, x0, **kwargs):
        """Start the optimizer"""
        if self._thread is not None:
            self.abort()
        self._abort = False
        self.failed = False
        self.history = []
        # Spawn thread
        self._thread = threading.Thread(target=self.__run, args=(x0,), kwargs=kwargs, daemon=True)
        self._thread.start()
    
    def abort(self):
        """Requests and waits for the optimizer to abort"""
        if self._thread is not None:
            self._abort = True
            with self._lock:
                self._obj_lock.notify()
            self._thread.join()
            self._thread = None
    
    def is_running(self):
        """If optimizer is running"""
        return self._thread is not None and self._thread.is_alive()
    
    def ask_parameters(self, blocking=False, timeout=None):
        """Ask for new parameters to evaluate
        May return None if not blocking, finished or timeout
        """        
        with self._lock:
            if not blocking or self.x is not None or not self.is_running():
                return self.x   
            self._x_lock.wait(timeout)
            return self.x
    
    def answer_objective(self, f):
        """Answer objective for recent parameters after evaluation"""
        with self._lock:
            self.x = None
            self.obj = f
            self._obj_lock.notify()
    
    def best(self):
        """Best parameters found so far"""
        if self.history:
            return min(self.history, key=lambda h: h[1])
    
    def call(self, callback, x0, **kwargs):
        """Implement the optimizer logic here
        
        Args:
          callback: Function to evaluate a set of values x yielding an objective. 
                    Signature: f(x) -> objective
          x0: Initial values for x
          kwargs: Keyword arguments passed to start method
        """
        raise NotImplementedError()
    
    def __run(self, x0, **kwargs):
        """Thread method to run optimizer"""        
        with self._lock:
            self.x = x0
        
        try:
            self.log_callback(f"Initialised: {x0}")
            self.call(self.__callback, x0, **kwargs)
            self.log_callback(f"Completed")
        
        except StopIteration as e:
            self.log_callback(f"{e}")
        except StuckError as e:
            self.log_callback(f"{e}")
            self.failed = True
            if self.error_callback is not None: self.error_callback()
        except:
            self.failed = True
            if self.error_callback is not None: self.error_callback()
            raise
            
        finally:
            with self._lock:
                self.x = None
                self._x_lock.notify_all()
            best = self.best()
            if best is not None:
                x, f = best
                self.log_callback(f"Best was: {x} --> {f}")
        
    
    def __callback(self, x):
        """Thread method for optimizer callback.
        Sets new parameters and waits until objective is evaluated
        """
        with self._lock:
            # Register new parameters
            self.x = x
            self._x_lock.notify_all()
            if self.new_parameter_callback is not None:
                self.new_parameter_callback(x)
        
            # Wait for objective
            self._obj_lock.wait()
            
            self.history.append((x, self.obj))
            self.log_callback(f"Step {len(self.history)}: {x} --> {self.obj}")
            self.log_callback(data=[len(self.history), *x, self.obj])
            
            # catch bug in Py-BOBYQA getting stuck somtimes
            if len(self.history) > 3:
                # compare most recent x vectors
                prev = [self.history[i][0] for i in (-1, -2, -3)]
                no_change = np.any([numpy.allclose(prev[i], prev[i+1], atol=0) for i in range(len(prev)-1)])
                if no_change:
                    raise StuckError("Aborted (got stuck)")
        
            if self._abort:
                self.x = None
                raise StopIteration("Aborted")
        
            return self.obj


class PyBobyqaOptimizerAlgorithm(OptimizerAlgorithm):
    
    def call(self, callback, x0, **kwargs):
        """Implement the optimizer logic here
        
        Args:
          callback: Function to evaluate a set of values x yielding an objective. 
                    Signature: f(x) -> objective
          x0: Initial values for x
          kwargs: Keyword arguments passed to start method
        """
        
        # Default parameters
        kwargs = dict(dict(
            objfun_has_noise=True,
            #seek_global_minimum=False,
            #print_progress=True,
            #bounds=None,
        ), **kwargs)
        
        # pybobyqa needs list, not tuples
        x0 = list(x0)
        if "bounds" in kwargs:
            kwargs["bounds"] = list([list(b) for b in kwargs["bounds"]])
        
        # do the optimisation
        solution = pybobyqa.solve(callback, x0, **kwargs)
        self.log_callback(f"{solution.msg}")
        self.log_callback(f"Solution: {solution.x} --> {solution.f}")
    
    def __repr__(self):
        return f"OptimizerAlgorithm Py-BOBYQA {pybobyqa.__version__}"
    

class RasterScanOptimizerAlgorithm(OptimizerAlgorithm):

    @staticmethod
    def order_coarse_fine(array):
        results = []
        parts = [array]
        while len(results) < len(array):
            new_parts = []
            for part in parts:
                # split parts
                n = len(part)//2
                left, p, right = part[:n], part[n], part[n:]
                # add center point to results
                if p not in results: results.append(p)
                # add remaining parts to list
                c = len(new_parts)//2
                new_parts.insert(c, left)
                new_parts.insert(c, right)
            parts = new_parts
        return results
    
    @staticmethod
    def raster_scan(*arrays):
        # order with increasing granularity
        arrays = [RasterScanOptimizerAlgorithm.order_coarse_fine(a) for a in arrays]
        
        # n-dimensional scan
        points = []
        max_n = np.max([len(a) for a in arrays])
        for n in range(1, max_n+1):
            for x in itertools.product(*[a[:n] for a in arrays]):
                if x in points: continue
                points.append(x)                
                yield x
    
    @staticmethod
    def random_scan(*arrays):
        ranges = list(itertools.product(*arrays))
        np.random.shuffle(ranges)
        for x in ranges:
            yield x
    
    @staticmethod
    def range_scan(*arrays):
        for x in itertools.product(*arrays):
            yield x
    
    
    def call(self, callback, x0, **kwargs):
        """Implement the optimizer logic here
        
        Args:
          callback: Function to evaluate a set of values x yielding an objective. 
                    Signature: f(x) -> objective
          x0: Initial values for x
          kwargs: Keyword arguments passed to start method
        """
        
        bounds = kwargs["bounds"]
        steps = kwargs["steps"]
        assert len(x0) == len(bounds[0]) and len(x0) == len(bounds[1]), "Bounds and variables must be of equal length"
        
        ranges = []
        for i, (mi, ma) in enumerate(zip(*bounds)):
            ranges.append(np.linspace(mi, ma, steps))
            
        # do the scan
        for x in RasterScanOptimizerAlgorithm.raster_scan(*ranges):
            obj = callback(x)
    
    def __repr__(self):
        return f"OptimizerAlgorithm Raster-Scan"
    