#
# Copyright 2008,2009 Free Software Foundation, Inc.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

# The presence of this file turns this directory into a Python package

'''
This is the GNU Radio BEAM_EXCITER module. Place your Python package
description here (python/__init__.py).
'''
import os
import pmt

# import pybind11 generated symbols into the beam_exciter namespace
try:
    # this might fail if the module is python-only
    from .beam_exciter_python import *
except ModuleNotFoundError:
    pass

# import any pure python here
from .variableFileDump import variableFileDump
from .Optimizer import Optimizer
from .replayAutoStart import replayAutoStart
from .ReplaySafeguard import ReplaySafeguard
from .saveRestoreVariables import saveRestoreVariables
from .replayRestarter import replayRestarter
from .messageToVar import messageToVar
from .qtgui_numericEntry import NumericEntry
#

# helper methods
def ensure_pmt(x=pmt.PMT_NIL):    
    return x if isinstance(x, pmt.pmt_python.pmt_base) else pmt.intern(x)
