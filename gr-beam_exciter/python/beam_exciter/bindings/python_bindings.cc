/*
 * Copyright 2020 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

#include <pybind11/pybind11.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

namespace py = pybind11;

// Headers for binding functions
/**************************************/
// The following comment block is used for
// gr_modtool to insert function prototypes
// Please do not delete
/**************************************/
// BINDING_FUNCTION_PROTOTYPES(
    void bind_triggerSwitchV1(py::module& m);
    void bind_triggerSwitchV2(py::module& m);
    void bind_LevelProbe(py::module& m);
    void bind_rampArbitrary(py::module& m);
    void bind_rampLinear(py::module& m);
    void bind_burstFileSink(py::module& m);
    void bind_feedbackController(py::module& m);
    void bind_burstEvaluate(py::module& m);
    void bind_complexTo(py::module& m);
    void bind_gpioTrigger(py::module& m);
    void bind_switchBlock(py::module& m);
    void bind_burstCopy(py::module& m);
    void bind_burstIntegrate(py::module& m);
// ) END BINDING_FUNCTION_PROTOTYPES


// We need this hack because import_array() returns NULL
// for newer Python versions.
// This function is also necessary because it ensures access to the C API
// and removes a warning.
void* init_numpy()
{
    import_array();
    return NULL;
}

PYBIND11_MODULE(beam_exciter_python, m)
{
    // Initialize the numpy C API
    // (otherwise we will see segmentation faults)
    init_numpy();

    // Allow access to base block methods
    py::module::import("gnuradio.gr");
    py::module::import("gnuradio.blocks");

    /**************************************/
    // The following comment block is used for
    // gr_modtool to insert binding function calls
    // Please do not delete
    /**************************************/
    // BINDING_FUNCTION_CALLS(
    bind_triggerSwitchV1(m);
    bind_triggerSwitchV2(m);
    bind_LevelProbe(m);
    bind_rampArbitrary(m);
    bind_rampLinear(m);
    bind_burstFileSink(m);
    bind_feedbackController(m);
    bind_burstEvaluate(m);
    bind_complexTo(m);
    bind_gpioTrigger(m);
    bind_switchBlock(m);
    bind_burstCopy(m);
    bind_burstIntegrate(m);
    // ) END BINDING_FUNCTION_CALLS
}