/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr, beam_exciter, __VA_ARGS__)
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


 static const char *__doc_gr_beam_exciter_burstEvaluate = R"doc()doc";


 static const char *__doc_gr_beam_exciter_burstEvaluate_burstEvaluate = R"doc()doc";


 static const char *__doc_gr_beam_exciter_burstEvaluate_make = R"doc()doc";


 static const char *__doc_gr_beam_exciter_burstEvaluate_set_start_offset_samp = R"doc()doc";


 static const char *__doc_gr_beam_exciter_burstEvaluate_set_eval_samp = R"doc()doc";


 static const char *__doc_gr_beam_exciter_burstEvaluate_set_function = R"doc()doc";


 static const char *__doc_gr_beam_exciter_burstEvaluate_set_pre_avg_samp = R"doc()doc";


 static const char *__doc_gr_beam_exciter_burstEvaluate_set_enable = R"doc()doc";


 static const char *__doc_gr_beam_exciter_burstEvaluate_set_repeat_every_samp = R"doc()doc";


 static const char *__doc_gr_beam_exciter_burstEvaluate_set_repeat_n_times = R"doc()doc";


 static const char *__doc_gr_beam_exciter_burstEvaluate_set_repeat_msg_avg = R"doc()doc";


 static const char *__doc_gr_beam_exciter_burstEvaluate_set_delay_msg_eob = R"doc()doc";
