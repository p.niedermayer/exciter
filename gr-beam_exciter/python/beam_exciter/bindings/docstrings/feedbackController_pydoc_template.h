/*
 * Copyright 2024 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr, beam_exciter, __VA_ARGS__)
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


static const char* __doc_gr_beam_exciter_feedbackController = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_feedbackController_0 =
    R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_feedbackController_1 =
    R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_make = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_feedback = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_feedforward = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_target_mode = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_target = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_rise_time = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_ta_samples = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_kp = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_ki_times_ta = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_kd_over_ta = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_output_min = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_output_max = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_init = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_kp_init = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_ki_times_ta_init =
    R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_kd_over_ta_init =
    R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_init_threshold =
    R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_smoothing = R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_output_max_init =
    R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_abort_on_max =
    R"doc()doc";


static const char* __doc_gr_beam_exciter_feedbackController_set_feedforward_const =
    R"doc()doc";


static const char*
    __doc_gr_beam_exciter_feedbackController_set_feedforward_memory_update = R"doc()doc";
