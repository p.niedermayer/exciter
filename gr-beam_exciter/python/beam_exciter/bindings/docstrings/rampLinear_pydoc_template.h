/*
 * Copyright 2024 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr, beam_exciter, __VA_ARGS__)
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


static const char* __doc_gr_beam_exciter_rampLinear = R"doc()doc";


static const char* __doc_gr_beam_exciter_rampLinear_rampLinear_0 = R"doc()doc";


static const char* __doc_gr_beam_exciter_rampLinear_rampLinear_1 = R"doc()doc";


static const char* __doc_gr_beam_exciter_rampLinear_make = R"doc()doc";


static const char* __doc_gr_beam_exciter_rampLinear_set_start = R"doc()doc";


static const char* __doc_gr_beam_exciter_rampLinear_set_stop = R"doc()doc";


static const char* __doc_gr_beam_exciter_rampLinear_set_duration_samples = R"doc()doc";


static const char* __doc_gr_beam_exciter_rampLinear_set_delay_samples = R"doc()doc";
