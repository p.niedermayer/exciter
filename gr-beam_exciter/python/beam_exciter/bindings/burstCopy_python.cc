/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

/***********************************************************************************/
/* This file is automatically generated using bindtool and can be manually edited  */
/* The following lines can be configured to regenerate this file during cmake      */
/* If manual edits are made, the following tags should be modified accordingly.    */
/* BINDTOOL_GEN_AUTOMATIC(0)                                                       */
/* BINDTOOL_USE_PYGCCXML(0)                                                        */
/* BINDTOOL_HEADER_FILE(burstCopy.h)                                        */
/* BINDTOOL_HEADER_FILE_HASH(676efcf98ba4a3ca7c7daf36e7fe56c7)                     */
/***********************************************************************************/

#include <pybind11/complex.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include <gnuradio/beam_exciter/burstCopy.h>
// pydoc.h is automatically generated in the build directory
#include <burstCopy_pydoc.h>

void bind_burstCopy(py::module& m)
{

    using burstCopy = ::gr::beam_exciter::burstCopy;


    py::class_<burstCopy, gr::block, gr::basic_block, std::shared_ptr<burstCopy>>(
        m, "burstCopy", D(burstCopy))

        .def(py::init(&burstCopy::make),
             py::arg("itemsize"),
             py::arg("tag_key_start") = pmt::intern(std::string("SOB")),
             py::arg("tag_key_stop") = pmt::intern(std::string("EOB")),
             D(burstCopy, make))


        ;
}
