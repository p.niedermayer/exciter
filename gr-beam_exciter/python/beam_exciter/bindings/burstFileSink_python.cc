/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

/***********************************************************************************/
/* This file is automatically generated using bindtool and can be manually edited  */
/* The following lines can be configured to regenerate this file during cmake      */
/* If manual edits are made, the following tags should be modified accordingly.    */
/* BINDTOOL_GEN_AUTOMATIC(0)                                                       */
/* BINDTOOL_USE_PYGCCXML(0)                                                        */
/* BINDTOOL_HEADER_FILE(burstFileSink.h)                                        */
/* BINDTOOL_HEADER_FILE_HASH(93e0f3a1e4248f1e36b8ac7fe70370ca)                     */
/***********************************************************************************/

#include <pybind11/complex.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include <gnuradio/beam_exciter/burstFileSink.h>
// pydoc.h is automatically generated in the build directory
#include <burstFileSink_pydoc.h>

void bind_burstFileSink(py::module& m)
{

    using burstFileSink = ::gr::beam_exciter::burstFileSink;


    py::class_<burstFileSink,
               gr::sync_block,
               gr::block,
               gr::basic_block,
               std::shared_ptr<burstFileSink>>(m, "burstFileSink", D(burstFileSink))

        .def(py::init(&burstFileSink::make),
             py::arg("itemsize"),
             py::arg("filename"),
             py::arg("tag_key_start"),
             py::arg("tag_key_stop"),
             py::arg("length"),
             py::arg("enable"),
             D(burstFileSink, make))


        .def("set_length",
             &burstFileSink::set_length,
             py::arg("length"),
             D(burstFileSink, set_length))


        .def("set_filename",
             &burstFileSink::set_filename,
             py::arg("filename"),
             D(burstFileSink, set_filename))


        .def("set_enable",
             &burstFileSink::set_enable,
             py::arg("enable"),
             D(burstFileSink, set_enable))

        ;
}
