# 
# Copyright 2023 Philipp Niedermayer.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


id: beam_exciter_burstEvaluate
label: Burst Evaluate
category: '[Beam exciter]'

documentation: |-
    Block to evaluate a function on a bursty stream
    
    The burst is controlled by stream tags with key `tag_key_start` (refered to as SOB tag
    in the following) and `tag_key_stop` (refered to as EOB tag in the following).
    
    We start after receiving an SOB tag.
    The first `start_offset_samp` samples after the SOB tag are ignored.
    Then the first evaluation starts.
    The evaluation function can be changed by `function` (see below) and it is possible to average over `pre_avg_samp` subsequent samples before the function is applied.
    The evaluation continues for `eval_samp` samples, or, if `eval_samp` is zero, until an EOB tag.
    If `eval_samp` is not zero, but the EOB tag comes before `eval_samp` samples are processed, the evaluation is aborted.
    Unless the evaluation is aborted, the result of the evaluation is emitted as a message on the message port.
    Then we wait for the next SOB tag to repeat.
    
    It is possible to make repetitive evaluations in between SOB and EOB tags.
    Therefore `repeat_count` must be set to specify the number of repetitions (a value of zero or less means infinite)
    The value of `repeat_every_samp` can be used to specify a fixed sampling number at which the evaluation should be repeated.
    Note that `repeat_every_samp` must be equal to or larger than `eval_samp`.
    For repetitive evaluations it is possible to emit a single message at the end of the burst rather than after every single evaluation.
    This is done by setting `repeat_msg_avg`, and then the message value will be the average of all evaluations in the burst.
    
    By default, messages are emitted immediately after the evaluation is done. The setting `delay_msg_eob` can be used to
    delay message emission until an EOB tag is received (or a new evaluation starts).
    
    The following evaluation functions are available:
    - RMS (0): Root mean square
    - STD (1): Standard deviation
    - MEAN (2): Mean value
    - MABS (3): Mean absolute value
    - CV (4): Coefficient of variation
    
    Note that RMS and STD are only identical if the mean is zero

templates:
  imports: |-
      from gnuradio import beam_exciter
      import pmt
  make: beam_exciter.burstEvaluate(${tag_key_start}, ${start_offset_samp}, ${tag_key_stop}, ${eval_samp}, ${function}, ${pre_avg_samp}, ${enable}, ${repeat_every_samp}, ${repeat_count}, ${repeat_msg_avg}, ${delay_msg_eob})
  callbacks:
  - set_start_offset_samp(${start_offset_samp})
  - set_eval_samp(${eval_samp})
  - set_function(${function})
  - set_pre_avg_samp(${pre_avg_samp})
  - set_enable(${enable})
  - set_repeat_every_samp(${repeat_every_samp})
  - set_repeat_n_times(${repeat_count})
  - set_repeat_msg_avg(${repeat_msg_avg})
  - set_delay_msg_eob(${delay_msg_eob})

parameters:
- id: enable
  label: Enabled
  dtype: bool
  default: True
  hide: part
- id: tag_key_start
  label: Start tag key
  dtype: raw
  default: pmt.intern("tx_sob")
  hide: part
- id: start_offset_samp
  label: Start offset [samples]
  dtype: int
  default: 0
  hide: ${ "none" if start_offset_samp>0 else "part" }
- id: tag_key_stop
  label: Stop tag key
  dtype: raw
  default: pmt.PMT_NIL
  hide: part

- id: eval_samp
  label: Eval length [samples]
  dtype: int
  default: 1000
  hide: ${ "none" if eval_samp>0 else "part" }
- id: function
  label: Eval function
  dtype: int
  default: 0
  options: [0, 1, 2, 3, 4]
  option_labels: ["0 RMS", "1 STD", "2 MEAN", "3 MABS", "4 CV"]
- id: pre_avg_samp
  label: Eval pre avg [samples]
  dtype: int
  default: 1
  hide: ${ "none" if pre_avg_samp>1 else "part" }

- id: repeat_every_samp
  label: Repeat every [samples]
  dtype: int
  default: 0
  hide: ${ "none" if repeat_every_samp>0 else "part" }
- id: repeat_count
  label: Repeat count
  dtype: int
  default: 1
  hide: ${ "none" if repeat_count>1 else "part" }
- id: repeat_msg_avg
  label: Repeat avg
  dtype: bool
  default: False
  hide: part

- id: delay_msg_eob
  label: Delay msg until EOB
  dtype: bool
  default: False
  hide: part

inputs:
- label: in
  domain: stream
  dtype: float

outputs:
- label: msg
  domain: message
  id: msg
  optional: true

#  'file_format' specifies the version of the GRC yml format used in the file
#  and should usually not be changed.
file_format: 1
