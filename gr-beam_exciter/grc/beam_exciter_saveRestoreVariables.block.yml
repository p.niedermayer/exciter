id: beam_exciter_saveRestoreVariables
label: Variable Save Restore
category: '[Beam exciter]'

documentation: |-
    Block to save and load variable values
    
    Variables are saved to (replace FLOWGRAPH with actual flowgraph name):
    ~/.config/gnuradio/FLOWGRAPH.variables.sav
    
    Slot is a string that can be used to save multiple sets.
    
    Slot descr. var. is an optional string variable, the value of which
    will be kept in sync with the selected slot. This allows to label
    the slots and read the label back before actually loading the data.
    Use the name of a QT GUI Entry widget string variable here.
    
    Variables is a list of variable names or a comma-separated
    string of variables names to save or restore.
    
    Saving and restoring can either be triggered by messages
    on the respective ports (message content is ignored), or
    by a dedicated variable on positive flank (e.g. if the 
    value changes from 0 -> 1 or False -> True).
    
    For GUI values it is strongly recommended to use the
    following widgets, which will reflect an update of the
    value on the display:
    - QT GUI Entry
    - QT GUI Chooser
    - QT GUI Check Box
    
    

templates:
  imports: |-
      from gnuradio import beam_exciter
  make: beam_exciter.saveRestoreVariables(self, ${slot}, ${variables}, ${save_trigger}, ${restore_trigger}, ${description_variable})
  callbacks:
  - set_slot(${slot})
  - set_variables(${variables})
  - set_save_trigger(${save_trigger})
  - set_restore_trigger(${restore_trigger})
  

parameters:
- id: slot
  label: Slot
  dtype: string
  default: "default"
- id: description_variable
  label: Slot descr. var.
  dtype: raw
  default: '""'
- id: variables
  label: Variables
  dtype: _multiline
  default: |
    """
    samp_rate,variable_1,
    other_variable
    """
- id: show_msg_ports
  label: Show msg ports
  dtype: enum
  default: "Yes"
  options: ["Yes", "No"]
  option_attributes:
      show: [True, False]
  hide: 'part'
- id: save_trigger
  label: Save trg
  dtype: raw
  default: '"variable_to_trigger_save"'
  hide: ${'all' if show_msg_ports.show else 'part'}
- id: restore_trigger
  label: Restore trg
  dtype: raw
  default: '"variable_to_trigger_restore"'
  hide: ${'all' if show_msg_ports.show else 'part'}

inputs:
- label: save
  domain: message
  id: save
  optional: true
  hide: ${not show_msg_ports.show}
- label: restore
  domain: message
  id: restore
  optional: true
  hide: ${not show_msg_ports.show}


#  'file_format' specifies the version of the GRC yml format used in the file
#  and should usually not be changed.
file_format: 1
