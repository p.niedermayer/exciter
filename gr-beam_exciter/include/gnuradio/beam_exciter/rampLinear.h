/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_RAMPLINEAR_H
#define INCLUDED_BEAM_EXCITER_RAMPLINEAR_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API rampLinear : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<rampLinear> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of beam_exciter::rampLinear.
     *
     * To avoid accidental use of raw pointers, beam_exciter::rampLinear's
     * constructor is in a private implementation
     * class. beam_exciter::rampLinear::make is the public interface for
     * creating new instances.
     */
    static sptr make(float start = 0,
                     float stop = 1,
                     int duration_samples = 10000,
                     int delay_samples = 0,
                     pmt::pmt_t tag_value = pmt::mp("start"),
                     pmt::pmt_t tag_key = pmt::PMT_T,
                     bool keep_last_value = true);
                     
    /*!
     * \brief Update start parameter
     */
    virtual void set_start(float start) = 0;
    /*!
     * \brief Update stop parameter
     */
    virtual void set_stop(float stop) = 0;
    /*!
     * \brief Update duration_samples parameter
     */
    virtual void set_duration_samples(int duration_samples) = 0;
    /*!
     * \brief Update delay_samples parameter
     */
    virtual void set_delay_samples(int delay_samples) = 0;
    
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_RAMPLINEAR_H */
