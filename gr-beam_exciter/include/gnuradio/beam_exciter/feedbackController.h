/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_FEEDBACKCONTROLLER_H
#define INCLUDED_BEAM_EXCITER_FEEDBACKCONTROLLER_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/sync_block.h>

// Feedforward parameter constants
#define FF_OFF         0
#define FF_CONST       1
#define FF_MEMORY_REC  2
#define FF_MEMORY_PLAY 3

// Target mode parameters
#define TARGET_VALUE   0
#define TARGET_SLOPE   1



namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API feedbackController : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<feedbackController> sptr;

    /*!
     * \brief Feedback Controller
     */
    static sptr make(bool feedback = true,
                     int feedforward = FF_OFF,
                     int target_mode = TARGET_VALUE,
                     float target = 1,
                     int rise_time = 0,
                     int ta_samples = 1000,
                     float kp = 1,
                     float ki_times_ta = 0,
                     float kd_over_ta = 0,
                     float output_min = -999,
                     float output_max = 999,
                     bool init = false,
                     float kp_init = 1,
                     float ki_times_ta_init = 0,
                     float kd_over_ta_init = 0,
                     float init_threshold = 0.8,
                     float smoothing = 100,
                     float output_max_init = 999,
                     bool abort_on_max = false,
                     float feedforward_const = 0,
                     float feedforward_memory_update = 0.2,
                     bool wait_first_sob = false);

    virtual void set_feedback(bool feedback) = 0;
    virtual void set_feedforward(int feedforward) = 0;
    virtual void set_target_mode(int target_mode) = 0;
    virtual void set_target(float target) = 0;
    virtual void set_rise_time(int rise_time) = 0;
    virtual void set_ta_samples(int ta_samples) = 0;
    virtual void set_kp(float kp) = 0;
    virtual void set_ki_times_ta(float ki_times_ta) = 0;
    virtual void set_kd_over_ta(float kd_over_ta) = 0;
    virtual void set_output_min(float output_min) = 0;
    virtual void set_output_max(float output_max) = 0;
    virtual void set_init(bool init) = 0;
    virtual void set_kp_init(float kp_init) = 0;
    virtual void set_ki_times_ta_init(float ki_times_ta_init) = 0;
    virtual void set_kd_over_ta_init(float kd_over_ta_init) = 0;
    virtual void set_init_threshold(float init_threshold) = 0;
    virtual void set_smoothing(float smoothing) = 0;
    virtual void set_output_max_init(float output_max_init) = 0;
    virtual void set_abort_on_max(bool abort_on_max) = 0;
    virtual void set_feedforward_const(float feedforward_const) = 0;
    virtual void set_feedforward_memory_update(float feedforward_memory_update) = 0;

};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_FEEDBACKCONTROLLER_H */
