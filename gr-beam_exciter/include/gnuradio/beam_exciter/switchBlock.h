/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_SWITCHBLOCK_H
#define INCLUDED_BEAM_EXCITER_SWITCHBLOCK_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/block.h>

namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API switchBlock : virtual public gr::block
{
public:
    typedef std::shared_ptr<switchBlock> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of beam_exciter::switchBlock.
     *
     * To avoid accidental use of raw pointers, beam_exciter::switchBlock's
     * constructor is in a private implementation
     * class. beam_exciter::switchBlock::make is the public interface for
     * creating new instances.
     */
    static sptr make(size_t itemsize,
                     int mode = 0,
                     int input_mode = 0,
                     int output_mode = 0,
                     int insert_samples = 0,
                     pmt::pmt_t tag_key_start = pmt::intern(std::string("SOB")),
                     pmt::pmt_t tag_key_stop = pmt::intern(std::string("EOB")),
                     pmt::pmt_t tag_key_ffw = pmt::intern(std::string("START")));

    /*!
     * \brief Update input_mode parameter
     */
    virtual void set_input_mode(int input_mode) = 0;

    /*!
     * \brief Update output_mode parameter
     */
    virtual void set_output_mode(int output_mode) = 0;

    /*!
     * \brief Update insert_samples parameter
     */
    virtual void set_insert_samples(int insert_samples) = 0;

};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_SWITCHBLOCK_H */
