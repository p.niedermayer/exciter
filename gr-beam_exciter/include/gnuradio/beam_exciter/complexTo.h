/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_COMPLEXTO_H
#define INCLUDED_BEAM_EXCITER_COMPLEXTO_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API complexTo : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<complexTo> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of beam_exciter::complexTo.
     *
     * To avoid accidental use of raw pointers, beam_exciter::complexTo's
     * constructor is in a private implementation
     * class. beam_exciter::complexTo::make is the public interface for
     * creating new instances.
     */
    static sptr make(bool toreal = true);
                     
    /*!
     * \brief Update toreal parameter
     */
    virtual void set_toreal(bool toreal) = 0;
    
};

} // namespace beam_exciter
} // namespace gr
#endif /* INCLUDED_BEAM_EXCITER_COMPLEXTO_H */
