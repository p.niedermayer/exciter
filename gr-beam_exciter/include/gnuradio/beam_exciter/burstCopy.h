/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_BURSTCOPY_H
#define INCLUDED_BEAM_EXCITER_BURSTCOPY_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/block.h>

namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API burstCopy : virtual public gr::block
{
public:
    typedef std::shared_ptr<burstCopy> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of beam_exciter::burstCopy.
     *
     * To avoid accidental use of raw pointers, beam_exciter::burstCopy's
     * constructor is in a private implementation
     * class. beam_exciter::burstCopy::make is the public interface for
     * creating new instances.
     */
    static sptr make(size_t itemsize,
                     pmt::pmt_t tag_key_start = pmt::intern(std::string("SOB")),
                     pmt::pmt_t tag_key_stop = pmt::intern(std::string("EOB")));
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_BURSTCOPY_H */
