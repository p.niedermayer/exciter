/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_BURSTEVALUATE_H
#define INCLUDED_BEAM_EXCITER_BURSTEVALUATE_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/sync_block.h>

// Evaluation function constants
#define FUNCT_RMS      0
#define FUNCT_STD      1
#define FUNCT_MEAN     2
#define FUNCT_MABS     3
#define FUNCT_CV       4

namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API burstEvaluate : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<burstEvaluate> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of beam_exciter::burstEvaluate.
     *
     * To avoid accidental use of raw pointers, beam_exciter::burstEvaluate's
     * constructor is in a private implementation
     * class. beam_exciter::burstEvaluate::make is the public interface for
     * creating new instances.
     */
    static sptr make(pmt::pmt_t tag_key_start = pmt::intern(std::string("SOB")),
                     int start_offset_samp = 0,
                     pmt::pmt_t tag_key_stop = pmt::intern(std::string("EOB")),
                     int eval_samp = 1000,
                     int function = FUNCT_RMS,
                     int pre_avg_samp = 1,
                     bool enable = true,
                     int repeat_every_samp = 0,
                     int repeat_n_times = 1,
                     bool repeat_msg_avg = false,
                     bool delay_msg_eob = false);
    /*!
     * \brief Update start_offset_samp parameter
     */
    virtual void set_start_offset_samp(int samples) = 0;
    /*!
     * \brief Update eval_samp parameter
     */
    virtual void set_eval_samp(int samples) = 0;
    /*!
     * \brief Update function parameter
     */
    virtual void set_function(int function) = 0;
    /*!
     * \brief Update pre_avg_samp parameter
     */
    virtual void set_pre_avg_samp(int samples) = 0;
    /*!
     * \brief Update enable parameter
     */
    virtual void set_enable(bool enable) = 0;
    /*!
     * \brief Update repeat_every_samp parameter
     */
    virtual void set_repeat_every_samp(int samples) = 0;
    /*!
     * \brief Update repeat_n_times parameter
     */
    virtual void set_repeat_n_times(int n) = 0;
    /*!
     * \brief Update repeat_msg_avg parameter
     */
    virtual void set_repeat_msg_avg(bool avg) = 0;
    /*!
     * \brief Update delay_msg_eob parameter
     */
    virtual void set_delay_msg_eob(bool delay) = 0;
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_BURSTEVALUATE_H */
