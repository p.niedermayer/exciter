/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_LEVELPROBE_H
#define INCLUDED_BEAM_EXCITER_LEVELPROBE_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/sync_decimator.h>

namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API LevelProbe : virtual public gr::sync_decimator
{
public:
    typedef std::shared_ptr<LevelProbe> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of beam_exciter::LevelProbe.
     *
     * To avoid accidental use of raw pointers, beam_exciter::LevelProbe's
     * constructor is in a private implementation
     * class. beam_exciter::LevelProbe::make is the public interface for
     * creating new instances.
     */
    static sptr make(int treshold = 1, int decimation = 10000);
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_LEVELPROBE_H */
