/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_RAMPARBITRARY_H
#define INCLUDED_BEAM_EXCITER_RAMPARBITRARY_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API rampArbitrary : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<rampArbitrary> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of beam_exciter::rampArbitrary.
     *
     * To avoid accidental use of raw pointers, beam_exciter::rampArbitrary's
     * constructor is in a private implementation
     * class. beam_exciter::rampArbitrary::make is the public interface for
     * creating new instances.
     */
    static sptr make(std::string filename = "",
                     float offset_scale = 1,
                     pmt::pmt_t tag_value = pmt::mp("start"),
                     pmt::pmt_t tag_key = pmt::PMT_T);
                     
    /*!
     * \brief Update filename parameter
     */
    virtual void set_filename(std::string filename) = 0;
                     
    /*!
     * \brief Update offset_scale parameter
     */
    virtual void set_offset_scale(float offset_scale) = 0;
    
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_RAMPARBITRARY_H */
