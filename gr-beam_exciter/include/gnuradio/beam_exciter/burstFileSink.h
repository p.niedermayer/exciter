/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_BURSTFILESINK_H
#define INCLUDED_BEAM_EXCITER_BURSTFILESINK_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/blocks/file_sink_base.h>
// Note: I also added blocks to the "find_package" call in toplevel CMakeList.txt
//       and to the "target_link_libraries" call in lib/CMakeLists.txt
#include <gnuradio/sync_block.h>

namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API burstFileSink : virtual public gr::sync_block, virtual public gr::blocks::file_sink_base
{
public:
    typedef std::shared_ptr<burstFileSink> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of beam_exciter::burstFileSink.
     *
     * To avoid accidental use of raw pointers, beam_exciter::burstFileSink's
     * constructor is in a private implementation
     * class. beam_exciter::burstFileSink::make is the public interface for
     * creating new instances.
     */
    static sptr make(size_t itemsize, const char* filename, pmt::pmt_t tag_key_start, pmt::pmt_t tag_key_stop, int length, bool enable);
	
	/*!
     * \brief Update length parameter
     */
    virtual void set_length(int length) = 0;
	/*!
     * \brief Update filename parameter
     */
    virtual void set_filename(const char* filename) = 0;
	/*!
     * \brief Update enable parameter
     */
    virtual void set_enable(bool enable) = 0;
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_BURSTFILESINK_H */
