/* -*- c++ -*- */
/*
 * Copyright 2023 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_GPIOTRIGGER_H
#define INCLUDED_BEAM_EXCITER_GPIOTRIGGER_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/sync_block.h>
#include <gnuradio/uhd/api.h>
#include <gnuradio/uhd/usrp_block.h>
#include <gnuradio/uhd/rfnoc_graph.h>
#include <uhd/rfnoc_graph.hpp>
#include <uhd/usrp/multi_usrp.hpp>

namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API gpioTrigger : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<gpioTrigger> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of beam_exciter::gpioTrigger.
     *
     * To avoid accidental use of raw pointers, beam_exciter::gpioTrigger's
     * constructor is in a private implementation
     * class. beam_exciter::gpioTrigger::make is the public interface for
     * creating new instances.
     */
    static sptr make(
        size_t itemsize,
        bool enable_polling = true,
        int poll_every_samples = 10000,
        int duration_samples = 0,
        pmt::pmt_t tag_key_start = pmt::intern(std::string("SOB")),
        pmt::pmt_t tag_key_stop = pmt::intern(std::string("EOB")),
        int manual_trigger = 0,
        ::gr::uhd::rfnoc_graph::sptr ptr_graph = nullptr,
        std::string device_addr = "",
        int device_index = 0,
        int radio_index = 0,
        std::string gpio_bank = "RXA",
        int gpio_pin = 0,
        bool inverted = false
    );
                     
    /*!
     * \brief Update enable_polling parameter
     */
    virtual void set_enable_polling(bool enable_polling) = 0;
    /*!
     * \brief Update duration_samples parameter
     */
    virtual void set_duration_samples(int duration_samples) = 0;
    /*!
     * \brief Update manual_trigger parameter
     */
    virtual void set_manual_trigger(int manual_trigger) = 0;

};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_GPIOTRIGGER_H */
