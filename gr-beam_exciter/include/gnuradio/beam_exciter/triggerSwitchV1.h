/* -*- c++ -*- */
/*
 * Copyright 2022 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_TRIGGERSWITCHV1_H
#define INCLUDED_BEAM_EXCITER_TRIGGERSWITCHV1_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/block.h>
#include <gnuradio/uhd/api.h>
#include <gnuradio/uhd/usrp_block.h>
#include <uhd/usrp/multi_usrp.hpp>

namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API triggerSwitchV1 : virtual public gr::block
{
public:
    typedef std::shared_ptr<triggerSwitchV1> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of beam_exciter::triggerSwitchV1.
     *
     * To avoid accidental use of raw pointers, beam_exciter::triggerSwitchV1's
     * constructor is in a private implementation
     * class. beam_exciter::triggerSwitchV1::make is the public interface for
     * creating new instances.
     */
    static sptr make(int poll_every_samples = 10000,
                     int duration_samples = 0,
                     int delay_samples = 0,
                     int manual_trigger = 0,
                     std::string device_addr = "",
                     int gpio_mboard = 0,
                     std::string gpio_bank = "RXA",
                     int gpio_pin = 0,
                     bool stop_output = true,
                     bool add_stream_tags = false);
                     
    /*!
     * \brief Update duration_samples parameter
     */
    virtual void set_duration_samples(int duration_samples) = 0;
    /*!
     * \brief Update delay_samples parameter
     */
    virtual void set_delay_samples(int delay_samples) = 0;
    /*!
     * \brief Update manual_trigger parameter
     */
    virtual void set_manual_trigger(int manual_trigger) = 0;
    /*!
     * \brief Update stop_output parameter
     */
    virtual void set_stop_output(bool stop_output) = 0;

};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_TRIGGERSWITCHV1_H */
