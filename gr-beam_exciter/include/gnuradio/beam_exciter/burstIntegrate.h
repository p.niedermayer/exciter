/* -*- c++ -*- */
/*
 * Copyright 2024 Philipp Niedermayer.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_BEAM_EXCITER_BURSTINTEGRATE_H
#define INCLUDED_BEAM_EXCITER_BURSTINTEGRATE_H

#include <gnuradio/beam_exciter/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace beam_exciter {

/*!
 * \brief <+description of block+>
 * \ingroup beam_exciter
 *
 */
class BEAM_EXCITER_API burstIntegrate : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<burstIntegrate> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of beam_exciter::burstIntegrate.
     *
     * To avoid accidental use of raw pointers, beam_exciter::burstIntegrate's
     * constructor is in a private implementation
     * class. beam_exciter::burstIntegrate::make is the public interface for
     * creating new instances.
     */
    static sptr make(pmt::pmt_t tag_key_reset, double initial_value = 0, double gain = 1);
};

} // namespace beam_exciter
} // namespace gr

#endif /* INCLUDED_BEAM_EXCITER_BURSTINTEGRATE_H */
