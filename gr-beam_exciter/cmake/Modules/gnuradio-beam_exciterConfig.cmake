find_package(PkgConfig)

PKG_CHECK_MODULES(PC_GR_BEAM_EXCITER gnuradio-beam_exciter)

FIND_PATH(
    GR_BEAM_EXCITER_INCLUDE_DIRS
    NAMES gnuradio/beam_exciter/api.h
    HINTS $ENV{BEAM_EXCITER_DIR}/include
        ${PC_BEAM_EXCITER_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    GR_BEAM_EXCITER_LIBRARIES
    NAMES gnuradio-beam_exciter
    HINTS $ENV{BEAM_EXCITER_DIR}/lib
        ${PC_BEAM_EXCITER_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/gnuradio-beam_exciterTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GR_BEAM_EXCITER DEFAULT_MSG GR_BEAM_EXCITER_LIBRARIES GR_BEAM_EXCITER_INCLUDE_DIRS)
MARK_AS_ADVANCED(GR_BEAM_EXCITER_LIBRARIES GR_BEAM_EXCITER_INCLUDE_DIRS)
