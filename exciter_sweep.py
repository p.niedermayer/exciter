#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Excitation with sinusoidal frequency sweep
# Author: Philipp Niedermayer
# Copyright: Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from PyQt5.QtCore import QObject, pyqtSlot
from gnuradio import analog
from gnuradio import beam_exciter
from gnuradio.beam_exciter import ensure_pmt
from gnuradio import uhd
import pmt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio.qtgui import Range, RangeWidget
from PyQt5 import QtCore
from usrp_sink import usrp_sink  # grc-generated hier_block
import sip


def snipfcn_snippet_1(self):
    # Handle an uncought exception
    def handle_exception(exc_type, exc_value, exc_traceback):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        print("Troubleshooting help at https://git.gsi.de/p.niedermayer/exciter/-/wikis/Troubleshooting")
        # Inform user by flashing window in red
        w = self # the top block, i.e. main qt window
        w.setAttribute(Qt.Qt.WA_StyledBackground, True)
        w.setStyleSheet('background-color: red;')
        from PyQt5 import QtCore
        QtCore.QTimer.singleShot(200, lambda: w.setStyleSheet(""))

    sys.excepthook = handle_exception


def snippets_init_before_blocks(tb):
    snipfcn_snippet_1(tb)

class exciter_sweep(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Excitation with sinusoidal frequency sweep", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Excitation with sinusoidal frequency sweep")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "exciter_sweep")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.trg_count = trg_count = 0
        self.output_duration = output_duration = 20
        self.trigger_gate_mode_label = trigger_gate_mode_label = "Gated mode" if output_duration == 0 else ""
        self.qtgui_label_trg_count = qtgui_label_trg_count = trg_count
        self.q_stop_y = q_stop_y = 0.34
        self.q_stop_x = q_stop_x = 0.34
        self.q_start_y = q_start_y = 0.31
        self.q_start_x = q_start_x = 0.31
        self.output_on = output_on = 0
        self.maxoutbuf = maxoutbuf = 64
        self.manual_trigger = manual_trigger = 0
        self.level_y = level_y = 0.1
        self.level_x = level_x = 0.1
        self.fs_signal = fs_signal = 5e6
        self.fs_gpio = fs_gpio = 1000*10
        self.frev = frev = 1e6
        self.enabled_y = enabled_y = 1
        self.enabled_x = enabled_x = 1
        self.enable_ext_trg = enable_ext_trg = False
        self.duration = duration = 5
        self.display_outp = display_outp = 'real'
        self.delay = delay = 0

        ##################################################
        # Blocks
        ##################################################
        snippets_init_before_blocks(self)
        self.tab_output = Qt.QTabWidget()
        self.tab_output_widget_0 = Qt.QWidget()
        self.tab_output_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_output_widget_0)
        self.tab_output_grid_layout_0 = Qt.QGridLayout()
        self.tab_output_layout_0.addLayout(self.tab_output_grid_layout_0)
        self.tab_output.addTab(self.tab_output_widget_0, 'Output')
        self.top_grid_layout.addWidget(self.tab_output, 40, 0, 1, 1)
        for r in range(40, 41):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.signal_tab = Qt.QTabWidget()
        self.signal_tab_widget_0 = Qt.QWidget()
        self.signal_tab_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.signal_tab_widget_0)
        self.signal_tab_grid_layout_0 = Qt.QGridLayout()
        self.signal_tab_layout_0.addLayout(self.signal_tab_grid_layout_0)
        self.signal_tab.addTab(self.signal_tab_widget_0, 'Frequency sweep')
        self.top_grid_layout.addWidget(self.signal_tab, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._q_stop_y_tool_bar = Qt.QToolBar(self)
        self._q_stop_y_tool_bar.addWidget(Qt.QLabel("Tune to" + ": "))
        self._q_stop_y_line_edit = Qt.QLineEdit(str(self.q_stop_y))
        self._q_stop_y_tool_bar.addWidget(self._q_stop_y_line_edit)
        self._q_stop_y_line_edit.editingFinished.connect(
            lambda: self.set_q_stop_y(eval(str(self._q_stop_y_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._q_stop_y_tool_bar, 20, 2, 1, 1)
        for r in range(20, 21):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        self._q_stop_x_tool_bar = Qt.QToolBar(self)
        self._q_stop_x_tool_bar.addWidget(Qt.QLabel("Tune to" + ": "))
        self._q_stop_x_line_edit = Qt.QLineEdit(str(self.q_stop_x))
        self._q_stop_x_tool_bar.addWidget(self._q_stop_x_line_edit)
        self._q_stop_x_line_edit.editingFinished.connect(
            lambda: self.set_q_stop_x(eval(str(self._q_stop_x_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._q_stop_x_tool_bar, 10, 2, 1, 1)
        for r in range(10, 11):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        self._q_start_y_tool_bar = Qt.QToolBar(self)
        self._q_start_y_tool_bar.addWidget(Qt.QLabel("Tune from" + ": "))
        self._q_start_y_line_edit = Qt.QLineEdit(str(self.q_start_y))
        self._q_start_y_tool_bar.addWidget(self._q_start_y_line_edit)
        self._q_start_y_line_edit.editingFinished.connect(
            lambda: self.set_q_start_y(eval(str(self._q_start_y_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._q_start_y_tool_bar, 20, 1, 1, 1)
        for r in range(20, 21):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        self._q_start_x_tool_bar = Qt.QToolBar(self)
        self._q_start_x_tool_bar.addWidget(Qt.QLabel("Tune from" + ": "))
        self._q_start_x_line_edit = Qt.QLineEdit(str(self.q_start_x))
        self._q_start_x_tool_bar.addWidget(self._q_start_x_line_edit)
        self._q_start_x_line_edit.editingFinished.connect(
            lambda: self.set_q_start_x(eval(str(self._q_start_x_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._q_start_x_tool_bar, 10, 1, 1, 1)
        for r in range(10, 11):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        self._output_duration_range = Range(0, int((2**31-1)/fs_signal), 0.1, 20, 200)
        self._output_duration_win = RangeWidget(self._output_duration_range, self.set_output_duration, "Burst duration [s]", "counter", float, QtCore.Qt.Horizontal)
        self.tab_output_grid_layout_0.addWidget(self._output_duration_win, 1, 0, 1, 1)
        for r in range(1, 2):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._manual_trigger_choices = {'Pressed': 1, 'Released': 0}

        _manual_trigger_toggle_button = qtgui.ToggleButton(self.set_manual_trigger, 'Manual trigger', self._manual_trigger_choices, False, 'value')
        _manual_trigger_toggle_button.setColors("default", "default", "default", "default")
        self.manual_trigger = _manual_trigger_toggle_button

        self.tab_output_grid_layout_0.addWidget(_manual_trigger_toggle_button, 0, 2, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._level_y_tool_bar = Qt.QToolBar(self)
        self._level_y_tool_bar.addWidget(Qt.QLabel("Amplitude" + ": "))
        self._level_y_line_edit = Qt.QLineEdit(str(self.level_y))
        self._level_y_tool_bar.addWidget(self._level_y_line_edit)
        self._level_y_line_edit.editingFinished.connect(
            lambda: self.set_level_y(eval(str(self._level_y_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._level_y_tool_bar, 20, 3, 1, 1)
        for r in range(20, 21):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(3, 4):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        self._level_x_tool_bar = Qt.QToolBar(self)
        self._level_x_tool_bar.addWidget(Qt.QLabel("Amplitude" + ": "))
        self._level_x_line_edit = Qt.QLineEdit(str(self.level_x))
        self._level_x_tool_bar.addWidget(self._level_x_line_edit)
        self._level_x_line_edit.editingFinished.connect(
            lambda: self.set_level_x(eval(str(self._level_x_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._level_x_tool_bar, 10, 3, 1, 1)
        for r in range(10, 11):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(3, 4):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        self._frev_tool_bar = Qt.QToolBar(self)
        self._frev_tool_bar.addWidget(Qt.QLabel("frev [Hz]" + ": "))
        self._frev_line_edit = Qt.QLineEdit(str(self.frev))
        self._frev_tool_bar.addWidget(self._frev_line_edit)
        self._frev_line_edit.editingFinished.connect(
            lambda: self.set_frev(eval(str(self._frev_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._frev_tool_bar, 0, 2, 1, 2)
        for r in range(0, 1):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 4):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        _enabled_y_check_box = Qt.QCheckBox("Y) Vertical")
        self._enabled_y_choices = {True: 1, False: 0}
        self._enabled_y_choices_inv = dict((v,k) for k,v in self._enabled_y_choices.items())
        self._enabled_y_callback = lambda i: Qt.QMetaObject.invokeMethod(_enabled_y_check_box, "setChecked", Qt.Q_ARG("bool", self._enabled_y_choices_inv[i]))
        self._enabled_y_callback(self.enabled_y)
        _enabled_y_check_box.stateChanged.connect(lambda i: self.set_enabled_y(self._enabled_y_choices[bool(i)]))
        self.signal_tab_grid_layout_0.addWidget(_enabled_y_check_box, 20, 0, 1, 1)
        for r in range(20, 21):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        _enabled_x_check_box = Qt.QCheckBox("X) Horizontal")
        self._enabled_x_choices = {True: 1, False: 0}
        self._enabled_x_choices_inv = dict((v,k) for k,v in self._enabled_x_choices.items())
        self._enabled_x_callback = lambda i: Qt.QMetaObject.invokeMethod(_enabled_x_check_box, "setChecked", Qt.Q_ARG("bool", self._enabled_x_choices_inv[i]))
        self._enabled_x_callback(self.enabled_x)
        _enabled_x_check_box.stateChanged.connect(lambda i: self.set_enabled_x(self._enabled_x_choices[bool(i)]))
        self.signal_tab_grid_layout_0.addWidget(_enabled_x_check_box, 10, 0, 1, 1)
        for r in range(10, 11):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        _enable_ext_trg_check_box = Qt.QCheckBox("Enable external trigger")
        self._enable_ext_trg_choices = {True: True, False: False}
        self._enable_ext_trg_choices_inv = dict((v,k) for k,v in self._enable_ext_trg_choices.items())
        self._enable_ext_trg_callback = lambda i: Qt.QMetaObject.invokeMethod(_enable_ext_trg_check_box, "setChecked", Qt.Q_ARG("bool", self._enable_ext_trg_choices_inv[i]))
        self._enable_ext_trg_callback(self.enable_ext_trg)
        _enable_ext_trg_check_box.stateChanged.connect(lambda i: self.set_enable_ext_trg(self._enable_ext_trg_choices[bool(i)]))
        self.tab_output_grid_layout_0.addWidget(_enable_ext_trg_check_box, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._duration_tool_bar = Qt.QToolBar(self)
        self._duration_tool_bar.addWidget(Qt.QLabel("Duration [s]" + ": "))
        self._duration_line_edit = Qt.QLineEdit(str(self.duration))
        self._duration_tool_bar.addWidget(self._duration_line_edit)
        self._duration_line_edit.editingFinished.connect(
            lambda: self.set_duration(eval(str(self._duration_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._duration_tool_bar, 0, 1, 1, 1)
        for r in range(0, 1):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        # Create the options list
        self._display_outp_options = ['real', 'imag']
        # Create the labels list
        self._display_outp_labels = ['X', 'Y']
        # Create the combo box
        self._display_outp_tool_bar = Qt.QToolBar(self)
        self._display_outp_tool_bar.addWidget(Qt.QLabel("Show output signal" + ": "))
        self._display_outp_combo_box = Qt.QComboBox()
        self._display_outp_tool_bar.addWidget(self._display_outp_combo_box)
        for _label in self._display_outp_labels: self._display_outp_combo_box.addItem(_label)
        self._display_outp_callback = lambda i: Qt.QMetaObject.invokeMethod(self._display_outp_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._display_outp_options.index(i)))
        self._display_outp_callback(self.display_outp)
        self._display_outp_combo_box.currentIndexChanged.connect(
            lambda i: self.set_display_outp(self._display_outp_options[i]))
        # Create the radio buttons
        self.tab_output_grid_layout_0.addWidget(self._display_outp_tool_bar, 20, 0, 1, 1)
        for r in range(20, 21):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._delay_tool_bar = Qt.QToolBar(self)
        self._delay_tool_bar.addWidget(Qt.QLabel("Delay [s]" + ": "))
        self._delay_line_edit = Qt.QLineEdit(str(self.delay))
        self._delay_tool_bar.addWidget(self._delay_line_edit)
        self._delay_line_edit.editingFinished.connect(
            lambda: self.set_delay(eval(str(self._delay_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._delay_tool_bar, 0, 0, 1, 1)
        for r in range(0, 1):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        self.usrp_sink_0 = usrp_sink(
            fs_signal=fs_signal,
        )
        self._trigger_gate_mode_label_tool_bar = Qt.QToolBar(self)

        if None:
            self._trigger_gate_mode_label_formatter = None
        else:
            self._trigger_gate_mode_label_formatter = lambda x: str(x)

        self._trigger_gate_mode_label_tool_bar.addWidget(Qt.QLabel(" "))
        self._trigger_gate_mode_label_label = Qt.QLabel(str(self._trigger_gate_mode_label_formatter(self.trigger_gate_mode_label)))
        self._trigger_gate_mode_label_tool_bar.addWidget(self._trigger_gate_mode_label_label)
        self.tab_output_grid_layout_0.addWidget(self._trigger_gate_mode_label_tool_bar, 1, 1, 1, 1)
        for r in range(1, 2):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_waterfall_sink_x_0_0_0 = qtgui.waterfall_sink_f(
            8192, #size
            window.WIN_BLACKMAN_hARRIS, #wintype
            0, #fc
            fs_signal, #bw
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_waterfall_sink_x_0_0_0.set_update_time(0.1)
        self.qtgui_waterfall_sink_x_0_0_0.enable_grid(True)
        self.qtgui_waterfall_sink_x_0_0_0.enable_axis_labels(True)


        self.qtgui_waterfall_sink_x_0_0_0.set_plot_pos_half(not False)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        colors = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_waterfall_sink_x_0_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_waterfall_sink_x_0_0_0.set_line_label(i, labels[i])
            self.qtgui_waterfall_sink_x_0_0_0.set_color_map(i, colors[i])
            self.qtgui_waterfall_sink_x_0_0_0.set_line_alpha(i, alphas[i])

        self.qtgui_waterfall_sink_x_0_0_0.set_intensity_range(-80, 10)

        self._qtgui_waterfall_sink_x_0_0_0_win = sip.wrapinstance(self.qtgui_waterfall_sink_x_0_0_0.qwidget(), Qt.QWidget)

        self.top_grid_layout.addWidget(self._qtgui_waterfall_sink_x_0_0_0_win, 50, 0, 1, 1)
        for r in range(50, 51):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_overload_y = qtgui.number_sink(
            gr.sizeof_float,
            1,
            qtgui.NUM_GRAPH_HORIZ,
            1,
            None # parent
        )
        self.qtgui_overload_y.set_update_time(0.10)
        self.qtgui_overload_y.set_title("")

        labels = ['Y overload:', '', '', '', '',
            '', '', '', '', '']
        units = ['%', '', '', '', '',
            '', '', '', '', '']
        colors = [("black", "red"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
            ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]

        for i in range(1):
            self.qtgui_overload_y.set_min(i, 0)
            self.qtgui_overload_y.set_max(i, 40)
            self.qtgui_overload_y.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_overload_y.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_overload_y.set_label(i, labels[i])
            self.qtgui_overload_y.set_unit(i, units[i])
            self.qtgui_overload_y.set_factor(i, factor[i])

        self.qtgui_overload_y.enable_autoscale(False)
        self._qtgui_overload_y_win = sip.wrapinstance(self.qtgui_overload_y.qwidget(), Qt.QWidget)
        self.tab_output_grid_layout_0.addWidget(self._qtgui_overload_y_win, 10, 2, 1, 1)
        for r in range(10, 11):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_overload = qtgui.number_sink(
            gr.sizeof_float,
            1,
            qtgui.NUM_GRAPH_HORIZ,
            1,
            None # parent
        )
        self.qtgui_overload.set_update_time(0.10)
        self.qtgui_overload.set_title("")

        labels = ['X overload:', '', '', '', '',
            '', '', '', '', '']
        units = ['%', '', '', '', '',
            '', '', '', '', '']
        colors = [("black", "red"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
            ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]

        for i in range(1):
            self.qtgui_overload.set_min(i, 0)
            self.qtgui_overload.set_max(i, 40)
            self.qtgui_overload.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_overload.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_overload.set_label(i, labels[i])
            self.qtgui_overload.set_unit(i, units[i])
            self.qtgui_overload.set_factor(i, factor[i])

        self.qtgui_overload.enable_autoscale(False)
        self._qtgui_overload_win = sip.wrapinstance(self.qtgui_overload.qwidget(), Qt.QWidget)
        self.tab_output_grid_layout_0.addWidget(self._qtgui_overload_win, 10, 1, 1, 1)
        for r in range(10, 11):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_ledindicator_0_0_0_0 = self._qtgui_ledindicator_0_0_0_0_win = qtgui.GrLEDIndicator('Output active', "lime", "black", output_on, 35, 1, 1, 1, self)
        self.qtgui_ledindicator_0_0_0_0 = self._qtgui_ledindicator_0_0_0_0_win
        self.tab_output_grid_layout_0.addWidget(self._qtgui_ledindicator_0_0_0_0_win, 10, 0, 1, 1)
        for r in range(10, 11):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._qtgui_label_trg_count_tool_bar = Qt.QToolBar(self)

        if None:
            self._qtgui_label_trg_count_formatter = None
        else:
            self._qtgui_label_trg_count_formatter = lambda x: str(x)

        self._qtgui_label_trg_count_tool_bar.addWidget(Qt.QLabel("Trigger count: "))
        self._qtgui_label_trg_count_label = Qt.QLabel(str(self._qtgui_label_trg_count_formatter(self.qtgui_label_trg_count)))
        self._qtgui_label_trg_count_tool_bar.addWidget(self._qtgui_label_trg_count_label)
        self.tab_output_grid_layout_0.addWidget(self._qtgui_label_trg_count_tool_bar, 0, 1, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.blocks_vco_f_0_0 = blocks.vco_f(fs_signal, 6.283185307179586, 1)
        self.blocks_vco_f_0_0.set_max_output_buffer(maxoutbuf)
        self.blocks_vco_f_0 = blocks.vco_f(fs_signal, 6.283185307179586, 1)
        self.blocks_vco_f_0.set_max_output_buffer(maxoutbuf)
        self.blocks_null_sink_0_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_multiply_const_vxx_1_0_0_0 = blocks.multiply_const_ff(100)
        self.blocks_multiply_const_vxx_1_0_0 = blocks.multiply_const_ff(100)
        self.blocks_multiply_const_vxx_0_0_0 = blocks.multiply_const_ff((enabled_y*level_y))
        self.blocks_multiply_const_vxx_0_0_0.set_max_output_buffer(maxoutbuf)
        self.blocks_multiply_const_vxx_0_0 = blocks.multiply_const_ff((enabled_x*level_x))
        self.blocks_multiply_const_vxx_0_0.set_max_output_buffer(maxoutbuf)
        self.blocks_msgpair_to_var_2 = blocks.msg_pair_to_var(self.set_output_on)
        self.blocks_msgpair_to_var_0_0 = blocks.msg_pair_to_var(self.set_trg_count)
        self.blocks_float_to_complex_0 = blocks.float_to_complex(1)
        self.blocks_float_to_complex_0.set_max_output_buffer(maxoutbuf)
        self.beam_exciter_switchBlock_0 = beam_exciter.switchBlock(gr.sizeof_gr_complex, 0, 0, 0, 0, ensure_pmt("SOB"), ensure_pmt("EOB"), ensure_pmt())
        self.beam_exciter_rampLinear_0_0 = beam_exciter.rampLinear(frev*q_start_y, frev*q_stop_y, (int(duration*fs_signal)), (int(delay*fs_signal)), pmt.PMT_NIL, pmt.intern("SOB"))
        self.beam_exciter_rampLinear_0_0.set_max_output_buffer(maxoutbuf)
        self.beam_exciter_rampLinear_0 = beam_exciter.rampLinear(frev*q_start_x, frev*q_stop_x, (int(duration*fs_signal)), (int(delay*fs_signal)), pmt.PMT_NIL, pmt.intern("SOB"))
        self.beam_exciter_rampLinear_0.set_max_output_buffer(maxoutbuf)
        self.beam_exciter_gpioTrigger_0_0 = beam_exciter.gpioTrigger(
            itemsize=gr.sizeof_float,
            enable_polling=enable_ext_trg,
            poll_every_samples=(int(fs_signal/fs_gpio)),
            duration_samples=(int(output_duration * fs_signal)),
            tag_key_start=ensure_pmt("SOB"),
            tag_key_stop=ensure_pmt("EOB"),
            manual_trigger=manual_trigger,
            ptr_graph=getattr(self, "rfnoc_graph", None),
            device_addr='',
            device_index=0,
            radio_index=0,
            gpio_bank='RX',
            gpio_pin=0,
            inverted=False,
        )
        self.beam_exciter_gpioTrigger_0_0.set_max_output_buffer(maxoutbuf)
        self.beam_exciter_complexTo_0 = beam_exciter.complexTo((display_outp).lower().startswith("r"))
        self.beam_exciter_LevelProbe_0_1_0_0 = beam_exciter.LevelProbe(1, 10000)
        self.beam_exciter_LevelProbe_0_1_0 = beam_exciter.LevelProbe(1, 10000)
        self.analog_rail_ff_0 = analog.rail_ff((-1), 1)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.beam_exciter_gpioTrigger_0_0, 'count'), (self.blocks_msgpair_to_var_0_0, 'inpair'))
        self.msg_connect((self.beam_exciter_switchBlock_0, 'active'), (self.blocks_msgpair_to_var_2, 'inpair'))
        self.connect((self.analog_rail_ff_0, 0), (self.qtgui_waterfall_sink_x_0_0_0, 0))
        self.connect((self.beam_exciter_LevelProbe_0_1_0, 1), (self.blocks_multiply_const_vxx_1_0_0, 0))
        self.connect((self.beam_exciter_LevelProbe_0_1_0, 0), (self.blocks_null_sink_0_0, 0))
        self.connect((self.beam_exciter_LevelProbe_0_1_0_0, 1), (self.blocks_multiply_const_vxx_1_0_0_0, 0))
        self.connect((self.beam_exciter_LevelProbe_0_1_0_0, 0), (self.blocks_null_sink_0, 0))
        self.connect((self.beam_exciter_complexTo_0, 0), (self.analog_rail_ff_0, 0))
        self.connect((self.beam_exciter_gpioTrigger_0_0, 0), (self.beam_exciter_rampLinear_0, 0))
        self.connect((self.beam_exciter_gpioTrigger_0_0, 0), (self.beam_exciter_rampLinear_0_0, 0))
        self.connect((self.beam_exciter_rampLinear_0, 0), (self.blocks_vco_f_0, 0))
        self.connect((self.beam_exciter_rampLinear_0_0, 0), (self.blocks_vco_f_0_0, 0))
        self.connect((self.beam_exciter_switchBlock_0, 0), (self.beam_exciter_complexTo_0, 0))
        self.connect((self.beam_exciter_switchBlock_0, 0), (self.usrp_sink_0, 0))
        self.connect((self.blocks_float_to_complex_0, 0), (self.beam_exciter_switchBlock_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.beam_exciter_LevelProbe_0_1_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.blocks_float_to_complex_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0_0, 0), (self.beam_exciter_LevelProbe_0_1_0_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0_0, 0), (self.blocks_float_to_complex_0, 1))
        self.connect((self.blocks_multiply_const_vxx_1_0_0, 0), (self.qtgui_overload, 0))
        self.connect((self.blocks_multiply_const_vxx_1_0_0_0, 0), (self.qtgui_overload_y, 0))
        self.connect((self.blocks_vco_f_0, 0), (self.blocks_multiply_const_vxx_0_0, 0))
        self.connect((self.blocks_vco_f_0_0, 0), (self.blocks_multiply_const_vxx_0_0_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "exciter_sweep")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_trg_count(self):
        return self.trg_count

    def set_trg_count(self, trg_count):
        self.trg_count = trg_count
        self.set_qtgui_label_trg_count(self.trg_count)

    def get_output_duration(self):
        return self.output_duration

    def set_output_duration(self, output_duration):
        self.output_duration = output_duration
        self.set_trigger_gate_mode_label("Gated mode" if self.output_duration == 0 else "")
        self.beam_exciter_gpioTrigger_0_0.set_duration_samples((int(self.output_duration * self.fs_signal)))

    def get_trigger_gate_mode_label(self):
        return self.trigger_gate_mode_label

    def set_trigger_gate_mode_label(self, trigger_gate_mode_label):
        self.trigger_gate_mode_label = trigger_gate_mode_label
        Qt.QMetaObject.invokeMethod(self._trigger_gate_mode_label_label, "setText", Qt.Q_ARG("QString", str(self._trigger_gate_mode_label_formatter(self.trigger_gate_mode_label))))

    def get_qtgui_label_trg_count(self):
        return self.qtgui_label_trg_count

    def set_qtgui_label_trg_count(self, qtgui_label_trg_count):
        self.qtgui_label_trg_count = qtgui_label_trg_count
        Qt.QMetaObject.invokeMethod(self._qtgui_label_trg_count_label, "setText", Qt.Q_ARG("QString", str(self._qtgui_label_trg_count_formatter(self.qtgui_label_trg_count))))

    def get_q_stop_y(self):
        return self.q_stop_y

    def set_q_stop_y(self, q_stop_y):
        self.q_stop_y = q_stop_y
        Qt.QMetaObject.invokeMethod(self._q_stop_y_line_edit, "setText", Qt.Q_ARG("QString", repr(self.q_stop_y)))
        self.beam_exciter_rampLinear_0_0.set_stop(self.frev*self.q_stop_y)

    def get_q_stop_x(self):
        return self.q_stop_x

    def set_q_stop_x(self, q_stop_x):
        self.q_stop_x = q_stop_x
        Qt.QMetaObject.invokeMethod(self._q_stop_x_line_edit, "setText", Qt.Q_ARG("QString", repr(self.q_stop_x)))
        self.beam_exciter_rampLinear_0.set_stop(self.frev*self.q_stop_x)

    def get_q_start_y(self):
        return self.q_start_y

    def set_q_start_y(self, q_start_y):
        self.q_start_y = q_start_y
        Qt.QMetaObject.invokeMethod(self._q_start_y_line_edit, "setText", Qt.Q_ARG("QString", repr(self.q_start_y)))
        self.beam_exciter_rampLinear_0_0.set_start(self.frev*self.q_start_y)

    def get_q_start_x(self):
        return self.q_start_x

    def set_q_start_x(self, q_start_x):
        self.q_start_x = q_start_x
        Qt.QMetaObject.invokeMethod(self._q_start_x_line_edit, "setText", Qt.Q_ARG("QString", repr(self.q_start_x)))
        self.beam_exciter_rampLinear_0.set_start(self.frev*self.q_start_x)

    def get_output_on(self):
        return self.output_on

    def set_output_on(self, output_on):
        self.output_on = output_on
        self.qtgui_ledindicator_0_0_0_0.setState(self.output_on)

    def get_maxoutbuf(self):
        return self.maxoutbuf

    def set_maxoutbuf(self, maxoutbuf):
        self.maxoutbuf = maxoutbuf

    def get_manual_trigger(self):
        return self.manual_trigger

    def set_manual_trigger(self, manual_trigger):
        self.manual_trigger = manual_trigger
        self.beam_exciter_gpioTrigger_0_0.set_manual_trigger(self.manual_trigger)

    def get_level_y(self):
        return self.level_y

    def set_level_y(self, level_y):
        self.level_y = level_y
        Qt.QMetaObject.invokeMethod(self._level_y_line_edit, "setText", Qt.Q_ARG("QString", repr(self.level_y)))
        self.blocks_multiply_const_vxx_0_0_0.set_k((self.enabled_y*self.level_y))

    def get_level_x(self):
        return self.level_x

    def set_level_x(self, level_x):
        self.level_x = level_x
        Qt.QMetaObject.invokeMethod(self._level_x_line_edit, "setText", Qt.Q_ARG("QString", repr(self.level_x)))
        self.blocks_multiply_const_vxx_0_0.set_k((self.enabled_x*self.level_x))

    def get_fs_signal(self):
        return self.fs_signal

    def set_fs_signal(self, fs_signal):
        self.fs_signal = fs_signal
        self.beam_exciter_gpioTrigger_0_0.set_duration_samples((int(self.output_duration * self.fs_signal)))
        self.beam_exciter_rampLinear_0.set_duration_samples((int(self.duration*self.fs_signal)))
        self.beam_exciter_rampLinear_0.set_delay_samples((int(self.delay*self.fs_signal)))
        self.beam_exciter_rampLinear_0_0.set_duration_samples((int(self.duration*self.fs_signal)))
        self.beam_exciter_rampLinear_0_0.set_delay_samples((int(self.delay*self.fs_signal)))
        self.qtgui_waterfall_sink_x_0_0_0.set_frequency_range(0, self.fs_signal)
        self.usrp_sink_0.set_fs_signal(self.fs_signal)

    def get_fs_gpio(self):
        return self.fs_gpio

    def set_fs_gpio(self, fs_gpio):
        self.fs_gpio = fs_gpio

    def get_frev(self):
        return self.frev

    def set_frev(self, frev):
        self.frev = frev
        Qt.QMetaObject.invokeMethod(self._frev_line_edit, "setText", Qt.Q_ARG("QString", repr(self.frev)))
        self.beam_exciter_rampLinear_0.set_start(self.frev*self.q_start_x)
        self.beam_exciter_rampLinear_0.set_stop(self.frev*self.q_stop_x)
        self.beam_exciter_rampLinear_0_0.set_start(self.frev*self.q_start_y)
        self.beam_exciter_rampLinear_0_0.set_stop(self.frev*self.q_stop_y)

    def get_enabled_y(self):
        return self.enabled_y

    def set_enabled_y(self, enabled_y):
        self.enabled_y = enabled_y
        self._enabled_y_callback(self.enabled_y)
        self.blocks_multiply_const_vxx_0_0_0.set_k((self.enabled_y*self.level_y))

    def get_enabled_x(self):
        return self.enabled_x

    def set_enabled_x(self, enabled_x):
        self.enabled_x = enabled_x
        self._enabled_x_callback(self.enabled_x)
        self.blocks_multiply_const_vxx_0_0.set_k((self.enabled_x*self.level_x))

    def get_enable_ext_trg(self):
        return self.enable_ext_trg

    def set_enable_ext_trg(self, enable_ext_trg):
        self.enable_ext_trg = enable_ext_trg
        self._enable_ext_trg_callback(self.enable_ext_trg)
        self.beam_exciter_gpioTrigger_0_0.set_enable_polling(self.enable_ext_trg)

    def get_duration(self):
        return self.duration

    def set_duration(self, duration):
        self.duration = duration
        Qt.QMetaObject.invokeMethod(self._duration_line_edit, "setText", Qt.Q_ARG("QString", repr(self.duration)))
        self.beam_exciter_rampLinear_0.set_duration_samples((int(self.duration*self.fs_signal)))
        self.beam_exciter_rampLinear_0_0.set_duration_samples((int(self.duration*self.fs_signal)))

    def get_display_outp(self):
        return self.display_outp

    def set_display_outp(self, display_outp):
        self.display_outp = display_outp
        self._display_outp_callback(self.display_outp)
        self.beam_exciter_complexTo_0.set_toreal((self.display_outp).lower().startswith("r"))

    def get_delay(self):
        return self.delay

    def set_delay(self, delay):
        self.delay = delay
        Qt.QMetaObject.invokeMethod(self._delay_line_edit, "setText", Qt.Q_ARG("QString", repr(self.delay)))
        self.beam_exciter_rampLinear_0.set_delay_samples((int(self.delay*self.fs_signal)))
        self.beam_exciter_rampLinear_0_0.set_delay_samples((int(self.delay*self.fs_signal)))




def main(top_block_cls=exciter_sweep, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        gr.logger("realtime").warning("Error: failed to enable real-time scheduling.")

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
