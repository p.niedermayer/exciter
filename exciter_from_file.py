#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Excitation with arbitrary signals from file
# Author: Philipp Niedermayer
# Copyright: Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from gnuradio import analog
from gnuradio import blocks
import pmt
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from triggered_output import triggered_output  # grc-generated hier_block
from usrp_sink import usrp_sink  # grc-generated hier_block


def snipfcn_snippet_1_0(self):
    # Handle an uncought exception
    def handle_exception(exc_type, exc_value, exc_traceback):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        print("Troubleshooting help at https://git.gsi.de/p.niedermayer/exciter/-/wikis/Troubleshooting")
        # Inform user by flashing window in red
        w = self # the top block, i.e. main qt window
        w.setAttribute(Qt.Qt.WA_StyledBackground, True)
        w.setStyleSheet('background-color: red;')
        from PyQt5 import QtCore
        QtCore.QTimer.singleShot(200, lambda: w.setStyleSheet(""))

    sys.excepthook = handle_exception


def snippets_init_before_blocks(tb):
    snipfcn_snippet_1_0(tb)

class exciter_from_file(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Excitation with arbitrary signals from file", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Excitation with arbitrary signals from file")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "exciter_from_file")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.maxoutbuf = maxoutbuf = 64
        self.length = length = 0
        self.fs_signal = fs_signal = 10e6
        self.fs_gpio = fs_gpio = 1000
        self.filename = filename = 'signals/void.0Sps.float32'
        self.file_reload = file_reload = 0
        self.amplitude = amplitude = 1

        ##################################################
        # Blocks
        ##################################################
        snippets_init_before_blocks(self)
        self.signal_tab = Qt.QTabWidget()
        self.signal_tab_widget_0 = Qt.QWidget()
        self.signal_tab_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.signal_tab_widget_0)
        self.signal_tab_grid_layout_0 = Qt.QGridLayout()
        self.signal_tab_layout_0.addLayout(self.signal_tab_grid_layout_0)
        self.signal_tab.addTab(self.signal_tab_widget_0, ""+f"Signal (dtype=float32, fs={fs_signal:g}Hz)")
        self.top_grid_layout.addWidget(self.signal_tab, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.tab_output = Qt.QTabWidget()
        self.tab_output_widget_0 = Qt.QWidget()
        self.tab_output_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_output_widget_0)
        self.tab_output_grid_layout_0 = Qt.QGridLayout()
        self.tab_output_layout_0.addLayout(self.tab_output_grid_layout_0)
        self.tab_output.addTab(self.tab_output_widget_0, 'Output (TX 1)')
        self.top_grid_layout.addWidget(self.tab_output, 10, 0, 1, 1)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._length_tool_bar = Qt.QToolBar(self)
        self._length_tool_bar.addWidget(Qt.QLabel("Length [samples]" + ": "))
        self._length_line_edit = Qt.QLineEdit(str(self.length))
        self._length_tool_bar.addWidget(self._length_line_edit)
        self._length_line_edit.editingFinished.connect(
            lambda: self.set_length(eval(str(self._length_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._length_tool_bar, 2, 0, 1, 1)
        for r in range(2, 3):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        self._filename_tool_bar = Qt.QToolBar(self)
        self._filename_tool_bar.addWidget(Qt.QLabel("Filename" + ": "))
        self._filename_line_edit = Qt.QLineEdit(str(self.filename))
        self._filename_tool_bar.addWidget(self._filename_line_edit)
        self._filename_line_edit.editingFinished.connect(
            lambda: self.set_filename(str(str(self._filename_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._filename_tool_bar, 0, 0, 1, 1)
        for r in range(0, 1):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        self._amplitude_tool_bar = Qt.QToolBar(self)
        self._amplitude_tool_bar.addWidget(Qt.QLabel("Amplitude" + ": "))
        self._amplitude_line_edit = Qt.QLineEdit(str(self.amplitude))
        self._amplitude_tool_bar.addWidget(self._amplitude_line_edit)
        self._amplitude_line_edit.editingFinished.connect(
            lambda: self.set_amplitude(eval(str(self._amplitude_line_edit.text()))))
        self.signal_tab_grid_layout_0.addWidget(self._amplitude_tool_bar, 5, 0, 1, 1)
        for r in range(5, 6):
            self.signal_tab_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.signal_tab_grid_layout_0.setColumnStretch(c, 1)
        self.usrp_sink_0 = usrp_sink(
            fs_signal=fs_signal,
        )
        self.output = triggered_output(
            delay=0,
            fs_gpio=fs_gpio,
            fs_signal=fs_signal,
            open_mode=True,
            rfnoc_graph=None,
            show_output_real_imag='real',
        )

        self.tab_output_grid_layout_0.addWidget(self.output, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.blocks_multiply_const_vxx_1_1_0_0 = blocks.multiply_const_ff(amplitude)
        self.blocks_msgpair_to_var_1_0 = blocks.msg_pair_to_var(self.set_file_reload)
        self.blocks_float_to_complex_0_0 = blocks.float_to_complex(1)
        self.blocks_float_to_complex_0_0.set_max_output_buffer(maxoutbuf)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_float*1, filename + str(file_reload)[0:0], True, 0, length)
        self.blocks_file_source_0.set_begin_tag(pmt.intern("trigger_switch_block"))
        self.analog_rail_ff_0 = analog.rail_ff((-1), 1)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.output, 'reset'), (self.blocks_msgpair_to_var_1_0, 'inpair'))
        self.connect((self.analog_rail_ff_0, 0), (self.blocks_float_to_complex_0_0, 0))
        self.connect((self.blocks_file_source_0, 0), (self.blocks_multiply_const_vxx_1_1_0_0, 0))
        self.connect((self.blocks_float_to_complex_0_0, 0), (self.output, 0))
        self.connect((self.blocks_multiply_const_vxx_1_1_0_0, 0), (self.analog_rail_ff_0, 0))
        self.connect((self.output, 0), (self.usrp_sink_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "exciter_from_file")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_maxoutbuf(self):
        return self.maxoutbuf

    def set_maxoutbuf(self, maxoutbuf):
        self.maxoutbuf = maxoutbuf

    def get_length(self):
        return self.length

    def set_length(self, length):
        self.length = length
        Qt.QMetaObject.invokeMethod(self._length_line_edit, "setText", Qt.Q_ARG("QString", repr(self.length)))

    def get_fs_signal(self):
        return self.fs_signal

    def set_fs_signal(self, fs_signal):
        self.fs_signal = fs_signal
        self.output.set_fs_signal(self.fs_signal)
        self.usrp_sink_0.set_fs_signal(self.fs_signal)

    def get_fs_gpio(self):
        return self.fs_gpio

    def set_fs_gpio(self, fs_gpio):
        self.fs_gpio = fs_gpio
        self.output.set_fs_gpio(self.fs_gpio)

    def get_filename(self):
        return self.filename

    def set_filename(self, filename):
        self.filename = filename
        Qt.QMetaObject.invokeMethod(self._filename_line_edit, "setText", Qt.Q_ARG("QString", str(self.filename)))
        self.blocks_file_source_0.open(self.filename + str(self.file_reload)[0:0], True)

    def get_file_reload(self):
        return self.file_reload

    def set_file_reload(self, file_reload):
        self.file_reload = file_reload
        self.blocks_file_source_0.open(self.filename + str(self.file_reload)[0:0], True)

    def get_amplitude(self):
        return self.amplitude

    def set_amplitude(self, amplitude):
        self.amplitude = amplitude
        Qt.QMetaObject.invokeMethod(self._amplitude_line_edit, "setText", Qt.Q_ARG("QString", repr(self.amplitude)))
        self.blocks_multiply_const_vxx_1_1_0_0.set_k(self.amplitude)




def main(top_block_cls=exciter_from_file, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        gr.logger("realtime").warning("Error: failed to enable real-time scheduling.")

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
