options:
  parameters:
    author: Philipp Niedermayer
    catch_exceptions: 'True'
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: test_rx_tx
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: Test RX & TX
    window_size: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 12.0]
    rotation: 0
    state: enabled

blocks:
- name: a1
  id: variable_qtgui_entry
  parameters:
    comment: ''
    entry_signal: editingFinished
    gui_hint: tab_tx:1,1
    label: 'TX 1: Amplitude [V]'
    type: raw
    value: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [184, 228.0]
    rotation: 0
    state: true
- name: a2
  id: variable_qtgui_entry
  parameters:
    comment: ''
    entry_signal: editingFinished
    gui_hint: tab_tx:1,2
    label: 'TX 2: Amplitude [V]'
    type: raw
    value: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [184, 324.0]
    rotation: 0
    state: true
- name: f1
  id: variable_qtgui_entry
  parameters:
    comment: ''
    entry_signal: editingFinished
    gui_hint: tab_tx:0,1
    label: 'TX 1: Frequency [Hz]'
    type: raw
    value: 1e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 228.0]
    rotation: 0
    state: true
- name: f2
  id: variable_qtgui_entry
  parameters:
    comment: ''
    entry_signal: editingFinished
    gui_hint: tab_tx:0,2
    label: 'TX 2: Frequency [Hz]'
    type: raw
    value: 1e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 324.0]
    rotation: 0
    state: true
- name: samp_rate
  id: variable
  parameters:
    comment: ''
    value: 10e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [192, 12.0]
    rotation: 0
    state: enabled
- name: analog_sig_source_x_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: a1
    comment: ''
    freq: f1
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: samp_rate
    showports: 'False'
    type: float
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [344, 180.0]
    rotation: 0
    state: true
- name: analog_sig_source_x_0_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: a2
    comment: ''
    freq: f2
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: samp_rate
    showports: 'False'
    type: float
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [344, 324.0]
    rotation: 0
    state: enabled
- name: blocks_complex_to_float_0
  id: blocks_complex_to_float
  parameters:
    affinity: ''
    alias: ''
    comment: 'RX 1 --> re

      RX 2 --> im'
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [208, 544.0]
    rotation: 0
    state: true
- name: blocks_float_to_complex_0
  id: blocks_float_to_complex
  parameters:
    affinity: ''
    alias: ''
    comment: 're --> TX 1

      im --> TX 2'
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [608, 280.0]
    rotation: 0
    state: true
- name: blocks_float_to_complex_1
  id: blocks_float_to_complex
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [480, 544.0]
    rotation: 0
    state: enabled
- name: blocks_float_to_complex_1_0
  id: blocks_float_to_complex
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [480, 672.0]
    rotation: 0
    state: enabled
- name: blocks_null_source_0
  id: blocks_null_source
  parameters:
    affinity: ''
    alias: ''
    bus_structure_source: '[[0,],]'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_outputs: '1'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [248, 704.0]
    rotation: 0
    state: true
- name: qtgui_sink_x_0
  id: qtgui_sink_x
  parameters:
    affinity: ''
    alias: ''
    bw: samp_rate
    comment: ''
    fc: '0'
    fftsize: '4096'
    gui_hint: tab_rx:0,1
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '"RX 1"'
    plotconst: 'False'
    plotfreq: 'True'
    plottime: 'True'
    plotwaterfall: 'True'
    rate: '10'
    showports: 'False'
    showrf: 'False'
    type: complex
    wintype: window.WIN_BLACKMAN_hARRIS
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [656, 524.0]
    rotation: 0
    state: true
- name: qtgui_sink_x_0_0
  id: qtgui_sink_x
  parameters:
    affinity: ''
    alias: ''
    bw: samp_rate
    comment: ''
    fc: '0'
    fftsize: '4096'
    gui_hint: tab_rx:0,2
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '"RX 2"'
    plotconst: 'False'
    plotfreq: 'True'
    plottime: 'True'
    plotwaterfall: 'True'
    rate: '10'
    showports: 'False'
    showrf: 'False'
    type: complex
    wintype: window.WIN_BLACKMAN_hARRIS
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [656, 652.0]
    rotation: 0
    state: true
- name: tab_rx
  id: qtgui_tab_widget
  parameters:
    alias: ''
    comment: ''
    gui_hint: 1,0
    label0: Signal input (RX)
    label1: Tab 1
    label10: Tab 10
    label11: Tab 11
    label12: Tab 12
    label13: Tab 13
    label14: Tab 14
    label15: Tab 15
    label16: Tab 16
    label17: Tab 17
    label18: Tab 18
    label19: Tab 19
    label2: Tab 2
    label3: Tab 3
    label4: Tab 4
    label5: Tab 5
    label6: Tab 6
    label7: Tab 7
    label8: Tab 8
    label9: Tab 9
    num_tabs: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 628.0]
    rotation: 0
    state: true
- name: tab_tx
  id: qtgui_tab_widget
  parameters:
    alias: ''
    comment: ''
    gui_hint: 0,0
    label0: Signal output (TX)
    label1: Tab 1
    label10: Tab 10
    label11: Tab 11
    label12: Tab 12
    label13: Tab 13
    label14: Tab 14
    label15: Tab 15
    label16: Tab 16
    label17: Tab 17
    label18: Tab 18
    label19: Tab 19
    label2: Tab 2
    label3: Tab 3
    label4: Tab 4
    label5: Tab 5
    label6: Tab 6
    label7: Tab 7
    label8: Tab 8
    label9: Tab 9
    num_tabs: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 140.0]
    rotation: 0
    state: true
- name: usrp_sink_0
  id: usrp_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    fs_signal: samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [816, 292.0]
    rotation: 0
    state: true
- name: usrp_source_0
  id: usrp_source
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    fs_signal: samp_rate
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 556.0]
    rotation: 0
    state: true

connections:
- [analog_sig_source_x_0, '0', blocks_float_to_complex_0, '0']
- [analog_sig_source_x_0_0, '0', blocks_float_to_complex_0, '1']
- [blocks_complex_to_float_0, '0', blocks_float_to_complex_1, '0']
- [blocks_complex_to_float_0, '1', blocks_float_to_complex_1_0, '0']
- [blocks_float_to_complex_0, '0', usrp_sink_0, '0']
- [blocks_float_to_complex_1, '0', qtgui_sink_x_0, '0']
- [blocks_float_to_complex_1_0, '0', qtgui_sink_x_0_0, '0']
- [blocks_null_source_0, '0', blocks_float_to_complex_1, '1']
- [blocks_null_source_0, '0', blocks_float_to_complex_1_0, '1']
- [usrp_source_0, '0', blocks_complex_to_float_0, '0']

metadata:
  file_format: 1
  grc_version: 3.10.8.0
