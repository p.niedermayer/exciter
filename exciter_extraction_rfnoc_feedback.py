#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Excitation for RF KO extraction with fast feedback (playback)
# Author: Philipp Niedermayer
# Copyright: Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
from PyQt5.QtCore import QObject, pyqtSlot
from gnuradio import beam_exciter
from gnuradio.beam_exciter import ensure_pmt
from gnuradio import uhd
import pmt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
import datetime
import time
import threading


def snipfcn_snippet_1(self):
    # Handle an uncought exception
    def handle_exception(exc_type, exc_value, exc_traceback):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        print("Troubleshooting help at https://git.gsi.de/p.niedermayer/exciter/-/wikis/Troubleshooting")
        # Inform user by flashing window in red
        w = self # the top block, i.e. main qt window
        w.setAttribute(Qt.Qt.WA_StyledBackground, True)
        w.setStyleSheet('background-color: red;')
        from PyQt5 import QtCore
        QtCore.QTimer.singleShot(200, lambda: w.setStyleSheet(""))

    sys.excepthook = handle_exception


def snippets_init_before_blocks(tb):
    snipfcn_snippet_1(tb)

class exciter_extraction_rfnoc_feedback(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Excitation for RF KO extraction with fast feedback (playback)", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Excitation for RF KO extraction with fast feedback (playback)")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "exciter_extraction_rfnoc_feedback")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.fs_adc = fs_adc = 200e6
        self.decim = decim = 80
        self.trg_count = trg_count = 0
        self.target_rate = target_rate = '1e6'
        self.ta_set = ta_set = 0.000001
        self.repeat = repeat = 10
        self.nparticles = nparticles = '1e7'
        self.kp_set = kp_set = 0.12
        self.ki_set = ki_set = 0.12/0.005
        self.kd_set = kd_set = 0
        self.inp_factor = inp_factor = 1
        self.fs_detector = fs_detector = fs_adc/decim
        self.feedforward = feedforward = 0
        self.count_rate_probe = count_rate_probe = 0
        self.controller_value = controller_value = 0
        self.y_pre = y_pre = float(target_rate)/float(nparticles) if feedforward else 0
        self.y_max = y_max = 1

        self.rfnoc_graph = uhd_rfnoc_graph = uhd.rfnoc_graph(uhd.device_addr(",fpga=HG_EXFB2"))
        self.spill_on = spill_on = 0
        self.slot = slot = '1'
        self.replay_duration = replay_duration = 5
        self.qtgui_label_trg_count = qtgui_label_trg_count = trg_count
        self.output_level = output_level = 1
        self.output_duration_info_label = output_duration_info_label = 0
        self.output_duration = output_duration = 5
        self.out_label = out_label = controller_value
        self.maxoutbuf = maxoutbuf = 64
        self.manual_trigger = manual_trigger = 0
        self.inp_thr_low = inp_thr_low = -0.5
        self.inp_thr_high = inp_thr_high = -0.3
        self.inp_coupling = inp_coupling = False
        self.info = info = 'Description'
        self.in_scale = in_scale = fs_detector*float(inp_factor)
        self.fs_signal = fs_signal = fs_detector*repeat
        self.fs_gpio = fs_gpio = 2000
        self.feedback = feedback = 0
        self.enable_ext_trg = enable_ext_trg = False
        self.duration = duration = (float(nparticles)/float(target_rate))
        self.delay = delay = 30*1e-6
        self.decim_save = decim_save = 10
        self.daq_save_enabled = daq_save_enabled = False
        self.daq_filename = daq_filename = ""+f"data/%y%m%d/%H%M%S_tmp.{fs_detector/decim_save/1e3:g}kSps.complex64"
        self.count_rate_label = count_rate_label = count_rate_probe
        self.Ta = Ta = float(ta_set)
        self.Kp = Kp = float(kp_set)/float(nparticles)
        self.Ki = Ki = float(ki_set)/float(nparticles)
        self.Kd = Kd = float(kd_set)/float(nparticles)

        ##################################################
        # Blocks
        ##################################################
        snippets_init_before_blocks(self)
        self.tab_output = Qt.QTabWidget()
        self.tab_output_widget_0 = Qt.QWidget()
        self.tab_output_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_output_widget_0)
        self.tab_output_grid_layout_0 = Qt.QGridLayout()
        self.tab_output_layout_0.addLayout(self.tab_output_grid_layout_0)
        self.tab_output.addTab(self.tab_output_widget_0, 'Output (TX1)')
        self.top_grid_layout.addWidget(self.tab_output, 10, 0, 1, 4)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 4):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.tab_input = Qt.QTabWidget()
        self.tab_input_widget_0 = Qt.QWidget()
        self.tab_input_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_input_widget_0)
        self.tab_input_grid_layout_0 = Qt.QGridLayout()
        self.tab_input_layout_0.addLayout(self.tab_input_grid_layout_0)
        self.tab_input.addTab(self.tab_input_widget_0, 'Detector Signal')
        self.top_grid_layout.addWidget(self.tab_input, 0, 0, 1, 4)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 4):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.tab_feedback = Qt.QTabWidget()
        self.tab_feedback_widget_0 = Qt.QWidget()
        self.tab_feedback_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_feedback_widget_0)
        self.tab_feedback_grid_layout_0 = Qt.QGridLayout()
        self.tab_feedback_layout_0.addLayout(self.tab_feedback_grid_layout_0)
        self.tab_feedback.addTab(self.tab_feedback_widget_0, 'Level Controller')
        self.top_grid_layout.addWidget(self.tab_feedback, 2, 0, 1, 4)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 4):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._y_max_tool_bar = Qt.QToolBar(self)
        self._y_max_tool_bar.addWidget(Qt.QLabel("Output limit [0..1]" + ": "))
        self._y_max_line_edit = Qt.QLineEdit(str(self.y_max))
        self._y_max_tool_bar.addWidget(self._y_max_line_edit)
        self._y_max_line_edit.editingFinished.connect(
            lambda: self.set_y_max(eval(str(self._y_max_line_edit.text()))))
        self.tab_feedback_grid_layout_0.addWidget(self._y_max_tool_bar, 2, 1, 1, 1)
        for r in range(2, 3):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        self._target_rate_tool_bar = Qt.QToolBar(self)
        self._target_rate_tool_bar.addWidget(Qt.QLabel("Target rate [particles/s]" + ": "))
        self._target_rate_line_edit = Qt.QLineEdit(str(self.target_rate))
        self._target_rate_tool_bar.addWidget(self._target_rate_line_edit)
        self._target_rate_line_edit.editingFinished.connect(
            lambda: self.set_target_rate(str(str(self._target_rate_line_edit.text()))))
        self.tab_feedback_grid_layout_0.addWidget(self._target_rate_tool_bar, 1, 0, 1, 1)
        for r in range(1, 2):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        # Create the options list
        self._slot_options = ['1', '2', '3', '4', '5']
        # Create the labels list
        self._slot_labels = ['1', '2', '3', '4', '5']
        # Create the combo box
        self._slot_tool_bar = Qt.QToolBar(self)
        self._slot_tool_bar.addWidget(Qt.QLabel("Configuration" + ": "))
        self._slot_combo_box = Qt.QComboBox()
        self._slot_tool_bar.addWidget(self._slot_combo_box)
        for _label in self._slot_labels: self._slot_combo_box.addItem(_label)
        self._slot_callback = lambda i: Qt.QMetaObject.invokeMethod(self._slot_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._slot_options.index(i)))
        self._slot_callback(self.slot)
        self._slot_combo_box.currentIndexChanged.connect(
            lambda i: self.set_slot(self._slot_options[i]))
        # Create the radio buttons
        self.top_grid_layout.addWidget(self._slot_tool_bar, 100, 0, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._output_level_tool_bar = Qt.QToolBar(self)
        self._output_level_tool_bar.addWidget(Qt.QLabel("Excitation level norm" + ": "))
        self._output_level_line_edit = Qt.QLineEdit(str(self.output_level))
        self._output_level_tool_bar.addWidget(self._output_level_line_edit)
        self._output_level_line_edit.editingFinished.connect(
            lambda: self.set_output_level(eval(str(self._output_level_line_edit.text()))))
        self.tab_feedback_grid_layout_0.addWidget(self._output_level_tool_bar, 3, 0, 1, 1)
        for r in range(3, 4):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        self._output_duration_tool_bar = Qt.QToolBar(self)
        self._output_duration_tool_bar.addWidget(Qt.QLabel("Burst duration [s]" + ": "))
        self._output_duration_line_edit = Qt.QLineEdit(str(self.output_duration))
        self._output_duration_tool_bar.addWidget(self._output_duration_line_edit)
        self._output_duration_line_edit.editingFinished.connect(
            lambda: self.set_output_duration(eval(str(self._output_duration_line_edit.text()))))
        self.tab_output_grid_layout_0.addWidget(self._output_duration_tool_bar, 1, 0, 1, 1)
        for r in range(1, 2):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._manual_trigger_choices = {'Pressed': 1, 'Released': 0}

        _manual_trigger_toggle_button = qtgui.ToggleButton(self.set_manual_trigger, 'Manual trigger', self._manual_trigger_choices, False, 'value')
        _manual_trigger_toggle_button.setColors("default", "default", "default", "default")
        self.manual_trigger = _manual_trigger_toggle_button

        self.tab_output_grid_layout_0.addWidget(_manual_trigger_toggle_button, 0, 2, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._inp_thr_low_tool_bar = Qt.QToolBar(self)
        self._inp_thr_low_tool_bar.addWidget(Qt.QLabel("Threshold low [V]" + ": "))
        self._inp_thr_low_line_edit = Qt.QLineEdit(str(self.inp_thr_low))
        self._inp_thr_low_tool_bar.addWidget(self._inp_thr_low_line_edit)
        self._inp_thr_low_line_edit.editingFinished.connect(
            lambda: self.set_inp_thr_low(eval(str(self._inp_thr_low_line_edit.text()))))
        self.tab_input_grid_layout_0.addWidget(self._inp_thr_low_tool_bar, 0, 0, 1, 2)
        for r in range(0, 1):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 2):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        self._inp_thr_high_tool_bar = Qt.QToolBar(self)
        self._inp_thr_high_tool_bar.addWidget(Qt.QLabel("Threshold high [V]" + ": "))
        self._inp_thr_high_line_edit = Qt.QLineEdit(str(self.inp_thr_high))
        self._inp_thr_high_tool_bar.addWidget(self._inp_thr_high_line_edit)
        self._inp_thr_high_line_edit.editingFinished.connect(
            lambda: self.set_inp_thr_high(eval(str(self._inp_thr_high_line_edit.text()))))
        self.tab_input_grid_layout_0.addWidget(self._inp_thr_high_tool_bar, 0, 2, 1, 2)
        for r in range(0, 1):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 4):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        self._inp_factor_tool_bar = Qt.QToolBar(self)
        self._inp_factor_tool_bar.addWidget(Qt.QLabel("Calibration [particles/s per Hz]" + ": "))
        self._inp_factor_line_edit = Qt.QLineEdit(str(self.inp_factor))
        self._inp_factor_tool_bar.addWidget(self._inp_factor_line_edit)
        self._inp_factor_line_edit.editingFinished.connect(
            lambda: self.set_inp_factor(eval(str(self._inp_factor_line_edit.text()))))
        self.tab_input_grid_layout_0.addWidget(self._inp_factor_tool_bar, 1, 2, 1, 3)
        for r in range(1, 2):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 5):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        _inp_coupling_push_button = Qt.QPushButton('Calibrate Offset')
        _inp_coupling_push_button = Qt.QPushButton('Calibrate Offset')
        self._inp_coupling_choices = {'Pressed': True, 'Released': False}
        _inp_coupling_push_button.pressed.connect(lambda: self.set_inp_coupling(self._inp_coupling_choices['Pressed']))
        _inp_coupling_push_button.released.connect(lambda: self.set_inp_coupling(self._inp_coupling_choices['Released']))
        self.tab_input_grid_layout_0.addWidget(_inp_coupling_push_button, 0, 4, 1, 1)
        for r in range(0, 1):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(4, 5):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        # Create the options list
        self._feedforward_options = [0, 1]
        # Create the labels list
        self._feedforward_labels = ['Off', 'Constant']
        # Create the combo box
        self._feedforward_tool_bar = Qt.QToolBar(self)
        self._feedforward_tool_bar.addWidget(Qt.QLabel("Feedforward" + ": "))
        self._feedforward_combo_box = Qt.QComboBox()
        self._feedforward_tool_bar.addWidget(self._feedforward_combo_box)
        for _label in self._feedforward_labels: self._feedforward_combo_box.addItem(_label)
        self._feedforward_callback = lambda i: Qt.QMetaObject.invokeMethod(self._feedforward_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._feedforward_options.index(i)))
        self._feedforward_callback(self.feedforward)
        self._feedforward_combo_box.currentIndexChanged.connect(
            lambda i: self.set_feedforward(self._feedforward_options[i]))
        # Create the radio buttons
        self.tab_feedback_grid_layout_0.addWidget(self._feedforward_tool_bar, 0, 1, 1, 1)
        for r in range(0, 1):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        # Create the options list
        self._feedback_options = [0, 1]
        # Create the labels list
        self._feedback_labels = ['Off', 'On']
        # Create the combo box
        self._feedback_tool_bar = Qt.QToolBar(self)
        self._feedback_tool_bar.addWidget(Qt.QLabel("Feedback" + ": "))
        self._feedback_combo_box = Qt.QComboBox()
        self._feedback_tool_bar.addWidget(self._feedback_combo_box)
        for _label in self._feedback_labels: self._feedback_combo_box.addItem(_label)
        self._feedback_callback = lambda i: Qt.QMetaObject.invokeMethod(self._feedback_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._feedback_options.index(i)))
        self._feedback_callback(self.feedback)
        self._feedback_combo_box.currentIndexChanged.connect(
            lambda i: self.set_feedback(self._feedback_options[i]))
        # Create the radio buttons
        self.tab_feedback_grid_layout_0.addWidget(self._feedback_tool_bar, 1, 1, 1, 1)
        for r in range(1, 2):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        _enable_ext_trg_check_box = Qt.QCheckBox("Enable external trigger")
        self._enable_ext_trg_choices = {True: True, False: False}
        self._enable_ext_trg_choices_inv = dict((v,k) for k,v in self._enable_ext_trg_choices.items())
        self._enable_ext_trg_callback = lambda i: Qt.QMetaObject.invokeMethod(_enable_ext_trg_check_box, "setChecked", Qt.Q_ARG("bool", self._enable_ext_trg_choices_inv[i]))
        self._enable_ext_trg_callback(self.enable_ext_trg)
        _enable_ext_trg_check_box.stateChanged.connect(lambda i: self.set_enable_ext_trg(self._enable_ext_trg_choices[bool(i)]))
        self.tab_output_grid_layout_0.addWidget(_enable_ext_trg_check_box, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._daq_save_enabled_choices = {'Pressed': bool(True), 'Released': bool(False)}

        _daq_save_enabled_toggle_button = qtgui.ToggleButton(self.set_daq_save_enabled, 'Data saving', self._daq_save_enabled_choices, False, 'value')
        _daq_save_enabled_toggle_button.setColors("default", "default", "green", "default")
        self.daq_save_enabled = _daq_save_enabled_toggle_button

        self.tab_input_grid_layout_0.addWidget(_daq_save_enabled_toggle_button, 10, 0, 1, 2)
        for r in range(10, 11):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 2):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        self._daq_filename_tool_bar = Qt.QToolBar(self)
        self._daq_filename_tool_bar.addWidget(Qt.QLabel("Filename" + ": "))
        self._daq_filename_line_edit = Qt.QLineEdit(str(self.daq_filename))
        self._daq_filename_tool_bar.addWidget(self._daq_filename_line_edit)
        self._daq_filename_line_edit.editingFinished.connect(
            lambda: self.set_daq_filename(str(str(self._daq_filename_line_edit.text()))))
        self.tab_input_grid_layout_0.addWidget(self._daq_filename_tool_bar, 10, 2, 1, 3)
        for r in range(10, 11):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 5):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        self.blocks_probe_count_rate = blocks.probe_signal_f()
        self.blocks_probe_controller_output = blocks.probe_signal_f()
        self.uhd_rfnoc_tx_radio_0_0 = uhd.rfnoc_tx_radio(
            self.rfnoc_graph,
            uhd.device_addr(''),
            (-1),
            1)
        self.uhd_rfnoc_tx_radio_0_0.set_rate(fs_adc)
        self.uhd_rfnoc_tx_radio_0_0.set_antenna('AB', 0)
        self.uhd_rfnoc_tx_radio_0_0.set_gain(0, 0)
        self.uhd_rfnoc_tx_radio_0_0.set_bandwidth(0, 0)
        self.uhd_rfnoc_tx_radio_0_0.set_frequency(0, 0)
        self.uhd_rfnoc_split_stream_0_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(),
            "SplitStream",
            (-1),
            1)
        self.uhd_rfnoc_split_stream_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(),
            "SplitStream",
            (-1),
            (-1))
        self.uhd_rfnoc_rx_streamer_0_1_0 = uhd.rfnoc_rx_streamer(
            self.rfnoc_graph,
            1,
            uhd.stream_args(
                cpu_format="fc32",
                otw_format="sc16",
                channels=[],
                args='',
            ),
            1,
            True,
            True,
            uhd.time_spec(1),
        )
        self.uhd_rfnoc_rx_streamer_0_1_0.set_max_output_buffer(maxoutbuf)
        self.uhd_rfnoc_rx_streamer_0_1 = uhd.rfnoc_rx_streamer(
            self.rfnoc_graph,
            1,
            uhd.stream_args(
                cpu_format="fc32",
                otw_format="sc16",
                channels=[],
                args='',
            ),
            1,
            True,
            True,
            uhd.time_spec(1),
        )
        self.uhd_rfnoc_rx_streamer_0_1.set_max_output_buffer(maxoutbuf)
        self.uhd_rfnoc_rx_radio_0 = uhd.rfnoc_rx_radio(
            self.rfnoc_graph,
            uhd.device_addr('spp=40'),
            (-1),
            0)
        self.uhd_rfnoc_rx_radio_0.set_rate(fs_adc)
        self.uhd_rfnoc_rx_radio_0.set_antenna('AB', 0)
        self.uhd_rfnoc_rx_radio_0.set_gain(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_bandwidth(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_frequency(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_dc_offset(inp_coupling, 0)
        self.uhd_rfnoc_rx_radio_0.set_iq_balance(False, 0)
        self.uhd_rfnoc_rx_radio_0.enable_rx_timestamps(True, 0)
        self.uhd_rfnoc_replay_0 = uhd.rfnoc_replay(
            self.rfnoc_graph,
            uhd.device_addr(''),
            (-1),
            (-1))
        self.uhd_rfnoc_replay_0.set_play_type("sc16", 0)
        self.uhd_rfnoc_replay_0.set_property('play_offset',0, 0, typename='uint64_t')
        self.uhd_rfnoc_replay_0.set_property('play_size',(int(4*replay_duration*fs_signal)), 0, typename='uint64_t')
        self.uhd_rfnoc_replay_0.set_max_output_buffer(maxoutbuf)
        self.uhd_rfnoc_keep_one_in_n_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(""),
            "KeepOneInN",
            (-1),
            (-1))
        self.uhd_rfnoc_keep_one_in_n_0.set_property('n', repeat)
        self.uhd_rfnoc_keep_one_in_n_0.set_property('mode', 0)
        self.uhd_rfnoc_duc_0_0 = uhd.rfnoc_duc(
            self.rfnoc_graph,
            uhd.device_addr(''),
            (-1),
            0)
        self.uhd_rfnoc_duc_0_0.set_freq(0, 0)
        self.uhd_rfnoc_duc_0_0.set_input_rate(fs_signal, 0)
        self._ta_set_tool_bar = Qt.QToolBar(self)
        self._ta_set_tool_bar.addWidget(Qt.QLabel("Update Ta [s]" + ": "))
        self._ta_set_line_edit = Qt.QLineEdit(str(self.ta_set))
        self._ta_set_tool_bar.addWidget(self._ta_set_line_edit)
        self._ta_set_line_edit.editingFinished.connect(
            lambda: self.set_ta_set(eval(str(self._ta_set_line_edit.text()))))
        self.tab_feedback_grid_layout_0.addWidget(self._ta_set_tool_bar, 0, 2, 1, 1)
        for r in range(0, 1):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        self.save = _save_toggle_button = qtgui.MsgPushButton('Save config', 'pressed',1,"default","default")
        self.save = _save_toggle_button

        self.top_grid_layout.addWidget(_save_toggle_button, 100, 2, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.restore = _restore_toggle_button = qtgui.MsgPushButton('Restore config', 'pressed',1,"default","default")
        self.restore = _restore_toggle_button

        self.top_grid_layout.addWidget(_restore_toggle_button, 100, 3, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(3, 4):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_ledindicator_0_0_0 = self._qtgui_ledindicator_0_0_0_win = qtgui.GrLEDIndicator('Output', "lime", "black", spill_on, 35, 1, 1, 1, self)
        self.qtgui_ledindicator_0_0_0 = self._qtgui_ledindicator_0_0_0_win
        self.tab_output_grid_layout_0.addWidget(self._qtgui_ledindicator_0_0_0_win, 10, 0, 1, 1)
        for r in range(10, 11):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._qtgui_label_trg_count_tool_bar = Qt.QToolBar(self)

        if None:
            self._qtgui_label_trg_count_formatter = None
        else:
            self._qtgui_label_trg_count_formatter = lambda x: str(x)

        self._qtgui_label_trg_count_tool_bar.addWidget(Qt.QLabel("Trigger count: "))
        self._qtgui_label_trg_count_label = Qt.QLabel(str(self._qtgui_label_trg_count_formatter(self.qtgui_label_trg_count)))
        self._qtgui_label_trg_count_tool_bar.addWidget(self._qtgui_label_trg_count_label)
        self.tab_output_grid_layout_0.addWidget(self._qtgui_label_trg_count_tool_bar, 0, 1, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._output_duration_info_label_tool_bar = Qt.QToolBar(self)

        if None:
            self._output_duration_info_label_formatter = None
        else:
            self._output_duration_info_label_formatter = lambda x: str(x)

        self._output_duration_info_label_tool_bar.addWidget(Qt.QLabel("Gated mode" if output_duration==0 else ""))
        self._output_duration_info_label_label = Qt.QLabel(str(self._output_duration_info_label_formatter(self.output_duration_info_label)))
        self._output_duration_info_label_tool_bar.addWidget(self._output_duration_info_label_label)
        self.tab_output_grid_layout_0.addWidget(self._output_duration_info_label_tool_bar, 1, 1, 1, 1)
        for r in range(1, 2):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._out_label_tool_bar = Qt.QToolBar(self)

        if lambda x: f'{x:.6f}':
            self._out_label_formatter = lambda x: f'{x:.6f}'
        else:
            self._out_label_formatter = lambda x: eng_notation.num_to_str(x)

        self._out_label_tool_bar.addWidget(Qt.QLabel("Controller value:  "))
        self._out_label_label = Qt.QLabel(str(self._out_label_formatter(self.out_label)))
        self._out_label_tool_bar.addWidget(self._out_label_label)
        self.tab_feedback_grid_layout_0.addWidget(self._out_label_tool_bar, 3, 1, 1, 1)
        for r in range(3, 4):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        self._nparticles_tool_bar = Qt.QToolBar(self)
        self._nparticles_tool_bar.addWidget(Qt.QLabel("Particles stored" + ": "))
        self._nparticles_line_edit = Qt.QLineEdit(str(self.nparticles))
        self._nparticles_tool_bar.addWidget(self._nparticles_line_edit)
        self._nparticles_line_edit.editingFinished.connect(
            lambda: self.set_nparticles(str(str(self._nparticles_line_edit.text()))))
        self.tab_feedback_grid_layout_0.addWidget(self._nparticles_tool_bar, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        self._kp_set_tool_bar = Qt.QToolBar(self)
        self._kp_set_tool_bar.addWidget(Qt.QLabel("Kp" + ": "))
        self._kp_set_line_edit = Qt.QLineEdit(str(self.kp_set))
        self._kp_set_tool_bar.addWidget(self._kp_set_line_edit)
        self._kp_set_line_edit.editingFinished.connect(
            lambda: self.set_kp_set(eval(str(self._kp_set_line_edit.text()))))
        self.tab_feedback_grid_layout_0.addWidget(self._kp_set_tool_bar, 1, 2, 1, 1)
        for r in range(1, 2):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        self._ki_set_tool_bar = Qt.QToolBar(self)
        self._ki_set_tool_bar.addWidget(Qt.QLabel("Ki [1/s]" + ": "))
        self._ki_set_line_edit = Qt.QLineEdit(str(self.ki_set))
        self._ki_set_tool_bar.addWidget(self._ki_set_line_edit)
        self._ki_set_line_edit.editingFinished.connect(
            lambda: self.set_ki_set(eval(str(self._ki_set_line_edit.text()))))
        self.tab_feedback_grid_layout_0.addWidget(self._ki_set_tool_bar, 2, 2, 1, 1)
        for r in range(2, 3):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        self._kd_set_tool_bar = Qt.QToolBar(self)
        self._kd_set_tool_bar.addWidget(Qt.QLabel("Kd [s]" + ": "))
        self._kd_set_line_edit = Qt.QLineEdit(str(self.kd_set))
        self._kd_set_tool_bar.addWidget(self._kd_set_line_edit)
        self._kd_set_line_edit.editingFinished.connect(
            lambda: self.set_kd_set(eval(str(self._kd_set_line_edit.text()))))
        self.tab_feedback_grid_layout_0.addWidget(self._kd_set_tool_bar, 3, 2, 1, 1)
        for r in range(3, 4):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        self._info_tool_bar = Qt.QToolBar(self)
        self._info_tool_bar.addWidget(Qt.QLabel("Info" + ": "))
        self._info_line_edit = Qt.QLineEdit(str(self.info))
        self._info_tool_bar.addWidget(self._info_line_edit)
        self._info_line_edit.editingFinished.connect(
            lambda: self.set_info(str(str(self._info_line_edit.text()))))
        self.top_grid_layout.addWidget(self._info_tool_bar, 100, 1, 1, 1)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._duration_tool_bar = Qt.QToolBar(self)

        if lambda x: f"{x:.3f} s":
            self._duration_formatter = lambda x: f"{x:.3f} s"
        else:
            self._duration_formatter = lambda x: eng_notation.num_to_str(x)

        self._duration_tool_bar.addWidget(Qt.QLabel("Expected spill duration:  "))
        self._duration_label = Qt.QLabel(str(self._duration_formatter(self.duration)))
        self._duration_tool_bar.addWidget(self._duration_label)
        self.tab_feedback_grid_layout_0.addWidget(self._duration_tool_bar, 2, 0, 1, 1)
        for r in range(2, 3):
            self.tab_feedback_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_feedback_grid_layout_0.setColumnStretch(c, 1)
        def _count_rate_probe_probe():
          while True:

            val = self.blocks_probe_count_rate.level()
            try:
              try:
                self.doc.add_next_tick_callback(functools.partial(self.set_count_rate_probe,val))
              except AttributeError:
                self.set_count_rate_probe(val)
            except AttributeError:
              pass
            time.sleep(1.0 / (10))
        _count_rate_probe_thread = threading.Thread(target=_count_rate_probe_probe)
        _count_rate_probe_thread.daemon = True
        _count_rate_probe_thread.start()
        self._count_rate_label_tool_bar = Qt.QToolBar(self)

        if lambda x: f'{x:.3e} particles/s':
            self._count_rate_label_formatter = lambda x: f'{x:.3e} particles/s'
        else:
            self._count_rate_label_formatter = lambda x: str(x)

        self._count_rate_label_tool_bar.addWidget(Qt.QLabel("Measured rate:  "))
        self._count_rate_label_label = Qt.QLabel(str(self._count_rate_label_formatter(self.count_rate_label)))
        self._count_rate_label_tool_bar.addWidget(self._count_rate_label_label)
        self.tab_input_grid_layout_0.addWidget(self._count_rate_label_tool_bar, 1, 0, 1, 2)
        for r in range(1, 2):
            self.tab_input_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 2):
            self.tab_input_grid_layout_0.setColumnStretch(c, 1)
        def _controller_value_probe():
          while True:

            val = self.blocks_probe_controller_output.level()
            try:
              try:
                self.doc.add_next_tick_callback(functools.partial(self.set_controller_value,val))
              except AttributeError:
                self.set_controller_value(val)
            except AttributeError:
              pass
            time.sleep(1.0 / (10))
        _controller_value_thread = threading.Thread(target=_controller_value_probe)
        _controller_value_thread.daemon = True
        _controller_value_thread.start()
        self.blocks_multiply_const_xx_1 = blocks.multiply_const_ff(32767*fs_detector*float(inp_factor), 1)
        self.blocks_multiply_const_xx_0 = blocks.multiply_const_cc(1/decim_save, 1)
        self.blocks_msgpair_to_var_0_0 = blocks.msg_pair_to_var(self.set_trg_count)
        self.blocks_msgpair_to_var_0 = blocks.msg_pair_to_var(self.set_spill_on)
        self.blocks_moving_average_xx_0_0 = blocks.moving_average_ff((int(fs_detector*100e-3)), (1/int(fs_detector*100e-3)), 4000, 1)
        self.blocks_integrate_xx_0 = blocks.integrate_cc(decim_save, 1)
        self.blocks_float_to_complex_1 = blocks.float_to_complex(1)
        self.blocks_complex_to_real_0_0 = blocks.complex_to_real(1)
        self.blocks_complex_to_real_0 = blocks.complex_to_real(1)
        self.beam_exciter_variableFileDump_0 = beam_exciter.variableFileDump(daq_filename + ".yml", daq_save_enabled)
        self.beam_exciter_saveRestoreVariables_1 = beam_exciter.saveRestoreVariables(self, slot, """
        inp_thr_low,inp_thr_high,inp_factor,
        daq_filename,
        nparticles,feedforward,ta_set,
        target_rate,feedback,kp_set,
        y_max,ki_set,
        output_level,kd_set,
        output_duration,
        """, "variable_to_trigger_save", "variable_to_trigger_restore", "info")
        self.beam_exciter_rfnoc_timestamp_mod_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(""),
            "TimestampMod",
            -1, # device_select
            -1, # instance_index
        )
        self.beam_exciter_rfnoc_timestamp_mod_0.set_property('ts_remove', int(False))
        self.beam_exciter_rfnoc_timestamp_mod_0.set_property('ts_offset', int((int(fs_adc*(delay-3e-6)))))
        self.beam_exciter_rfnoc_multiply_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(""),
            "Multiply",
            -1, # device_select
            -1, # instance_index
        )
        self.beam_exciter_rfnoc_feedback_controller_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(""),
            "FeedbackController",
            -1, # device_select
            -1, # instance_index
        )
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward', int(feedforward))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedback', int(feedback))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ta', int((int(Ta*fs_detector))))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('target', float(float(target_rate)/in_scale))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kp', float(Kp*in_scale))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ki_times_ta', float(Ki*Ta*in_scale))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kd_over_ta', float(Kd/Ta*in_scale))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward_const', float(y_pre))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_scale', float(float(output_level)))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_min', float(0))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_max', float(float(y_max)/float(output_level)))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('repeat', int(repeat))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('active', int(spill_on))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(""),
            "DiscriminatingPulseCounter",
            -1, # device_select
            -1, # instance_index
        )
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_low', float(min(-inp_thr_low, -inp_thr_high)))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_high', float(max(-inp_thr_low, -inp_thr_high)))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('decimation', decim)
        self.beam_exciter_replay_autostart_0 = beam_exciter.replayAutoStart("play", 0, (-1), (-1), (-1), True)
        self.beam_exciter_replayRestarter_0 = beam_exciter.replayRestarter(0, (-1), (-1), (-1), True)
        self.beam_exciter_gpioTrigger_0 = beam_exciter.gpioTrigger(
            itemsize=gr.sizeof_gr_complex,
            enable_polling=enable_ext_trg,
            poll_every_samples=(int(fs_detector/fs_gpio)),
            duration_samples=(int(output_duration * fs_detector)),
            tag_key_start=ensure_pmt("SOB"),
            tag_key_stop=ensure_pmt("EOB"),
            manual_trigger=manual_trigger,
            ptr_graph=getattr(self, "rfnoc_graph", None),
            device_addr='addr=192.168.10.2',
            device_index=0,
            radio_index=0,
            gpio_bank='RXA',
            gpio_pin=0,
            inverted=False,
        )
        self.beam_exciter_burstFileSink_0 = beam_exciter.burstFileSink(gr.sizeof_gr_complex, daq_filename, ensure_pmt(pmt.intern("SOB")), ensure_pmt(pmt.intern("EOB")), (int(60*fs_detector)), daq_save_enabled)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.beam_exciter_gpioTrigger_0, 'active'), (self.blocks_msgpair_to_var_0, 'inpair'))
        self.msg_connect((self.beam_exciter_gpioTrigger_0, 'count'), (self.blocks_msgpair_to_var_0_0, 'inpair'))
        self.msg_connect((self.beam_exciter_replayRestarter_0, 'cmd'), (self.uhd_rfnoc_replay_0, 'command'))
        self.msg_connect((self.beam_exciter_replay_autostart_0, 'cmd'), (self.uhd_rfnoc_replay_0, 'command'))
        self.msg_connect((self.restore, 'pressed'), (self.beam_exciter_saveRestoreVariables_1, 'restore'))
        self.msg_connect((self.save, 'pressed'), (self.beam_exciter_saveRestoreVariables_1, 'save'))
        self.rfnoc_graph.connect(self.beam_exciter_rfnoc_discriminating_pulse_counter_0.get_unique_id(), 0, self.uhd_rfnoc_split_stream_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.beam_exciter_rfnoc_feedback_controller_0.get_unique_id(), 0, self.uhd_rfnoc_split_stream_0_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.beam_exciter_rfnoc_multiply_0.get_unique_id(), 0, self.beam_exciter_rfnoc_timestamp_mod_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.beam_exciter_rfnoc_timestamp_mod_0.get_unique_id(), 0, self.uhd_rfnoc_duc_0_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_duc_0_0.get_unique_id(), 0, self.uhd_rfnoc_tx_radio_0_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_keep_one_in_n_0.get_unique_id(), 0, self.uhd_rfnoc_rx_streamer_0_1_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_replay_0.get_unique_id(), 0, self.beam_exciter_rfnoc_multiply_0.get_unique_id(), 1, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_rx_radio_0.get_unique_id(), 0, self.beam_exciter_rfnoc_discriminating_pulse_counter_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_split_stream_0.get_unique_id(), 1, self.beam_exciter_rfnoc_feedback_controller_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_split_stream_0.get_unique_id(), 0, self.uhd_rfnoc_rx_streamer_0_1.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_split_stream_0_0.get_unique_id(), 0, self.beam_exciter_rfnoc_multiply_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_split_stream_0_0.get_unique_id(), 1, self.uhd_rfnoc_keep_one_in_n_0.get_unique_id(), 0, False)
        self.connect((self.beam_exciter_gpioTrigger_0, 0), (self.blocks_complex_to_real_0, 0))
        self.connect((self.blocks_complex_to_real_0, 0), (self.blocks_multiply_const_xx_1, 0))
        self.connect((self.blocks_complex_to_real_0_0, 0), (self.blocks_float_to_complex_1, 1))
        self.connect((self.blocks_complex_to_real_0_0, 0), (self.blocks_probe_controller_output, 0))
        self.connect((self.blocks_float_to_complex_1, 0), (self.blocks_integrate_xx_0, 0))
        self.connect((self.blocks_integrate_xx_0, 0), (self.blocks_multiply_const_xx_0, 0))
        self.connect((self.blocks_moving_average_xx_0_0, 0), (self.blocks_probe_count_rate, 0))
        self.connect((self.blocks_multiply_const_xx_0, 0), (self.beam_exciter_burstFileSink_0, 0))
        self.connect((self.blocks_multiply_const_xx_1, 0), (self.blocks_float_to_complex_1, 0))
        self.connect((self.blocks_multiply_const_xx_1, 0), (self.blocks_moving_average_xx_0_0, 0))
        self.connect((self.uhd_rfnoc_rx_streamer_0_1, 0), (self.beam_exciter_gpioTrigger_0, 0))
        self.connect((self.uhd_rfnoc_rx_streamer_0_1_0, 0), (self.blocks_complex_to_real_0_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "exciter_extraction_rfnoc_feedback")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_fs_adc(self):
        return self.fs_adc

    def set_fs_adc(self, fs_adc):
        self.fs_adc = fs_adc
        self.set_fs_detector(self.fs_adc/self.decim)
        self.uhd_rfnoc_rx_radio_0.set_rate(self.fs_adc)
        self.uhd_rfnoc_tx_radio_0_0.set_rate(self.fs_adc)

    def get_decim(self):
        return self.decim

    def set_decim(self, decim):
        self.decim = decim
        self.set_fs_detector(self.fs_adc/self.decim)
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('decimation', self.decim)

    def get_trg_count(self):
        return self.trg_count

    def set_trg_count(self, trg_count):
        self.trg_count = trg_count
        self.set_qtgui_label_trg_count(self.trg_count)
        self.beam_exciter_replayRestarter_0.set_trigger(self.trg_count)

    def get_target_rate(self):
        return self.target_rate

    def set_target_rate(self, target_rate):
        self.target_rate = target_rate
        self.set_duration((float(self.nparticles)/float(self.target_rate)))
        Qt.QMetaObject.invokeMethod(self._target_rate_line_edit, "setText", Qt.Q_ARG("QString", str(self.target_rate)))
        self.set_y_pre(float(self.target_rate)/float(self.nparticles) if self.feedforward else 0)
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('target', float(float(self.target_rate)/self.in_scale))

    def get_ta_set(self):
        return self.ta_set

    def set_ta_set(self, ta_set):
        self.ta_set = ta_set
        self.set_Ta(float(self.ta_set))
        Qt.QMetaObject.invokeMethod(self._ta_set_line_edit, "setText", Qt.Q_ARG("QString", repr(self.ta_set)))

    def get_repeat(self):
        return self.repeat

    def set_repeat(self, repeat):
        self.repeat = repeat
        self.set_fs_signal(self.fs_detector*self.repeat)
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('repeat', int(self.repeat))
        self.uhd_rfnoc_keep_one_in_n_0.set_property('n', self.repeat)

    def get_nparticles(self):
        return self.nparticles

    def set_nparticles(self, nparticles):
        self.nparticles = nparticles
        self.set_Kd(float(self.kd_set)/float(self.nparticles))
        self.set_Ki(float(self.ki_set)/float(self.nparticles))
        self.set_Kp(float(self.kp_set)/float(self.nparticles))
        self.set_duration((float(self.nparticles)/float(self.target_rate)))
        Qt.QMetaObject.invokeMethod(self._nparticles_line_edit, "setText", Qt.Q_ARG("QString", str(self.nparticles)))
        self.set_y_pre(float(self.target_rate)/float(self.nparticles) if self.feedforward else 0)

    def get_kp_set(self):
        return self.kp_set

    def set_kp_set(self, kp_set):
        self.kp_set = kp_set
        self.set_Kp(float(self.kp_set)/float(self.nparticles))
        Qt.QMetaObject.invokeMethod(self._kp_set_line_edit, "setText", Qt.Q_ARG("QString", repr(self.kp_set)))

    def get_ki_set(self):
        return self.ki_set

    def set_ki_set(self, ki_set):
        self.ki_set = ki_set
        self.set_Ki(float(self.ki_set)/float(self.nparticles))
        Qt.QMetaObject.invokeMethod(self._ki_set_line_edit, "setText", Qt.Q_ARG("QString", repr(self.ki_set)))

    def get_kd_set(self):
        return self.kd_set

    def set_kd_set(self, kd_set):
        self.kd_set = kd_set
        self.set_Kd(float(self.kd_set)/float(self.nparticles))
        Qt.QMetaObject.invokeMethod(self._kd_set_line_edit, "setText", Qt.Q_ARG("QString", repr(self.kd_set)))

    def get_inp_factor(self):
        return self.inp_factor

    def set_inp_factor(self, inp_factor):
        self.inp_factor = inp_factor
        self.set_in_scale(self.fs_detector*float(self.inp_factor))
        Qt.QMetaObject.invokeMethod(self._inp_factor_line_edit, "setText", Qt.Q_ARG("QString", repr(self.inp_factor)))
        self.blocks_multiply_const_xx_1.set_k(32767*self.fs_detector*float(self.inp_factor))

    def get_fs_detector(self):
        return self.fs_detector

    def set_fs_detector(self, fs_detector):
        self.fs_detector = fs_detector
        self.set_fs_signal(self.fs_detector*self.repeat)
        self.set_in_scale(self.fs_detector*float(self.inp_factor))
        self.beam_exciter_burstFileSink_0.set_length((int(60*self.fs_detector)))
        self.beam_exciter_gpioTrigger_0.set_duration_samples((int(self.output_duration * self.fs_detector)))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ta', int((int(self.Ta*self.fs_detector))))
        self.blocks_moving_average_xx_0_0.set_length_and_scale((int(self.fs_detector*100e-3)), (1/int(self.fs_detector*100e-3)))
        self.blocks_multiply_const_xx_1.set_k(32767*self.fs_detector*float(self.inp_factor))

    def get_feedforward(self):
        return self.feedforward

    def set_feedforward(self, feedforward):
        self.feedforward = feedforward
        self._feedforward_callback(self.feedforward)
        self.set_y_pre(float(self.target_rate)/float(self.nparticles) if self.feedforward else 0)
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward', int(self.feedforward))

    def get_count_rate_probe(self):
        return self.count_rate_probe

    def set_count_rate_probe(self, count_rate_probe):
        self.count_rate_probe = count_rate_probe
        self.set_count_rate_label(self.count_rate_probe)

    def get_controller_value(self):
        return self.controller_value

    def set_controller_value(self, controller_value):
        self.controller_value = controller_value
        self.set_out_label(self.controller_value)

    def get_y_pre(self):
        return self.y_pre

    def set_y_pre(self, y_pre):
        self.y_pre = y_pre
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward_const', float(self.y_pre))

    def get_y_max(self):
        return self.y_max

    def set_y_max(self, y_max):
        self.y_max = y_max
        Qt.QMetaObject.invokeMethod(self._y_max_line_edit, "setText", Qt.Q_ARG("QString", repr(self.y_max)))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_max', float(float(self.y_max)/float(self.output_level)))

    def get_uhd_rfnoc_graph(self):
        return self.uhd_rfnoc_graph

    def set_uhd_rfnoc_graph(self, uhd_rfnoc_graph):
        self.uhd_rfnoc_graph = uhd_rfnoc_graph

    def get_spill_on(self):
        return self.spill_on

    def set_spill_on(self, spill_on):
        self.spill_on = spill_on
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('active', int(self.spill_on))
        self.beam_exciter_variableFileDump_0.save(self.spill_on, (f"""
        # Metadata
        timestamp: {datetime.datetime.now()}

        # Sample rates
        fs_adc: {self.fs_adc} # Hz
        decim: {self.decim}
        fs_detector: {self.fs_detector} # Hz
        repeat: {self.repeat}
        fs_signal: {self.fs_signal} # Hz
        fs_gpio: {self.fs_gpio} # Hz

        # Pulse counter
        inp_thr_low: {self.inp_thr_low} # V
        inp_thr_high: {self.inp_thr_high} # V
        inp_factor: {self.inp_factor} # particles/s per Hz

        # Feedback controller
        nparticles: {self.nparticles}
        target_rate: {self.target_rate} # particles/s
        output_level: {self.output_level}
        feedforward: {self.feedforward}
        feedback: {self.feedback}
        ta_set: {self.ta_set}
        kp_set: {self.kp_set}
        ki_set: {self.ki_set} # 1/s
        kd_set: {self.kd_set} # s
        y_max: {self.y_max}
        delay: {self.delay:g} # s

        # Excitation signal
        replay_duration: {self.replay_duration} # s

        # Output
        enable_ext_trg: {self.enable_ext_trg}
        trg_count: {self.trg_count}
        output_duration: {self.output_duration} # s



        """))
        self.qtgui_ledindicator_0_0_0.setState(self.spill_on)

    def get_slot(self):
        return self.slot

    def set_slot(self, slot):
        self.slot = slot
        self._slot_callback(self.slot)
        self.beam_exciter_saveRestoreVariables_1.set_slot(self.slot)

    def get_replay_duration(self):
        return self.replay_duration

    def set_replay_duration(self, replay_duration):
        self.replay_duration = replay_duration
        self.uhd_rfnoc_replay_0.set_property('play_size',(int(4*self.replay_duration*self.fs_signal)), 0, typename='uint64_t')

    def get_qtgui_label_trg_count(self):
        return self.qtgui_label_trg_count

    def set_qtgui_label_trg_count(self, qtgui_label_trg_count):
        self.qtgui_label_trg_count = qtgui_label_trg_count
        Qt.QMetaObject.invokeMethod(self._qtgui_label_trg_count_label, "setText", Qt.Q_ARG("QString", str(self._qtgui_label_trg_count_formatter(self.qtgui_label_trg_count))))

    def get_output_level(self):
        return self.output_level

    def set_output_level(self, output_level):
        self.output_level = output_level
        Qt.QMetaObject.invokeMethod(self._output_level_line_edit, "setText", Qt.Q_ARG("QString", repr(self.output_level)))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_scale', float(float(self.output_level)))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_max', float(float(self.y_max)/float(self.output_level)))

    def get_output_duration_info_label(self):
        return self.output_duration_info_label

    def set_output_duration_info_label(self, output_duration_info_label):
        self.output_duration_info_label = output_duration_info_label
        Qt.QMetaObject.invokeMethod(self._output_duration_info_label_label, "setText", Qt.Q_ARG("QString", str(self._output_duration_info_label_formatter(self.output_duration_info_label))))

    def get_output_duration(self):
        return self.output_duration

    def set_output_duration(self, output_duration):
        self.output_duration = output_duration
        Qt.QMetaObject.invokeMethod(self._output_duration_line_edit, "setText", Qt.Q_ARG("QString", repr(self.output_duration)))
        self.beam_exciter_gpioTrigger_0.set_duration_samples((int(self.output_duration * self.fs_detector)))

    def get_out_label(self):
        return self.out_label

    def set_out_label(self, out_label):
        self.out_label = out_label
        Qt.QMetaObject.invokeMethod(self._out_label_label, "setText", Qt.Q_ARG("QString", str(self._out_label_formatter(self.out_label))))

    def get_maxoutbuf(self):
        return self.maxoutbuf

    def set_maxoutbuf(self, maxoutbuf):
        self.maxoutbuf = maxoutbuf

    def get_manual_trigger(self):
        return self.manual_trigger

    def set_manual_trigger(self, manual_trigger):
        self.manual_trigger = manual_trigger
        self.beam_exciter_gpioTrigger_0.set_manual_trigger(self.manual_trigger)

    def get_inp_thr_low(self):
        return self.inp_thr_low

    def set_inp_thr_low(self, inp_thr_low):
        self.inp_thr_low = inp_thr_low
        Qt.QMetaObject.invokeMethod(self._inp_thr_low_line_edit, "setText", Qt.Q_ARG("QString", repr(self.inp_thr_low)))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_low', float(min(-self.inp_thr_low, -self.inp_thr_high)))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_high', float(max(-self.inp_thr_low, -self.inp_thr_high)))

    def get_inp_thr_high(self):
        return self.inp_thr_high

    def set_inp_thr_high(self, inp_thr_high):
        self.inp_thr_high = inp_thr_high
        Qt.QMetaObject.invokeMethod(self._inp_thr_high_line_edit, "setText", Qt.Q_ARG("QString", repr(self.inp_thr_high)))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_low', float(min(-self.inp_thr_low, -self.inp_thr_high)))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_high', float(max(-self.inp_thr_low, -self.inp_thr_high)))

    def get_inp_coupling(self):
        return self.inp_coupling

    def set_inp_coupling(self, inp_coupling):
        self.inp_coupling = inp_coupling
        self.uhd_rfnoc_rx_radio_0.set_dc_offset(self.inp_coupling, 0)

    def get_info(self):
        return self.info

    def set_info(self, info):
        self.info = info
        Qt.QMetaObject.invokeMethod(self._info_line_edit, "setText", Qt.Q_ARG("QString", str(self.info)))

    def get_in_scale(self):
        return self.in_scale

    def set_in_scale(self, in_scale):
        self.in_scale = in_scale
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('target', float(float(self.target_rate)/self.in_scale))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kp', float(self.Kp*self.in_scale))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ki_times_ta', float(self.Ki*self.Ta*self.in_scale))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kd_over_ta', float(self.Kd/self.Ta*self.in_scale))

    def get_fs_signal(self):
        return self.fs_signal

    def set_fs_signal(self, fs_signal):
        self.fs_signal = fs_signal
        self.uhd_rfnoc_duc_0_0.set_input_rate(self.fs_signal, 0)
        self.uhd_rfnoc_replay_0.set_property('play_size',(int(4*self.replay_duration*self.fs_signal)), 0, typename='uint64_t')

    def get_fs_gpio(self):
        return self.fs_gpio

    def set_fs_gpio(self, fs_gpio):
        self.fs_gpio = fs_gpio

    def get_feedback(self):
        return self.feedback

    def set_feedback(self, feedback):
        self.feedback = feedback
        self._feedback_callback(self.feedback)
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedback', int(self.feedback))

    def get_enable_ext_trg(self):
        return self.enable_ext_trg

    def set_enable_ext_trg(self, enable_ext_trg):
        self.enable_ext_trg = enable_ext_trg
        self._enable_ext_trg_callback(self.enable_ext_trg)
        self.beam_exciter_gpioTrigger_0.set_enable_polling(self.enable_ext_trg)

    def get_duration(self):
        return self.duration

    def set_duration(self, duration):
        self.duration = duration
        Qt.QMetaObject.invokeMethod(self._duration_label, "setText", Qt.Q_ARG("QString", str(self._duration_formatter(self.duration))))

    def get_delay(self):
        return self.delay

    def set_delay(self, delay):
        self.delay = delay

    def get_decim_save(self):
        return self.decim_save

    def set_decim_save(self, decim_save):
        self.decim_save = decim_save
        self.blocks_multiply_const_xx_0.set_k(1/self.decim_save)

    def get_daq_save_enabled(self):
        return self.daq_save_enabled

    def set_daq_save_enabled(self, daq_save_enabled):
        self.daq_save_enabled = daq_save_enabled
        self.beam_exciter_burstFileSink_0.set_enable(self.daq_save_enabled)
        self.beam_exciter_variableFileDump_0.set_enable(self.daq_save_enabled)

    def get_daq_filename(self):
        return self.daq_filename

    def set_daq_filename(self, daq_filename):
        self.daq_filename = daq_filename
        Qt.QMetaObject.invokeMethod(self._daq_filename_line_edit, "setText", Qt.Q_ARG("QString", str(self.daq_filename)))
        self.beam_exciter_burstFileSink_0.set_filename(self.daq_filename)
        self.beam_exciter_variableFileDump_0.set_filename(self.daq_filename + ".yml")

    def get_count_rate_label(self):
        return self.count_rate_label

    def set_count_rate_label(self, count_rate_label):
        self.count_rate_label = count_rate_label
        Qt.QMetaObject.invokeMethod(self._count_rate_label_label, "setText", Qt.Q_ARG("QString", str(self._count_rate_label_formatter(self.count_rate_label))))

    def get_Ta(self):
        return self.Ta

    def set_Ta(self, Ta):
        self.Ta = Ta
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ta', int((int(self.Ta*self.fs_detector))))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ki_times_ta', float(self.Ki*self.Ta*self.in_scale))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kd_over_ta', float(self.Kd/self.Ta*self.in_scale))

    def get_Kp(self):
        return self.Kp

    def set_Kp(self, Kp):
        self.Kp = Kp
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kp', float(self.Kp*self.in_scale))

    def get_Ki(self):
        return self.Ki

    def set_Ki(self, Ki):
        self.Ki = Ki
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ki_times_ta', float(self.Ki*self.Ta*self.in_scale))

    def get_Kd(self):
        return self.Kd

    def set_Kd(self, Kd):
        self.Kd = Kd
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kd_over_ta', float(self.Kd/self.Ta*self.in_scale))




def main(top_block_cls=exciter_extraction_rfnoc_feedback, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        gr.logger("realtime").warning("Error: failed to enable real-time scheduling.")

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
