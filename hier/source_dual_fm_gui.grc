options:
  parameters:
    author: Philipp Niedermayer
    catch_exceptions: 'True'
    category: '[Beam exciter]/GUI'
    cmake_opt: ''
    comment: ''
    copyright: "Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum f\xFCr Schwerionenforschung"
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: hb_qt_gui
    hier_block_src_path: '.:'
    id: source_dual_fm_gui
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: QT GUI Dual FM source
    window_size: (1000,1000)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 12.0]
    rotation: 0
    state: enabled

blocks:
- name: aex
  id: variable_qtgui_entry
  parameters:
    comment: ''
    entry_signal: editingFinished
    gui_hint: 10,3
    label: Amplitude
    type: raw
    value: '0.3'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [408, 196.0]
    rotation: 0
    state: enabled
- name: dfex
  id: variable
  parameters:
    comment: ''
    value: dqex*frev
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [280, 276.0]
    rotation: 180
    state: enabled
- name: dqex
  id: variable_qtgui_entry
  parameters:
    comment: ''
    entry_signal: editingFinished
    gui_hint: 10,2
    label: Width dq
    type: raw
    value: '0.01'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [280, 196.0]
    rotation: 0
    state: enabled
- name: dtex
  id: variable_qtgui_entry
  parameters:
    comment: ''
    entry_signal: editingFinished
    gui_hint: 10,4
    label: Period [s]
    type: raw
    value: '0.001'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [544, 196.0]
    rotation: 0
    state: enabled
- name: ex_enabled
  id: variable_qtgui_check_box
  parameters:
    comment: ''
    'false': '0'
    gui_hint: 10,0
    label: '"" + label_prefix + "Dual FM signal"'
    'true': '1'
    type: real
    value: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 196.0]
    rotation: 0
    state: true
- name: fex
  id: variable
  parameters:
    comment: ''
    value: qex*frev
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [144, 276.0]
    rotation: 180
    state: enabled
- name: qex
  id: variable_qtgui_entry
  parameters:
    comment: ''
    entry_signal: editingFinished
    gui_hint: 10,1
    label: Tune q
    type: raw
    value: '0.340'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [144, 196.0]
    rotation: 0
    state: enabled
- name: blocks_complex_to_real_0
  id: blocks_complex_to_real
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [504, 416.0]
    rotation: 0
    state: true
- name: blocks_multiply_ex1
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [360, 400.0]
    rotation: 0
    state: enabled
- name: ex1_analog_sig_source
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: aex*ex_enabled
    comment: ''
    freq: fex
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: samp_rate
    showports: 'False'
    type: complex
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [56, 356.0]
    rotation: 0
    state: enabled
- name: ex5_source_dual_fm
  id: source_dual_fm
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    period: dtex
    samp_rate: samp_rate
    width: dfex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [80, 500.0]
    rotation: 0
    state: enabled
- name: frev
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: frev[Hz]
    short_id: ''
    type: eng_float
    value: 1e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [376, 12.0]
    rotation: 0
    state: true
- name: import_0_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import scipy.signal
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [872, 12.0]
    rotation: 0
    state: true
- name: label_prefix
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: part
    label: label prefix
    short_id: ''
    type: str
    value: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [480, 12.0]
    rotation: 0
    state: true
- name: pad_sink_0
  id: pad_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    label: out
    num_streams: '1'
    optional: 'False'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [688, 412.0]
    rotation: 0
    state: true
- name: samp_rate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: samp_rate [Hz]
    short_id: ''
    type: eng_float
    value: 5e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [240, 12.0]
    rotation: 0
    state: true

connections:
- [blocks_complex_to_real_0, '0', pad_sink_0, '0']
- [blocks_multiply_ex1, '0', blocks_complex_to_real_0, '0']
- [ex1_analog_sig_source, '0', blocks_multiply_ex1, '0']
- [ex5_source_dual_fm, '0', blocks_multiply_ex1, '1']

metadata:
  file_format: 1
  grc_version: 3.10.8.0
