options:
  parameters:
    author: Philipp Niedermayer
    catch_exceptions: 'True'
    category: '[Beam exciter]/Signal sources'
    cmake_opt: ''
    comment: ''
    copyright: "Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum f\xFCr Schwerionenforschung"
    description: Random binary phase shift keying
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: hb
    hier_block_src_path: '.:'
    id: source_rbpsk
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: RBPSK source
    window_size: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 12.0]
    rotation: 0
    state: enabled

blocks:
- name: repeat
  id: variable
  parameters:
    comment: ''
    value: max(1, int(samp_rate/width)) if width>0 else 1
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [496, 12.0]
    rotation: 0
    state: true
- name: analog_phase_modulator_fc_0_0
  id: analog_phase_modulator_fc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    sensitivity: 3.141592653589793 if width>0 else 0
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [456, 212.0]
    rotation: 0
    state: enabled
- name: analog_random_source_x_1_0
  id: analog_random_source_x
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    max: '2'
    maxoutbuf: '0'
    min: '0'
    minoutbuf: '0'
    num_samps: '10101001'
    repeat: 'True'
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [96, 188.0]
    rotation: 0
    state: enabled
- name: blocks_char_to_float_0_0
  id: blocks_char_to_float
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    scale: '1'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [296, 212.0]
    rotation: 0
    state: enabled
- name: blocks_repeat_0_0
  id: blocks_repeat
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    interp: repeat
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [640, 212.0]
    rotation: 0
    state: enabled
- name: pad_sink_0
  id: pad_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    label: out
    num_streams: '1'
    optional: 'False'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [808, 212.0]
    rotation: 0
    state: enabled
- name: samp_rate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Sample rate [Hz]
    short_id: ''
    type: eng_float
    value: 5e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [200, 12.0]
    rotation: 0
    state: true
- name: width
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Bandwidth [Hz]
    short_id: ''
    type: eng_float
    value: 1e5
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [352, 12.0]
    rotation: 0
    state: enabled

connections:
- [analog_phase_modulator_fc_0_0, '0', blocks_repeat_0_0, '0']
- [analog_random_source_x_1_0, '0', blocks_char_to_float_0_0, '0']
- [blocks_char_to_float_0_0, '0', analog_phase_modulator_fc_0_0, '0']
- [blocks_repeat_0_0, '0', pad_sink_0, '0']

metadata:
  file_format: 1
  grc_version: 3.10.8.0
