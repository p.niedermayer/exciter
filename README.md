# Beam excitation signal generator based on software-defined radio

This repository contains the [GNU Radio](https://wiki.gnuradio.org/index.php?title=Main_Page) flow graphs, hier, OOT and RFNoC blocks for use with a [Software Defined Radio](https://en.wikipedia.org/wiki/Software-defined_radio) such as the Ettus [USRP N210](https://www.ettus.com/all-products/un210-kit) or [X310](https://www.ettus.com/all-products/x310-kit).
It enables triggered signal generation for transverse excitation of stored particle beams as required for tune measurement and RF Knock Out resonant slow extraction.
The following publications give a first introduction:

---
1️⃣ P. Niedermayer and R. Singh:
**"Novel beam excitation system based on software-defined radio"**, in *Proc. IBIC'22*, Krakow, Poland, Sept 2022.
https://doi.org/10.18429/JACoW-IBIC2022-MOP36  
<details><summary>Abstract</summary>

> _A signal generator for transverse excitation of stored particle beams is developed and commissioned at GSI SIS18. 
Thereby a novel approach using a software-defined radio system and the open-source GNU Radio ecosystem is taken. 
This allows for a low cost yet highly flexible setup for creating customizable and tuneable excitation spectra, which -- due to its open-source nature -- has the potential for long term maintainability and integrability into the accelerator environment. 
Furthermore, this opens up the possibility to easily share waveform generation algorithms across accelerator facilities._ 

</details>

---
2️⃣ P. Niedermayer, R. Geißler and R. Singh: **"Software-Defined Radio Based Feedback System for Beam Spill Control in Particle Accelerators"**, in *Proc. GRCon 2023*, Tempe, Arizona, US, Sept 2023.
https://pubs.gnuradio.org/index.php/grcon/article/view/133
<details><summary>Abstract</summary>

> _Controlling stored beams in particle accelerator requires specially designed RF signals, such as needed for spill control via transverse excitation.
The software-defined radio (SDR) technology is adopted as a low cost, yet highly flexible setup to generate such signals in the kHz to MHz regime.
A feedback system is build using a combination of digital signal processing with GNU Radio and RF Network-on-Chip (RFNoC) on a Universal Software Radio Peripheral (USRP).
The system enables digitization of signals from particle detectors and direct tuning of the produced RF waveforms via a feedback controller - implemented on a single device.
To allow for triggered operation and to reduce the loop delay to a few ms, custom OOT and RFNoC blocks have been implemented.
This contribution reports on the implementation and first test results with beam of the developed spill control system._

</details>

---



# Documentation

For documentation please refere to the description below, the **[→ Wiki Pages](https://git.gsi.de/p.niedermayer/exciter/-/wikis/home)**, and the documentation tab of the block properties in GNU Radio.

For stable versions please refer to the [→ Release Page](https://git.gsi.de/p.niedermayer/exciter/-/releases).



# Contents



## Flow graphs

The top level flow graphs are located in the root folder and the respective python files can directly be executed. They provide a GUI to control and monitor the signal generation.


- `exciter_extraction`  
  **Beam Exciter for KO extraction**  
  This flow graph can be used to generate excitation signals for the purpose of slow resonant RF Knock-Out extraction. The excitation amplitude is defined as a function of time via an arbitrary ramp, such as to maintain a constant spill rate.  
  [→ Usage instructions](https://git.gsi.de/p.niedermayer/exciter/-/wikis/Operation/exciter_extraction)

- `exciter_extraction_feedback_optimize` 2️⃣    
  **Beam Exciter for KO extraction with feedback and signal optimisation**  
  This flow graph can be used to generate excitation signals for the purpose of slow resonant RF Knock-Out extraction. The excitation amplitude is controlled by a feedback loop from the measured spill rate.
The excitation signal can automatically be optimized to improve spill quality, and detector data can be recorded along with the feedback output.  
  [→ Usage instructions](https://git.gsi.de/p.niedermayer/exciter/-/wikis/Operation/exciter_extraction_feedback_optimize)

- `exciter_from_file`  
  **Beam Exciter with arbitrary signal from file**  
  This flow graph can be used to play excitation signals from a file. The playback is triggered and always starts from the beginning of the file, repeating the waveform for the specified burst duration.  
  [→ Usage instructions](https://git.gsi.de/p.niedermayer/exciter/-/wikis/Operation/exciter_from_file)

- `exciter_tune` 1️⃣  
  **Beam Exciter for Tune Measurement**  
  This flow graph can be used to generate excitation signals for the purpose of tune measurement. Two independend signals for X and Y are supported, and the excitation spectrum is scaled along with the increasing revolution frequency during acceleration based on an RF reference signal.  
  [→ Usage instructions](https://git.gsi.de/p.niedermayer/exciter/-/wikis/Operation/exciter_tune)

- `exciter_sweep`  
  **Beam Exciter with frequency swept sine**  
  Simple sinusoidal excitation with a configurable triggered linear sweep. Useful for beam transfer function (BTF) measurements etc.  
  [→ Usage instructions](https://git.gsi.de/p.niedermayer/exciter/-/wikis/Operation/exciter_sweep)



## GNU Radio OOT module (`gr-beam_exciter`)
The custom GNU Radio OOT blocks are contained in the [gr-beam_exciter](gr-beam_exciter) module.

*For an extensive documentation of the functionality and parameters of these blocks, please refere to the documentation tab in GNU Radio Companion or the block defining [YML files](gr-beam_exciter/grc). Usage examples can be found in the [examples directory](gr-beam_exciter/examples).*

- **Arbitrary Ramp**  
  Creates single shot arbitrary ramps defined by a CSV files
- **Burst Evaluate**  
  Evaluates an objective function on a bursty stream emitting messages regularly
- **Burst File Sink**  
  Saves a bursty stream to a file, either overwriting the file or incrementing the file name based on a date format
- **Burst Switch**  
  Switch block to (dis-)connect in and output stream for a bursty transmission with minimum latency (buffer flushing)
- **Feedback Controller**  
  Implementing a bursty PID controller with optional feedforward offset
- **GPIO Trigger Switch**  
  Opens and closes a stream based on a trigger signal on one of the USRP GPIO pins, adding the required stream tags and emitting several messages for flow control.  
  The block is simmilar to the combination of a *GPIO Trigger Tagger* with the *Burst Switch* block
- **GPIO Trigger Tagger**  
  Adds stream tags based on a trigger signal on one of the USRP GPIO pins
- **LevelProbe**  
  Probes signal level (RMS and overload ratio) in junks (decimating)
- **Linear Ramp**  
  Creates single shot linear ramps
- **Optimizer (Python only)**  
  Optimizes flow graph variables based on an objective function using the [Py-BOBYQA](https://github.com/numericalalgorithmsgroup/pybobyqa) algorithm
- **Variable Config Save**  
  Saves variable data to a file upon request, such as to record the state of flow graph variables to a yml file



## RFNoC OOT module (`rfnoc-beam_exciter`)
The custom RFNoC OOT blocks are contained in the [rfnoc-beam_exciter](rfnoc-beam_exciter) module.

*For an extensive documentation of the functionality and parameters of these blocks, please refere to the documentation tab in GNU Radio Companion or the block defining [YML files](rfnoc-beam_exciter/grc). Usage examples can be found in the [examples directory](rfnoc-beam_exciter/examples).*

- **RFNoC Gain**  
  The Gain Block from the RFNoC tutorial
- **RFNoC Discriminator**  
  Discriminate input signal with two tresholds
- **RFNoC Pulse Counter**  
  Discriminate input signal with two tresholds.
  Then counts number of pulses in given window (decimating).
- **RFNoC Feedback controller**  
  Fast feedback controller implementation on the FPGA
- **RFNoC Multiply**  
  Multiply two complex streams
- **RFNoC Replay Autostarter**  
  Make the RFNoC replay block play when the flowgraph starts

➡️ See the [README file](rfnoc-beam_exciter/README.md) inside the rfnoc module directory for more information.



## Hier blocks (`hier`)
The hier blocks are contained in the [hier](hier) folder:
- _► Signal sources_
  - **Chirp source** (source_chirp.grc)  
    Generates a chirped sinusoid sweeping the desired bandwidth
  - **Dual FM source** (source_dual_fm.grc)  
    Generates two chirped sinusoid sweeping the desired bandwidth
  - **Noise band source** (source_noise_band.grc)  
    Generates a band filtered uniform noise signal
  - **RBPSK source** (source_rbpsk.grc)  
    Generates a randomly phase flipped sinusoid via RBPSK (random binary phase shift keying)
- _► GUI_
  - **QT GUI Chirp source** (source_chirp_gui.grc)  
  - **QT GUI Dual FM source** (source_dual_fm_gui.grc)  
  - **QT GUI Noise band source** (source_noise_band_gui.grc)  
  - **QT GUI RBPSK source** (source_rbpsk_gui.grc)  
  - **QT GUI Sinusoidal source** (source_sine_gui.grc)    
  - **QT GUI Exciter signals** (exciter_signals.grc)  
    Collection of various configurable excitation signal blocks to create combined excitation signals with GUI controls  
  - **QT GUI Triggered output** (triggered_output.grc)  
    Wraps the *GPIO Trigger Switch* block providing all required GUI elements to control and monitor signal output.
  - **USRP Sink** (usrp_sink.grc) and  
    **USRP Source** (usrp_source.grc)  
    Wraps the UHD USRP Sink/Source blocks providing a central location for the hardware configuration used by many flow graphs.


# License

Software Radio Beam Exciter  
Copyright (c) 2022-2024 Philipp Niedermayer,
GSI Helmholtzzentrum für Schwerionenforschung,
p.niedermayer@gsi.de

This software is distributed under the terms of the GNU General Public Licence version 3 (GPLv3),
copied verbatim in the file [LICENSE](LICENSE).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses.





