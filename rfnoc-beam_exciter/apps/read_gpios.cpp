//
// Copyright 2019 Ettus Research, a National Instruments Brand
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

// Example application to show how to write applications that depend on both UHD
// and out-of-tree RFNoC modules.
//
// It will see if a USRP is runnging the gain block, if so, it will test to see
// if it can change the gain.

#include <uhd/exception.hpp>
#include <uhd/rfnoc_graph.hpp>
#include <uhd/rfnoc/radio_control.hpp>
#include <uhd/utils/safe_main.hpp>
#include <boost/program_options.hpp>

#include <time.h>

namespace po = boost::program_options;

int UHD_SAFE_MAIN(int argc, char* argv[])
{
    std::string args;

    // setup the program options
    po::options_description desc("Allowed options");
    // clang-format off
    desc.add_options()
        ("help", "help message")
        ("args", po::value<std::string>(&args)->default_value(""), "USRP device address args")
    ;
    // clang-format on
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    // print the help message
    if (vm.count("help")) {
        std::cout << "Read GPIO " << desc << std::endl;
        std::cout << std::endl
                  << "This application attempts to find and poll GPIOs\n"
                  << std::endl;
        return EXIT_SUCCESS;
    }

    // Create RFNoC graph object:
    auto graph = uhd::rfnoc::rfnoc_graph::make(args);
    
    
    // List available GPIO banks
    // https://files.ettus.com/manual/classuhd_1_1rfnoc_1_1radio__control.html#ac4e8177679bc40dccc5e532ea9f5e10e    
    std::cout << "Available GPIO controllers: " << std::endl;
    auto radio_control_blocks = graph->find_blocks<uhd::rfnoc::radio_control>("");
    if (radio_control_blocks.empty()) {
        std::cout << "Error: No radio_control block found to access GPIOs." << std::endl;
        return EXIT_FAILURE;
    }
    for (auto& block_id : radio_control_blocks) {
        std::cout << "* " << block_id << std::endl;
        
        auto radio_control_block = graph->get_block<uhd::rfnoc::radio_control>(block_id);
		if (radio_control_block) {
			auto banks = radio_control_block->get_gpio_banks();
			for (auto& bank : banks) {
				std::cout << "  * " << bank << std::endl;
			}
		} else {
			std::cout << "  ERROR: Failed to extract radio_control_block!" << std::endl;
		}
    }
    
    // Configure trigger pin
    int radio = 0;
    std::string bank = "RX";
    int pin = 0;
    
    auto radio_id = radio_control_blocks[radio];
    auto radio_control_block = graph->get_block<uhd::rfnoc::radio_control>(radio_id);
    
    // set control bit to manual
    auto bits_ctrl = radio_control_block->get_gpio_attr(bank, "CTRL");
    bits_ctrl &= ~(1 << pin); // set bit to 0
    radio_control_block->set_gpio_attr(bank, "CTRL", bits_ctrl);
    
    // set data direction to input
    auto bits_ddr = radio_control_block->get_gpio_attr(bank, "DDR");
    bits_ddr &= ~(1 << pin); // set bit to 0
    radio_control_block->set_gpio_attr(bank, "DDR", bits_ddr);
    
    std::cout << "Pin " << pin << " on bank " << bank << " of " << radio_id << " configured as trigger input." << std::endl;

	std::cout << "Polling every 10 ms, press CTRL+C to exit" << std::endl;
	while (true){
	//for (int i = 0; i < 1000; i++){
		// Read pin state
		int readback = radio_control_block->get_gpio_attr(bank, "READBACK");
		bool level = (readback >> pin) & 1;
		std::cout << (level ? "#" : "_") << std::flush;
		
		struct timespec ts;
		ts.tv_sec = 0; // whole seconds
		ts.tv_nsec = 10 * 1000 * 1000; // remainder in nanoseconds
		nanosleep(&ts, NULL);
	}

    return EXIT_SUCCESS;
}
