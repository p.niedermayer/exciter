#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: RFNoC Feedback Controller example with ADC data
# Author: Philipp Niedermayer
# GNU Radio version: 3.10.5.1

from packaging.version import Version as StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from PyQt5 import Qt
from gnuradio import eng_notation
from gnuradio import qtgui
from gnuradio.filter import firdes
import sip
from gnuradio import blocks
from gnuradio import gr
from gnuradio.fft import window
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import uhd



from gnuradio import qtgui

class rfnoc_feedback_controller_example_adc(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "RFNoC Feedback Controller example with ADC data", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("RFNoC Feedback Controller example with ADC data")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "rfnoc_feedback_controller_example_adc")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Variables
        ##################################################

        self.rfnoc_graph = uhd_rfnoc_graph_0 = uhd.rfnoc_graph(uhd.device_addr("addr=192.168.10.2"))
        self.samp_rate = samp_rate = 200000
        self.repeat = repeat = 1
        self.out_scale = out_scale = 3
        self.out_min = out_min = 0
        self.out_max = out_max = 0.2
        self.feedforward_const = feedforward_const = 0.01
        self.feedforward = feedforward = 1
        self.feedback = feedback = 0

        ##################################################
        # Blocks
        ##################################################

        self._out_scale_tool_bar = Qt.QToolBar(self)
        self._out_scale_tool_bar.addWidget(Qt.QLabel("Output scale" + ": "))
        self._out_scale_line_edit = Qt.QLineEdit(str(self.out_scale))
        self._out_scale_tool_bar.addWidget(self._out_scale_line_edit)
        self._out_scale_line_edit.returnPressed.connect(
            lambda: self.set_out_scale(eval(str(self._out_scale_line_edit.text()))))
        self.top_layout.addWidget(self._out_scale_tool_bar)
        self._out_min_tool_bar = Qt.QToolBar(self)
        self._out_min_tool_bar.addWidget(Qt.QLabel("Output min" + ": "))
        self._out_min_line_edit = Qt.QLineEdit(str(self.out_min))
        self._out_min_tool_bar.addWidget(self._out_min_line_edit)
        self._out_min_line_edit.returnPressed.connect(
            lambda: self.set_out_min(eval(str(self._out_min_line_edit.text()))))
        self.top_layout.addWidget(self._out_min_tool_bar)
        self._out_max_tool_bar = Qt.QToolBar(self)
        self._out_max_tool_bar.addWidget(Qt.QLabel("Output max" + ": "))
        self._out_max_line_edit = Qt.QLineEdit(str(self.out_max))
        self._out_max_tool_bar.addWidget(self._out_max_line_edit)
        self._out_max_line_edit.returnPressed.connect(
            lambda: self.set_out_max(eval(str(self._out_max_line_edit.text()))))
        self.top_layout.addWidget(self._out_max_tool_bar)
        self._feedforward_const_tool_bar = Qt.QToolBar(self)
        self._feedforward_const_tool_bar.addWidget(Qt.QLabel("Feedforward const" + ": "))
        self._feedforward_const_line_edit = Qt.QLineEdit(str(self.feedforward_const))
        self._feedforward_const_tool_bar.addWidget(self._feedforward_const_line_edit)
        self._feedforward_const_line_edit.returnPressed.connect(
            lambda: self.set_feedforward_const(eval(str(self._feedforward_const_line_edit.text()))))
        self.top_layout.addWidget(self._feedforward_const_tool_bar)
        _feedforward_check_box = Qt.QCheckBox("Feedforward")
        self._feedforward_choices = {True: 1, False: 0}
        self._feedforward_choices_inv = dict((v,k) for k,v in self._feedforward_choices.items())
        self._feedforward_callback = lambda i: Qt.QMetaObject.invokeMethod(_feedforward_check_box, "setChecked", Qt.Q_ARG("bool", self._feedforward_choices_inv[i]))
        self._feedforward_callback(self.feedforward)
        _feedforward_check_box.stateChanged.connect(lambda i: self.set_feedforward(self._feedforward_choices[bool(i)]))
        self.top_layout.addWidget(_feedforward_check_box)
        _feedback_check_box = Qt.QCheckBox("feedback")
        self._feedback_choices = {True: 1, False: 0}
        self._feedback_choices_inv = dict((v,k) for k,v in self._feedback_choices.items())
        self._feedback_callback = lambda i: Qt.QMetaObject.invokeMethod(_feedback_check_box, "setChecked", Qt.Q_ARG("bool", self._feedback_choices_inv[i]))
        self._feedback_callback(self.feedback)
        _feedback_check_box.stateChanged.connect(lambda i: self.set_feedback(self._feedback_choices[bool(i)]))
        self.top_layout.addWidget(_feedback_check_box)
        self.uhd_rfnoc_rx_streamer_0 = uhd.rfnoc_rx_streamer(
            self.rfnoc_graph,
            1,
            uhd.stream_args(
                cpu_format="fc32",
                otw_format="sc16",
                channels=[],
                args='',
            ),
            1,
            True
        )
        self.uhd_rfnoc_rx_radio_0 = uhd.rfnoc_rx_radio(
            self.rfnoc_graph,
            uhd.device_addr(''),
            (-1),
            0)
        self.uhd_rfnoc_rx_radio_0.set_rate(200e6)
        self.uhd_rfnoc_rx_radio_0.set_antenna('AB', 0)
        self.uhd_rfnoc_rx_radio_0.set_frequency(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_gain(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_bandwidth(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_dc_offset(False, 0)
        self.uhd_rfnoc_rx_radio_0.set_iq_balance(False, 0)
        self.qtgui_time_sink_x_0_0_0_0 = qtgui.time_sink_f(
            (1024+0*4*65536), #size
            samp_rate, #samp_rate
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_0_0_0_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0_0_0_0.set_y_axis(-.1, 1.1)

        self.qtgui_time_sink_x_0_0_0_0.set_y_label('Value', "")

        self.qtgui_time_sink_x_0_0_0_0.enable_tags(True)
        self.qtgui_time_sink_x_0_0_0_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0_0_0_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0_0_0_0.enable_grid(True)
        self.qtgui_time_sink_x_0_0_0_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_0_0_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0_0_0_0.enable_stem_plot(False)


        labels = ['Actual output', 'Actual output', 'Actual output', 'Input pulse count', 'Multiply Const (real)',
            'Multiply Const (imag)', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'green', 'green', 'yellow', 'red',
            'dark red', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [2, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0_0_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0_0_0_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_0_0_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_0_0_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_0_0_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_0_0_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_0_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_0_0_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0_0_0_0.qwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_0_0_0_win, 15, 0, 1, 1)
        for r in range(15, 16):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_probe_rate_0 = blocks.probe_rate(gr.sizeof_gr_complex*1, 500.0, 0.15)
        self.blocks_complex_to_real_0 = blocks.complex_to_real(1)
        self.beam_exciter_rfnoc_feedback_controller_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(""),
            "FeedbackController",
            -1, # device_select
            -1, # instance_index
        )
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward', int(feedforward))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedback', int(feedback))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ta', int(200))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('target', float(4.5))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kp', float(1.33333333333e-3))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ki_times_ta', float(2.66666666667e-4))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kd_over_ta', float(0.0))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward_const', float(feedforward_const))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_scale', float(out_scale))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_min', float(out_min))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_max', float(out_max))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('repeat', int(repeat))
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(""),
            "DiscriminatingPulseCounter",
            -1, # device_select
            -1, # instance_index
        )
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_low', 0.2)
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('threshold_high', 0.3)
        self.beam_exciter_rfnoc_discriminating_pulse_counter_0.set_property('decimation', 1000)


        ##################################################
        # Connections
        ##################################################
        self.rfnoc_graph.connect(self.beam_exciter_rfnoc_discriminating_pulse_counter_0.get_unique_id(), 0, self.beam_exciter_rfnoc_feedback_controller_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.beam_exciter_rfnoc_feedback_controller_0.get_unique_id(), 0, self.uhd_rfnoc_rx_streamer_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_rx_radio_0.get_unique_id(), 0, self.beam_exciter_rfnoc_discriminating_pulse_counter_0.get_unique_id(), 0, False)
        self.connect((self.blocks_complex_to_real_0, 0), (self.qtgui_time_sink_x_0_0_0_0, 0))
        self.connect((self.uhd_rfnoc_rx_streamer_0, 0), (self.blocks_complex_to_real_0, 0))
        self.connect((self.uhd_rfnoc_rx_streamer_0, 0), (self.blocks_probe_rate_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "rfnoc_feedback_controller_example_adc")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_uhd_rfnoc_graph_0(self):
        return self.uhd_rfnoc_graph_0

    def set_uhd_rfnoc_graph_0(self, uhd_rfnoc_graph_0):
        self.uhd_rfnoc_graph_0 = uhd_rfnoc_graph_0

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.qtgui_time_sink_x_0_0_0_0.set_samp_rate(self.samp_rate)

    def get_repeat(self):
        return self.repeat

    def set_repeat(self, repeat):
        self.repeat = repeat
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('repeat', int(self.repeat))

    def get_out_scale(self):
        return self.out_scale

    def set_out_scale(self, out_scale):
        self.out_scale = out_scale
        Qt.QMetaObject.invokeMethod(self._out_scale_line_edit, "setText", Qt.Q_ARG("QString", repr(self.out_scale)))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_scale', float(self.out_scale))

    def get_out_min(self):
        return self.out_min

    def set_out_min(self, out_min):
        self.out_min = out_min
        Qt.QMetaObject.invokeMethod(self._out_min_line_edit, "setText", Qt.Q_ARG("QString", repr(self.out_min)))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_min', float(self.out_min))

    def get_out_max(self):
        return self.out_max

    def set_out_max(self, out_max):
        self.out_max = out_max
        Qt.QMetaObject.invokeMethod(self._out_max_line_edit, "setText", Qt.Q_ARG("QString", repr(self.out_max)))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_max', float(self.out_max))

    def get_feedforward_const(self):
        return self.feedforward_const

    def set_feedforward_const(self, feedforward_const):
        self.feedforward_const = feedforward_const
        Qt.QMetaObject.invokeMethod(self._feedforward_const_line_edit, "setText", Qt.Q_ARG("QString", repr(self.feedforward_const)))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward_const', float(self.feedforward_const))

    def get_feedforward(self):
        return self.feedforward

    def set_feedforward(self, feedforward):
        self.feedforward = feedforward
        self._feedforward_callback(self.feedforward)
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward', int(self.feedforward))

    def get_feedback(self):
        return self.feedback

    def set_feedback(self, feedback):
        self.feedback = feedback
        self._feedback_callback(self.feedback)
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedback', int(self.feedback))




def main(top_block_cls=rfnoc_feedback_controller_example_adc, options=None):

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
