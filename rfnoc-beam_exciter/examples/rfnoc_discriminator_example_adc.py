#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: RFNoC Discriminator Block example
# Author: Philipp Niedermayer
# Copyright: Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.5.1

from packaging.version import Version as StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from PyQt5 import Qt
from PyQt5.QtCore import QObject, pyqtSlot
from gnuradio import qtgui
from gnuradio.filter import firdes
import sip
from gnuradio import blocks
from gnuradio import gr
from gnuradio.fft import window
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
from gnuradio.qtgui import Range, RangeWidget
from PyQt5 import QtCore



from gnuradio import qtgui

class rfnoc_discriminator_example_adc(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "RFNoC Discriminator Block example", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("RFNoC Discriminator Block example")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "rfnoc_discriminator_example_adc")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Variables
        ##################################################

        self.rfnoc_graph = uhd_rfnoc_graph_0 = uhd.rfnoc_graph(uhd.device_addr("addr=192.168.10.2"))
        self.samp_rate = samp_rate = 5e6
        self.low = low = -0.1
        self.high = high = 0.1
        self.coupling = coupling = False

        ##################################################
        # Blocks
        ##################################################

        self._low_range = Range(-1, 1, 0.01, -0.1, 200)
        self._low_win = RangeWidget(self._low_range, self.set_low, "Threshold low", "counter_slider", float, QtCore.Qt.Horizontal)
        self.top_layout.addWidget(self._low_win)
        self._high_range = Range(-1, 1, 0.01, 0.1, 200)
        self._high_win = RangeWidget(self._high_range, self.set_high, "Threshold high", "counter_slider", float, QtCore.Qt.Horizontal)
        self.top_layout.addWidget(self._high_win)
        # Create the options list
        self._coupling_options = [True, False]
        # Create the labels list
        self._coupling_labels = ['On', 'Off']
        # Create the combo box
        self._coupling_tool_bar = Qt.QToolBar(self)
        self._coupling_tool_bar.addWidget(Qt.QLabel("DC Offset Correction" + ": "))
        self._coupling_combo_box = Qt.QComboBox()
        self._coupling_tool_bar.addWidget(self._coupling_combo_box)
        for _label in self._coupling_labels: self._coupling_combo_box.addItem(_label)
        self._coupling_callback = lambda i: Qt.QMetaObject.invokeMethod(self._coupling_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._coupling_options.index(i)))
        self._coupling_callback(self.coupling)
        self._coupling_combo_box.currentIndexChanged.connect(
            lambda i: self.set_coupling(self._coupling_options[i]))
        # Create the radio buttons
        self.top_layout.addWidget(self._coupling_tool_bar)
        self.uhd_rfnoc_rx_streamer_0 = uhd.rfnoc_rx_streamer(
            self.rfnoc_graph,
            1,
            uhd.stream_args(
                cpu_format="fc32",
                otw_format="sc16",
                channels=[],
                args='',
            ),
            1,
            True
        )
        self.uhd_rfnoc_rx_radio_0 = uhd.rfnoc_rx_radio(
            self.rfnoc_graph,
            uhd.device_addr(''),
            (-1),
            0)
        self.uhd_rfnoc_rx_radio_0.set_rate(200e6)
        self.uhd_rfnoc_rx_radio_0.set_antenna('AB', 0)
        self.uhd_rfnoc_rx_radio_0.set_frequency(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_gain(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_bandwidth(0, 0)
        self.uhd_rfnoc_rx_radio_0.set_dc_offset(coupling, 0)
        self.uhd_rfnoc_rx_radio_0.set_iq_balance(False, 0)
        self.uhd_rfnoc_ddc_0 = uhd.rfnoc_ddc(
            self.rfnoc_graph,
            uhd.device_addr(''),
            (-1),
            0)
        self.uhd_rfnoc_ddc_0.set_freq(0, 0)
        self.uhd_rfnoc_ddc_0.set_output_rate(samp_rate, 0)
        self.qtgui_time_sink_x_0_0 = qtgui.time_sink_c(
            1024, #size
            samp_rate, #samp_rate
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_0_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0_0.set_y_axis(-1.2, 1.2)

        self.qtgui_time_sink_x_0_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0_0.enable_tags(True)
        self.qtgui_time_sink_x_0_0.set_trigger_mode(qtgui.TRIG_MODE_AUTO, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0_0.enable_grid(True)
        self.qtgui_time_sink_x_0_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_0.enable_control_panel(True)
        self.qtgui_time_sink_x_0_0.enable_stem_plot(False)


        labels = ['RX1 discriminated (real)', 'RX2 (imag)', 'RFNoC Gain (real)', 'RFNoC Gain (imag)', 'Multiply Const (real)',
            'Multiply Const (imag)', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'green', 'green', 'dark green', 'red',
            'dark red', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [2, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(2):
            if len(labels[i]) == 0:
                if (i % 2 == 0):
                    self.qtgui_time_sink_x_0_0.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_0_0.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_0_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0_0.qwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_time_sink_x_0_0_win)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_ff(32767)
        self.blocks_float_to_complex_0_0 = blocks.float_to_complex(1)
        self.blocks_complex_to_float_0_0 = blocks.complex_to_float(1)
        self.beam_exciter_rfnoc_discriminator_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(""),
            "Discriminator",
            -1, # device_select
            -1, # instance_index
        )
        self.beam_exciter_rfnoc_discriminator_0.set_property('threshold_low', low)
        self.beam_exciter_rfnoc_discriminator_0.set_property('threshold_high', high)


        ##################################################
        # Connections
        ##################################################
        self.rfnoc_graph.connect(self.beam_exciter_rfnoc_discriminator_0.get_unique_id(), 0, self.uhd_rfnoc_ddc_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_ddc_0.get_unique_id(), 0, self.uhd_rfnoc_rx_streamer_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_rx_radio_0.get_unique_id(), 0, self.beam_exciter_rfnoc_discriminator_0.get_unique_id(), 0, False)
        self.connect((self.blocks_complex_to_float_0_0, 1), (self.blocks_float_to_complex_0_0, 1))
        self.connect((self.blocks_complex_to_float_0_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.blocks_float_to_complex_0_0, 0), (self.qtgui_time_sink_x_0_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_float_to_complex_0_0, 0))
        self.connect((self.uhd_rfnoc_rx_streamer_0, 0), (self.blocks_complex_to_float_0_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "rfnoc_discriminator_example_adc")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_uhd_rfnoc_graph_0(self):
        return self.uhd_rfnoc_graph_0

    def set_uhd_rfnoc_graph_0(self, uhd_rfnoc_graph_0):
        self.uhd_rfnoc_graph_0 = uhd_rfnoc_graph_0

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.qtgui_time_sink_x_0_0.set_samp_rate(self.samp_rate)
        self.uhd_rfnoc_ddc_0.set_output_rate(self.samp_rate, 0)

    def get_low(self):
        return self.low

    def set_low(self, low):
        self.low = low
        self.beam_exciter_rfnoc_discriminator_0.set_property('threshold_low', self.low)

    def get_high(self):
        return self.high

    def set_high(self, high):
        self.high = high
        self.beam_exciter_rfnoc_discriminator_0.set_property('threshold_high', self.high)

    def get_coupling(self):
        return self.coupling

    def set_coupling(self, coupling):
        self.coupling = coupling
        self._coupling_callback(self.coupling)
        self.uhd_rfnoc_rx_radio_0.set_dc_offset(self.coupling, 0)




def main(top_block_cls=rfnoc_discriminator_example_adc, options=None):

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
