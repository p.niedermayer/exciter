#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: RFNoC Feedback Controller - Example Flow Graph
# Author: Philipp Niedermayer
# Copyright: Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.5.1

from packaging.version import Version as StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from PyQt5 import Qt
from gnuradio import qtgui
import sip
from gnuradio import analog
from gnuradio import blocks
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
from gnuradio.fft import window
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
from gnuradio.qtgui import Range, RangeWidget
from PyQt5 import QtCore
import time
import threading



from gnuradio import qtgui

class rfnoc_feedback_controller_example(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "RFNoC Feedback Controller - Example Flow Graph", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("RFNoC Feedback Controller - Example Flow Graph")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "rfnoc_feedback_controller_example")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Variables
        ##################################################

        self.rfnoc_graph = uhd_rfnoc_graph_0 = uhd.rfnoc_graph(uhd.device_addr("addr=192.168.10.2"))
        self.ti = ti = 0.15
        self.td = td = 0
        self.target = target = 0.5
        self.ta = ta = 0.1
        self.samp_rate = samp_rate = 100000
        self.out_scale = out_scale = 1
        self.out_min = out_min = 0
        self.out_max = out_max = 1
        self.kp = kp = 0.1
        self.ffw_const = ffw_const = 0.1
        self.feedforward = feedforward = 0
        self.feedback = feedback = True
        self.disturbance = disturbance = 0.1
        self.controller = controller = 0
        self.burst = burst = 1

        ##################################################
        # Blocks
        ##################################################

        self._target_range = Range(0, 2, 0.01, 0.5, 200)
        self._target_win = RangeWidget(self._target_range, self.set_target, "target", "counter_slider", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._target_win, 10, 0, 1, 3)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_probe_signal_x_0 = blocks.probe_signal_f()
        self._ti_range = Range(-999, 999, 0.01, 0.15, 200)
        self._ti_win = RangeWidget(self._ti_range, self.set_ti, "Ti", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._ti_win, 2, 0, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._td_range = Range(-999, 999, 0.01, 0, 200)
        self._td_win = RangeWidget(self._td_range, self.set_td, "Td", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._td_win, 3, 0, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._out_scale_range = Range(-999, 999, 0.01, 1, 200)
        self._out_scale_win = RangeWidget(self._out_scale_range, self.set_out_scale, "Output scale", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._out_scale_win, 0, 1, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._out_min_range = Range(-999, 999, 0.01, 0, 200)
        self._out_min_win = RangeWidget(self._out_min_range, self.set_out_min, "Output min", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._out_min_win, 1, 1, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._out_max_range = Range(-999, 999, 0.01, 1, 200)
        self._out_max_win = RangeWidget(self._out_max_range, self.set_out_max, "Output max", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._out_max_win, 2, 1, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._kp_range = Range(-999, 999, 0.01, 0.1, 200)
        self._kp_win = RangeWidget(self._kp_range, self.set_kp, "Kp", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._kp_win, 1, 0, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._ffw_const_range = Range(-999, 999, 0.01, 0.1, 200)
        self._ffw_const_win = RangeWidget(self._ffw_const_range, self.set_ffw_const, "Feedforward constant", "counter", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._ffw_const_win, 1, 2, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        _feedforward_check_box = Qt.QCheckBox("Feedforward")
        self._feedforward_choices = {True: 1, False: 0}
        self._feedforward_choices_inv = dict((v,k) for k,v in self._feedforward_choices.items())
        self._feedforward_callback = lambda i: Qt.QMetaObject.invokeMethod(_feedforward_check_box, "setChecked", Qt.Q_ARG("bool", self._feedforward_choices_inv[i]))
        self._feedforward_callback(self.feedforward)
        _feedforward_check_box.stateChanged.connect(lambda i: self.set_feedforward(self._feedforward_choices[bool(i)]))
        self.top_grid_layout.addWidget(_feedforward_check_box, 0, 2, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        _feedback_check_box = Qt.QCheckBox("Feedback on")
        self._feedback_choices = {True: True, False: False}
        self._feedback_choices_inv = dict((v,k) for k,v in self._feedback_choices.items())
        self._feedback_callback = lambda i: Qt.QMetaObject.invokeMethod(_feedback_check_box, "setChecked", Qt.Q_ARG("bool", self._feedback_choices_inv[i]))
        self._feedback_callback(self.feedback)
        _feedback_check_box.stateChanged.connect(lambda i: self.set_feedback(self._feedback_choices[bool(i)]))
        self.top_grid_layout.addWidget(_feedback_check_box, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._disturbance_range = Range(0, 2, 0.01, 0.1, 200)
        self._disturbance_win = RangeWidget(self._disturbance_range, self.set_disturbance, "disturbance", "counter_slider", float, QtCore.Qt.Horizontal)
        self.top_grid_layout.addWidget(self._disturbance_win, 11, 0, 1, 3)
        for r in range(11, 12):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        def _controller_probe():
          while True:

            val = self.blocks_probe_signal_x_0.level()
            try:
              try:
                self.doc.add_next_tick_callback(functools.partial(self.set_controller,val))
              except AttributeError:
                self.set_controller(val)
            except AttributeError:
              pass
            time.sleep(1.0 / ((1/ta)))
        _controller_thread = threading.Thread(target=_controller_probe)
        _controller_thread.daemon = True
        _controller_thread.start()
        _burst_check_box = Qt.QCheckBox("Burst active")
        self._burst_choices = {True: 1, False: 0}
        self._burst_choices_inv = dict((v,k) for k,v in self._burst_choices.items())
        self._burst_callback = lambda i: Qt.QMetaObject.invokeMethod(_burst_check_box, "setChecked", Qt.Q_ARG("bool", self._burst_choices_inv[i]))
        self._burst_callback(self.burst)
        _burst_check_box.stateChanged.connect(lambda i: self.set_burst(self._burst_choices[bool(i)]))
        self.top_grid_layout.addWidget(_burst_check_box, 20, 0, 1, 1)
        for r in range(20, 21):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.uhd_rfnoc_tx_streamer_0 = uhd.rfnoc_tx_streamer(
            self.rfnoc_graph,
            1,
            uhd.stream_args(
                cpu_format="fc32",
                otw_format="sc16",
                channels=[],
                args='',
            ),
            1
        )
        self.uhd_rfnoc_rx_streamer_0 = uhd.rfnoc_rx_streamer(
            self.rfnoc_graph,
            1,
            uhd.stream_args(
                cpu_format="fc32",
                otw_format="sc16",
                channels=[],
                args='',
            ),
            1,
            True
        )
        self.qtgui_number_sink_0 = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_HORIZ,
            3,
            None # parent
        )
        self.qtgui_number_sink_0.set_update_time(0.10)
        self.qtgui_number_sink_0.set_title("")

        labels = ['Target value', 'Actual value', 'Controller output', '', '',
            '', '', '', '', '']
        units = ['', '', '', '', '',
            '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
            ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]

        for i in range(3):
            self.qtgui_number_sink_0.set_min(i, 0)
            self.qtgui_number_sink_0.set_max(i, 2)
            self.qtgui_number_sink_0.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_number_sink_0.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_number_sink_0.set_label(i, labels[i])
            self.qtgui_number_sink_0.set_unit(i, units[i])
            self.qtgui_number_sink_0.set_factor(i, factor[i])

        self.qtgui_number_sink_0.enable_autoscale(False)
        self._qtgui_number_sink_0_win = sip.wrapinstance(self.qtgui_number_sink_0.qwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_number_sink_0_win, 100, 0, 1, 3)
        for r in range(100, 101):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.low_pass_filter_0 = filter.fir_filter_fff(
            1,
            firdes.low_pass(
                1,
                samp_rate,
                3,
                3,
                window.WIN_HAMMING,
                6.76))
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_float*1, samp_rate,True)
        self.blocks_probe_rate_0 = blocks.probe_rate(gr.sizeof_float*1, 500.0, 0.15)
        self.blocks_multiply_const_vxx_0_0 = blocks.multiply_const_ff((10000/32767))
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_ff(0.8)
        self.blocks_message_debug_0 = blocks.message_debug(True)
        self.blocks_float_to_complex_0 = blocks.float_to_complex(1)
        self.blocks_delay_0 = blocks.delay(gr.sizeof_float*1, (int(samp_rate*0*ta)))
        self.blocks_complex_to_real_0 = blocks.complex_to_real(1)
        self.blocks_burst_tagger_0 = blocks.burst_tagger(gr.sizeof_float)
        self.blocks_burst_tagger_0.set_true_tag('SOB',True)
        self.blocks_burst_tagger_0.set_false_tag('EOB',True)
        self.blocks_add_xx_0 = blocks.add_vff(1)
        self.beam_exciter_rfnoc_feedback_controller_0 = uhd.rfnoc_block_generic(
            self.rfnoc_graph,
            uhd.device_addr(""),
            "FeedbackController",
            -1, # device_select
            -1, # instance_index
        )
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward', int(feedforward))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedback', int(feedback))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ta', int((int(ta*samp_rate))))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('target', float(target*10000))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kp', float(kp/10000))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ki_times_ta', float((0 if ti==0 else (kp*ta/ti))/10000))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kd_over_ta', float((kp*td/ta)/10000))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward_const', float(ffw_const))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_scale', float(out_scale))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_min', float(out_min))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_max', float(out_max))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('repeat', int(1))
        self.analog_const_source_x_0_1 = analog.sig_source_s(0, analog.GR_CONST_WAVE, 0, 0, burst)
        self.analog_const_source_x_0_0_0 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, controller)
        self.analog_const_source_x_0_0 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, disturbance)
        self.analog_const_source_x_0 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, target)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_probe_rate_0, 'rate'), (self.blocks_message_debug_0, 'print'))
        self.rfnoc_graph.connect(self.beam_exciter_rfnoc_feedback_controller_0.get_unique_id(), 0, self.uhd_rfnoc_rx_streamer_0.get_unique_id(), 0, False)
        self.rfnoc_graph.connect(self.uhd_rfnoc_tx_streamer_0.get_unique_id(), 0, self.beam_exciter_rfnoc_feedback_controller_0.get_unique_id(), 0, False)
        self.connect((self.analog_const_source_x_0, 0), (self.qtgui_number_sink_0, 0))
        self.connect((self.analog_const_source_x_0_0, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.analog_const_source_x_0_0_0, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.analog_const_source_x_0_0_0, 0), (self.qtgui_number_sink_0, 2))
        self.connect((self.analog_const_source_x_0_1, 0), (self.blocks_burst_tagger_0, 1))
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.blocks_burst_tagger_0, 0), (self.blocks_multiply_const_vxx_0_0, 0))
        self.connect((self.blocks_complex_to_real_0, 0), (self.blocks_probe_rate_0, 0))
        self.connect((self.blocks_complex_to_real_0, 0), (self.blocks_probe_signal_x_0, 0))
        self.connect((self.blocks_delay_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.blocks_float_to_complex_0, 0), (self.uhd_rfnoc_tx_streamer_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.low_pass_filter_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.blocks_float_to_complex_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.blocks_burst_tagger_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.qtgui_number_sink_0, 1))
        self.connect((self.low_pass_filter_0, 0), (self.blocks_delay_0, 0))
        self.connect((self.uhd_rfnoc_rx_streamer_0, 0), (self.blocks_complex_to_real_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "rfnoc_feedback_controller_example")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_uhd_rfnoc_graph_0(self):
        return self.uhd_rfnoc_graph_0

    def set_uhd_rfnoc_graph_0(self, uhd_rfnoc_graph_0):
        self.uhd_rfnoc_graph_0 = uhd_rfnoc_graph_0

    def get_ti(self):
        return self.ti

    def set_ti(self, ti):
        self.ti = ti
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ki_times_ta', float((0 if self.ti==0 else (self.kp*self.ta/self.ti))/10000))

    def get_td(self):
        return self.td

    def set_td(self, td):
        self.td = td
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kd_over_ta', float((self.kp*self.td/self.ta)/10000))

    def get_target(self):
        return self.target

    def set_target(self, target):
        self.target = target
        self.analog_const_source_x_0.set_offset(self.target)
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('target', float(self.target*10000))

    def get_ta(self):
        return self.ta

    def set_ta(self, ta):
        self.ta = ta
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ta', int((int(self.ta*self.samp_rate))))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ki_times_ta', float((0 if self.ti==0 else (self.kp*self.ta/self.ti))/10000))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kd_over_ta', float((self.kp*self.td/self.ta)/10000))
        self.blocks_delay_0.set_dly(int((int(self.samp_rate*0*self.ta))))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ta', int((int(self.ta*self.samp_rate))))
        self.blocks_delay_0.set_dly(int((int(self.samp_rate*0*self.ta))))
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, 3, 3, window.WIN_HAMMING, 6.76))

    def get_out_scale(self):
        return self.out_scale

    def set_out_scale(self, out_scale):
        self.out_scale = out_scale
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_scale', float(self.out_scale))

    def get_out_min(self):
        return self.out_min

    def set_out_min(self, out_min):
        self.out_min = out_min
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_min', float(self.out_min))

    def get_out_max(self):
        return self.out_max

    def set_out_max(self, out_max):
        self.out_max = out_max
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('out_max', float(self.out_max))

    def get_kp(self):
        return self.kp

    def set_kp(self, kp):
        self.kp = kp
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kp', float(self.kp/10000))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('ki_times_ta', float((0 if self.ti==0 else (self.kp*self.ta/self.ti))/10000))
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('kd_over_ta', float((self.kp*self.td/self.ta)/10000))

    def get_ffw_const(self):
        return self.ffw_const

    def set_ffw_const(self, ffw_const):
        self.ffw_const = ffw_const
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward_const', float(self.ffw_const))

    def get_feedforward(self):
        return self.feedforward

    def set_feedforward(self, feedforward):
        self.feedforward = feedforward
        self._feedforward_callback(self.feedforward)
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedforward', int(self.feedforward))

    def get_feedback(self):
        return self.feedback

    def set_feedback(self, feedback):
        self.feedback = feedback
        self._feedback_callback(self.feedback)
        self.beam_exciter_rfnoc_feedback_controller_0.set_property('feedback', int(self.feedback))

    def get_disturbance(self):
        return self.disturbance

    def set_disturbance(self, disturbance):
        self.disturbance = disturbance
        self.analog_const_source_x_0_0.set_offset(self.disturbance)

    def get_controller(self):
        return self.controller

    def set_controller(self, controller):
        self.controller = controller
        self.analog_const_source_x_0_0_0.set_offset(self.controller)

    def get_burst(self):
        return self.burst

    def set_burst(self, burst):
        self.burst = burst
        self._burst_callback(self.burst)
        self.analog_const_source_x_0_1.set_offset(self.burst)




def main(top_block_cls=rfnoc_feedback_controller_example, options=None):

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
