options:
  parameters:
    author: Philipp Niedermayer
    catch_exceptions: 'True'
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: "Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum f\xFCr Schwerionenforschung"
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: rfnoc_discriminator_example
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: RFNoC Discriminator Block example
    window_size: (1000,1000)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 12.0]
    rotation: 0
    state: enabled

blocks:
- name: high
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: Threshold high
    min_len: '200'
    orient: QtCore.Qt.Horizontal
    rangeType: float
    start: '-1'
    step: '0.01'
    stop: '1'
    value: '0.1'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [840, 180.0]
    rotation: 0
    state: true
- name: low
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: Threshold low
    min_len: '200'
    orient: QtCore.Qt.Horizontal
    rangeType: float
    start: '-1'
    step: '0.01'
    stop: '1'
    value: '-0.1'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [696, 180.0]
    rotation: 0
    state: true
- name: samp_rate
  id: variable
  parameters:
    comment: ''
    value: '32000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [184, 12.0]
    rotation: 0
    state: enabled
- name: uhd_rfnoc_graph_0
  id: uhd_rfnoc_graph
  parameters:
    alias: ''
    clock_source: ''
    comment: 'Flow graph must match static connections

      on the FPGA image! Everything connected

      to or from an SEP (Stream End-Point) can

      be dynamically connected as desired

      (including a streamer block).

      Check with `uhd_usrp_probe` command'
    dev_addr: addr=192.168.10.2
    dev_args: ''
    num_mboards: '1'
    time_source: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [280, 12.0]
    rotation: 0
    state: enabled
- name: analog_sig_source_x_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: '1'
    comment: ''
    freq: '100'
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: samp_rate
    type: complex
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [120, 260.0]
    rotation: 0
    state: enabled
- name: beam_exciter_rfnoc_discriminator_0
  id: beam_exciter_rfnoc_discriminator
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    threshold_high: high
    threshold_low: low
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [768, 324.0]
    rotation: 0
    state: true
- name: blocks_complex_to_float_0
  id: blocks_complex_to_float
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [568, 440.0]
    rotation: 0
    state: enabled
- name: blocks_complex_to_float_0_0
  id: blocks_complex_to_float
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1144.0, 160]
    rotation: 90
    state: true
- name: blocks_float_to_complex_0
  id: blocks_float_to_complex
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [976, 440.0]
    rotation: 0
    state: enabled
- name: blocks_float_to_complex_0_0
  id: blocks_float_to_complex
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1232.0, 160]
    rotation: 270
    state: true
- name: blocks_multiply_const_vxx_0
  id: blocks_multiply_const_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    const: '32767'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1152, 44.0]
    rotation: 0
    state: true
- name: blocks_threshold_ff_0
  id: blocks_threshold_ff
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    high: high
    init: '0'
    low: low
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [784, 404.0]
    rotation: 0
    state: enabled
- name: blocks_throttle_0
  id: blocks_throttle
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    ignoretag: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    samples_per_second: samp_rate
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [344, 300.0]
    rotation: 0
    state: enabled
- name: qtgui_time_sink_x_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: red
    color10: dark blue
    color2: dark blue
    color3: green
    color4: dark green
    color5: red
    color6: dark red
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'True'
    entags: 'True'
    grid: 'True'
    gui_hint: ''
    label1: Signal (real)
    label10: Signal 10
    label2: Signal (imag)
    label3: RFNoC Discriminator Block (real)
    label4: RFNoC Discriminator Block (imag)
    label5: Threshold Block (real)
    label6: Threshold Block (imag)
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '3'
    size: '1024'
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_AUTO
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '2'
    ymin: '-2'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1352, 304.0]
    rotation: 0
    state: enabled
- name: uhd_rfnoc_rx_streamer_0
  id: uhd_rfnoc_rx_streamer
  parameters:
    adapter_id_list: '[0]'
    affinity: ''
    alias: ''
    args: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_chans: '1'
    otw: sc16
    output_type: fc32
    use_default_adapter_id: 'True'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [976, 336.0]
    rotation: 0
    state: enabled
- name: uhd_rfnoc_tx_streamer_0
  id: uhd_rfnoc_tx_streamer
  parameters:
    adapter_id_list: '[0]'
    affinity: ''
    alias: ''
    args: ''
    comment: ''
    input_type: fc32
    maxoutbuf: '0'
    minoutbuf: '0'
    num_chans: '1'
    otw: sc16
    use_default_adapter_id: 'True'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [568, 336.0]
    rotation: 0
    state: enabled

connections:
- [analog_sig_source_x_0, '0', blocks_throttle_0, '0']
- [beam_exciter_rfnoc_discriminator_0, '0', uhd_rfnoc_rx_streamer_0, '0']
- [blocks_complex_to_float_0, '0', blocks_threshold_ff_0, '0']
- [blocks_complex_to_float_0, '1', blocks_float_to_complex_0, '1']
- [blocks_complex_to_float_0_0, '0', blocks_multiply_const_vxx_0, '0']
- [blocks_complex_to_float_0_0, '1', blocks_float_to_complex_0_0, '1']
- [blocks_float_to_complex_0, '0', qtgui_time_sink_x_0, '2']
- [blocks_float_to_complex_0_0, '0', qtgui_time_sink_x_0, '1']
- [blocks_multiply_const_vxx_0, '0', blocks_float_to_complex_0_0, '0']
- [blocks_threshold_ff_0, '0', blocks_float_to_complex_0, '0']
- [blocks_throttle_0, '0', blocks_complex_to_float_0, '0']
- [blocks_throttle_0, '0', qtgui_time_sink_x_0, '0']
- [blocks_throttle_0, '0', uhd_rfnoc_tx_streamer_0, '0']
- [uhd_rfnoc_rx_streamer_0, '0', blocks_complex_to_float_0_0, '0']
- [uhd_rfnoc_tx_streamer_0, '0', beam_exciter_rfnoc_discriminator_0, '0']

metadata:
  file_format: 1
  grc_version: 3.10.5.1
