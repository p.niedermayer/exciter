# RFNoC: out-of-tree module

Note: when creating RFNoC-only flowgraphs such as loopback, make sure to remove or delay the timestamps with the *RFNoC Modify Timestamp* block!

## Installation

### Prerequisites
- UHD 4.4.0.0 build from source in `~/uhd`  
- GNU Radio 3.10.7.0

See [Wiki](https://git.gsi.de/p.niedermayer/exciter/-/wikis/Installation) for details.

### Installation of custom blocks

Build and install
```bash
cd rfnoc-beam_exciter

mkdir build
cd build
cmake ../
make

sudo make install
sudo ldconfig
```

Add this to your `~/.bashrc` so UHD can find the shared library:
```bash
# Custom RFNoC OOT Module for UHD & GR
export UHD_MODULE_PATH=/usr/local/lib/librfnoc-beam_exciter_rfnoc.so:$UHD_MODULE_PATH
```

### Building the FPGA image

See also the [official guide](https://kb.ettus.com/Getting_Started_with_RFNoC_in_UHD_4.0)

Build the image with Vivado
```bash
cd rfnoc-beam_exciter/build
cmake -DUHD_FPGA_DIR=~/uhd/fpga/ ../
make x310_rfnoc_image_core
ll ~/uhd/fpga/usrp3/top/x300/build/
```

Load the image onto the USRP X310
```bash
uhd_usrp_probe
uhd_image_loader --args "type=x300,addr=192.168.10.2" --fpga-path ~/uhd/fpga/usrp3/top/x300/build/usrp_x310_fpga_HG.bin
```



## Directory Structure

* `blocks`: This directory contains all the block definitions. These block
  definitions can be read by the RFNoC tools, and will get installed into the
  system for use by other out-of-tree modules.

* `cmake`: This directory only needs to be modified if this OOT module will
  come with its own custom CMake modules.

* `fpga`: This directory contains the source code for the HDL modules of the
  individual RFNoC blocks, along with their testbenches, and additional modules
  required to build the blocks. There is one subdirectory for every block.

* `include/rfnoc/beam_exciter_rfnoc`: Here, all the header files for the block controllers
  are stored, along with any other include files that should be installed when
  installing this OOT module.

* `lib`: Here, all the non-header source files for the block controllers are stored,
  along with any other include file that should be installed when installing
  this OOT module. This includes the block controller cpp files.

* `apps`: This contains an example application that links against UHD and this
  OOT module. The app does not get installed, it resides in the build directory.


## FPGA Developer hints

Note: For GPIO related blocks, use the branch https://github.com/eltos/uhd/tree/UHD-4.4-GPIO where the GPIO IO Port related changes have been implemented already (see below)!

**Literature**
- [RFNoC Specification](https://files.ettus.com/app_notes/RFNoC_Specification.pdf)
- [Getting Started with RFNoC](https://kb.ettus.com/Getting_Started_with_RFNoC_Development)
- [Getting Started with RFNoC in UHD 4.0](https://kb.ettus.com/Getting_Started_with_RFNoC_in_UHD_4.0)
- [USRP 3 concepts](http://ionconcepts.com/files/USRP3_concepts.pdf)


**Clocks**
X3xx devices have a
- ce clock (214.286 MHz)
- rfnoc_chdr clock (200 MHz)

**AXI Streams**
- https://zipcpu.com/doc/axi-stream.pdf

### Add a new OOT Module

As described in [Getting Started with RFNoC](https://kb.ettus.com/Getting_Started_with_RFNoC_in_UHD_4.0#Creating_Your_Own_OOT_Module):
- Copy the example
  ```bash
  cp -r ~/uhd/host/examples/rfnoc-example .
  mv rfnoc-example rfnoc-beam_exciter
  ```
- Rename `rfnoc-beam_exciter/include/rfnoc/example` to `rfnoc-beam_exciter/include/rfnoc/beam_exciter_rfnoc`
- Rename all occurances of `example` in all files to `beam_exciter_rfnoc` (like in [this commit](82c8054693cc9bac9d598c56e1506c3854fc27d3))
  

### Add a new block

- Make a copy of `rfnoc-beam_exciter/blocks/template.yml` and rename all occurences of `template` to `my_new_block`
  - Set noc_id to `0x064289XX` (replace XX with an incrementing number)
- Make a copy of `rfnoc-beam_exciter/fpga/rfnoc_block_template` and rename all occurences of `template` to `my_new_block`
  - Change the NOC_ID in `rfnoc_block_my_new_block.v` to match the value from the YML file

### Modify block signature

- Backup the code block or make sure all code has been commited
- Make the intended changes to `rfnoc-beam_exciter/blocks/my_new_block.yml`  
  *See [UHD blocks](https://github.com/EttusResearch/uhd/tree/UHD-4.4/host/include/uhd/rfnoc/blocks) and [Getting started guide](https://kb.ettus.com/Getting_Started_with_RFNoC_in_UHD_4.0#Creating_Your_Own_OOT_Module) for examples*
- Re-run the blocktool (this will overwrite the HDL code!)
  ```bash
  cd rfnoc-beam_exciter/build
  python3 ~/uhd/host/utils/rfnoc_blocktool/rfnoc_create_verilog.py -c ../blocks/my_new_block.yml -d ../fpga/rfnoc_block_my_new_block
  cmake -DUHD_FPGA_DIR=~/uhd/fpga/ ../
  ```
- Inspect the git diff and revert all changes not intended (such as your custom user code which will have been deleted)

### Implement block logic

When adding VHDL or Verilog source files
- In `rfnoc-beam_exciter/fpga/rfnoc_block_my_new_block/Makefile.srcs` add them to `RFNOC_OOT_SRCS`

When adding IP (`*.xci` files)
- Put them in a new folder `rfnoc-beam_exciter/fpga/ip/my_if_files`
- Create the `Makefile.inc` in each `rfnoc-beam_exciter/fpga/ip/my_if_files` folder
- Link the `Makefile.inc` in `rfnoc-beam_exciter/fpga/Makefile.srcs` by include
- Also link the `Makefile.inc` in `rfnoc-beam_exciter/fpga/rfnoc_block_my_new_block/Makefile` by include

When implementing blocks that change the sampling rate
- Decimating or interpolating blocks require special caution!
- Use the [`axi_rate_change`](https://github.com/EttusResearch/uhd/blob/UHD-4.4/fpga/usrp3/lib/rfnoc/axi_rate_change.v) module to handle packages, headers and timestamps.
  See `rfnoc_block_template` or `rfnoc_block_discriminating_pulse_counter` for example.



### Add block to image

- Add the block to `rfnoc-beam_exciter/icores/x310_rfnoc_image_core.yml`

### Build

*Note: Make sure that at this time `LD_PRELOAD` is **not** set! If in doubt, append it like `LD_PRELOAD= make ...`. Otherweise vivado will claim some versions were not found...*

Create build folder
```bash
mkdir rfnoc-beam_exciter/build
cd rfnoc-beam_exciter/build
cmake -DUHD_FPGA_DIR=~/uhd/fpga/ ../
```

Run FPGA testbenches (if available)
```bash
cd rfnoc-beam_exciter/build
make testbenches
```

Build the FPGA image
```bash
cd rfnoc-beam_exciter/build
make x310_rfnoc_image_core
```

Alternative 1 for building FPGA images
```bash
cd rfnoc-beam_exciter/build
rfnoc_image_builder -F ~/uhd/fpga/ -I ../ -y ../icores/x310_rfnoc_image_core.yml --GUI
```

Alternative 2 for building FPGA images
```bash
cd rfnoc-beam_exciter/build
# generate verilog sources
rfnoc_image_builder -F ~/uhd/fpga/ -I ../ -y ../icores/x310_rfnoc_image_core.yml --generate-only
# manually build the image with vivado
...
```

The intermediate build directory is `~/uhd/fpga/usrp3/top/x300/build-X310_HG/` (or simmilar depending on the build target). The `build.log` is in this folder.

On success, the FPGA images are finally copied to `~/uhd/fpga/usrp3/top/x300/build/usrp_x310_fpga_HG.bin`.

### Load image to hardware

If using the ethernet connection, load the image to the hardware:
```bash
uhd_usrp_probe
uhd_image_loader --args "type=x300,addr=192.168.10.2" --fpga-path ~/uhd/fpga/usrp3/top/x300/build/usrp_x310_fpga_HG.bin
# power-cycle the USRP
uhd_usrp_probe
```

If using the PCIe MXI connection, you just need to copy the image to the shared directory to use them:
```bash
cp ~/uhd/fpga/usrp3/top/x300/build/usrp_x310_fpga_HG.bit /usr/local/share/uhd/images/usrp_x310_fpga_CUSTOM.bit
cp ~/uhd/fpga/usrp3/top/x300/build/usrp_x310_fpga_HG.rpt /usr/local/share/uhd/images/usrp_x310_fpga_CUSTOM.rpt
cp ~/uhd/fpga/usrp3/top/x300/build/usrp_x310_fpga_HG.lvbitx /usr/local/share/uhd/images/usrp_x310_fpga_CUSTOM.lvbitx

uhd_usrp_probe --args "fpga=CUSTOM"
```



### IO Ports

See also
- https://github.com/EttusResearch/uhd/issues/666
- https://github.com/eltos/uhd/tree/UHD-4.4-GPIO
- Commit da9d58f4726a3006e5383bd527bf3bb710e5bf24
- https://github.com/EttusResearch/uhd/blob/UHD-4.4/host/include/uhd/rfnoc/blocks/radio.yml#L54

Relevant sections in the files mentioned below are marked with `// Dedicated GPIO signal`

Files to change in UHD (see https://github.com/eltos/uhd/tree/UHD-4.4-GPIO):
- `uhd/host/include/uhd/rfnoc/core/io_signatures.yml`
  - Define signals and widths
- `uhd/host/include/uhd/rfnoc/core/x310_bsp.yml`
  - Define IO Ports
- `uhd/fpga/usrp3/top/x300/x300.v`   
  - loop signal through to x300_core
- `uhd/fpga/usrp3/top/x300/x300_core.v`  
  - loop signal through to bus_int
  - check possible interference with db_control logic (`uhd/fpga/usrp3/lib/control/db_control.v`)
- `uhd/fpga/usrp3/top/x300/bus_int.v`  
  - loop signal through to rfnoc_image_core
- Note: after this, the default images will probably not work anymore.

Then re-build
```bash
cd uhd/host/build
...
sudo make install
```

Files to change in OOT Module (see commit da9d58f4726a3006e5383bd527bf3bb710e5bf24):
- `rfnoc-beam_exciter/blocks/gpio_pulse_counter.yml`  
  - Define IO Ports
- `rfnoc-beam_exciter/icores/x310_rfnoc_image_core.yml`  
  - Add BSP Connections

Then re-generate verilog files
```bash
cd rfnoc-beam_exciter/build
rfnoc_image_builder -F ~/uhd/fpga/ -I ../ -y ../icores/x310_rfnoc_image_core.yml --generate-only
```

Then re-build
```bash
cd rfnoc-beam_exciter/build
make x310_rfnoc_image_core
```


## UHD Developer hints

UHD bindings are located in:
- `rfnoc-beam_exciter/include/rfnoc/beam_exciter_rfnoc`
- `rfnoc-beam_exciter/lib`

These are so called "RFNoC block controller" which directly interact with the hardware through the RFNoC framework.

**Properties**
- User properties allow to use generic uhd block bindings in python
- For rate changing blocks it is crucial to handle the `samp_rate` property correctly (see [`discriminating_pulse_counter_block_control.cpp`](https://git.gsi.de/p.niedermayer/exciter/-/blob/e153ea29dbe8220c06494a03bc2981df52cb832c/rfnoc-beam_exciter/lib/discriminating_pulse_counter_block_control.cpp) for example)
- Read https://files.ettus.com/manual/page_properties.html

Note: poke32 expects bits, while settings registers are defined with byte offset, so use something like `regs().poke32(SR_PARAM*8, this->_prop_param.get());`!

Helpful examples:
- All blocks in this repo
- [siggen_block_control.cpp](https://github.com/EttusResearch/uhd/blob/d18647ddf9432033e06a762cffb7a87650c9f0fe/host/lib/rfnoc/siggen_block_control.cpp)
- [keep_one_in_n_block_control.cpp](https://github.com/EttusResearch/uhd/blob/d18647ddf9432033e06a762cffb7a87650c9f0fe/host/lib/rfnoc/keep_one_in_n_block_control.cpp)

### Build and install

```bash
mkdir rfnoc-beam_exciter/build
cd rfnoc-beam_exciter/build
cmake -DUHD_FPGA_DIR=~/uhd/fpga/ ../

make
sudo make install
sudo ldconfig
```

<details>
<summary> Example build output </summary>

```
-- Install configuration: ""
-- Installing: /usr/local/share/uhd/rfnoc/blocks/gain.yml
-- Installing: /usr/local/share/uhd/rfnoc/beam_exciter_rfnoc/fpga/Makefile.srcs
-- Installing: /usr/local/share/uhd/rfnoc/beam_exciter_rfnoc/fpga/rfnoc_block_gain/Makefile.srcs
-- Installing: /usr/local/include/uhd/rfnoc/beam_exciter_rfnoc/gain_block_control.hpp
-- Installing: /usr/local/lib/librfnoc-beam_exciter_rfnoc.so
-- Set runtime path of "/usr/local/lib/librfnoc-beam_exciter_rfnoc.so" to ""
```

</details>


**Note:**
It is likely that UHD does not automatically find the shared library.
In this case, add something like the following in your `~/.bashrc`:
```bash
# Custom RFNoC OOT Module for UHD & GR
export UHD_MODULE_PATH=/usr/local/lib/librfnoc-beam_exciter_rfnoc.so:$UHD_MODULE_PATH
```

Setting `LD_PRELOAD=/usr/local/lib/librfnoc-beam_exciter_rfnoc.so` instead will also fix it, but might cause issues with other binaries like vivado


### Test

Test if the block is available and recognized properly
```bash
uhd_usrp_probe
```

Test with the C++ app
```bash
cd rfnoc-beam_exciter/build
apps/init_gain_block
```

Test with the python bindings (don't forget LD_PRELOAD)
```python
from gnuradio import uhd
rfnoc_graph = uhd.rfnoc_graph(uhd.device_addr("addr=192.168.10.2"))
block = uhd.rfnoc_block_generic(rfnoc_graph, "", "Gain", -1, -1)
block.get_property("gain")
block.set_property("gain", 2)
block.get_property("gain")
```



## GNU Radio Developer hints

Usually it is sufficient to use the generic uhd bindings using properties:
- `rfnoc-beam_exciter/grc`

Helpful examples:
- [uhd_rfnoc_keep_one_in_n.block.yml](https://github.com/gnuradio/gnuradio/blob/e2981b5cdccc4cdc14f57a28976b39fb130ca3ab/gr-uhd/grc/uhd_rfnoc_keep_one_in_n.block.yml)


### Build and install

Build and install UHD bindings first!

```bash
cd rfnoc-beam_exciter/build
sudo make install
```

### Test

See examples in:
- `rfnoc-beam_exciter/examples/*.grc`

  
