//
// Copyright 2019 Ettus Research, a National Instruments Brand
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

#ifndef INCLUDED_RFNOC_DISCRIMINATOR_BLOCK_CONTROL_HPP
#define INCLUDED_RFNOC_DISCRIMINATOR_BLOCK_CONTROL_HPP

#include <uhd/config.hpp>
#include <uhd/rfnoc/noc_block_base.hpp>
#include <uhd/types/stream_cmd.hpp>

namespace rfnoc { namespace beam_exciter_rfnoc {

/*! Block controller for the discriminator block
 *
 */
class UHD_API discriminator_block_control : public uhd::rfnoc::noc_block_base {
public:
    RFNOC_DECLARE_BLOCK(discriminator_block_control)

    virtual void set_threshold_low(const float threshold) = 0;
    virtual void set_threshold_high(const float threshold) = 0;

    virtual float get_threshold_low() = 0;
    virtual float get_threshold_high() = 0;
};

}}

#endif
