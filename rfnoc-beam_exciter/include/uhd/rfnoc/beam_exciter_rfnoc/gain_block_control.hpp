//
// Copyright 2019 Ettus Research, a National Instruments Brand
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

#ifndef INCLUDED_RFNOC_EXAMPLE_GAIN_BLOCK_CONTROL_HPP
#define INCLUDED_RFNOC_EXAMPLE_GAIN_BLOCK_CONTROL_HPP

#include <uhd/config.hpp>
#include <uhd/rfnoc/noc_block_base.hpp>
#include <uhd/types/stream_cmd.hpp>

namespace rfnoc { namespace beam_exciter_rfnoc {

/*! Block controller for the gain block: Multiply amplitude of signal
 *
 * This block multiplies the signal input with a fixed gain value.
 */
class UHD_API gain_block_control : public uhd::rfnoc::noc_block_base {
public:
    RFNOC_DECLARE_BLOCK(gain_block_control)

    /*! Set the gain value
     */
    virtual void set_gain_value(const int gain) = 0;

    /*! Get the current gain value (read it from the device)
     */
    virtual int get_gain_value() = 0;
};

}} // namespace rfnoc::beam_exciter_rfnoc

#endif /* INCLUDED_RFNOC_EXAMPLE_GAIN_BLOCK_CONTROL_HPP */
