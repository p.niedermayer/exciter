//
// Copyright 2019 Ettus Research, a National Instruments Brand
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

#ifndef INCLUDED_RFNOC_DISCRIMINATING_PULSE_COUNTER_BLOCK_CONTROL_HPP
#define INCLUDED_RFNOC_DISCRIMINATING_PULSE_COUNTER_BLOCK_CONTROL_HPP

#include <uhd/config.hpp>
#include <uhd/rfnoc/noc_block_base.hpp>
#include <uhd/types/stream_cmd.hpp>

namespace rfnoc { namespace beam_exciter_rfnoc {

/*! Block controller for the discriminating_pulse_counter block
 *
 */
class UHD_API discriminating_pulse_counter_block_control : public uhd::rfnoc::noc_block_base {
public:
    RFNOC_DECLARE_BLOCK(discriminating_pulse_counter_block_control)

};

}}

#endif
