//
// Copyright 2019 Ettus Research, a National Instruments Brand
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

#ifndef INCLUDED_RFNOC_FEEDBACK_CONTROLLER_BLOCK_CONTROL_HPP
#define INCLUDED_RFNOC_FEEDBACK_CONTROLLER_BLOCK_CONTROL_HPP

#include <uhd/config.hpp>
#include <uhd/rfnoc/noc_block_base.hpp>
#include <uhd/types/stream_cmd.hpp>

namespace rfnoc { namespace beam_exciter_rfnoc {

/*! Block controller for the feedback_controller block
 *
 */
class UHD_API feedback_controller_block_control : public uhd::rfnoc::noc_block_base {
public:
    RFNOC_DECLARE_BLOCK(feedback_controller_block_control)

};

}}

#endif
