//
// Copyright 2019 Ettus Research, a National Instruments Brand
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

#ifndef INCLUDED_RFNOC_GPIO_PULSE_COUNTER_BLOCK_CONTROL_HPP
#define INCLUDED_RFNOC_GPIO_PULSE_COUNTER_BLOCK_CONTROL_HPP

#include <uhd/config.hpp>
#include <uhd/rfnoc/noc_block_base.hpp>
#include <uhd/types/stream_cmd.hpp>

namespace rfnoc { namespace beam_exciter_rfnoc {

/*! Block controller for the gpio_pulse_counter block
 *
 */
class UHD_API gpio_pulse_counter_block_control : public uhd::rfnoc::noc_block_base {
public:
    RFNOC_DECLARE_BLOCK(gpio_pulse_counter_block_control)

    /*! Set the decimation value
     */
    virtual void set_decimation_value(const int decimation) = 0;

    /*! Get the current decimation value (read it from the device)
     */
    virtual int get_decimation_value() = 0;
};

}}

#endif
