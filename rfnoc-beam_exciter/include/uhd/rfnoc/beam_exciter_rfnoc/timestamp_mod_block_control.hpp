//
// Copyright 2019 Ettus Research, a National Instruments Brand
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

#ifndef INCLUDED_RFNOC_EXCITER_TIMEMOD_CONTROL_HPP
#define INCLUDED_RFNOC_EXCITER_TIMEMOD_CONTROL_HPP

#include <uhd/config.hpp>
#include <uhd/rfnoc/noc_block_base.hpp>
#include <uhd/types/stream_cmd.hpp>

namespace rfnoc { namespace beam_exciter_rfnoc {

class UHD_API timestamp_mod_block_control : public uhd::rfnoc::noc_block_base {
public:
    RFNOC_DECLARE_BLOCK(timestamp_mod_block_control)

};

}} 

#endif
