
//
// Conversion helpers
// to convert numeric data between CPU and RFNoC domain
//
// 


#ifndef INCLUDED_RFNOC_UTIL_HPP
#define INCLUDED_RFNOC_UTIL_HPP


#include <stdint.h>
#include <complex>
#include <limits>
#include <cmath>
#include <cstring>

namespace rfnoc { namespace beam_exciter_rfnoc {


template <class T>
const T clamp(const double v){
    constexpr T min_t = std::numeric_limits<T>::min();
    constexpr T max_t = std::numeric_limits<T>::max();
    return (v < min_t) ? min_t : (v > max_t) ? max_t : T(v);
};

/**
 * Convert float to uint16_t
 */
const uint16_t float2i(const float f);

/**
 * Convert uint16_t to float
 */
const float i2float(const uint16_t ui);

/**
 * Convert complex double to sc16
 */
const uint32_t complex2iq(const std::complex<double> v);

/**
 * Convert float to unsigned fixed point representation
 * @param value floating point value
 * @param bits total number of bits of the fixed point
 * @param point_offset bit offset of the fixed point
 * @return binary
 *
 * Examples:
 *   0*2^point_offset --> 0x0
 *   1*2^point_offset --> 0x1
 *   2*2^point_offset --> 0x2
 */
const uint32_t float2fixed(const float value, const int bits, const int point_offset);

/**
 * Convert unsigned fixed point representation to float
 * @param value binary
 * @param bits total number of bits of the fixed point
 * @param point_offset bit offset of the fixed point
 * @return floating point value
 *
 * Examples:
 *   0x0 --> 0*2^point_offset
 *   0x1 --> 1*2^point_offset
 *   0x2 --> 2*2^point_offset
 */
const float fixed2float(const uint32_t value, const int bits, const int point_offset);

/**
 * Convert float to IEEE 754 representation
 * @param value floating point value
 * @return binary
 */
const uint32_t float2ieee(const float value);

/**
 * Convert IEEE 754 representation to float
 * @param value binary
 * @return floating point value
 */
const float ieee2float(const uint32_t binary);



}}

#endif
