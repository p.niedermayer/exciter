//
// Copyright 2023 Ettus Research, a National Instruments Brand
//
// SPDX-License-Identifier: LGPL-3.0-or-later
//
// Module: rfnoc_block_template
//
// Description:
//
//   A block template
//
// Parameters:
//
//   NOC_ID      : The block's NOC ID
//   THIS_PORTID : Control crossbar port to which this block is connected
//   CHDR_W      : AXIS-CHDR data bus width
//   MTU         : Maximum transmission unit (i.e., maximum packet size in
//                 CHDR words is 2**MTU).
//

`default_nettype none


module rfnoc_block_template #(
  parameter [31:0] NOC_ID          = 32'h0BADC0DE, // <-- TODO: change NOC ID here!
  parameter [ 9:0] THIS_PORTID     = 10'd0,
  parameter        CHDR_W          = 64,
  parameter [ 5:0] MTU             = 10
)(
  // RFNoC Framework Clocks and Resets
  input  wire                   rfnoc_chdr_clk,
  input  wire                   rfnoc_ctrl_clk,
  input  wire                   ce_clk,
  // RFNoC Backend Interface
  input  wire [511:0]           rfnoc_core_config,
  output wire [511:0]           rfnoc_core_status,
  // AXIS-CHDR Input Ports (from framework)
  input  wire [(1)*CHDR_W-1:0] s_rfnoc_chdr_tdata,
  input  wire [(1)-1:0]        s_rfnoc_chdr_tlast,
  input  wire [(1)-1:0]        s_rfnoc_chdr_tvalid,
  output wire [(1)-1:0]        s_rfnoc_chdr_tready,
  // AXIS-CHDR Output Ports (to framework)
  output wire [(1)*CHDR_W-1:0] m_rfnoc_chdr_tdata,
  output wire [(1)-1:0]        m_rfnoc_chdr_tlast,
  output wire [(1)-1:0]        m_rfnoc_chdr_tvalid,
  input  wire [(1)-1:0]        m_rfnoc_chdr_tready,
  // AXIS-Ctrl Input Port (from framework)
  input  wire [31:0]            s_rfnoc_ctrl_tdata,
  input  wire                   s_rfnoc_ctrl_tlast,
  input  wire                   s_rfnoc_ctrl_tvalid,
  output wire                   s_rfnoc_ctrl_tready,
  // AXIS-Ctrl Output Port (to framework)
  output wire [31:0]            m_rfnoc_ctrl_tdata,
  output wire                   m_rfnoc_ctrl_tlast,
  output wire                   m_rfnoc_ctrl_tvalid,
  input  wire                   m_rfnoc_ctrl_tready
);

  //---------------------------------------------------------------------------
  // Signal Declarations
  //---------------------------------------------------------------------------

  // Clocks and Resets
  wire               ctrlport_clk;
  wire               ctrlport_rst;
  wire               axis_data_clk;
  wire               axis_data_rst;
  // CtrlPort Master
  wire               m_ctrlport_req_wr;
  wire               m_ctrlport_req_rd;
  wire [19:0]        m_ctrlport_req_addr;
  wire [31:0]        m_ctrlport_req_data;
  wire               m_ctrlport_resp_ack;
  wire [31:0]        m_ctrlport_resp_data;
  // Data Stream to User Logic: in
  wire [32*1-1:0]    m_in_axis_tdata;
  wire [1-1:0]       m_in_axis_tkeep;
  wire               m_in_axis_tlast;
  wire               m_in_axis_tvalid;
  wire               m_in_axis_tready;
  wire [63:0]        m_in_axis_ttimestamp;
  wire               m_in_axis_thas_time;
  wire [15:0]        m_in_axis_tlength;
  wire               m_in_axis_teov;
  wire               m_in_axis_teob;
  // Data Stream from User Logic: out
  wire [32*1-1:0]    s_out_axis_tdata;
  wire [0:0]         s_out_axis_tkeep;
  wire               s_out_axis_tlast;
  wire               s_out_axis_tvalid;
  wire               s_out_axis_tready;
  wire [63:0]        s_out_axis_ttimestamp;
  wire               s_out_axis_thas_time;
  wire [15:0]        s_out_axis_tlength;
  wire               s_out_axis_teov;
  wire               s_out_axis_teob;

  //---------------------------------------------------------------------------
  // NoC Shell
  //---------------------------------------------------------------------------

  noc_shell_template #(
    .NOC_ID              (NOC_ID),
    .CHDR_W              (CHDR_W),
    .THIS_PORTID         (THIS_PORTID),
    .MTU                 (MTU)
  ) noc_shell_template_i (
    //---------------------
    // Framework Interface
    //---------------------

    // Clock Inputs
    .rfnoc_chdr_clk      (rfnoc_chdr_clk),
    .rfnoc_ctrl_clk      (rfnoc_ctrl_clk),
    .ce_clk              (ce_clk),
    // Reset Outputs
    .rfnoc_chdr_rst      (),
    .rfnoc_ctrl_rst      (),
    .ce_rst              (),
    // RFNoC Backend Interface
    .rfnoc_core_config   (rfnoc_core_config),
    .rfnoc_core_status   (rfnoc_core_status),
    // CHDR Input Ports  (from framework)
    .s_rfnoc_chdr_tdata  (s_rfnoc_chdr_tdata),
    .s_rfnoc_chdr_tlast  (s_rfnoc_chdr_tlast),
    .s_rfnoc_chdr_tvalid (s_rfnoc_chdr_tvalid),
    .s_rfnoc_chdr_tready (s_rfnoc_chdr_tready),
    // CHDR Output Ports (to framework)
    .m_rfnoc_chdr_tdata  (m_rfnoc_chdr_tdata),
    .m_rfnoc_chdr_tlast  (m_rfnoc_chdr_tlast),
    .m_rfnoc_chdr_tvalid (m_rfnoc_chdr_tvalid),
    .m_rfnoc_chdr_tready (m_rfnoc_chdr_tready),
    // AXIS-Ctrl Input Port (from framework)
    .s_rfnoc_ctrl_tdata  (s_rfnoc_ctrl_tdata),
    .s_rfnoc_ctrl_tlast  (s_rfnoc_ctrl_tlast),
    .s_rfnoc_ctrl_tvalid (s_rfnoc_ctrl_tvalid),
    .s_rfnoc_ctrl_tready (s_rfnoc_ctrl_tready),
    // AXIS-Ctrl Output Port (to framework)
    .m_rfnoc_ctrl_tdata  (m_rfnoc_ctrl_tdata),
    .m_rfnoc_ctrl_tlast  (m_rfnoc_ctrl_tlast),
    .m_rfnoc_ctrl_tvalid (m_rfnoc_ctrl_tvalid),
    .m_rfnoc_ctrl_tready (m_rfnoc_ctrl_tready),

    //---------------------
    // Client Interface
    //---------------------

    // CtrlPort Clock and Reset
    .ctrlport_clk              (ctrlport_clk),
    .ctrlport_rst              (ctrlport_rst),
    // CtrlPort Master
    .m_ctrlport_req_wr         (m_ctrlport_req_wr),
    .m_ctrlport_req_rd         (m_ctrlport_req_rd),
    .m_ctrlport_req_addr       (m_ctrlport_req_addr),
    .m_ctrlport_req_data       (m_ctrlport_req_data),
    .m_ctrlport_resp_ack       (m_ctrlport_resp_ack),
    .m_ctrlport_resp_data      (m_ctrlport_resp_data),

    // AXI-Stream Clock and Reset
    .axis_data_clk (axis_data_clk),
    .axis_data_rst (axis_data_rst),
    // Data Stream to User Logic: in
    .m_in_axis_tdata      (m_in_axis_tdata),
    .m_in_axis_tkeep      (m_in_axis_tkeep),
    .m_in_axis_tlast      (m_in_axis_tlast),
    .m_in_axis_tvalid     (m_in_axis_tvalid),
    .m_in_axis_tready     (m_in_axis_tready),
    .m_in_axis_ttimestamp (m_in_axis_ttimestamp),
    .m_in_axis_thas_time  (m_in_axis_thas_time),
    .m_in_axis_tlength    (m_in_axis_tlength),
    .m_in_axis_teov       (m_in_axis_teov),
    .m_in_axis_teob       (m_in_axis_teob),
    // Data Stream from User Logic: out
    .s_out_axis_tdata      (s_out_axis_tdata),
    .s_out_axis_tkeep      (s_out_axis_tkeep),
    .s_out_axis_tlast      (s_out_axis_tlast),
    .s_out_axis_tvalid     (s_out_axis_tvalid),
    .s_out_axis_tready     (s_out_axis_tready),
    .s_out_axis_ttimestamp (s_out_axis_ttimestamp),
    .s_out_axis_thas_time  (s_out_axis_thas_time),
    .s_out_axis_tlength    (s_out_axis_tlength),
    .s_out_axis_teov       (s_out_axis_teov),
    .s_out_axis_teob       (s_out_axis_teob)
  );










  //---------------------------------------------------------------------------
  // Registers (settings bus)
  //---------------------------------------------------------------------------
  
  // TODO
  localparam SR_PARAM_A                =  0; // param a (1 bit)
  localparam SR_PARAM_B                =  1; // param b (16 bit unsigned integer)
  localparam SR_RESERVED               = 99; // reserved register
  
  localparam DEFAULT_PARAM_A           =  1'b0;        // false
  localparam DEFAULT_PARAM_B           = 16'd200;      // 200
  
  
  wire [ 8-1:0] set_addr;
  wire [32-1:0] set_data;
  //wire [ 1-1:0] set_has_time;
  wire [ 1-1:0] set_stb;
  //wire [64-1:0] set_time;
  wire [ 8-1:0] rb_addr;
  reg  [64-1:0] rb_data;

  ctrlport_to_settings_bus # (
    .NUM_PORTS(1)
  ) ctrlport_to_settings_bus_i (
    .ctrlport_clk             (ctrlport_clk),
    .ctrlport_rst             (ctrlport_rst),
    .s_ctrlport_req_wr        (m_ctrlport_req_wr),
    .s_ctrlport_req_rd        (m_ctrlport_req_rd),
    .s_ctrlport_req_addr      (m_ctrlport_req_addr),
    .s_ctrlport_req_data      (m_ctrlport_req_data),
    //.s_ctrlport_req_has_time  (  ctrlport_req_has_time),
    //.s_ctrlport_req_time      (  ctrlport_req_time),
    .s_ctrlport_resp_ack      (m_ctrlport_resp_ack),
    .s_ctrlport_resp_data     (m_ctrlport_resp_data),
    .set_data                 (set_data),
    .set_addr                 (set_addr),
    .set_stb                  (set_stb),
    //.set_time                 (set_time),
    //.set_has_time             (set_has_time),
    .rb_stb                   (1'b1),
    .rb_addr                  (rb_addr),
    .rb_data                  (rb_data),
    .timestamp                (64'b0)
  );
  
  
  // Settings registers
  // TODO
  wire [ 0:0] reg_param_a;
  setting_reg #(.my_addr(SR_PARAM_A), .width(1), .at_reset(DEFAULT_PARAM_A)
  ) set_param_a (.out(reg_param_a), .in(set_data), .addr(set_addr), .strobe(set_stb), .clk(ctrlport_clk), .rst(ctrlport_rst));
  
  wire [15:0] reg_param_b;
  setting_reg #(.my_addr(SR_PARAM_B), .width(16), .at_reset(DEFAULT_PARAM_B)
  ) set_param_b (.out(reg_param_b), .in(set_data), .addr(set_addr), .strobe(set_stb), .clk(ctrlport_clk), .rst(ctrlport_rst));
  
 
  // Readback registers
  // TODO
  always @(*) begin
	case(rb_addr)
	  //RB_XXXX         : rb_data <= XXXX;
	  default           : rb_data <= 64'h0BADC0DE0BADC0DE;
	endcase
  end
  
  
  
  
  
  
  
  
  
  

  //---------------------------------------------------------------------------
  // Header encoder
  //---------------------------------------------------------------------------
  
  wire [128-1:0] m_in_axis_tuser;
  
  // m_in_axis_ttimestamp  -\
  // m_in_axis_thas_time   --\
  // m_in_axis_tlength     --->  m_in_axis_tuser
  // m_in_axis_teov        --/
  // m_in_axis_teob        -/
  
  cvita_hdr_encoder cvita_hdr_encoder_i (
    .pkt_type       (2'b0),
    .eob            (m_in_axis_teob),
    .has_time       (m_in_axis_thas_time),
    .seqnum         (12'b0),
    .payload_length (m_in_axis_tlength[0 +: 16]),
    .src_sid        (16'b0),
    .dst_sid        (16'b0),
    .vita_time      (m_in_axis_ttimestamp[0 +: 64]),
    .header         (m_in_axis_tuser[0 +: 128])
  );
  
  
  //---------------------------------------------------------------------------
  // Header decoder
  //---------------------------------------------------------------------------
  
  wire [128-1:0] s_out_axis_tuser;
  
  //                    /->  s_out_axis_ttimestamp
  //                   /-->  s_out_axis_thas_time
  // s_out_axis_tuser  --->  s_out_axis_tlength
  //                   \-->  s_out_axis_teov
  //                    \->  s_out_axis_teob
  
  cvita_hdr_decoder cvita_hdr_decoder_i (
    .header(s_out_axis_tuser),
    .pkt_type(),
    .eob(s_out_axis_teob),
    .has_time(s_out_axis_thas_time),
    .seqnum(),
    .length(),
    .payload_length(s_out_axis_tlength),
    .src_sid(),
    .dst_sid(),
    .vita_time(s_out_axis_ttimestamp)
  );

  
  //---------------------------------------------------------------------------
  // Sample en-/decoder with rate change
  //---------------------------------------------------------------------------

  wire [31:0] m_in_sample_tdata,  s_out_sample_tdata;
  wire        m_in_sample_tvalid, s_out_sample_tvalid;
  wire        m_in_sample_tready, s_out_sample_tready;
  
  // m_in_axis_tdata   -\
  // m_in_axis_tvalid  --\   m_in_sample_tdata
  // m_in_axis_tready  --->  m_in_sample_tvalid
  // m_in_axis_tlast   --/   m_in_sample_tready
  // m_in_axis_tuser   -/
  
  //                       /->  s_out_axis_tdata
  // s_out_sample_tdata   /-->  s_out_axis_tvalid
  // s_out_sample_tvalid  --->  s_out_axis_tready
  // s_out_sample_tready  \-->  s_out_axis_tlast
  //                       \->  s_out_axis_tuser
  
  axi_rate_change #(
    .WIDTH(32),
    // N:M = ratio of in:out rate
    // TODO
    .DEFAULT_N(1),
    .MAX_N(1),
    .SR_N_ADDR(SR_RESERVED),
    .DEFAULT_M(1),
    .MAX_M(1),
    .SR_M_ADDR(SR_RESERVED),
    //.MAXIMIZE_OUTPUT_PKT_LEN(1),
    .SR_CONFIG_ADDR(SR_RESERVED),
    .SR_TIME_INCR_ADDR(SR_RESERVED)
    
  ) axi_rate_change (
    .clk(axis_data_clk),
    .reset(axis_data_rst),    
    // Settings bus
    .set_stb(set_stb),
    .set_addr(set_addr),
    .set_data(set_data),
    //.clear(clear_tx_seqnum[i]),
    //.clear_user(clear_user),
    //.src_sid(16'b0),
    //.dst_sid(16'b0),    
    // AXI interface on RFNoC side
    .i_tdata (m_in_axis_tdata),
    .i_tvalid(m_in_axis_tvalid),
    .i_tready(m_in_axis_tready),
    .i_tlast (m_in_axis_tlast),
    .i_tuser (m_in_axis_tuser),	
    .o_tdata (s_out_axis_tdata),
    .o_tvalid(s_out_axis_tvalid),
    .o_tready(s_out_axis_tready),
    .o_tlast (s_out_axis_tlast),
    .o_tuser (s_out_axis_tuser),    
    // AXI interface on user code side	
    .m_axis_data_tdata(m_in_sample_tdata),
    .m_axis_data_tvalid(m_in_sample_tvalid),
    .m_axis_data_tready(m_in_sample_tready),
    .m_axis_data_tlast(),
    .s_axis_data_tdata(s_out_sample_tdata),
    .s_axis_data_tvalid(s_out_sample_tvalid),
    .s_axis_data_tready(s_out_sample_tready),
    .s_axis_data_tlast(1'b0),
    // Debugging signals
    .warning_long_throttle(), .error_extra_outputs(), .error_drop_pkt_lockup()
  );
  
  // Only 1-sample per clock, so tkeep should always be asserted
  assign s_out_axis_tkeep = 1'b1;







  //---------------------------------------------------------------------------
  // User Logic
  //---------------------------------------------------------------------------
  
  
  // When N:M is the ratio of in:out rate (i.e. N is decimating and M is interpolating)
  // - User code is responsible for generating correct number of outputs per input
  //   > Example: After N input samples block should output M samples. If user code's
  //              pipelining requires additional samples to "push" the M sample out,
  //              it is the user's responsibility to make the mechanism (such as 
  //              injecting extra samples) to do so.
  // - User code will always see an integer multiple of N samples. This ensures
  //   the user will not need to manually clear a "partial output sample" stuck in their
  //   pipeline due to an uneven (in respect to decimation rate) number of input samples.
  //
  // sc16 format is used, i.e. [31:16] contains I/real and [15:0] Q/imaginary data
  // The convention is to map int16 range [-32768; 32767] to float32 range [-1; 1]
  
  // TODO: Replace this with your user logic
  assign s_out_sample_tdata = m_in_sample_tdata;
  assign s_out_sample_tvalid = m_in_sample_tvalid;
  assign m_in_sample_tready = s_out_sample_tready;
  
  


  


endmodule


`default_nettype wire
