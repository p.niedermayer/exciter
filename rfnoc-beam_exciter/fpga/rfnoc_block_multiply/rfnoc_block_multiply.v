//
// Copyright 2023 Ettus Research, a National Instruments Brand
//
// SPDX-License-Identifier: LGPL-3.0-or-later
//
// Module: rfnoc_block_multiply
//
// Description:
//
//   <Add block description here>
//
// Parameters:
//
//   THIS_PORTID : Control crossbar port to which this block is connected
//   CHDR_W      : AXIS-CHDR data bus width
//   MTU         : Maximum transmission unit (i.e., maximum packet size in
//                 CHDR words is 2**MTU).
//

`default_nettype none


module rfnoc_block_multiply #(
  parameter [9:0] THIS_PORTID     = 10'd0,
  parameter       CHDR_W          = 64,
  parameter [5:0] MTU             = 10
)(
  // RFNoC Framework Clocks and Resets
  input  wire                   rfnoc_chdr_clk,
  input  wire                   rfnoc_ctrl_clk,
  // RFNoC Backend Interface
  input  wire [511:0]           rfnoc_core_config,
  output wire [511:0]           rfnoc_core_status,
  // AXIS-CHDR Input Ports (from framework)
  input  wire [(2)*CHDR_W-1:0] s_rfnoc_chdr_tdata,
  input  wire [(2)-1:0]        s_rfnoc_chdr_tlast,
  input  wire [(2)-1:0]        s_rfnoc_chdr_tvalid,
  output wire [(2)-1:0]        s_rfnoc_chdr_tready,
  // AXIS-CHDR Output Ports (to framework)
  output wire [(1)*CHDR_W-1:0] m_rfnoc_chdr_tdata,
  output wire [(1)-1:0]        m_rfnoc_chdr_tlast,
  output wire [(1)-1:0]        m_rfnoc_chdr_tvalid,
  input  wire [(1)-1:0]        m_rfnoc_chdr_tready,
  // AXIS-Ctrl Input Port (from framework)
  input  wire [31:0]            s_rfnoc_ctrl_tdata,
  input  wire                   s_rfnoc_ctrl_tlast,
  input  wire                   s_rfnoc_ctrl_tvalid,
  output wire                   s_rfnoc_ctrl_tready,
  // AXIS-Ctrl Output Port (to framework)
  output wire [31:0]            m_rfnoc_ctrl_tdata,
  output wire                   m_rfnoc_ctrl_tlast,
  output wire                   m_rfnoc_ctrl_tvalid,
  input  wire                   m_rfnoc_ctrl_tready
);

  //---------------------------------------------------------------------------
  // Signal Declarations
  //---------------------------------------------------------------------------

  // Clocks and Resets
  wire               ctrlport_clk;
  wire               ctrlport_rst;
  wire               axis_data_clk;
  wire               axis_data_rst;
  // CtrlPort Master
  wire               m_ctrlport_req_wr;
  wire               m_ctrlport_req_rd;
  wire [19:0]        m_ctrlport_req_addr;
  wire [31:0]        m_ctrlport_req_data;
  wire               m_ctrlport_resp_ack;
  wire [31:0]        m_ctrlport_resp_data;
  // Payload Stream to User Logic: in0
  wire [32*1-1:0]    m_in0_payload_tdata;
  wire [1-1:0]       m_in0_payload_tkeep;
  wire               m_in0_payload_tlast;
  wire               m_in0_payload_tvalid;
  wire               m_in0_payload_tready;
  // Context Stream to User Logic: in0
  wire [CHDR_W-1:0]  m_in0_context_tdata;
  wire [3:0]         m_in0_context_tuser;
  wire               m_in0_context_tlast;
  wire               m_in0_context_tvalid;
  wire               m_in0_context_tready;
  // Payload Stream to User Logic: in1
  wire [32*1-1:0]    m_in1_payload_tdata;
  wire [1-1:0]       m_in1_payload_tkeep;
  wire               m_in1_payload_tlast;
  wire               m_in1_payload_tvalid;
  wire               m_in1_payload_tready;
  // Context Stream to User Logic: in1
  wire [CHDR_W-1:0]  m_in1_context_tdata;
  wire [3:0]         m_in1_context_tuser;
  wire               m_in1_context_tlast;
  wire               m_in1_context_tvalid;
  wire               m_in1_context_tready;
  // Payload Stream from User Logic: out
  wire [32*1-1:0]    s_out_payload_tdata;
  wire [0:0]         s_out_payload_tkeep;
  wire               s_out_payload_tlast;
  wire               s_out_payload_tvalid;
  wire               s_out_payload_tready;
  // Context Stream from User Logic: out
  wire [CHDR_W-1:0]  s_out_context_tdata;
  wire [3:0]         s_out_context_tuser;
  wire               s_out_context_tlast;
  wire               s_out_context_tvalid;
  wire               s_out_context_tready;

  //---------------------------------------------------------------------------
  // NoC Shell
  //---------------------------------------------------------------------------

  noc_shell_multiply #(
    .CHDR_W              (CHDR_W),
    .THIS_PORTID         (THIS_PORTID),
    .MTU                 (MTU)
  ) noc_shell_multiply_i (
    //---------------------
    // Framework Interface
    //---------------------

    // Clock Inputs
    .rfnoc_chdr_clk      (rfnoc_chdr_clk),
    .rfnoc_ctrl_clk      (rfnoc_ctrl_clk),
    // Reset Outputs
    .rfnoc_chdr_rst      (),
    .rfnoc_ctrl_rst      (),
    // RFNoC Backend Interface
    .rfnoc_core_config   (rfnoc_core_config),
    .rfnoc_core_status   (rfnoc_core_status),
    // CHDR Input Ports  (from framework)
    .s_rfnoc_chdr_tdata  (s_rfnoc_chdr_tdata),
    .s_rfnoc_chdr_tlast  (s_rfnoc_chdr_tlast),
    .s_rfnoc_chdr_tvalid (s_rfnoc_chdr_tvalid),
    .s_rfnoc_chdr_tready (s_rfnoc_chdr_tready),
    // CHDR Output Ports (to framework)
    .m_rfnoc_chdr_tdata  (m_rfnoc_chdr_tdata),
    .m_rfnoc_chdr_tlast  (m_rfnoc_chdr_tlast),
    .m_rfnoc_chdr_tvalid (m_rfnoc_chdr_tvalid),
    .m_rfnoc_chdr_tready (m_rfnoc_chdr_tready),
    // AXIS-Ctrl Input Port (from framework)
    .s_rfnoc_ctrl_tdata  (s_rfnoc_ctrl_tdata),
    .s_rfnoc_ctrl_tlast  (s_rfnoc_ctrl_tlast),
    .s_rfnoc_ctrl_tvalid (s_rfnoc_ctrl_tvalid),
    .s_rfnoc_ctrl_tready (s_rfnoc_ctrl_tready),
    // AXIS-Ctrl Output Port (to framework)
    .m_rfnoc_ctrl_tdata  (m_rfnoc_ctrl_tdata),
    .m_rfnoc_ctrl_tlast  (m_rfnoc_ctrl_tlast),
    .m_rfnoc_ctrl_tvalid (m_rfnoc_ctrl_tvalid),
    .m_rfnoc_ctrl_tready (m_rfnoc_ctrl_tready),

    //---------------------
    // Client Interface
    //---------------------

    // CtrlPort Clock and Reset
    .ctrlport_clk              (ctrlport_clk),
    .ctrlport_rst              (ctrlport_rst),
    // CtrlPort Master
    .m_ctrlport_req_wr         (m_ctrlport_req_wr),
    .m_ctrlport_req_rd         (m_ctrlport_req_rd),
    .m_ctrlport_req_addr       (m_ctrlport_req_addr),
    .m_ctrlport_req_data       (m_ctrlport_req_data),
    .m_ctrlport_resp_ack       (m_ctrlport_resp_ack),
    .m_ctrlport_resp_data      (m_ctrlport_resp_data),

    // AXI-Stream Payload Context Clock and Reset
    .axis_data_clk (axis_data_clk),
    .axis_data_rst (axis_data_rst),
    // Payload Stream to User Logic: in0
    .m_in0_payload_tdata  (m_in0_payload_tdata),
    .m_in0_payload_tkeep  (m_in0_payload_tkeep),
    .m_in0_payload_tlast  (m_in0_payload_tlast),
    .m_in0_payload_tvalid (m_in0_payload_tvalid),
    .m_in0_payload_tready (m_in0_payload_tready),
    // Context Stream to User Logic: in0
    .m_in0_context_tdata  (m_in0_context_tdata),
    .m_in0_context_tuser  (m_in0_context_tuser),
    .m_in0_context_tlast  (m_in0_context_tlast),
    .m_in0_context_tvalid (m_in0_context_tvalid),
    .m_in0_context_tready (m_in0_context_tready),
    // Payload Stream to User Logic: in1
    .m_in1_payload_tdata  (m_in1_payload_tdata),
    .m_in1_payload_tkeep  (m_in1_payload_tkeep),
    .m_in1_payload_tlast  (m_in1_payload_tlast),
    .m_in1_payload_tvalid (m_in1_payload_tvalid),
    .m_in1_payload_tready (m_in1_payload_tready),
    // Context Stream to User Logic: in1
    .m_in1_context_tdata  (m_in1_context_tdata),
    .m_in1_context_tuser  (m_in1_context_tuser),
    .m_in1_context_tlast  (m_in1_context_tlast),
    .m_in1_context_tvalid (m_in1_context_tvalid),
    .m_in1_context_tready (m_in1_context_tready),
    // Payload Stream from User Logic: out
    .s_out_payload_tdata  (s_out_payload_tdata),
    .s_out_payload_tkeep  (s_out_payload_tkeep),
    .s_out_payload_tlast  (s_out_payload_tlast),
    .s_out_payload_tvalid (s_out_payload_tvalid),
    .s_out_payload_tready (s_out_payload_tready),
    // Context Stream from User Logic: out
    .s_out_context_tdata  (s_out_context_tdata),
    .s_out_context_tuser  (s_out_context_tuser),
    .s_out_context_tlast  (s_out_context_tlast),
    .s_out_context_tvalid (s_out_context_tvalid),
    .s_out_context_tready (s_out_context_tready)
  );

  //---------------------------------------------------------------------------
  // User Logic
  //---------------------------------------------------------------------------

  // Multiply result. I/real in [63:32], Q/imaginary in [31:0] (sc32).
  wire [33:0] mult_tdata;
  wire        mult_tlast;
  wire        mult_tvalid;
  wire        mult_tready;


  // Use the out-of-tree "cmplx_mul" IP, which is a Xilinx Complex
  // Multiplier LogiCORE IP located in the IP directory of this example.
  // Documentation: https://docs.xilinx.com/v/u/en-US/pg104-cmpy
  // This IP expects real/I in the lower bits and imaginary/Q in the upper
  // bits, each padded to byte boundaries.
  // config.APortWidth = 16;
  // config.BPortWidth = 16;
  // config.OutputWidth = 33;
  // --> 2x16 bit input: imag in [31:16] and real in [15:0]
  // --> 2x33 bit output: imag in [72:40] and real in [32:0]
  // --> so we swap imag and real
  wire [79:0] m_axis_dout_tdata;

  cmplx_mul cmplx_mul_i (
	.aclk               (axis_data_clk),
	.aresetn            (~axis_data_rst),
	.s_axis_a_tdata     ({m_in0_payload_tdata[15:0],     // Imaginary
						  m_in0_payload_tdata[31:16]}),  // Real
	.s_axis_a_tlast     (m_in0_payload_tlast),
	.s_axis_a_tvalid    (m_in0_payload_tvalid),
	.s_axis_a_tready    (m_in0_payload_tready),
	.s_axis_b_tdata     ({m_in1_payload_tdata[15:0],     // Imaginary
						  m_in1_payload_tdata[31:16]}),  // Real
	.s_axis_b_tlast     (m_in1_payload_tlast),
	.s_axis_b_tvalid    (m_in1_payload_tvalid),
	.s_axis_b_tready    (m_in1_payload_tready),
	.m_axis_dout_tdata  (m_axis_dout_tdata),
	.m_axis_dout_tlast  (mult_tlast),
	.m_axis_dout_tvalid (mult_tvalid),
	.m_axis_dout_tready (mult_tready)
  );
  
  // since both input and output must be sc16 (signed fixed point [-1; +1])
  // use the upper 16+1 bits and discard the lower ones
  assign mult_tdata[33:17] = m_axis_dout_tdata[31:15]; // Real
  assign mult_tdata[16: 0] = m_axis_dout_tdata[71:55]; // Imaginary  
  
  // Clip the results to prevent overflow
  axi_clip_complex #(
    .WIDTH_IN  (17),
    .WIDTH_OUT (16)
  ) axi_clip_complex_i (
    .clk      (axis_data_clk),
    .reset    (axis_data_rst),
    .i_tdata  (mult_tdata),
    .i_tlast  (mult_tlast),
    .i_tvalid (mult_tvalid),
    .i_tready (mult_tready),
    .o_tdata  (s_out_payload_tdata),
    .o_tlast  (s_out_payload_tlast),
    .o_tvalid (s_out_payload_tvalid),
    .o_tready (s_out_payload_tready)
  );
  



  // Only 1-sample per clock, so tkeep should always be asserted
  assign s_out_payload_tkeep = 1'b1;

  // We use the first input to control the packet size
  // and other attributes of the output packets.
  // So we pass the first context through
  assign s_out_context_tdata  = m_in0_context_tdata;
  assign s_out_context_tuser  = m_in0_context_tuser;
  assign s_out_context_tlast  = m_in0_context_tlast;
  assign s_out_context_tvalid = m_in0_context_tvalid;
  assign m_in0_context_tready = s_out_context_tready;
  // And discard the second context
  assign m_in1_context_tready = 1;
  

endmodule // rfnoc_block_multiply


`default_nettype wire
