#
# Copyright 2021 Ettus Research, a National Instruments Brand
#
# SPDX-License-Identifier: LGPL-3.0-or-later
#

include $(TOOLS_DIR)/make/viv_ip_builder.mak

LIB_IP_FLOATCOMPARELESSTHAN_SRCS = $(IP_BUILD_DIR)/FloatCompareLessThan/FloatCompareLessThan.xci

LIB_IP_FLOATCOMPARELESSTHAN_OUTS = $(addprefix $(IP_BUILD_DIR)/FloatCompareLessThan/, \
FloatCompareLessThan.xci.out \
synth/FloatCompareLessThan.vhd \
)

.INTERMEDIATE: LIB_IP_FLOATCOMPARELESSTHAN_TRGT
$(LIB_IP_FLOATCOMPARELESSTHAN_SRCS) $(LIB_IP_FLOATCOMPARELESSTHAN_OUTS): LIB_IP_FLOATCOMPARELESSTHAN_TRGT
	@:

LIB_IP_FLOATCOMPARELESSTHAN_TRGT: $(OOT_FPGA_DIR)/ip/FloatCompareLessThan/FloatCompareLessThan.xci
	$(call BUILD_VIVADO_IP,FloatCompareLessThan,$(ARCH),$(PART_ID),$(OOT_FPGA_DIR)/ip,$(IP_BUILD_DIR),0)
