#
# Copyright 2021 Ettus Research, a National Instruments Brand
#
# SPDX-License-Identifier: LGPL-3.0-or-later
#

include $(TOOLS_DIR)/make/viv_ip_builder.mak

LIB_IP_FLOATTOFIXED_S16_16_SRCS = $(IP_BUILD_DIR)/FloatToFixed_S16_16/FloatToFixed_S16_16.xci

LIB_IP_FLOATTOFIXED_S16_16_OUTS = $(addprefix $(IP_BUILD_DIR)/FloatToFixed_S16_16/, \
FloatToFixed_S16_16.xci.out \
synth/FloatToFixed_S16_16.vhd \
)

.INTERMEDIATE: LIB_IP_FLOATTOFIXED_S16_16_TRGT
$(LIB_IP_FLOATTOFIXED_S16_16_SRCS) $(LIB_IP_FLOATTOFIXED_S16_16_OUTS): LIB_IP_FLOATTOFIXED_S16_16_TRGT
	@:

LIB_IP_FLOATTOFIXED_S16_16_TRGT: $(OOT_FPGA_DIR)/ip/FloatToFixed_S16_16/FloatToFixed_S16_16.xci
	$(call BUILD_VIVADO_IP,FloatToFixed_S16_16,$(ARCH),$(PART_ID),$(OOT_FPGA_DIR)/ip,$(IP_BUILD_DIR),0)
