--------------------------------------------------------------------------------
-- Copyright 2023
-- GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
-- Planckstr. 1, 64291 Darmstadt
-- Authors: Rene Geissler, r.geissler@gsi.de
--          Philipp Niedermayer, p.niedermayer@gsi.de
--------------------------------------------------------------------------------
-- VHDL standard: VHDL-2008
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--
-- A module counting pulses by pattern matching
--
entity pulse_counter is
    port (
        clk                 : in  std_logic;
        srst                : in  std_logic;
        count_window        : in  std_logic_vector(31 downto 0);
        -- signals from/to upstream
        pulse_in         : in  std_logic;
        pulse_in_valid   : in  std_logic;
        pulse_in_last    : in  std_logic;
        pulse_in_ready   : out std_logic;
        -- signals to/from downstream
        num_pulses       : out std_logic_vector(15 downto 0);
        num_pulses_valid : out std_logic;
        num_pulses_last  : out std_logic;
        num_pulses_ready : in  std_logic
    );
end pulse_counter;

architecture behavioral of pulse_counter is

    signal pulse_in_d1          : std_logic;
    signal pulse_in_d2          : std_logic;
    signal sample_counter       : unsigned(31 downto 0);
    signal sample_counter_limit : unsigned(31 downto 0);
    signal sample_counter_last  : std_logic;
    signal pulse_counter        : unsigned(15 downto 0);
    signal last_pulse_counter   : unsigned(15 downto 0);
    signal num_pulses_valid_i   : std_logic;
    signal pulse_in_ready_i     : std_logic;
    signal num_pulses_busy      : std_logic;

begin

    num_pulses_valid <= num_pulses_valid_i;
    -- we are busy when we are about to finish the current window, but still have the
    -- valid number from the previous window and downstream is still not ready to accept it
    num_pulses_busy  <= sample_counter_last and num_pulses_valid_i and not num_pulses_ready;
    pulse_in_ready_i <= (not srst) and not num_pulses_busy;
    pulse_in_ready   <= pulse_in_ready_i;

    p_count : process(clk)
    begin
        if rising_edge(clk) then
            if srst = '1' then
                -- reset
                pulse_in_d1          <= '0';
                pulse_in_d2          <= '0';
                sample_counter       <= to_unsigned(1, 32);
                sample_counter_limit <= unsigned(count_window);
                sample_counter_last  <= '0';
                pulse_counter        <= (others => '0');
                last_pulse_counter   <= (others => '0');
                num_pulses           <= (others => '0');
                num_pulses_valid_i   <= '0';
                num_pulses_last      <= '0';
            else
            
                -- reset valid after downstream acknowledged
                if num_pulses_ready = '1' then
                    num_pulses_valid_i <= '0';
                end if;
                
                -- wait for valid input signals
                if pulse_in_valid = '1' and pulse_in_ready_i = '1' then   
                             
                    -- delayed signals for rising edge detection
                    pulse_in_d1 <= pulse_in;
                    pulse_in_d2 <= pulse_in_d1;
                    
                    -- count window handling
                    if sample_counter >= sample_counter_limit then
                        num_pulses           <= std_logic_vector(pulse_counter - last_pulse_counter);
                        last_pulse_counter   <= pulse_counter;
                        num_pulses_valid_i   <= '1';
                        num_pulses_last      <= pulse_in_last;
                        sample_counter       <= to_unsigned(1, 32);
                        sample_counter_limit <= unsigned(count_window);
                        if unsigned(count_window) <= 1 then
                            sample_counter_last <= '1';
                        else
                            sample_counter_last <= '0';
                        end if;
                      
                    else
                        sample_counter <= sample_counter + 1;
                        if sample_counter + 1 >= sample_counter_limit then
                            sample_counter_last <= '1';
                        else
                            sample_counter_last <= '0';
                        end if;
                    end if;
                    
                    -- pulse counting
                    -- detect rising edge with at least two clock cycles high time
                    if pulse_in = '1' and pulse_in_d1 = '1' and pulse_in_d2 = '0' then
                        pulse_counter <= pulse_counter + 1;
                    end if;
                end if;
                
            end if;
            
        end if;
    end process;

end behavioral;
