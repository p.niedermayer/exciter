#
# Copyright 2021 Ettus Research, a National Instruments Brand
#
# SPDX-License-Identifier: LGPL-3.0-or-later
#

include $(TOOLS_DIR)/make/viv_ip_builder.mak

LIB_IP_FLOATMULTIPLIER_SRCS = $(IP_BUILD_DIR)/FloatMultiplier/FloatMultiplier.xci

LIB_IP_FLOATMULTIPLIER_OUTS = $(addprefix $(IP_BUILD_DIR)/FloatMultiplier/, \
FloatMultiplier.xci.out \
synth/FloatMultiplier.vhd \
)

.INTERMEDIATE: LIB_IP_FLOATMULTIPLIER_TRGT
$(LIB_IP_FLOATMULTIPLIER_SRCS) $(LIB_IP_FLOATMULTIPLIER_OUTS): LIB_IP_FLOATMULTIPLIER_TRGT
	@:

LIB_IP_FLOATMULTIPLIER_TRGT: $(OOT_FPGA_DIR)/ip/FloatMultiplier/FloatMultiplier.xci
	$(call BUILD_VIVADO_IP,FloatMultiplier,$(ARCH),$(PART_ID),$(OOT_FPGA_DIR)/ip,$(IP_BUILD_DIR),0)
