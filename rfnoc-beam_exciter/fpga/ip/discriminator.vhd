--------------------------------------------------------------------------------
-- Copyright 2023
-- GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
-- Planckstr. 1, 64291 Darmstadt
-- Authors: Rene Geissler, r.geissler@gsi.de
--          Philipp Niedermayer, p.niedermayer@gsi.de
--------------------------------------------------------------------------------
-- VHDL standard: VHDL-2008
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--
-- A two level discriminator
--
entity discriminator is
    port (
        clk             : in  std_logic;
        srst            : in  std_logic;
        lower_threshold : in  std_logic_vector(15 downto 0);
        upper_threshold : in  std_logic_vector(15 downto 0);
        -- signals from/to upstream
        data_in         : in  std_logic_vector(15 downto 0);
        data_in_valid   : in  std_logic;
        data_in_last    : in  std_logic;
        data_in_ready   : out std_logic;
        -- signals to/from downstream
        pulse_out       : out std_logic;
        pulse_out_valid : out std_logic;
        pulse_out_last  : out std_logic;
        pulse_out_ready : in  std_logic
    );
end discriminator;

architecture behavioral of discriminator is

    signal pulse_out_valid_i : std_logic;

begin

    pulse_out_valid <= pulse_out_valid_i;
    data_in_ready   <= (not srst) and ((not pulse_out_valid_i) or pulse_out_ready);

    p_discriminate : process(clk)
    begin
        if rising_edge(clk) then
            if srst = '1' then
                -- reset
                pulse_out         <= '0';
                pulse_out_valid_i <= '0';
            else
                -- process
                if data_in_valid = '1' and (pulse_out_ready = '1' or pulse_out_valid_i = '0') then
                    if signed(data_in) < signed(lower_threshold) then
                        pulse_out <= '0';
                    elsif signed(data_in) > signed(upper_threshold) then
                        pulse_out <= '1';
                    end if;
                    pulse_out_last    <= data_in_last;
                    pulse_out_valid_i <= '1';
                elsif pulse_out_ready = '1' then
                    pulse_out_valid_i <= '0';
                end if;
            end if;
        end if;
    end process;

end behavioral;
