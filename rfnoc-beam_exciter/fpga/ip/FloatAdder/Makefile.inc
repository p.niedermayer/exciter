#
# Copyright 2021 Ettus Research, a National Instruments Brand
#
# SPDX-License-Identifier: LGPL-3.0-or-later
#

include $(TOOLS_DIR)/make/viv_ip_builder.mak

LIB_IP_FLOATADDER_SRCS = $(IP_BUILD_DIR)/FloatAdder/FloatAdder.xci

LIB_IP_FLOATADDER_OUTS = $(addprefix $(IP_BUILD_DIR)/FloatAdder/, \
FloatAdder.xci.out \
synth/FloatAdder.vhd \
)

.INTERMEDIATE: LIB_IP_FLOATADDER_TRGT
$(LIB_IP_FLOATADDER_SRCS) $(LIB_IP_FLOATADDER_OUTS): LIB_IP_FLOATADDER_TRGT
	@:

LIB_IP_FLOATADDER_TRGT: $(OOT_FPGA_DIR)/ip/FloatAdder/FloatAdder.xci
	$(call BUILD_VIVADO_IP,FloatAdder,$(ARCH),$(PART_ID),$(OOT_FPGA_DIR)/ip,$(IP_BUILD_DIR),0)
