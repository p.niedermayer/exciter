#
# Copyright 2021 Ettus Research, a National Instruments Brand
#
# SPDX-License-Identifier: LGPL-3.0-or-later
#

include $(TOOLS_DIR)/make/viv_ip_builder.mak



LIB_IP_FIXEDTOFLOAT_U32_0_SRCS = $(IP_BUILD_DIR)/FixedToFloat_U32_0/FixedToFloat_U32_0.xci

LIB_IP_FIXEDTOFLOAT_U32_0_OUTS = $(addprefix $(IP_BUILD_DIR)/FixedToFloat_U32_0/, \
FixedToFloat_U32_0.xci.out \
synth/FixedToFloat_U32_0.vhd \
)

.INTERMEDIATE: LIB_IP_FIXEDTOFLOAT_U32_0_TRGT
$(LIB_IP_FIXEDTOFLOAT_U32_0_SRCS) $(LIB_IP_FIXEDTOFLOAT_U32_0_OUTS): LIB_IP_FIXEDTOFLOAT_U32_0_TRGT
	@:

LIB_IP_FIXEDTOFLOAT_U32_0_TRGT: $(OOT_FPGA_DIR)/ip/FixedToFloat_U32_0/FixedToFloat_U32_0.xci
	$(call BUILD_VIVADO_IP,FixedToFloat_U32_0,$(ARCH),$(PART_ID),$(OOT_FPGA_DIR)/ip,$(IP_BUILD_DIR),0)
