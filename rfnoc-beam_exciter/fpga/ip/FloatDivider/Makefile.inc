#
# Copyright 2021 Ettus Research, a National Instruments Brand
#
# SPDX-License-Identifier: LGPL-3.0-or-later
#

include $(TOOLS_DIR)/make/viv_ip_builder.mak

LIB_IP_FLOATDIVIDER_SRCS = $(IP_BUILD_DIR)/FloatDivider/FloatDivider.xci

LIB_IP_FLOATDIVIDER_OUTS = $(addprefix $(IP_BUILD_DIR)/FloatDivider/, \
FloatDivider.xci.out \
synth/FloatDivider.vhd \
)

.INTERMEDIATE: LIB_IP_FLOATDIVIDER_TRGT
$(LIB_IP_FLOATDIVIDER_SRCS) $(LIB_IP_FLOATDIVIDER_OUTS): LIB_IP_FLOATDIVIDER_TRGT
	@:

LIB_IP_FLOATDIVIDER_TRGT: $(OOT_FPGA_DIR)/ip/FloatDivider/FloatDivider.xci
	$(call BUILD_VIVADO_IP,FloatDivider,$(ARCH),$(PART_ID),$(OOT_FPGA_DIR)/ip,$(IP_BUILD_DIR),0)
