library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gpio_pulse_counter is
	port (
		clk                 : in  std_logic;
		srst                : in  std_logic;
		gpio_in             : in  std_logic;
		count_window        : in  std_logic_vector(31 downto 0);
		counter_value       : out std_logic_vector(15 downto 0);
		counter_value_valid : out std_logic
	);
end gpio_pulse_counter;

architecture behavioral of gpio_pulse_counter is

	signal gpio_in_d1                 : std_logic;
	signal gpio_in_d2                 : std_logic;
	signal gpio_in_d3                 : std_logic;
	signal gpio_in_d4                 : std_logic;
	signal clock_counter              : unsigned(31 downto 0);
	signal clock_counter_upper_limit  : unsigned(31 downto 0);
	signal pulse_counter              : unsigned(15 downto 0);
	signal last_pulse_counter         : unsigned(15 downto 0);
	signal num_pulses_in_window       : unsigned(15 downto 0);
	signal num_pulses_in_window_valid : std_logic;
	signal init_done                  : std_logic;

begin

	counter_value       <= std_logic_vector(num_pulses_in_window);
	counter_value_valid <= num_pulses_in_window_valid;

	p_count : process(clk)
	begin
		if rising_edge(clk) then
			-- input synchronization
			gpio_in_d1   <= gpio_in;
			gpio_in_d2   <= gpio_in_d1;
			-- delayed signals for rising edge detection
			gpio_in_d3   <= gpio_in_d2;
			gpio_in_d4   <= gpio_in_d3;
			if srst = '1' then
				clock_counter              <= (others => '0');
				clock_counter_upper_limit  <= (others => '0');
				pulse_counter              <= (others => '0');
				last_pulse_counter         <= (others => '0');
				num_pulses_in_window       <= (others => '0');
				num_pulses_in_window_valid <= '0';
				init_done                  <= '0';
			else
				init_done <= '1';
				-- count window handling
				if clock_counter = clock_counter_upper_limit then
					num_pulses_in_window <= pulse_counter - last_pulse_counter;
					last_pulse_counter   <= pulse_counter;
					if init_done = '1' then
						num_pulses_in_window_valid <= '1';
					end if;
					clock_counter <= (others => '0');
					if count_window = (count_window'high downto 0 => '0') then
						clock_counter_upper_limit <= (others => '0');
					else
						clock_counter_upper_limit <= unsigned(count_window) - 1;
					end if;
				else
					clock_counter              <= clock_counter + 1;
					num_pulses_in_window_valid <= '0';
				end if;
				-- pulse counting
				-- detect rising edge with at least two clock cycles high time
				if gpio_in_d2 = '1' and gpio_in_d3 = '1' and gpio_in_d4 = '0' then
					pulse_counter <= pulse_counter + 1;
				end if;
			end if;
		end if;
	end process;

end behavioral;
