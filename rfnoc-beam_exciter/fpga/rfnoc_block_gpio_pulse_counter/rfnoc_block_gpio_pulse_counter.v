//
// Copyright 2023 Ettus Research, a National Instruments Brand
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: LGPL-3.0-or-later
//
// Module: rfnoc_block_gpio_pulse_counter
//
// Description:
//
//   <Add block description here>
//
// Parameters:
//
//   THIS_PORTID : Control crossbar port to which this block is connected
//   CHDR_W      : AXIS-CHDR data bus width
//   MTU         : Maximum transmission unit (i.e., maximum packet size in
//                 CHDR words is 2**MTU).
//

`default_nettype none


module rfnoc_block_gpio_pulse_counter #(
  parameter [9:0] THIS_PORTID     = 10'd0,
  parameter       CHDR_W          = 64,
  parameter [5:0] MTU             = 10
)(
  // RFNoC Framework Clocks and Resets
  input  wire                   rfnoc_chdr_clk,
  input  wire                   rfnoc_ctrl_clk,
  input  wire                   radio_clk,
  // RFNoC Backend Interface
  input  wire [511:0]           rfnoc_core_config,
  output wire [511:0]           rfnoc_core_status,
  // AXIS-CHDR Input Ports (from framework)
  input  wire [(1)*CHDR_W-1:0] s_rfnoc_chdr_tdata,
  input  wire [(1)-1:0]        s_rfnoc_chdr_tlast,
  input  wire [(1)-1:0]        s_rfnoc_chdr_tvalid,
  output wire [(1)-1:0]        s_rfnoc_chdr_tready,
  // AXIS-CHDR Output Ports (to framework)
  output wire [(1)*CHDR_W-1:0] m_rfnoc_chdr_tdata,
  output wire [(1)-1:0]        m_rfnoc_chdr_tlast,
  output wire [(1)-1:0]        m_rfnoc_chdr_tvalid,
  input  wire [(1)-1:0]        m_rfnoc_chdr_tready,
  // AXIS-Ctrl Input Port (from framework)
  input  wire [31:0]            s_rfnoc_ctrl_tdata,
  input  wire                   s_rfnoc_ctrl_tlast,
  input  wire                   s_rfnoc_ctrl_tvalid,
  output wire                   s_rfnoc_ctrl_tready,
  // AXIS-Ctrl Output Port (to framework)
  output wire [31:0]            m_rfnoc_ctrl_tdata,
  output wire                   m_rfnoc_ctrl_tlast,
  output wire                   m_rfnoc_ctrl_tvalid,
  input  wire                   m_rfnoc_ctrl_tready,
  // GPIO pulse input
  input  wire                   gpio_bank
);

  //---------------------------------------------------------------------------
  // Signal Declarations
  //---------------------------------------------------------------------------

  // Clocks and Resets
  wire               ctrlport_clk;
  wire               ctrlport_rst;
  wire               axis_data_clk;
  wire               axis_data_rst;
  // CtrlPort Master
  wire               m_ctrlport_req_wr;
  wire               m_ctrlport_req_rd;
  wire [19:0]        m_ctrlport_req_addr;
  wire [31:0]        m_ctrlport_req_data;
  reg                m_ctrlport_resp_ack;
  reg [31:0]         m_ctrlport_resp_data;
  // Data Stream to User Logic: unused
  wire [32*1-1:0]    m_unused_axis_tdata;
  wire [1-1:0]       m_unused_axis_tkeep;
  wire               m_unused_axis_tlast;
  wire               m_unused_axis_tvalid;
  wire               m_unused_axis_tready;
  wire [63:0]        m_unused_axis_ttimestamp;
  wire               m_unused_axis_thas_time;
  wire [15:0]        m_unused_axis_tlength;
  wire               m_unused_axis_teov;
  wire               m_unused_axis_teob;
  // Data Stream from User Logic: out
  wire [32*1-1:0]    s_out_axis_tdata;
  wire [0:0]         s_out_axis_tkeep;
  wire               s_out_axis_tlast;
  wire               s_out_axis_tvalid;
  wire               s_out_axis_tready;
  wire [63:0]        s_out_axis_ttimestamp;
  wire               s_out_axis_thas_time;
  wire [15:0]        s_out_axis_tlength;
  wire               s_out_axis_teov;
  wire               s_out_axis_teob;
  // User Logic
  wire [15:0]        counter_value;
  wire               counter_value_valid;

  //---------------------------------------------------------------------------
  // NoC Shell
  //---------------------------------------------------------------------------

  noc_shell_gpio_pulse_counter #(
    .CHDR_W              (CHDR_W),
    .THIS_PORTID         (THIS_PORTID),
    .MTU                 (MTU)
  ) noc_shell_gpio_pulse_counter_i (
    //---------------------
    // Framework Interface
    //---------------------

    // Clock Inputs
    .rfnoc_chdr_clk      (rfnoc_chdr_clk),
    .rfnoc_ctrl_clk      (rfnoc_ctrl_clk),
    .radio_clk           (radio_clk),
    // Reset Outputs
    .rfnoc_chdr_rst      (),
    .rfnoc_ctrl_rst      (),
    .radio_rst           (),
    // RFNoC Backend Interface
    .rfnoc_core_config   (rfnoc_core_config),
    .rfnoc_core_status   (rfnoc_core_status),
    // CHDR Input Ports  (from framework)
    .s_rfnoc_chdr_tdata  (s_rfnoc_chdr_tdata),
    .s_rfnoc_chdr_tlast  (s_rfnoc_chdr_tlast),
    .s_rfnoc_chdr_tvalid (s_rfnoc_chdr_tvalid),
    .s_rfnoc_chdr_tready (s_rfnoc_chdr_tready),
    // CHDR Output Ports (to framework)
    .m_rfnoc_chdr_tdata  (m_rfnoc_chdr_tdata),
    .m_rfnoc_chdr_tlast  (m_rfnoc_chdr_tlast),
    .m_rfnoc_chdr_tvalid (m_rfnoc_chdr_tvalid),
    .m_rfnoc_chdr_tready (m_rfnoc_chdr_tready),
    // AXIS-Ctrl Input Port (from framework)
    .s_rfnoc_ctrl_tdata  (s_rfnoc_ctrl_tdata),
    .s_rfnoc_ctrl_tlast  (s_rfnoc_ctrl_tlast),
    .s_rfnoc_ctrl_tvalid (s_rfnoc_ctrl_tvalid),
    .s_rfnoc_ctrl_tready (s_rfnoc_ctrl_tready),
    // AXIS-Ctrl Output Port (to framework)
    .m_rfnoc_ctrl_tdata  (m_rfnoc_ctrl_tdata),
    .m_rfnoc_ctrl_tlast  (m_rfnoc_ctrl_tlast),
    .m_rfnoc_ctrl_tvalid (m_rfnoc_ctrl_tvalid),
    .m_rfnoc_ctrl_tready (m_rfnoc_ctrl_tready),

    //---------------------
    // Client Interface
    //---------------------

    // CtrlPort Clock and Reset
    .ctrlport_clk              (ctrlport_clk),
    .ctrlport_rst              (ctrlport_rst),
    // CtrlPort Master
    .m_ctrlport_req_wr         (m_ctrlport_req_wr),
    .m_ctrlport_req_rd         (m_ctrlport_req_rd),
    .m_ctrlport_req_addr       (m_ctrlport_req_addr),
    .m_ctrlport_req_data       (m_ctrlport_req_data),
    .m_ctrlport_resp_ack       (m_ctrlport_resp_ack),
    .m_ctrlport_resp_data      (m_ctrlport_resp_data),

    // AXI-Stream Clock and Reset
    .axis_data_clk (axis_data_clk),
    .axis_data_rst (axis_data_rst),
    // Data Stream to User Logic: unused
    .m_unused_axis_tdata      (m_unused_axis_tdata),
    .m_unused_axis_tkeep      (m_unused_axis_tkeep),
    .m_unused_axis_tlast      (m_unused_axis_tlast),
    .m_unused_axis_tvalid     (m_unused_axis_tvalid),
    .m_unused_axis_tready     (m_unused_axis_tready),
    .m_unused_axis_ttimestamp (m_unused_axis_ttimestamp),
    .m_unused_axis_thas_time  (m_unused_axis_thas_time),
    .m_unused_axis_tlength    (m_unused_axis_tlength),
    .m_unused_axis_teov       (m_unused_axis_teov),
    .m_unused_axis_teob       (m_unused_axis_teob),
    // Data Stream from User Logic: out
    .s_out_axis_tdata      (s_out_axis_tdata),
    .s_out_axis_tkeep      (s_out_axis_tkeep),
    .s_out_axis_tlast      (s_out_axis_tlast),
    .s_out_axis_tvalid     (s_out_axis_tvalid),
    .s_out_axis_tready     (s_out_axis_tready),
    .s_out_axis_ttimestamp (s_out_axis_ttimestamp),
    .s_out_axis_thas_time  (s_out_axis_thas_time),
    .s_out_axis_tlength    (s_out_axis_tlength),
    .s_out_axis_teov       (s_out_axis_teov),
    .s_out_axis_teob       (s_out_axis_teob)
  );

  //---------------------------------------------------------------------------
  // User Logic
  //---------------------------------------------------------------------------
  //
  // The code above this point is essentially unmodified from what was
  // generated by the tool. The code below implements the gpio_pulse_counter.
  //
  // All registers are in the ctrlport_clk domain and the signal processing is
  // in the axis_data_clk domain. However, we specified in the block YAML
  // configuration file that we want both the control and data interfaces on
  // the rfnoc_chdr clock. So we don't need to worry about crossing the
  // register data from ctrlport_clk and axis_data_clk.
  //
  //---------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Registers
  //---------------------------------------------------------------------------
  //
  // There's only one register now, but we'll structure the register code to
  // make it easier to add more registers later.
  //
  //---------------------------------------------------------------------------

  localparam REG_DECIMATION_ADDR    = 0;    // Address for decimation value
  localparam REG_DECIMATION_DEFAULT = 20;   // Default decimation value

  reg [31:0] reg_decimation = REG_DECIMATION_DEFAULT;

  always @(posedge ctrlport_clk) begin
    if (ctrlport_rst) begin
      reg_decimation = REG_DECIMATION_DEFAULT;
    end else begin
      // Default assignment
      m_ctrlport_resp_ack <= 0;

      // Handle read requests
      if (m_ctrlport_req_rd) begin
        case (m_ctrlport_req_addr)
          REG_DECIMATION_ADDR: begin
            m_ctrlport_resp_ack  <= 1;
            m_ctrlport_resp_data <= reg_decimation;
          end
        endcase
      end

      // Handle write requests
      if (m_ctrlport_req_wr) begin
        case (m_ctrlport_req_addr)
          REG_DECIMATION_ADDR: begin
            m_ctrlport_resp_ack <= 1;
            reg_decimation      <= m_ctrlport_req_data[31:0];
          end
        endcase
      end
    end
  end

  //---------------------------------------------------------------------------
  // Signal Processing
  //---------------------------------------------------------------------------

  gpio_pulse_counter gpio_pulse_counter_i (
    .clk                 (axis_data_clk),
    .srst                (axis_data_rst),
    .gpio_in             (gpio_bank),
    .count_window        (reg_decimation),
    .counter_value       (counter_value),
    .counter_value_valid (counter_value_valid)
  );

  assign s_out_axis_tdata  = { 16'b0, counter_value };
  assign s_out_axis_tvalid = counter_value_valid;

  // Only 1-sample per clock, so tkeep should always be asserted
  assign s_out_axis_tkeep      =  1'b1;
  assign s_out_axis_tlast      =  1'b0;
  assign s_out_axis_ttimestamp = 64'b0;
  assign s_out_axis_thas_time  =  1'b0;
  assign s_out_axis_tlength    = 16'b0;
  assign s_out_axis_teov       =  1'b0;
  assign s_out_axis_teob       =  1'b0;

  assign m_unused_axis_tready = 1'b0;

endmodule // rfnoc_block_gpio_pulse_counter


`default_nettype wire
