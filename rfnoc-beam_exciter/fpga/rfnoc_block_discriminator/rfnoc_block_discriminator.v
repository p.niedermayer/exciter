//
// Copyright 2023 Ettus Research, a National Instruments Brand
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: LGPL-3.0-or-later
//
// Module: rfnoc_block_discriminator
//
// Description:
//
//   Discriminate input signal with two tresholds
//   - If input < threshold_low -> output = 0
//   - If input > threshold_high -> output = 1
//   - Else: output remains unchanged
//
// Parameters:
//
//   THIS_PORTID : Control crossbar port to which this block is connected
//   CHDR_W      : AXIS-CHDR data bus width
//   MTU         : Maximum transmission unit (i.e., maximum packet size in
//                 CHDR words is 2**MTU).
//

`default_nettype none


module rfnoc_block_discriminator #(
  parameter [9:0] THIS_PORTID     = 10'd0,
  parameter       CHDR_W          = 64,
  parameter [5:0] MTU             = 10
)(
  // RFNoC Framework Clocks and Resets
  input  wire                   rfnoc_chdr_clk,
  input  wire                   rfnoc_ctrl_clk,
  input  wire                   ce_clk,
  // RFNoC Backend Interface
  input  wire [511:0]           rfnoc_core_config,
  output wire [511:0]           rfnoc_core_status,
  // AXIS-CHDR Input Ports (from framework)
  input  wire [(1)*CHDR_W-1:0] s_rfnoc_chdr_tdata,
  input  wire [(1)-1:0]        s_rfnoc_chdr_tlast,
  input  wire [(1)-1:0]        s_rfnoc_chdr_tvalid,
  output wire [(1)-1:0]        s_rfnoc_chdr_tready,
  // AXIS-CHDR Output Ports (to framework)
  output wire [(1)*CHDR_W-1:0] m_rfnoc_chdr_tdata,
  output wire [(1)-1:0]        m_rfnoc_chdr_tlast,
  output wire [(1)-1:0]        m_rfnoc_chdr_tvalid,
  input  wire [(1)-1:0]        m_rfnoc_chdr_tready,
  // AXIS-Ctrl Input Port (from framework)
  input  wire [31:0]            s_rfnoc_ctrl_tdata,
  input  wire                   s_rfnoc_ctrl_tlast,
  input  wire                   s_rfnoc_ctrl_tvalid,
  output wire                   s_rfnoc_ctrl_tready,
  // AXIS-Ctrl Output Port (to framework)
  output wire [31:0]            m_rfnoc_ctrl_tdata,
  output wire                   m_rfnoc_ctrl_tlast,
  output wire                   m_rfnoc_ctrl_tvalid,
  input  wire                   m_rfnoc_ctrl_tready
);

  //---------------------------------------------------------------------------
  // Signal Declarations
  //---------------------------------------------------------------------------

  // Clocks and Resets
  wire               ctrlport_clk;
  wire               ctrlport_rst;
  wire               axis_data_clk;
  wire               axis_data_rst;
  // CtrlPort Master
  wire               m_ctrlport_req_wr;
  wire               m_ctrlport_req_rd;
  wire [19:0]        m_ctrlport_req_addr;
  wire [31:0]        m_ctrlport_req_data;
  reg                m_ctrlport_resp_ack;
  reg  [31:0]        m_ctrlport_resp_data;
  // Payload Stream to User Logic: in
  wire [32*1-1:0]    m_in_payload_tdata;
  wire [1-1:0]       m_in_payload_tkeep;
  wire               m_in_payload_tlast;
  wire               m_in_payload_tvalid;
  wire               m_in_payload_tready;
  // Context Stream to User Logic: in
  wire [CHDR_W-1:0]  m_in_context_tdata;
  wire [3:0]         m_in_context_tuser;
  wire               m_in_context_tlast;
  wire               m_in_context_tvalid;
  wire               m_in_context_tready;
  // Payload Stream from User Logic: out
  wire [32*1-1:0]    s_out_payload_tdata;
  wire [0:0]         s_out_payload_tkeep;
  wire               s_out_payload_tlast;
  wire               s_out_payload_tvalid;
  wire               s_out_payload_tready;
  // Context Stream from User Logic: out
  wire [CHDR_W-1:0]  s_out_context_tdata;
  wire [3:0]         s_out_context_tuser;
  wire               s_out_context_tlast;
  wire               s_out_context_tvalid;
  wire               s_out_context_tready;

  //---------------------------------------------------------------------------
  // NoC Shell
  //---------------------------------------------------------------------------

  noc_shell_discriminator #(
    .CHDR_W              (CHDR_W),
    .THIS_PORTID         (THIS_PORTID),
    .MTU                 (MTU)
  ) noc_shell_discriminator_i (
    //---------------------
    // Framework Interface
    //---------------------

    // Clock Inputs
    .rfnoc_chdr_clk      (rfnoc_chdr_clk),
    .rfnoc_ctrl_clk      (rfnoc_ctrl_clk),
    .ce_clk              (ce_clk),
    // Reset Outputs
    .rfnoc_chdr_rst      (),
    .rfnoc_ctrl_rst      (),
    .ce_rst              (),
    // RFNoC Backend Interface
    .rfnoc_core_config   (rfnoc_core_config),
    .rfnoc_core_status   (rfnoc_core_status),
    // CHDR Input Ports  (from framework)
    .s_rfnoc_chdr_tdata  (s_rfnoc_chdr_tdata),
    .s_rfnoc_chdr_tlast  (s_rfnoc_chdr_tlast),
    .s_rfnoc_chdr_tvalid (s_rfnoc_chdr_tvalid),
    .s_rfnoc_chdr_tready (s_rfnoc_chdr_tready),
    // CHDR Output Ports (to framework)
    .m_rfnoc_chdr_tdata  (m_rfnoc_chdr_tdata),
    .m_rfnoc_chdr_tlast  (m_rfnoc_chdr_tlast),
    .m_rfnoc_chdr_tvalid (m_rfnoc_chdr_tvalid),
    .m_rfnoc_chdr_tready (m_rfnoc_chdr_tready),
    // AXIS-Ctrl Input Port (from framework)
    .s_rfnoc_ctrl_tdata  (s_rfnoc_ctrl_tdata),
    .s_rfnoc_ctrl_tlast  (s_rfnoc_ctrl_tlast),
    .s_rfnoc_ctrl_tvalid (s_rfnoc_ctrl_tvalid),
    .s_rfnoc_ctrl_tready (s_rfnoc_ctrl_tready),
    // AXIS-Ctrl Output Port (to framework)
    .m_rfnoc_ctrl_tdata  (m_rfnoc_ctrl_tdata),
    .m_rfnoc_ctrl_tlast  (m_rfnoc_ctrl_tlast),
    .m_rfnoc_ctrl_tvalid (m_rfnoc_ctrl_tvalid),
    .m_rfnoc_ctrl_tready (m_rfnoc_ctrl_tready),

    //---------------------
    // Client Interface
    //---------------------

    // CtrlPort Clock and Reset
    .ctrlport_clk              (ctrlport_clk),
    .ctrlport_rst              (ctrlport_rst),
    // CtrlPort Master
    .m_ctrlport_req_wr         (m_ctrlport_req_wr),
    .m_ctrlport_req_rd         (m_ctrlport_req_rd),
    .m_ctrlport_req_addr       (m_ctrlport_req_addr),
    .m_ctrlport_req_data       (m_ctrlport_req_data),
    .m_ctrlport_resp_ack       (m_ctrlport_resp_ack),
    .m_ctrlport_resp_data      (m_ctrlport_resp_data),

    // AXI-Stream Payload Context Clock and Reset
    .axis_data_clk (axis_data_clk),
    .axis_data_rst (axis_data_rst),
    // Payload Stream to User Logic: in
    .m_in_payload_tdata  (m_in_payload_tdata),
    .m_in_payload_tkeep  (m_in_payload_tkeep),
    .m_in_payload_tlast  (m_in_payload_tlast),
    .m_in_payload_tvalid (m_in_payload_tvalid),
    .m_in_payload_tready (m_in_payload_tready),
    // Context Stream to User Logic: in
    .m_in_context_tdata  (m_in_context_tdata),
    .m_in_context_tuser  (m_in_context_tuser),
    .m_in_context_tlast  (m_in_context_tlast),
    .m_in_context_tvalid (m_in_context_tvalid),
    .m_in_context_tready (m_in_context_tready),
    // Payload Stream from User Logic: out
    .s_out_payload_tdata  (s_out_payload_tdata),
    .s_out_payload_tkeep  (s_out_payload_tkeep),
    .s_out_payload_tlast  (s_out_payload_tlast),
    .s_out_payload_tvalid (s_out_payload_tvalid),
    .s_out_payload_tready (s_out_payload_tready),
    // Context Stream from User Logic: out
    .s_out_context_tdata  (s_out_context_tdata),
    .s_out_context_tuser  (s_out_context_tuser),
    .s_out_context_tlast  (s_out_context_tlast),
    .s_out_context_tvalid (s_out_context_tvalid),
    .s_out_context_tready (s_out_context_tready)
  );

  //---------------------------------------------------------------------------
  // User Logic
  //---------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Registers
  //---------------------------------------------------------------------------

  localparam REG_ADDR_THRESHOLD_LOW = 0;
  localparam REG_ADDR_THRESHOLD_HIGH = 4;
  
  localparam REG_DEFAULT_THRESHOLD_LOW = -16383; // -0.5 V
  localparam REG_DEFAULT_THRESHOLD_HIGH = -9830; // -0.3 V

  reg [15:0] reg_threshold_low = REG_DEFAULT_THRESHOLD_LOW;
  reg [15:0] reg_threshold_high = REG_DEFAULT_THRESHOLD_HIGH;

  always @(posedge ctrlport_clk) begin
    if (ctrlport_rst) begin
	  reg_threshold_low = REG_DEFAULT_THRESHOLD_LOW;
	  reg_threshold_high = REG_DEFAULT_THRESHOLD_HIGH;
    end else begin
      // Default assignment
      m_ctrlport_resp_ack <= 0;

      // Handle read requests
      if (m_ctrlport_req_rd) begin
        case (m_ctrlport_req_addr)
          REG_ADDR_THRESHOLD_LOW: begin
            m_ctrlport_resp_ack <= 1;
            m_ctrlport_resp_data <= { 16'b0, reg_threshold_low };
          end
          REG_ADDR_THRESHOLD_HIGH: begin
            m_ctrlport_resp_ack <= 1;
            m_ctrlport_resp_data <= { 16'b0, reg_threshold_high };
          end
        endcase
      end

      // Handle write requests
      if (m_ctrlport_req_wr) begin
        case (m_ctrlport_req_addr)
          REG_ADDR_THRESHOLD_LOW: begin
            m_ctrlport_resp_ack <= 1;
            reg_threshold_low <= m_ctrlport_req_data[15:0];
          end
          REG_ADDR_THRESHOLD_HIGH: begin
            m_ctrlport_resp_ack <= 1;
            reg_threshold_high <= m_ctrlport_req_data[15:0];
          end
        endcase
      end
    end
  end
  
  //---------------------------------------------------------------------------
  // Context handling
  //---------------------------------------------------------------------------
   
  // The input packets are the same configuration as the output packets, so
  // just use the header information for each incoming to create the header for
  // each outgoing packet. This is done by connecting m_axis_context to
  // directly to s_axis_context.
  assign s_out_context_tdata  = m_in_context_tdata;
  assign s_out_context_tuser  = m_in_context_tuser;
  assign s_out_context_tlast  = m_in_context_tlast;
  assign s_out_context_tvalid = m_in_context_tvalid;
  assign m_in_context_tready = s_out_context_tready;

  //---------------------------------------------------------------------------
  // Signal Processing
  //---------------------------------------------------------------------------
  
  // Discriminate real (in-phase) part of signal
  discriminator discriminator_i (
    .clk(axis_data_clk),
    .srst(axis_data_rst),
    .lower_threshold(reg_threshold_low),
    .upper_threshold(reg_threshold_high),
    // from/to upstream (I/real in upper 16 bits)
    .data_in(m_in_payload_tdata[31:16]),
    .data_in_last(m_in_payload_tlast),
    .data_in_valid(m_in_payload_tvalid),
    .data_in_ready(m_in_payload_tready),
    // to/from downstream (using only one bit of I/real)
    .pulse_out(s_out_payload_tdata[16]),
    .pulse_out_last(s_out_payload_tlast),
    .pulse_out_valid(s_out_payload_tvalid),
    .pulse_out_ready(s_out_payload_tready)
  );
  assign s_out_payload_tdata[31:17] = 0;
  
  // Pass through imaginary (quadrature) part of signal
  localparam DELAY = 1; // to keep signals synchronised
  delay_line #(.WIDTH(16)) delay_i1 (
    .clk(axis_data_clk),
    .delay(DELAY-1), // delay line has 1 clock internal delay
    .din(m_in_payload_tdata[15:0]),
    .dout(s_out_payload_tdata[15:0])
  );
  
  // Only 1-sample per clock, so tkeep should always be asserted
  assign s_out_payload_tkeep = 1'b1;
  
  // Metadata signals
  //delay_line #(.WIDTH(6)) delay_i2 (
  //  .clk(axis_data_clk),
  //  .delay(DELAY-1), // delay line has 1 clock internal delay
  //  .din( {m_in_axis_tkeep,  m_in_axis_ttimestamp,  m_in_axis_thas_time,  m_in_axis_tlength,  m_in_axis_teov,  m_in_axis_teob }),
  //  .dout({s_out_axis_tkeep, s_out_axis_ttimestamp, s_out_axis_thas_time, s_out_axis_tlength, s_out_axis_teov, s_out_axis_teob})
  //);
  //assign s_out_axis_tkeep = m_in_axis_tkeep;
  // Timeing signals
  //assign s_out_axis_ttimestamp = m_in_axis_ttimestamp;
  //assign s_out_axis_thas_time  = m_in_axis_thas_time;
  //assign s_out_axis_tlength    = m_in_axis_tlength;    // not used since SIDEBAND_AT_END=True
  //assign s_out_axis_teov       = m_in_axis_teov;
  //assign s_out_axis_teob       = m_in_axis_teob;
  
  
  
endmodule // rfnoc_block_discriminator


`default_nettype wire
