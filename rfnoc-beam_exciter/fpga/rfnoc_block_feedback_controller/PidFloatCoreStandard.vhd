---------------------------------------------------------------------------------------------------
--! @brief  PID module using standard VHDL IEEE types and tfnoc interface
--!   
--! @author Blaz Kelbl, Cosylab (blaz.kelbl@cosylab.com)
--!
--! @date Jun 28 2023 created
--! @date Jun 28 2023 last modify
--!
--! @version v0.1
--!
--! @file PidFloatCoreStandard.vhd
---------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.CslStdRtlPkg.all;
use work.CslAxiPkg.all;

entity PidFloatCoreStandard is
   generic (

      CHDR_W : natural := 16
   );
   port (
      active_i              : in std_logic;
      ta_i                  : in std_logic_vector(15 downto 0);
      kp_i                  : in std_logic_vector(31 downto 0);
      kiTimesTa_i           : in std_logic_vector(31 downto 0);
      kdOverTa_i            : in std_logic_vector(31 downto 0);
      target_i              : in std_logic_vector(31 downto 0);
      feedback_i            : in std_logic;
      feedForward_i         : in std_logic;
      feedForwardConstant_i : in std_logic_vector(31 downto 0);
      outMin_i              : in std_logic_vector(31 downto 0);
      outMax_i              : in std_logic_vector(31 downto 0);
      outScale_i            : in std_logic_vector(31 downto 0);
      repeat_i              : in std_logic_vector( 7 downto 0);

      s_rfnoc_chdr_tdata_i  : in  std_logic_vector(15 downto 0);
      s_rfnoc_chdr_tlast_i  : in  std_logic;
      s_rfnoc_chdr_tvalid_i : in  std_logic;
      s_rfnoc_chdr_tready_o : out std_logic;
      s_rfnoc_chdr_tkeep_i  : in  std_logic;

      m_rfnoc_chdr_tdata_o  : out std_logic_vector(15 downto 0);
      m_rfnoc_chdr_tlast_o  : out std_logic;
      m_rfnoc_chdr_tvalid_o : out std_logic;
      m_rfnoc_chdr_tready_i : in  std_logic;
      m_rfnoc_chdr_tkeep_o  : out std_logic;

      clk_i : in std_logic; --! Global clock  
      rst_i : in std_logic  --! Global reset
   );
end PidFloatCoreStandard;


architecture structural of PidFloatCoreStandard is

   constant TPD_C             : time    := 1 ns; --! Simulation delta time offset
   constant DATA_WIDTH_C      : natural := 16;
   constant TA_WIDTH_C        : natural := 16;
   constant OUTPUT_WIDTH_C    : natural := 16;
   signal sampleInAxisMaster  : AxiStreamMasterType;
   signal sampleInAxisSlave   : AxiStreamSlaveType;
   signal sampleOutAxisMaster : AxiStreamMasterType;
   signal sampleOutAxisSlave  : AxiStreamSlaveType;


begin
   p_in : process
      (
         s_rfnoc_chdr_tdata_i ,
         s_rfnoc_chdr_tlast_i ,
         s_rfnoc_chdr_tvalid_i ,
         s_rfnoc_chdr_tkeep_i ,
         sampleInAxisSlave
      )
   begin
      sampleInAxisMaster                                   <= AXI_STREAM_MASTER_INIT_C;
      sampleInAxisMaster.tdata(s_rfnoc_chdr_tdata_i'range) <= s_rfnoc_chdr_tdata_i;
      sampleInAxisMaster.tlast                             <= s_rfnoc_chdr_tlast_i;
      sampleInAxisMaster.tvalid                            <= s_rfnoc_chdr_tvalid_i;
      s_rfnoc_chdr_tready_o                                <= sampleInAxisSlave.tready;
   end process;

   p_out : process
      (
         sampleOutAxisMaster,
         m_rfnoc_chdr_tready_i
      )
   begin
      if active_i = '1' then
         m_rfnoc_chdr_tdata_o <= sampleOutAxisMaster.tdata(m_rfnoc_chdr_tdata_o'range);
      else
         m_rfnoc_chdr_tdata_o <= (others => '0');
      end if;
      m_rfnoc_chdr_tlast_o      <= sampleOutAxisMaster.tlast;
      m_rfnoc_chdr_tvalid_o     <= sampleOutAxisMaster.tvalid;
      m_rfnoc_chdr_tkeep_o      <= '1';
      sampleOutAxisSlave.tready <= m_rfnoc_chdr_tready_i;
   end process;

   i_core : entity work.PidFloatCore
      generic map (
         TPD_G          => TPD_C,
         DATA_WIDTH_G   => DATA_WIDTH_C,
         TA_WIDTH_G     => TA_WIDTH_C,
         OUTPUT_WIDTH_G => OUTPUT_WIDTH_C
      )
      port map (
         active_i              => active_i,
         ta_i                  => ta_i,
         kp_i                  => kp_i,
         kiTimesTa_i           => kiTimesTa_i,
         kdOverTa_i            => kdOverTa_i,
         target_i              => target_i,
         feedback_i            => feedback_i,
         feedForward_i         => feedForward_i,
         feedForwardConstant_i => feedForwardConstant_i,
         outMin_i              => outMin_i,
         outMax_i              => outMax_i,
         outScale_i            => outScale_i,
         repeat_i              => repeat_i,
         sampleInAxisMaster_i  => sampleInAxisMaster,
         sampleInAxisSlave_o   => sampleInAxisSlave,
         sampleOutAxisMaster_o => sampleOutAxisMaster,
         sampleOutAxisSlave_i  => sampleOutAxisSlave,
         floatOut_o            => open,
         floatValid_o          => open,
         clk_i                 => clk_i,
         rst_i                 => rst_i
      );

end structural;