-------------------------------------------------------------------------------
-- File       : CslAxiPkg.vhd
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-05-01
-------------------------------------------------------------------------------
-- Description: Standard AXI Package File
------------------------------------------------------------------------------
-- This file is part of 'SLAC Firmware Standard Library'.
-- It is subject to the license terms in the LICENSE section found in this file 
-- and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'SLAC Firmware Standard Library', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the following section:
--
-- LICENSE:
--
-- Copyright (c) 2021, The Board of Trustees of the Leland Stanford Junior 
-- University, through SLAC National Accelerator Laboratory (subject to receipt 
-- of any required approvals from the U.S. Dept. of Energy). All rights reserved. 
-- Redistribution and use in source and binary forms, with or without 
-- modification, are permitted provided that the following conditions are met:
-- 
-- (1) Redistributions of source code must retain the above copyright notice, 
--     this list of conditions and the following disclaimer. 
--
-- (2) Redistributions in binary form must reproduce the above copyright notice, 
--     this list of conditions and the following disclaimer in the documentation 
--     and/or other materials provided with the distribution. 
--
-- (3) Neither the name of the Leland Stanford Junior University, SLAC National 
--     Accelerator Laboratory, U.S. Dept. of Energy nor the names of its 
--     contributors may be used to endorse or promote products derived from this 
--     software without specific prior written permission. 
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
-- DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER, THE UNITED STATES GOVERNMENT, 
-- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
-- EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
-- IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
-- OF SUCH DAMAGE.
--
-- You are under no obligation whatsoever to provide any bug fixes, patches, or 
-- upgrades to the features, functionality or performance of the source code 
-- ("Enhancements") to anyone; however, if you choose to make your Enhancements 
-- available either publicly, or directly to SLAC National Accelerator Laboratory, 
-- without imposing a separate written license agreement for such Enhancements, 
-- then you hereby grant the following license: a non-exclusive, royalty-free 
-- perpetual license to install, use, modify, prepare derivative works, incorporate
-- into other computer software, distribute, and sublicense such Enhancements or 
-- derivative works thereof, in binary and source code form.
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.CslStdRtlPkg.all;

package CslAxiPkg is

   constant AXI4L_AW_C : positive := 32;  -- address width
   constant AXI4L_DW_C : positive := 32;  -- data width

   constant AXI4_AW_C : positive :=   64;  -- address width
   constant AXI4_DW_C : positive := 1024;  -- data width
   constant AXI4_SW_C : positive :=  128;  -- strobe width
   constant AXI4_IW_C : positive :=   32;  -- identification width

-------------------------------------------------------------------------------------------------
-- address decoder configuration data types
-------------------------------------------------------------------------------------------------

   -- decoder address/mask pair for single slave
   type DecoderAddressMaskType is record
      addr  : std_logic_vector(AXI4L_AW_C-1 downto 0);  -- address
      mask  : std_logic_vector(AXI4L_AW_C-1 downto 0);  -- mask
   end record;

   -- array of decoder address/mask pair for slaves in a demultiplexer
   type DecoderAddressMaskArray is array (natural range <>) of DecoderAddressMaskType;

-------------------------------------------------------------------------------------------------
-- address decoder configuration data types
-------------------------------------------------------------------------------------------------

   -- array of arbiter priorities for masters in a multiplexer
   type ArbiterPriorityArray is array (natural range <>) of natural;

-------------------------------------------------------------------------------------------------
-- AXI4 bus response codes
-------------------------------------------------------------------------------------------------

   constant AXI_RESP_OK_C     : std_logic_vector(1 downto 0) := "00";  -- Access ok
   constant AXI_RESP_EXOKAY_C : std_logic_vector(1 downto 0) := "01";  -- Exclusive access ok
   constant AXI_RESP_SLVERR_C : std_logic_vector(1 downto 0) := "10";  -- Slave Error
   -- Note: There are no "exclusive access" in AXI4-Lite.  This is just a placeholder constant.
   -- Note: A SLVERR response is returned to the master if the AXI peripheral interface receives any
   --       of the following unsupported accesses:
   --
   --          1) Any accesses with AWSIZE information other than 32-bit receives a SLVERR response.
   --          2) Any accesses with AWLEN information other than zero receives a SLVERR response.
   --          3) Any access that is unaligned, for example, where AWADDRP[1:0] is not equal to 2'b00,
   --             returns a SLVERR response where a read access returns all zeros and a write access
   --             does not modify the address location.
   --          4) Any write access that attempts to make use of the WSTRB lines,
   --             for example where any bits of WSTRB[3:0] are 0, returns a SLVERR response
   --             and does not modify the address location.

   constant AXI_RESP_DECERR_C : std_logic_vector(1 downto 0) := "11";  -- Decode Error
   -- Note: Any transaction that does not decode to a legal master interface destination,
   --       or programmers view register, receives a DECERR response. For an AHB master,
   --       the AXI DECERR is mapped back to an AHB ERROR.

--------------------------------------------------------------------------------
-- AXI4-Lite definitions
--------------------------------------------------------------------------------

   -- AR channel
   type AxiLiteChARType is record
      araddr  : std_logic_vector(AXI4L_AW_C-1 downto 0);
      arprot  : std_logic_vector(         3-1 downto 0);
   end record;

   -- R channel
   type AxiLiteChRType is record
      rdata   : std_logic_vector(AXI4L_DW_C-1 downto 0);
      rresp   : std_logic_vector(         2-1 downto 0);
   end record;

   -- AW channel
   type AxiLiteChAWType is record
      awaddr  : std_logic_vector(AXI4L_AW_C-1 downto 0);
      awprot  : std_logic_vector(         3-1 downto 0);
   end record;

   -- AW channel
   type AxiLiteChWType is record
      wdata   : std_logic_vector(AXI4L_DW_C-1 downto 0);
      wstrb   : std_logic_vector(         4-1 downto 0);
   end record;

   -- R channel
   type AxiLiteChBType is record
      bresp   : std_logic_vector(         2-1 downto 0);
   end record;

   --------------------------------------------------------
   -- AXI4-Lite bus, read master signal record
   --------------------------------------------------------

   -- Base Record
   type AxiLiteReadMasterType is record
      -- Read Address channel
      araddr  : std_logic_vector(AXI4L_AW_C-1 downto 0);
      arprot  : std_logic_vector(         3-1 downto 0);
      arvalid : std_logic;
      -- Read data channel
      rready  : std_logic;
   end record;

   -- Record length
   constant AxiLiteReadMasterLength : natural := AXI4L_AW_C+3+1+1;

   -- from record to vector
   function AxiLiteReadMasterR2V (x : AxiLiteReadMasterType) return std_logic_vector;
   -- from vector to record
   function AxiLiteReadMasterV2R (x : std_logic_vector) return AxiLiteReadMasterType;

   -- Initialization constants
   constant AXI_LITE_READ_MASTER_INIT_C : AxiLiteReadMasterType := (
      araddr  => (others => '0'),
      arprot  => (others => '0'),
      arvalid => '0',
      rready  => '1'
   );

   -- Array
   type AxiLiteReadMasterArray is array (natural range <>) of AxiLiteReadMasterType;

   --------------------------------------------------------
   -- AXI4-Lite bus, read slave signal record
   --------------------------------------------------------

   -- Base Record
   type AxiLiteReadSlaveType is record
      -- Read Address channel
      arready : std_logic;
      -- Read data channel
      rdata   : std_logic_vector(AXI4L_DW_C-1 downto 0);
      rresp   : std_logic_vector(         2-1 downto 0);
      rvalid  : std_logic;
   end record;

   -- Record length
   constant AxiLiteReadSlaveLength : natural := 1+AXI4L_DW_C+2+1;

   -- from record to vector
   function AxiLiteReadSlaveR2V (x : AxiLiteReadSlaveType) return std_logic_vector;
   -- from vector to record
   function AxiLiteReadSlaveV2R (x : std_logic_vector) return AxiLiteReadSlaveType;

   -- Initialization constants
   constant AXI_LITE_READ_SLAVE_INIT_C : AxiLiteReadSlaveType := (
      arready => '0',
      rdata   => (others => '0'),
      rresp   => (others => '0'),
      rvalid  => '0'
   );

   -- Array
   type AxiLiteReadSlaveArray is array (natural range<>) of AxiLiteReadSlaveType;

   --------------------------------------------------------
   -- AXI4-Lite bus, write master signal record
   --------------------------------------------------------

   -- Base Record
   type AxiLiteWriteMasterType is record
      -- Write address channel
      awaddr  : std_logic_vector(AXI4L_AW_C-1 downto 0);
      awprot  : std_logic_vector(         3-1 downto 0);
      awvalid : std_logic;
      -- Write data channel
      wdata   : std_logic_vector(AXI4L_DW_C-1 downto 0);
      wstrb   : std_logic_vector(         4-1 downto 0);
      wvalid  : std_logic;
      -- Write ack channel
      bready  : std_logic;
   end record;

   -- Record length
   constant AxiLiteWriteMasterLength : natural := AXI4L_AW_C+3+1+AXI4L_DW_C+4+1+1;

   -- from record to vector
   function AxiLiteWriteMasterR2V (x : AxiLiteWriteMasterType) return std_logic_vector;
   -- from vector to record
   function AxiLiteWriteMasterV2R (x : std_logic_vector) return AxiLiteWriteMasterType;

   -- Initialization constants
   constant AXI_LITE_WRITE_MASTER_INIT_C : AxiLiteWriteMasterType := (
      awaddr  => (others => '0'),
      awprot  => (others => '0'),
      awvalid => '0',
      wdata   => (others => '0'),
      wstrb   => (others => '1'),
      wvalid  => '0',
      bready  => '1'
   );

   -- Array
   type AxiLiteWriteMasterArray is array (natural range<>) of AxiLiteWriteMasterType;

   --------------------------------------------------------
   -- AXI4-Lite bus, write slave signal record
   --------------------------------------------------------

   -- Base Record
   type AxiLiteWriteSlaveType is record
      -- Write address channel
      awready : std_logic;
      -- Write data channel
      wready  : std_logic;
      -- Write ack channel
      bresp   : std_logic_vector(         2-1 downto 0);
      bvalid  : std_logic;
   end record;

   -- Record length
   constant AxiLiteWriteSlaveLength : natural := 1+1+2+1;

   -- from record to vector
   function AxiLiteWriteSlaveR2V (x : AxiLiteWriteSlaveType) return std_logic_vector;
   -- from vector to record
   function AxiLiteWriteSlaveV2R (x : std_logic_vector) return AxiLiteWriteSlaveType;

   -- Initialization constants
   constant AXI_LITE_WRITE_SLAVE_INIT_C : AxiLiteWriteSlaveType := (
      awready => '0',
      wready  => '0',
      bresp   => (others => '0'),
      bvalid  => '0'
   );

   -- Array
   type AxiLiteWriteSlaveArray is array (natural range<>) of AxiLiteWriteSlaveType;

   ----------------------------------------------------------------------------------
   -- Constants for endpoint abstractions (migrated from legacy AxiLiteMasterPkg.vhd)
   ----------------------------------------------------------------------------------

   type AxiLiteReqType is record
      request : sl;
      rnw     : sl;
      address : slv(AXI4L_AW_C-1 downto 0);
      wrData  : slv(AXI4L_DW_C-1 downto 0);
   end record AxiLiteReqType;

   constant AXI_LITE_REQ_INIT_C : AxiLiteReqType := (
      request => '0',
      rnw     => '1',
      address => (others => '0'),
      wrData  => (others => '0')
   );

   type AxiLiteAckType is record
      done   : sl;
      resp   : slv(         2-1 downto 0);
      rdData : slv(AXI4L_DW_C-1 downto 0);
   end record AxiLiteAckType;

   constant AXI_LITE_ACK_INIT_C : AxiLiteAckType := (
      done   => '0',
      resp   => (others => '0'),
      rdData => (others => '0')
   );

--------------------------------------------------------------------------------
-- Axi4 defintions
--------------------------------------------------------------------------------

   -------------------------------------
   -- AXI4 bus, read master signal record
   -------------------------------------

   type AxiReadMasterType is record
      -- Read Address channel
      arvalid  : sl;                         -- Address valid
      araddr   : slv(AXI4_AW_C-1 downto 0);  -- Address
      arid     : slv(AXI4_IW_C-1 downto 0);  -- Address ID
      arlen    : slv(        8-1 downto 0);  -- Transfer count
      arsize   : slv(        3-1 downto 0);  -- Bytes per transfer
      arburst  : slv(        2-1 downto 0);  -- Burst Type
      arlock   : slv(        2-1 downto 0);  -- Lock control
      arprot   : slv(        3-1 downto 0);  -- Protection control
      arcache  : slv(        4-1 downto 0);  -- Cache control
      arqos    : slv(        4-1 downto 0);  -- QoS value
      arregion : slv(        4-1 downto 0);  -- Region identifier
      -- Read data channel
      rready   : sl;                         -- Master is ready for data
   end record;

   type AxiReadMasterArray is array (natural range<>) of AxiReadMasterType;

   constant AXI_READ_MASTER_INIT_C : AxiReadMasterType := (
      arvalid  =>            '0' ,
      araddr   => (others => '0'),
      arid     => (others => '0'),
      arlen    => (others => '0'),
      arsize   => (others => '0'),
      arburst  => (others => '0'),
      arlock   => (others => '0'),
      arprot   => (others => '0'),
      arcache  => (others => '0'),
      arqos    => (others => '0'),
      arregion => (others => '0'),
      rready   =>            '0'
   );

   ------------------------------------
   -- AXI bus, read slave signal record
   ------------------------------------
   type AxiReadSlaveType is record
      -- Read Address channel
      arready : sl;                         -- Slave is ready for address
      -- Read data channel
      rdata   : slv(AXI4_DW_C-1 downto 0);  -- Read data from slave
      rlast   : sl;                         -- Read data last strobe
      rvalid  : sl;                         -- Read data is valid
      rid     : slv(AXI4_IW_C-1 downto 0);  -- Read ID tag
      rresp   : slv(        2-1 downto 0);  -- Read data result
   end record;

   type AxiReadSlaveArray is array (natural range<>) of AxiReadSlaveType;

   constant AXI_READ_SLAVE_INIT_C : AxiReadSlaveType := (
      arready =>            '0' ,
      rdata   => (others => '0'),
      rlast   =>            '0' ,
      rvalid  =>            '0' ,
      rid     => (others => '0'),
      rresp   => (others => '0')
   );

   --------------------------------------
   -- AXI bus, write master signal record
   --------------------------------------
   type AxiWriteMasterType is record
      -- Write address channel
      awvalid  : sl;                         -- Address valid
      awaddr   : slv(AXI4_AW_C-1 downto 0);  -- Address
      awid     : slv(AXI4_IW_C-1 downto 0);  -- Address ID
      awlen    : slv(        8-1 downto 0);  -- Transfer count (burst length)
      awsize   : slv(        3-1 downto 0);  -- Bytes per transfer
      awburst  : slv(        2-1 downto 0);  -- Burst Type
      awlock   : slv(        2-1 downto 0);  -- Lock control
      awprot   : slv(        3-1 downto 0);  -- Protection control
      awcache  : slv(        4-1 downto 0);  -- Cache control
      awqos    : slv(        4-1 downto 0);  -- QoS value
      awregion : slv(        4-1 downto 0);  -- Region identifier
      -- Write data channel
      wdata    : slv(AXI4_DW_C-1 downto 0);  -- Write data
      wlast    : sl;                         -- Write data is last
      wvalid   : sl;                         -- Write data is valid
      wid      : slv(AXI4_IW_C-1 downto 0);  -- Write ID tag
      wstrb    : slv(AXI4_SW_C-1 downto 0);  -- Write enable strobes, 1 per byte
      -- Write ack channel
      bready   : sl;                         -- Write master is ready for status
   end record;

   type AxiWriteMasterArray is array (natural range<>) of AxiWriteMasterType;

   constant AXI_WRITE_MASTER_INIT_C : AxiWriteMasterType := (
      awvalid  => '0',
      awaddr   => (others => '0'),
      awid     => (others => '0'),
      awlen    => (others => '0'),
      awsize   => (others => '0'),
      awburst  => (others => '0'),
      awlock   => (others => '0'),
      awprot   => (others => '0'),
      awcache  => (others => '0'),
      awqos    => (others => '0'),
      awregion => (others => '0'),
      wdata    => (others => '0'),
      wlast    => '0',
      wvalid   => '0',
      wid      => (others => '0'),
      wstrb    => (others => '1'),
      bready   => '0'
   );

   -------------------------------------
   -- AXI bus, write slave signal record
   -------------------------------------
   type AxiWriteSlaveType is record
      -- Write address channel
      awready : sl;                         -- Write slave is ready for address
      -- Write data channel
      wready  : sl;                         -- Write slave is ready for data
      -- Write ack channel
      bresp   : slv(        2-1 downto 0);  -- Write access status
      bvalid  : sl;                         -- Write status valid
      bid     : slv(AXI4_IW_C-1 downto 0);  -- Channel ID
   end record;

   type AxiWriteSlaveArray is array (natural range<>) of AxiWriteSlaveType;

   constant AXI_WRITE_SLAVE_INIT_C : AxiWriteSlaveType := (
      awready => '0',
      wready  => '0',
      bresp   => (others => '0'),
      bvalid  => '0',
      bid     => (others => '0')
   );

--------------------------------------------------------------------------------
-- Axi Stream defintions
--------------------------------------------------------------------------------
   constant AXI_STREAM_MAX_TDATA_WIDTH_C : natural  := 1024;  -- Units of bits
   constant AXI_STREAM_MAX_TKEEP_WIDTH_C : natural  := AXI_STREAM_MAX_TDATA_WIDTH_C/8;  -- Units of bytes
   constant AXI_STREAM_MAX_TDEST_WIDTH_C : natural  := 8;
   constant AXI_STREAM_MAX_TID_WIDTH_C   : natural  := 8;
   constant AXI_STREAM_MAX_TUSER_WIDTH_C : natural  := AXI_STREAM_MAX_TDATA_WIDTH_C;

   constant AXI4S_DW_C : natural := 4*8;  -- TDATA width
   constant AXI4S_SW_C : natural := 4;    -- TSTRB width
   constant AXI4S_KW_C : natural := 4;    -- TKEEP width
   constant AXI4S_RW_C : natural := 8;    -- TDEST width (R - routing)
   constant AXI4S_IW_C : natural := 8;    -- TID   width
   constant AXI4S_UW_C : natural := 0;    -- TUSER width

   type AxiStreamMasterType is record
      tvalid : sl;
      tdata  : slv(AXI_STREAM_MAX_TDATA_WIDTH_C-1 downto 0);
      tstrb  : slv(AXI_STREAM_MAX_TKEEP_WIDTH_C-1 downto 0);
      tkeep  : slv(AXI_STREAM_MAX_TKEEP_WIDTH_C-1 downto 0);
      tlast  : sl;
      tdest  : slv(AXI_STREAM_MAX_TDEST_WIDTH_C-1 downto 0);
      tid    : slv(AXI_STREAM_MAX_TID_WIDTH_C  -1 downto 0);
      tuser  : slv(AXI_STREAM_MAX_TUSER_WIDTH_C-1 downto 0);
   end record AxiStreamMasterType;

   constant AXI_STREAM_MASTER_INIT_C : AxiStreamMasterType := (
      tvalid =>            '0' ,
      tdata  => (others => '0'),
      tstrb  => (others => '1'),
      tkeep  => (others => '1'),
      tlast  =>            '0' ,
      tdest  => (others => '0'),
      tid    => (others => '0'),
      tuser  => (others => '0')
   );

   -- Record length
   constant AxiStreamMasterLength : natural := 1 +
      AXI_STREAM_MAX_TDATA_WIDTH_C +
      AXI_STREAM_MAX_TKEEP_WIDTH_C * 2 + 1 +
      AXI_STREAM_MAX_TDEST_WIDTH_C +
      AXI_STREAM_MAX_TID_WIDTH_C +
      AXI_STREAM_MAX_TUSER_WIDTH_C;

   -- from record to vector
   function AxiStreamMasterR2V (x : AxiStreamMasterType) return std_logic_vector;
   -- from vector to record
   function AxiStreamMasterV2R (x : std_logic_vector) return AxiStreamMasterType;

   type AxiStreamMasterArray is array (natural range<>) of AxiStreamMasterType;

   type AxiStreamSlaveType is record
      tReady : std_logic;
   end record AxiStreamSlaveType;

   constant AXI_STREAM_SLAVE_INIT_C : AxiStreamSlaveType := (tReady => '0');

   constant AXI_STREAM_SLAVE_FORCE_C : AxiStreamSlaveType := (tReady => '1');

   type AxiStreamSlaveArray is array (natural range<>) of AxiStreamSlaveType;

   ------------------------
   -- AXI bus configuration
   ------------------------
   type AxiConfigType is record
      ADDR_WIDTH_C : positive range 12 to  64;
      DATA_BYTES_C : positive range  1 to 128;
      ID_BITS_C    : positive range  1 to  32;
      LEN_BITS_C   : natural  range  0 to   8;
   end record AxiConfigType;

   type AxiLiteStatusType is record
      writeEnable : sl;
      readEnable  : sl;
   end record AxiLiteStatusType;

   constant AXI_LITE_STATUS_INIT_C : AxiLiteStatusType := (
      writeEnable => '0',
      readEnable  => '0'
   );

   --------------------------------------------------------
   -- AXI bus, read/write endpoint record, RTH 1/27/2016
   --------------------------------------------------------
   type AxiLiteEndpointType is record
      axiReadMaster  : AxiLiteReadMasterType;
      axiReadSlave   : AxiLiteReadSlaveType;
      axiWriteMaster : AxiLiteWriteMasterType;
      axiWriteSlave  : AxiLiteWriteSlaveType;
      axiStatus      : AxiLiteStatusType;
   end record AxiLiteEndpointType;

   constant AXI_LITE_ENDPOINT_INIT_C : AxiLiteEndpointType := (
      axiReadMaster  => AXI_LITE_READ_MASTER_INIT_C,
      axiReadSlave   => AXI_LITE_READ_SLAVE_INIT_C,
      axiWriteMaster => AXI_LITE_WRITE_MASTER_INIT_C,
      axiWriteSlave  => AXI_LITE_WRITE_SLAVE_INIT_C,
      axiStatus      => AXI_LITE_STATUS_INIT_C
   );

   function axiConfig (
      constant ADDR_WIDTH_C : in positive range 12 to  64 := 32;
      constant DATA_BYTES_C : in positive range  1 to 128 :=  4;
      constant ID_BITS_C    : in positive range  1 to  32 := 12;
      constant LEN_BITS_C   : in natural  range  0 to   8 :=  4
   ) return AxiConfigType;

   constant AXI_CONFIG_INIT_C : AxiConfigType := axiConfig(
      ADDR_WIDTH_C => 32,
      DATA_BYTES_C => 4,
      ID_BITS_C    => 12,
      LEN_BITS_C   => 4
   );

   function getAxiLen (
      axiConfig  : AxiConfigType;
      burstBytes : integer range 1 to 4096 := 4096
   ) return slv;

   -------------------------------------------------------------------------------------------------
   -- Slave AXI Processing procedures
   -------------------------------------------------------------------------------------------------

   procedure axiSlaveWaitWriteTxn (
      signal   axiWriteMaster : in    AxiLiteWriteMasterType;
      variable axiWriteSlave  : inout AxiLiteWriteSlaveType;
      variable writeEnable    : inout sl
   );

   procedure axiSlaveWaitReadTxn (
      signal   axiReadMaster  : in    AxiLiteReadMasterType;
      variable axiReadSlave   : inout AxiLiteReadSlaveType;
      variable readEnable     : inout sl
   );

   procedure axiSlaveWaitTxn (
      signal   axiWriteMaster : in    AxiLiteWriteMasterType;
      signal   axiReadMaster  : in    AxiLiteReadMasterType;
      variable axiWriteSlave  : inout AxiLiteWriteSlaveType;
      variable axiReadSlave   : inout AxiLiteReadSlaveType;
      variable axiStatus      : inout AxiLiteStatusType
   );

   procedure axiSlaveWriteResponse (
      variable axiWriteSlave  : inout AxiLiteWriteSlaveType;
      axiResp                 : in    slv(2-1 downto 0) := AXI_RESP_OK_C
   );

   procedure axiSlaveReadResponse (
      variable axiReadSlave   : inout AxiLiteReadSlaveType;
      axiResp                 : in    slv(2-1 downto 0) := AXI_RESP_OK_C
   );

--------------------------------------------------------------------------------
-- Axis mux defintions
--------------------------------------------------------------------------------

   type AxisMuxConfigType is record
      AXIS_SIZE    : positive; --! Number of Axi Stream transactions on transfer (should be consistent with BYTES_SLV_ARR_G)
      PIPELINE     : natural;  --! output data delay in clock cycles                 
      BRAM         : boolean;  --! boolean for FIFO resources false => distributed LUT                 
      AWIDTH       : positive; --! fifo address width               
      THRESHOLD    : positive; --! threshold for pause signal                    
      TUSER_NORMAL : boolean;  --! tuser MODE. Normal => all user bits, Not normal => last user bits
      TID_WIDTH    : positive; --! width of TID vector
      TDEST_WIDTH  : positive; --! width of TDEST vector
      TUSER_WIDTH  : positive; --! width of TUSER vector per byte
      BYTES_SLV    : positive; --! nubmer of data bytes in slave ports (should be consistent with AXIS_SIZE_ARR_G)
      BYTES_MST    : positive; --! number of data bytes in master ports
      ECC_EN       : boolean;  --! encode data in fifo with ECC
   end record AxisMuxConfigType;
   
   constant AXIS_MUX_CONFIG_INIT_C : AxisMuxConfigType := (
      AXIS_SIZE     => 8,
      PIPELINE      => 1,
      BRAM          => false,
      AWIDTH        => 5,
      THRESHOLD     => 16,
      TUSER_NORMAL  => false,
      TID_WIDTH     => 1,
      TDEST_WIDTH   => 1,
      TUSER_WIDTH   => 1,
      BYTES_SLV     => 8,
      BYTES_MST     => 4,
      ECC_EN        => false
   );

   type AxisMuxConfigArrayType is array (natural range<>) of AxisMuxConfigType;

   type AxisMuxStatusType is record
      wordCnt         : slv(31 downto 0); --! word count in fifo
      almostFull      : sl;               --! fifo fullness exceeds THRESHOLD value
      eccSingleBitErr : sl;               --! ecc single bit error signal
      eccDoubleBitErr : sl;               --! ecc double bit error signal
   end record AxisMuxStatusType;

   constant AXIS_MUX_STATUS_INIT_C : AxisMuxStatusType := (
      wordCnt         => (others =>'0'),
      almostFull      => '0',
      eccSingleBitErr => '0',
      eccDoubleBitErr => '0'
   );

   type AxisMuxStatusArrayType is array (natural range<>) of AxisMuxStatusType;

end CslAxiPkg;

--------------------------------------------------------------------------------
-- ???
--------------------------------------------------------------------------------

package body CslAxiPkg is

   --------------------------------------------------------------------------------
   -- AXI4-Lite definitions
   --------------------------------------------------------------------------------

   function AxiLiteReadMasterR2V (x : AxiLiteReadMasterType) return std_logic_vector is
      variable t : std_logic_vector(AxiLiteReadMasterLength-1 downto 0);
   begin
      t := x.araddr
         & x.arprot
         & x.arvalid
         & x.rready;
      return t;
   end AxiLiteReadMasterR2V;

   function AxiLiteReadMasterV2R (x : std_logic_vector) return AxiLiteReadMasterType is
      variable t : AxiLiteReadMasterType;
   begin
      t.araddr  := x(x'right+AXI4L_AW_C+3+1+1-1 downto x'right+3+1+1);
      t.arprot  := x(x'right+           3+1+1-1 downto x'right+  1+1);
      t.arvalid := x(x'right+             1+1-1);
      t.rready  := x(x'right+               1-1);
      return t;
   end AxiLiteReadMasterV2R;

   function AxiLiteReadSlaveR2V (x : AxiLiteReadSlaveType) return std_logic_vector is
      variable t : std_logic_vector(AxiLiteReadSlaveLength-1 downto 0);
   begin
      t := x.arready
         & x.rdata
         & x.rresp
         & x.rvalid;
      return t;
   end AxiLiteReadSlaveR2V;

   function AxiLiteReadSlaveV2R (x : std_logic_vector) return AxiLiteReadSlaveType is
      variable t : AxiLiteReadSlaveType;
   begin
      t.arready := x(x'right+1+AXI4L_DW_C+2+1-1);
      t.rdata   := x(x'right+  AXI4L_DW_C+2+1-1 downto x'right+2+1);
      t.rresp   := x(x'right+             2+1-1 downto x'right+  1);
      t.rvalid  := x(x'right+               1-1);
      return t;
   end AxiLiteReadSlaveV2R;

   function AxiLiteWriteMasterR2V (x : AxiLiteWriteMasterType) return std_logic_vector is
      variable t : std_logic_vector(AxiLiteWriteMasterLength-1 downto 0);
   begin
      t := x.awaddr
         & x.awprot
         & x.awvalid
         & x.wdata
         & x.wstrb
         & x.wvalid
         & x.bready;
      return t;
   end AxiLiteWriteMasterR2V;

   function AxiLiteWriteMasterV2R (x : std_logic_vector) return AxiLiteWriteMasterType is
      variable t : AxiLiteWriteMasterType;
   begin
      t.awaddr  := x(x'right+AXI4L_AW_C+3+1+AXI4L_DW_C+4+1+1-1 downto x'right+3+1+AXI4L_DW_C+4+1+1);
      t.awprot  := x(x'right+           3+1+AXI4L_DW_C+4+1+1-1 downto x'right+  1+AXI4L_DW_C+4+1+1);
      t.awvalid := x(x'right+             1+AXI4L_DW_C+4+1+1-1);
      t.wdata   := x(x'right+               AXI4L_DW_C+4+1+1-1 downto x'right+               4+1+1);
      t.wstrb   := x(x'right+                          4+1+1-1 downto x'right+                 1+1);
      t.wvalid  := x(x'right+                            1+1-1);
      t.bready  := x(x'right+                              1-1);
      return t;
   end AxiLiteWriteMasterV2R;

   function AxiLiteWriteSlaveR2V (x : AxiLiteWriteSlaveType) return std_logic_vector is
      variable t : std_logic_vector(AxiLiteWriteSlaveLength-1 downto 0);
   begin
      t := x.awready
         & x.wready
         & x.bresp
         & x.bvalid;
      return t;
   end AxiLiteWriteSlaveR2V;

   function AxiLiteWriteSlaveV2R (x : std_logic_vector) return AxiLiteWriteSlaveType is
      variable t : AxiLiteWriteSlaveType;
   begin
      t.awready := x(x'right+1+1+2+1-1);
      t.wready  := x(x'right+  1+2+1-1);
      t.bresp   := x(x'right+    2+1-1 downto x'right+1);
      t.bvalid  := x(x'right+      1-1);
      return t;
   end AxiLiteWriteSlaveV2R;

   --------------------------------------------------------------------------------
   -- AXI4-Stream definitions
   --------------------------------------------------------------------------------

   function AxiStreamMasterR2V (x : AxiStreamMasterType) return std_logic_vector is
      constant DW : natural := x.tdata'length;
      constant SW : natural := x.tstrb'length;
      constant KW : natural := x.tkeep'length;
      constant RW : natural := x.tdest'length;
      constant IW : natural := x.tid  'length;
      constant UW : natural := x.tuser'length;
      variable t : std_logic_vector(1+DW+SW+KW+1+RW+IW+UW-1 downto 0);
   begin
      t(1+DW+SW+KW+1+RW+IW+UW-1                        ) := x.tvalid               ;
      t(  DW+SW+KW+1+RW+IW+UW-1 downto SW+KW+1+RW+IW+UW) := x.tdata (DW-1 downto 0);
      t(     SW+KW+1+RW+IW+UW-1 downto    KW+1+RW+IW+UW) := x.tstrb (SW-1 downto 0);
      t(        KW+1+RW+IW+UW-1 downto       1+RW+IW+UW) := x.tkeep (KW-1 downto 0);
      t(           1+RW+IW+UW-1                        ) := x.tlast                ;
      t(             RW+IW+UW-1 downto            IW+UW) := x.tdest (RW-1 downto 0);
      t(                IW+UW-1 downto               UW) := x.tid   (IW-1 downto 0);
      t(                   UW-1 downto                0) := x.tuser (UW-1 downto 0);
      return t;
   end AxiStreamMasterR2V;

   function AxiStreamMasterV2R (x : std_logic_vector) return AxiStreamMasterType is
      variable t : AxiStreamMasterType;
      constant DW : natural := t.tdata'length;
      constant SW : natural := t.tstrb'length;
      constant KW : natural := t.tkeep'length;
      constant RW : natural := t.tdest'length;
      constant IW : natural := t.tid  'length;
      constant UW : natural := t.tuser'length;
   begin
      t.tvalid                := x(x'right+1+DW+SW+KW+1+RW+IW+UW-1                                );
      t.tdata (DW-1 downto 0) := x(x'right+  DW+SW+KW+1+RW+IW+UW-1 downto x'right+SW+KW+1+RW+IW+UW);
      t.tstrb (SW-1 downto 0) := x(x'right+     SW+KW+1+RW+IW+UW-1 downto x'right+   KW+1+RW+IW+UW);
      t.tkeep (KW-1 downto 0) := x(x'right+        KW+1+RW+IW+UW-1 downto x'right+      1+RW+IW+UW);
      t.tlast                 := x(x'right+           1+RW+IW+UW-1                                );
      t.tdest (RW-1 downto 0) := x(x'right+             RW+IW+UW-1 downto x'right+           IW+UW);
      t.tid   (IW-1 downto 0) := x(x'right+                IW+UW-1 downto x'right+              UW);
      t.tuser (UW-1 downto 0) := x(x'right+                   UW-1 downto x'right                 );
      return t;
   end AxiStreamMasterV2R;

   --------------------------------------------------------------------------------
   -- AXI4 definitions
   --------------------------------------------------------------------------------

   function axiConfig (
      constant ADDR_WIDTH_C : in positive range 12 to  64 := 32;
      constant DATA_BYTES_C : in positive range  1 to 128 :=  4;
      constant ID_BITS_C    : in positive range  1 to  32 := 12;
      constant LEN_BITS_C   : in natural  range  0 to   8 :=  4
   ) return AxiConfigType is
      variable ret : AxiConfigType;
   begin
      ret := (
         ADDR_WIDTH_C => ADDR_WIDTH_C,
         DATA_BYTES_C => DATA_BYTES_C,
         ID_BITS_C    => ID_BITS_C,
         LEN_BITS_C   => LEN_BITS_C);
      return ret;
   end function axiConfig;

   function getAxiLen (
      axiConfig  : AxiConfigType;
      burstBytes : integer range 1 to 4096 := 4096)
      return slv is
   begin
      -- burstBytes / data bytes width is number of txns required.
      -- Subtract by 1 for A*LEN value for even divides.
      -- Convert to SLV and truncate to size of A*LEN port for this AXI bus
      -- This limits number of txns approraiately based on size of len port
      -- Then resize to 8 bits because our records define A*LEN as 8 bits always.
      return resize(toSlv(wordCount(burstBytes,axiConfig.DATA_BYTES_C)-1, axiConfig.LEN_BITS_C), 8);
   end function getAxiLen;

   procedure axiSlaveWaitWriteTxn (
      signal axiWriteMaster  : in    AxiLiteWriteMasterType;
      variable axiWriteSlave : inout AxiLiteWriteSlaveType;
      variable writeEnable   : inout sl) is
   begin
      ----------------------------------------------------------------------------------------------
      -- AXI Write Logic
      ----------------------------------------------------------------------------------------------
      writeEnable := '0';

      axiWriteSlave.awready := '0';
      axiWriteSlave.wready  := '0';

      -- Incomming Write txn and last txn has concluded
      if (axiWriteMaster.awvalid = '1' and axiWriteMaster.wvalid = '1' and axiWriteSlave.bvalid = '0') then
         writeEnable := '1';
      end if;

      -- Reset resp valid
      if (axiWriteMaster.bready = '1') then
         axiWriteSlave.bvalid := '0';
      end if;
   end procedure;

   procedure axiSlaveWaitReadTxn (
      signal axiReadMaster  : in    AxiLiteReadMasterType;
      variable axiReadSlave : inout AxiLiteReadSlaveType;
      variable readEnable   : inout sl) is
   begin
      ----------------------------------------------------------------------------------------------
      -- AXI Read Logic
      ----------------------------------------------------------------------------------------------
      readEnable := '0';

      axiReadSlave.arready := '0';

      -- Incomming read txn and last txn has concluded
      if (axiReadMaster.arvalid = '1' and axiReadSlave.rvalid = '0') then
         readEnable := '1';
      end if;

      -- Reset rvalid upon rready
      if (axiReadMaster.rready = '1') then
         axiReadSlave.rvalid := '0';
         axiReadSlave.rdata  := (others => '0');
      end if;
   end procedure axiSlaveWaitReadTxn;

   procedure axiSlaveWaitTxn (
      signal axiWriteMaster  : in    AxiLiteWriteMasterType;
      signal axiReadMaster   : in    AxiLiteReadMasterType;
      variable axiWriteSlave : inout AxiLiteWriteSlaveType;
      variable axiReadSlave  : inout AxiLiteReadSlaveType;
      variable axiStatus     : inout AxiLiteStatusType) is
   begin
      axiSlaveWaitWriteTxn(axiWriteMaster, axiWriteSlave, axiStatus.writeEnable);
      axiSlaveWaitReadTxn(axiReadMaster, axiReadSlave, axiStatus.readEnable);
   end procedure;

   procedure axiSlaveWriteResponse (
      variable axiWriteSlave : inout AxiLiteWriteSlaveType;
      axiResp                : in    slv(2-1 downto 0) := AXI_RESP_OK_C
   ) is
   begin
      axiWriteSlave.awready := '1';
      axiWriteSlave.wready  := '1';
      axiWriteSlave.bvalid  := '1';
      axiWriteSlave.bresp   := axiResp;
   end procedure;

   procedure axiSlaveReadResponse (
      variable axiReadSlave : inout AxiLiteReadSlaveType;
      axiResp               : in    slv(2-1 downto 0) := AXI_RESP_OK_C
   ) is
   begin
      axiReadSlave.arready := '1';      -- not sure this is necessary
      axiReadSlave.rvalid  := '1';
      axiReadSlave.rresp   := axiResp;
   end procedure;

end package body CslAxiPkg;
