---------------------------------------------------------------------------------------------------
--! @brief  Averaging of the input samples
--! @details 
--!   Sums up the input @p param ta samples, then divides the samples by the number of samples. 
--!   Creates backpressure on the input AXI stream. Division with floating point.
--!   
--! @author Blaz Kelbl, Cosylab (blaz.kelbl@cosylab.com)
--!
--! @date Jun 23 2023 created
--! @date Jun 26 2023 last modify
--!
--! @version v0.2
--!
--! @file SampleAveraging.vhd
---------------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.CslStdRtlPkg.all;
use work.CslAxiPkg.all;
---------------------------------------------------------------------------------------------------
entity SampleAveragingFloat is
   generic (
      TPD_G        : time    := 1 ns; --! Simulation delta time offset
      DATA_WIDTH_G : natural := 16;
      TA_WIDTH_G   : natural := 16
   );
   port (
      ta_i                  : in  slv(TA_WIDTH_G - 1 downto 0);
      sampleInAxisMaster_i  : in  AxiStreamMasterType;
      sampleInAxisSlave_o   : out AxiStreamSlaveType;
      sampleOutAxisMaster_o : out AxiStreamMasterType;
      sampleOutAxisSlave_i  : in  AxiStreamSlaveType;
      clk_i                 : in  sl; --! Global clock  
      rst_i                 : in  sl  --! Global reset
   );
end SampleAveragingFloat;

architecture float of SampleAveragingFloat is

   COMPONENT FixedToFloat_U32_0
      PORT (
         aclk                 : IN  STD_LOGIC;
         s_axis_a_tvalid      : IN  STD_LOGIC;
         s_axis_a_tready      : OUT STD_LOGIC;
         s_axis_a_tdata       : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
         m_axis_result_tvalid : OUT STD_LOGIC;
         m_axis_result_tready : IN  STD_LOGIC;
         m_axis_result_tdata  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
   END COMPONENT;
   COMPONENT FloatDivider
      PORT (
         aclk                 : IN  STD_LOGIC;
         s_axis_a_tvalid      : IN  STD_LOGIC;
         s_axis_a_tready      : OUT STD_LOGIC;
         s_axis_a_tdata       : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
         s_axis_b_tvalid      : IN  STD_LOGIC;
         s_axis_b_tready      : OUT STD_LOGIC;
         s_axis_b_tdata       : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
         m_axis_result_tvalid : OUT STD_LOGIC;
         m_axis_result_tready : IN  STD_LOGIC;
         m_axis_result_tdata  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
   END COMPONENT;


   constant SUM_WIDTH_C : natural := DATA_WIDTH_G + TA_WIDTH_G;

   type StateType is (
         IDLE_S,
         SUMMING_S,
         CONVERT_SUM_S,
         CONVERT_COUNT_S,
         WAIT_FOR_SUM_S,
         WAIT_FOR_COUNT_S,
         REQUEST_DIVISION_S
      );

   type RegType is record
      state : StateType;
      sum   : slv(31 downto 0);
      count : slv(31 downto 0);
   end record RegType;

   constant REG_INIT_C : RegType := (
         state => IDLE_S,
         sum   => (others => '0'),
         count => (others => '0')
      );

   signal r, rin : RegType;

   type AxiSType32 is record
      valid : sl;
      data  : slv(31 downto 0);
   end record;

   signal fixedToFloat             : AxiSType32;
   signal fixedToFloat_ready       : sl;
   signal fixedToFloatResult       : AxiSType32;
   signal fixedToFloatResult_ready : sl;

   signal dividerA            : AxiSType32;
   signal dividerA_ready      : sl;
   signal dividerB            : AxiSType32;
   signal dividerB_ready      : sl;
   signal dividerResult       : AxiSType32;
   signal dividerResult_ready : sl;

begin

   fixedToFloatResult_ready <= '1';
   dividerB.valid           <= dividerA.valid;

   comb : process(
         r,
         
         fixedToFloat_ready,
         fixedToFloatResult,
         dividerA_ready,
         dividerB_ready,
         ta_i,
         sampleInAxisMaster_i
      )
      variable v : RegType;
   begin
      v := r;

      sampleInAxisSlave_o.tReady <= '0';

      fixedToFloat.valid <= '0';
      dividerA.valid     <= '0';

      fixedToFloat.data <= (others => 'U');
      dividerA.data     <= (others => 'U');
      dividerB.data     <= (others => 'U');

      case r.state is
         when IDLE_S =>
            v.state := SUMMING_S;
            v.sum   := (others => '0');
            v.count := (others => '0');

         when SUMMING_S =>
            sampleInAxisSlave_o.tReady <= '1';
            if sampleInAxisMaster_i.tValid = '1' then
               v.sum := slv(
                     unsigned(v.sum)
                     + resize(unsigned(sampleInAxisMaster_i.tdata(DATA_WIDTH_G - 1 downto 0)), r.sum'length));
               v.count := slv(unsigned(r.count) + 1);
               if unsigned(v.count(ta_i'range)) >= unsigned(ta_i) then
                  v.state := CONVERT_SUM_S;
               end if;
            end if;

         when CONVERT_SUM_S =>
            fixedToFloat.data  <= r.sum;
            fixedToFloat.valid <= '1';
            if fixedToFloat_ready = '1' then
               v.state := CONVERT_COUNT_S;
            end if;
         when CONVERT_COUNT_S =>
            fixedToFloat.data  <= r.count;
            fixedToFloat.valid <= '1';
            if fixedToFloat_ready = '1' then
               v.state := WAIT_FOR_SUM_S;
            end if;
         when WAIT_FOR_SUM_S =>
            if fixedToFloatResult.valid = '1' then
               v.sum   := fixedToFloatResult.data;
               v.state := WAIT_FOR_COUNT_S;
            end if;
         when WAIT_FOR_COUNT_S =>
            if fixedToFloatResult.valid = '1' then
               v.state := REQUEST_DIVISION_S;
               v.count := fixedToFloatResult.data;
            end if;
         when REQUEST_DIVISION_S =>
            dividerA.valid <= '1';
            dividerA.data  <= r.sum;
            dividerB.data  <= r.count;

            if dividerA_ready = '1' then
               v.state := IDLE_S;

            end if;
         when others =>
            v.state := IDLE_S;
            v.sum   := (others => '0');
            v.count := (others => '0');
      end case;

      -- Reset
      if (rst_i = '1') then
         v := REG_INIT_C;
      end if;
      rin <= v;

   end process comb;

   --! @brief Sequential process
   --! @details Assign rin to r on rising edge of clk to create registers
   seq : process (clk_i) is
   begin
      if (rising_edge(clk_i)) then
         r <= rin after TPD_G;
      end if;
   end process seq;

   -- divider is directly at the output stage
   dividerResult_ready <= sampleOutAxisSlave_i.tReady;
   p_sampleOut : process(dividerResult)
   begin
      sampleOutAxisMaster_o                                 <= AXI_STREAM_MASTER_INIT_C;
      sampleOutAxisMaster_o.tValid                          <= dividerResult.valid;
      sampleOutAxisMaster_o.tData(dividerResult.data'range) <= dividerResult.data;
   end process;

   i_fixedToFloat : FixedToFloat_U32_0
      PORT MAP (
         aclk                 => clk_i,
         s_axis_a_tvalid      => fixedToFloat.valid,
         s_axis_a_tready      => fixedToFloat_ready,
         s_axis_a_tdata       => fixedToFloat.data,
         m_axis_result_tvalid => fixedToFloatResult.valid,
         m_axis_result_tready => fixedToFloatResult_ready,
         m_axis_result_tdata  => fixedToFloatResult.data
      );

   i_multiplier : FloatDivider
      PORT MAP (
         aclk                 => clk_i,
         s_axis_a_tvalid      => dividerA.valid,
         s_axis_a_tready      => dividerA_ready,
         s_axis_a_tdata       => dividerA.data,
         s_axis_b_tvalid      => dividerB.valid,
         s_axis_b_tready      => dividerB_ready,
         s_axis_b_tdata       => dividerB.data,
         m_axis_result_tvalid => dividerResult.valid,
         m_axis_result_tready => dividerResult_ready,
         m_axis_result_tdata  => dividerResult.data
      );

end float;


