----------------------------------------------------------------------------------------------------
--! @brief Standard CSL Testbench Package full of usefullness
--! @details When using this package, VHDL2008 standard must be included.
--!
--! @author Klemen Erjavec, Anze Jakos, Domen Cvenkel, Iztok Jeras
--!
--! @date 23/12/19
--!
--! @version v1.0
--!
--! @file CslTbBasePkg.vhd
--!
--! Copyright (c) 2020 Cosylab d.d.
--! This software is distributed under the terms found
--! in file LICENSE.txt that is included with this distribution.
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use std.env.all;
use std.textio.all;

use work.CslStdRtlPkg.all;
use work.CslPrintfPkg.all;

package CslTbBasePkg is

   -------------------------------------------------------------------------------------------------
   -- Clock & Reset
   -------------------------------------------------------------------------------------------------

   -- Generate simple clock
   procedure tbClock (
      signal   clk : out sl;  -- clock output
      constant per : time     -- default clock frequency is 100MHz
   );

   -- Generate reset pulse of desired length
   procedure tbReset (
      signal   clk : in  sl;             -- clock input
      signal   rst : out sl;             -- reset output
      constant pol : in  sl := '1';      -- reset polarity
      constant num : in  natural := 1;   -- number clock periods for reset to be active
      constant tpd : in  time := 0 ns    -- driver delay
   );

   -------------------------------------------------------------------------------------------------
   -- Clocking
   -------------------------------------------------------------------------------------------------

   -- Wait for a number of rising edges and TPD
   procedure tbClkPeriod (
      signal   clk : in  sl;             -- clock
      constant num : in  natural := 1;   -- number clock periods to wait
      constant tpd : in  time := 0 ns    -- final delay
   );

   -------------------------------------------------------------------------------------------------
   -- Value check
   -------------------------------------------------------------------------------------------------

   --  check sl value against sl reference
   procedure tbCheck (
      signal   val : in  sl;               -- observed value
      constant ref : in  sl;               -- reference value
      constant str : in  string := "";     -- error string
      constant dbg : in  boolean := false  -- optional debug
   );

   --  check slv value against slv reference
   procedure tbCheck (
      signal   val : in  slv;              -- observed value
      constant ref : in  slv;              -- reference value
      constant str : in  string := "";     -- error string
      constant dbg : in  boolean := false  -- optional debug
   );

   --  check integer value against integer reference
   procedure tbCheck (
      signal   val : in  integer;          -- observed value
      constant ref : in  integer;          -- reference value
      constant str : in  string := "";     -- error string
      constant dbg : in  boolean := false  -- optional debug
   );

   --  check slv value against natural reference
   procedure tbCheck (
      signal   val : in  slv;              -- observed value
      constant ref : in  natural;          -- reference value
      constant str : in  string := "";     -- error string
      constant dbg : in  boolean := false  -- optional debug
   );

   --  check sl value against integer reference
   procedure tbCheck (
      signal   val : in  sl;                   -- observed value
      constant ref : in  integer range 0 to 1; -- reference value
      constant str : in  string := "";         -- error string
      constant dbg : in  boolean := false      -- optional debug
   );

   -------------------------------------------------------------------------------------------------
   -- Others
   -------------------------------------------------------------------------------------------------
   -- Waits until val = ref or until timeout is reached. In case of timeout error is reported.
   procedure waitEqual(
      signal   val     : in slv;
      constant ref     : in slv;
      constant timeout : in time;
      constant str     : in string := ""
   );

   -- Waits until val = ref or until timeout is reached. In case of timeout error is reported.
   procedure waitEqual(
      signal   val     : in sl;
      constant ref     : in sl;
      constant timeout : in time;
      constant str     : in string := ""
   );

   -------------------------------------------------------------------------------------------------
   -- Global error counter and final error report
   -------------------------------------------------------------------------------------------------

   -- protected data type for error counter
   type errorCntProtected is protected
      impure function get return natural;
      procedure       set (val : natural := 0);
      procedure       inc (val : natural := 1);
      procedure       dec (val : natural := 1);
   end protected errorCntProtected;

   -- shared (global) error counter variable
   shared variable tbErrorCnt_v : errorCntProtected;

   -- procedure for finalizing a testbench
   procedure tbReportErrorCnt;

end CslTbBasePkg;

package body CslTbBasePkg is

   -------------------------------------------------------------------------------------------------
   -- Clock & Reset
   -------------------------------------------------------------------------------------------------

   -- Generate clock
   procedure tbClock (
      signal   clk : out sl;  -- clock
      constant per : time     -- there is no defult clock frequency
   ) is
   begin
      loop
         clk <= '1';  wait for per/2;
         clk <= '0';  wait for per/2;
      end loop;
   end tbClock;
   
   -- Generate reset pulse of desired length (number of clock periods)
   procedure tbReset (
      signal   clk : in  sl;
      signal   rst : out sl;
      constant pol : in  sl := '1';
      constant num : in  natural := 1;
      constant tpd : in  time := 0 ns
   ) is
   begin
      rst <=     pol after tpd;  tbClkPeriod(clk, num);
      rst <= not pol after tpd;
   end tbReset;

   -------------------------------------------------------------------------------------------------
   -- Clocking
   -------------------------------------------------------------------------------------------------

   -- Wait for a number of rising edges and TPD
   procedure tbClkPeriod(
      signal   clk : in  sl;
      constant num : in  natural := 1;
      constant tpd : in  time := 0 ns
   ) is
   begin
      for i in 0 to num-1 loop
         wait until rising_edge(clk);
      end loop;
      if tpd > 0 ns then
         wait for tpd;
      end if;
   end tbClkPeriod;

   -------------------------------------------------------------------------------------------------
   -- Value check
   -------------------------------------------------------------------------------------------------

   --  check sl value against sl reference
   procedure tbCheck(
      signal   val : in  sl;               -- observed value
      constant ref : in  sl;               -- reference value
      constant str : in  string := "";     -- error string
      constant dbg : in  boolean := false  -- optional debug
   ) is
      variable slvVal : slv(0 downto 0);
      variable slvRef : slv(0 downto 0);
   begin
      -- map bits to vectors
      slvVal(0) := val;
      slvRef(0) := ref;
      -- check and report error
      if (val /= ref) then
         -- report error
         printf(":: ERR: tbCheck(sl): " & str & " \@");
         printf("   ref: %h \@", slvRef);
         printf("   val: %h \@", slvVal);
         -- increment error counter
         tbErrorCnt_v.inc;
      elsif dbg then
         -- print debug message
         printf(":: DBG: tbCheck(sl): " & str & " \@");
         printf("   val: %h \@", slvVal);
      end if;
   end tbCheck;

   --  check slv value against slv reference
   procedure tbCheck(
      signal   val : in  slv;              -- observed value
      constant ref : in  slv;              -- reference value
      constant str : in  string := "";     -- error string
      constant dbg : in  boolean := false  -- optional debug
   ) is
   begin
      if (val /= ref) then
         -- report error
         printf(":: ERR: tbCheck(slv): " & str & " \@");
         printf("   ref: %h \@", ref);
         printf("   val: %h \@", val);
         -- increment error counter
         tbErrorCnt_v.inc;
      elsif dbg then
         -- print debug message
         printf(":: DBG: tbCheck(slv): " & str & " \@");
         printf("   val: %h \@", val);
      end if;
   end tbCheck;

   --  check integer value against integer reference
   procedure tbCheck(
      signal   val : in  integer;          -- observed value
      constant ref : in  integer;          -- reference value
      constant str : in  string := "";     -- error string
      constant dbg : in  boolean := false  -- optional debug
   ) is
   begin
      if (val /= ref) then
         -- report error
         printf(":: ERR: tbCheck(slv): " & str & " \@");
         printf("   ref: %h \@", toSlv(ref, 32));
         printf("   val: %h \@", toSlv(val, 32));
         -- increment error counter
         tbErrorCnt_v.inc;
      elsif dbg then
         -- print debug message
         printf(":: DBG: tbCheck(slv): " & str & " \@");
         printf("   val: %h \@", toSlv(val, 32));
      end if;
   end tbCheck;

   --  check slv value against natural reference
   procedure tbCheck (
      signal   val : in  slv;              -- observed value
      constant ref : in  natural;          -- reference value
      constant str : in  string := "";     -- error string
      constant dbg : in  boolean := false  -- optional debug
   ) is
   begin
      if (val /= toSlv(ref, val'length)) then
         -- report error
         printf(":: ERR: tbCheck(slv): " & str & " \@");
         printf("   ref: %h \@", toSlv(ref, val'length));
         printf("   val: %h \@", val);
         -- increment error counter
         tbErrorCnt_v.inc;
      elsif dbg then
         -- print debug message
         printf(":: DBG: tbCheck(slv): " & str & " \@");
         printf("   val: %h \@", val);
      end if;
   end tbCheck;

   --  check sl value against integer reference
   procedure tbCheck(
      signal   val : in  sl;                   -- observed value
      constant ref : in  integer range 0 to 1; -- reference value
      constant str : in  string := "";         -- error string
      constant dbg : in  boolean := false      -- optional debug
   ) is
      variable slvVal : slv(0 downto 0);
      variable slvRef : slv(0 downto 0);
   begin
      -- map bits to vectors
      slvVal(0) := val;

      if (ref = 1) then
         slvRef(0) := '1';
      else
         slvRef(0) := '0';
      end if;

      -- check and report error
      if (slvVal /= slvRef) then
         -- report error
         printf(":: ERR: tbCheck(sl): " & str & " \@");
         printf("   ref: %h \@", slvRef);
         printf("   val: %h \@", slvVal);
         -- increment error counter
         tbErrorCnt_v.inc;
      elsif dbg then
         -- print debug message
         printf(":: DBG: tbCheck(sl): " & str & " \@");
         printf("   val: %h \@", slvVal);
      end if;
   end tbCheck;

   --------------------------------------------------------------------------------------------------
   -- Others
   --------------------------------------------------------------------------------------------------
   procedure waitEqual(
      signal   val     : in slv;
      constant ref     : in slv;
      constant timeout : in time;
      constant str     : in string := ""
   ) is
   begin

      if (val /= ref) then
         wait until val = ref for timeout;
         tbCheck(val, ref, "waitEqual timeout: " & str);
      end if;

   end waitEqual;

   procedure waitEqual(
      signal   val     : in sl;
      constant ref     : in sl;
      constant timeout : in time;
      constant str     : in string := ""
   ) is
   begin

      if (val /= ref) then
         wait until val = ref for timeout;
         tbCheck(val, ref, "waitEqual timeout: " & str);
      end if;

   end waitEqual;
   -------------------------------------------------------------------------------------------------
   -- Global error counter and error report
   -------------------------------------------------------------------------------------------------

   -- protected data type for error counter
   type errorCntProtected is protected body
      variable cnt : natural := 0;
      -- setter
      impure function get return natural is
      begin
         return cnt;
      end function get;
      -- setter
      procedure set (val : natural := 0) is
      begin
         cnt := val;
      end procedure set;
      -- increment
      procedure inc (val : natural := 1) is
      begin
         cnt := cnt + val;
      end procedure inc;
      -- decrement
      procedure dec (val : natural := 1) is
      begin
         cnt := cnt - val;
      end procedure dec;
   end protected body errorCntProtected;

   -- procedure for finalizing a testbench
   procedure tbReportErrorCnt is
   begin
      if tbErrorCnt_v.get > 0 then
         report "FAILURE";
      else
         report "SUCCESS";
      end if;
   end tbReportErrorCnt;

end package body CslTbBasePkg;