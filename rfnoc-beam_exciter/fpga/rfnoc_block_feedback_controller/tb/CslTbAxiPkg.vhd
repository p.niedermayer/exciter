----------------------------------------------------------------------------------------------------
--! @brief Standard CSL Testbench Package full of usefullness
--! @details When using this package, VHDL2008 standard must be included.
--!
--! @author Klemen Erjavec, Anze Jakos, Domen Cvenkel, Iztok Jeras
--!
--! @date 23/12/19
--!
--! @version v1.0
--!
--! @file CslTbAxiPkg.vhd
--!
--! Copyright (c) 2020 Cosylab d.d.
--! This software is distributed under the terms found
--! in file LICENSE.txt that is included with this distribution.
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use std.env.all;
use std.textio.all;

use work.CslPrintfPkg.all;
use work.CslStdRtlPkg.all;
use work.CslAxiPkg.all;
use work.CslTbBasePkg.all;

package CslTbAxiPkg is

   -------------------------------------------------------------------------------------------------
   -- AMBA4 AXI4-Lite
   -------------------------------------------------------------------------------------------------

   procedure tbAxilRead (
      signal   clk              : in  sl;
      signal   axilReadMaster_o : out AxiLiteReadMasterType;
      signal   axilReadSlave_i  : in  AxiLiteReadSlaveType;
      constant addr             : in  slv(32-1 downto 0);
      signal   data             : out slv(32-1 downto 0);
      constant tpd              : in  time := 1ns;
      constant rreadyfix        : in  boolean := false
   );

   procedure tbAxilReadToVar (
      signal   clk              : in  sl;
      signal   axilReadMaster_o : out AxiLiteReadMasterType;
      signal   axilReadSlave_i  : in  AxiLiteReadSlaveType;
      constant addr             : in  slv(32-1 downto 0);
      variable dataVar          : out slv(32-1 downto 0);
      constant tpd              : in  time := 1ns
   );

   procedure tbAxilWrite (
      signal   clk               : in  sl;
      signal   axilWriteMaster_o : out AxiLiteWriteMasterType;
      signal   axilWriteSlave_i  : in  AxiLiteWriteSlaveType;
      constant addr              : in  slv(32-1 downto 0);
      constant data              : in  slv(32-1 downto 0);
      constant tpd               : in  time := 1ns
   );

   -------------------------------------------------------------------------------------------------
   -- AMBA4 AXI4
   -------------------------------------------------------------------------------------------------

   procedure tbAxiRead32 (
      signal   clk             : in  sl;
      signal   axiReadMaster_o : out AxiReadMasterType;
      signal   axiReadSlave_i  : in  AxiReadSlaveType;
      constant addr            : in  slv(32-1 downto 0);
      signal   data            : out Slv32Array(0 to 255);
      constant len             : in  slv( 8-1 downto 0);
      constant tpd             : in  time := 1 ns
   );

   procedure tbAxiWrite32 (
      signal   clk              : in  sl;
      signal   axiWriteMaster_o : out AxiWriteMasterType;
      signal   axiWriteSlave_i  : in  AxiWriteSlaveType;
      constant addr             : in  slv(32-1 downto 0);
      constant data             : in  Slv32Array(0 to 255);
      constant len              : in  slv( 8-1 downto 0);
      constant tpd              : in  time := 1 ns
   );

   -- Function for checking the Axi write data. It acts as an Axi slave.
   -- refArray_i      : Data used to compare the Axi write data to.
   -- checkSelect_i   : Select with '1', which elements of array should be checked. By default all
   --                   elements are checked.
   -- tValidTimeout_i : Clock cycles to wait for Axi master to be valid before timeout.
   procedure tbAxiCheckWrData (
      signal clk_i             : in  sl; 
      signal axiWriteMaster_i  : in  AxiWriteMasterType; 
      signal axiWriteSlave_o   : out AxiWriteSlaveType;
      constant refArray_i      : in  Slv32Array;
      constant checkSelect_i   : in  slv := (others => '1');
      constant tValidTimeout_i : in  natural := 2147483647;
      constant tpd_i           : in  time := 0 ns
   ); 

   --  Checks the current state of axi signals against the reference value.
   procedure tbAxiStateCheck (
      signal   val : in  AxiLiteReadSlaveType; -- observed value
      constant ref : in  AxiLiteReadSlaveType; -- reference value
      constant str : in  string := "";         -- error string
      constant dbg : in  boolean := false      -- optional debug
   );

   --  Checks the current state of axi signals against the reference value.
   procedure tbAxiStateCheck (
      signal   val : in  AxiLiteWriteSlaveType; -- observed value
      constant ref : in  AxiLiteWriteSlaveType; -- reference value
      constant str : in  string := "";          -- error string
      constant dbg : in  boolean := false       -- optional debug
   );

   --  Checks the current state of axi signals against the reference value.
   procedure tbAxiStateCheck (
      signal   val : in  AxiLiteWriteMasterType; -- observed value
      constant ref : in  AxiLiteWriteMasterType; -- reference value
      constant str : in  string := "";          -- error string
      constant dbg : in  boolean := false       -- optional debug
   );

   --  Checks the current state of axi signals against the reference value.
   procedure tbAxiStateCheck (
      signal   val : in  AxiReadMasterType; -- observed value
      constant ref : in  AxiReadMasterType; -- reference value
      constant str : in  string := "";          -- error string
      constant dbg : in  boolean := false       -- optional debug
   );

   --  Checks the current state of axi signals against the reference value.
   procedure tbAxiStateCheck (
      signal   val : in  AxiStreamMasterType; -- observed value
      constant ref : in  AxiStreamMasterType; -- reference value
      constant str : in  string := "";          -- error string
      constant dbg : in  boolean := false       -- optional debug
   );

   --  Checks the current state of axi signals against the reference value.
   procedure tbAxiStateCheck (
      signal   val : in  AxiStreamSlaveType; -- observed value
      constant ref : in  AxiStreamSlaveType; -- reference value
      constant str : in  string := "";          -- error string
      constant dbg : in  boolean := false       -- optional debug
   );

   --  Checks the current state of axi signals against the reference value.
   procedure tbAxiStateCheck (
      signal   val : in  AxiWriteMasterType; -- observed value
      constant ref : in  AxiWriteMasterType; -- reference value
      constant str : in  string := "";          -- error string
      constant dbg : in  boolean := false       -- optional debug
   );

   --  Checks the current state of axi signals against the reference value.
   procedure tbAxiStateCheck (
      signal   val : in  AxiWriteMasterArray; -- observed value
      constant ref : in  AxiWriteMasterArray; -- reference value
      constant str : in  string := "";          -- error string
      constant dbg : in  boolean := false       -- optional debug
   );

   --  Checks the current state of axi signals against the reference value.
   procedure tbAxiStateCheck (
      signal   val : in  AxiStreamMasterArray; -- observed value
      constant ref : in  AxiStreamMasterArray; -- reference value
      constant str : in  string := "";          -- error string
      constant dbg : in  boolean := false       -- optional debug
   );

   --  Checks the current state of axi signals against the reference value.
   procedure tbAxiStateCheck (
      signal   val : in  AxiStreamSlaveArray; -- observed value
      constant ref : in  AxiStreamSlaveArray; -- reference value
      constant str : in  string := "";          -- error string
      constant dbg : in  boolean := false       -- optional debug
   );

   -------------------------------------------------------------------------------------------------
   -- Procedures for writing/reading to/from file to/from different interfaces
   -------------------------------------------------------------------------------------------------
   -- File to native AXIS
   -- File must have a following format: 
   -- data0
   -- data1
   -- data2
   -- type_i specifies format of data. "hex", "dec"
   -- dataWidth_i specifies tdata width.
   -- Empty lines in file are allowed and are ignored by the procedure
   -- WARNING: Function does not support tvalid. It is endless stream.
   -- WARNING: Max/min "dec" that can be read is 32 bit integer.
   procedure tbFile2Axis(
      signal clk_i         : in  sl;
      signal mAxisMaster_o : out AxiStreamMasterType;
      signal mAxisSlave_i  : in AxiStreamSlaveType;
      constant type_i      : string;
      constant dataWidth_i : in  positive;
      constant tpd_i       : in  time := 1 ns;
      constant filePath_i  : in  string;
      constant waitN        : in  positive := 1
      );

end CslTbAxiPkg;

package body CslTbAxiPkg is

   -------------------------------------------------------------------------------------------------
   -- AXI4-Lite functions
   -------------------------------------------------------------------------------------------------

   procedure tbAxilRead (
      signal   clk              : in  sl;
      signal   axilReadMaster_o : out AxiLiteReadMasterType;
      signal   axilReadSlave_i  : in  AxiLiteReadSlaveType;
      constant addr             : in  slv(32-1 downto 0);
      signal   data             : out slv(32-1 downto 0);
      constant tpd              : in  time := 1 ns;
      constant rreadyfix        : in  boolean := false
   ) is
   begin
      -- Choose if rready is up from start
      if rreadyfix then
         axilReadMaster_o.rready <= '1' after tpd;
      else
         axilReadMaster_o.rready <= '0' after tpd;
      end if;
      -- put the read address on the bus
      axilReadMaster_o.araddr(addr'range) <= addr after tpd;
      axilReadMaster_o.arprot             <= (others => '0') after tpd;
      -- wait for address ready
      axilReadMaster_o.arvalid            <= '1' after tpd;
      while (axilReadSlave_i.arready = '0') loop
         tbClkPeriod(clk);
      end loop;
      -- stop providing the address
      axilReadMaster_o.arvalid            <= '0' after tpd;
      -- start waiting for read data
      axilReadMaster_o.rready             <= '1' after tpd;
      while (axilReadSlave_i.rvalid = '0') loop
         tbClkPeriod(clk);
      end loop;
      -- read the data
      data <= axilReadSlave_i.rdata;
      -- stop waiting for data
      axilReadMaster_o.rready             <= '0' after tpd;
   end procedure;

   procedure tbAxilReadToVar (
      signal   clk              : in  sl;
      signal   axilReadMaster_o : out AxiLiteReadMasterType;
      signal   axilReadSlave_i  : in  AxiLiteReadSlaveType;
      constant addr             : in  slv(32-1 downto 0);
      variable dataVar          : out slv(32-1 downto 0);
      constant tpd              : in  time := 1 ns
   ) is
   begin
      -- do not wait for data yet
      axilReadMaster_o.rready             <= '0' after tpd;
      -- put the read address on the bus
      axilReadMaster_o.araddr(addr'range) <= addr after tpd;
      axilReadMaster_o.arprot             <= (others => '0') after tpd;
      -- wait for address ready
      axilReadMaster_o.arvalid            <= '1' after tpd;
      while (axilReadSlave_i.arready = '0') loop
         tbClkPeriod(clk);
      end loop;
      -- stop providing the address
      axilReadMaster_o.arvalid            <= '0' after tpd;
      -- start waiting for read data
      axilReadMaster_o.rready             <= '1' after tpd;
      while (axilReadSlave_i.rvalid = '0') loop
         tbClkPeriod(clk);
      end loop;
      -- read the data
      dataVar := axilReadSlave_i.rdata;
      -- do not wait for data yet
      axilReadMaster_o.rready             <= '0' after tpd;
   end procedure;

   procedure tbAxilWrite (
      signal   clk               : in  sl;
      signal   axilWriteMaster_o : out AxiLiteWriteMasterType;
      signal   axilWriteSlave_i  : in  AxiLiteWriteSlaveType;
      constant addr              : in  slv(32-1 downto 0);
      constant data              : in  slv(32-1 downto 0);
      constant tpd               : in  time := 1ns
   ) is
   begin
      axilWriteMaster_o.awaddr  <= addr after tpd;
      axilWriteMaster_o.wdata   <= data after tpd;
      axilWriteMaster_o.awprot  <= (others => '0') after tpd;
      axilWriteMaster_o.wstrb   <= (others => '1') after tpd;
      axilWriteMaster_o.awvalid <= '1' after tpd;
      axilWriteMaster_o.wvalid  <= '1' after tpd;
      axilWriteMaster_o.bready  <= '1' after tpd;
      tbClkPeriod(clk);
      -- Wait for a response
      while (axilWriteSlave_i.bvalid = '0') loop
         -- Clear control signals when acked
         if axilWriteSlave_i.awready = '1' then
            axilWriteMaster_o.awvalid <= '0';
         end if;
         if axilWriteSlave_i.wready = '1' then
            axilWriteMaster_o.wvalid <= '0' after tpd;
         end if;
         tbClkPeriod(clk);
      end loop;
      -- Clear control signals if bvalid and ready arrive at the same time
      if axilWriteSlave_i.awready = '1' then
         axilWriteMaster_o.awvalid <= '0' after tpd;
      end if;
      if axilWriteSlave_i.wready = '1' then
         axilWriteMaster_o.wvalid <= '0' after tpd;
      end if;
      -- Done. Check for errors
      axilWriteMaster_o.bready <= '0' after tpd;
   end procedure;

   -------------------------------------------------------------------------------------------------
   -- AXI4 functions
   -------------------------------------------------------------------------------------------------

   procedure tbAxiRead32 (
      signal   clk             : in  sl;
      signal   axiReadMaster_o : out AxiReadMasterType;
      signal   axiReadSlave_i  : in  AxiReadSlaveType;
      constant addr            : in  slv(32-1 downto 0);
      signal   data            : out Slv32Array(0 to 255);
      constant len             : in  slv( 8-1 downto 0);
      constant tpd             : in  time := 1 ns
   ) is
   begin
      -- Initialize
      axiReadMaster_o <= AXI_READ_MASTER_INIT_C;
      -- Read request
      axiReadMaster_o.araddr(addr'range) <= addr;
      axiReadMaster_o.arlen (len'range)  <= len;
      axiReadMaster_o.arsize             <= "010"; --4 bytes in each transfer (reffer to axi specs)
      axiReadMaster_o.arburst            <= "01";  --incremental
      axiReadMaster_o.arvalid            <= '1';
      tbClkPeriod(clk, 1, tpd);

      if(axiReadSlave_i.arready = '0')then
         wait until axiReadSlave_i.arready = '1';
         tbClkPeriod(clk, 1, tpd);
      end if;

      axiReadMaster_o.arvalid <= '0';

      axiReadMaster_o.rready <= '1';
      if(axiReadSlave_i.rvalid = '0')then
         wait until axiReadSlave_i.rvalid = '1';
      end if;

      tbClkPeriod(clk, 1, tpd);

      for i in 0 to to_integer(unsigned(len))-1 loop
         data(i) <= axiReadSlave_i.rdata(31 downto 0);
         tbClkPeriod(clk, 1, tpd);
         if(to_integer(unsigned(len))>0)then
            if(axiReadSlave_i.rlast = '1' and i = to_integer(unsigned(len))-1)then
               axiReadMaster_o.rready <= '0';
            elsif(i = to_integer(unsigned(len))-1)then
               printf(":: No rlast signal!  \@");
               assert false severity failure;
            end if;
         else
            if(axiReadSlave_i.rlast = '0')then
            --   printf(":: No rlast signal!  \@");
            --  assert false severity failure;
            end if;
         end if;
      end loop;
   end procedure;

   procedure tbAxiWrite32 (
      signal   clk              : in  sl;
      signal   axiWriteMaster_o : out AxiWriteMasterType;
      signal   axiWriteSlave_i  : in  AxiWriteSlaveType;
      constant addr             : in  slv(32-1 downto 0);
      constant data             : in  Slv32Array(0 to 255);
      constant len              : in  slv( 8-1 downto 0);
      constant tpd              : in  time := 1 ns
   ) is
   begin
      -- Initialize
      axiWriteMaster_o <= AXI_WRITE_MASTER_INIT_C;
      -- Read request
      axiWriteMaster_o.awaddr(addr'range) <= addr;
      axiWriteMaster_o.awlen (len'range)  <= len;
      axiWriteMaster_o.awsize             <= "010"; --4 bytes in each transfer (reffer to axi specs)
      axiWriteMaster_o.awburst            <= "01";  --incremental
      axiWriteMaster_o.awvalid            <= '1';
      -- Irl this goes hight here
      axiWriteMaster_o.bready <= '1';
      wait until axiWriteSlave_i.awready = '1';
      tbClkPeriod(clk, 1, tpd);
      axiWriteMaster_o.awvalid <= '0';
      tbClkPeriod(clk, 1, tpd);
      for i in 0 to to_integer(unsigned(len))-1 loop
         axiWriteMaster_o.wdata(31 downto 0) <= data(i);
         axiWriteMaster_o.wvalid             <= '1';
         if (i < to_integer(unsigned(len))) then
            axiWriteMaster_o.wlast <= '0';
         else
            axiWriteMaster_o.wlast <= '1';
         end if;
         if(axiWriteSlave_i.wready = '0')then
            wait until axiWriteSlave_i.wready = '1';
         end if;
         tbClkPeriod(clk, 1, tpd);
      end loop;
      axiWriteMaster_o.wvalid <= '0';
      wait until axiWriteSlave_i.bvalid = '1';
      tbClkPeriod(clk, 1, tpd);
      axiWriteMaster_o <= AXI_WRITE_MASTER_INIT_C;
      tbClkPeriod(clk, 1, tpd);
   end procedure;

   procedure tbAxiCheckWrData (
      signal clk_i             : in  sl; 
      signal axiWriteMaster_i  : in  AxiWriteMasterType; 
      signal axiWriteSlave_o   : out AxiWriteSlaveType;
      constant refArray_i      : in  Slv32Array;
      constant checkSelect_i   : in  slv := (others => '1');
      constant tValidTimeout_i : in  natural := 2147483647;
      constant tpd_i           : in  time := 0 ns
   ) is

      constant dataArrayLength : natural := refArray_i'length;
      constant ones : slv(dataArrayLength - 1 downto 0) := (others => '1');
      variable actualWordsToCheck : slv(dataArrayLength - 1 downto 0)
         := ite(dataArrayLength /= checkSelect_i'length, ones, checkSelect_i);
      variable wordCount      : natural := 0;
      variable timeoutCounter : natural := 0;
   begin

      -- array and select musth have same length
      if (dataArrayLength /= checkSelect_i'length) then
         printf(":: ERR: tbAxiCheckWrData(): array and select mismatch \@");
         assert false severity failure;
      end if;

      -- Set ready on slave interface
      axiWriteSlave_o.awready <= '1';
      axiWriteSlave_o.wready  <= '1';
      axiWriteSlave_o.bresp   <= "00";
      axiWriteSlave_o.bvalid  <= '1';

      while (wordCount < dataArrayLength) loop
         -- Wait until next clock cycle
         tbClkPeriod(clk_i, 1, tpd_i);

         if (axiWriteMaster_i.wvalid = '1') then
            if (actualWordsToCheck(wordCount) = '1') then
               -- Check for expected slv value in Axi data
               tbCheck(axiWriteMaster_i.wdata(31 downto 0), refArray_i(wordCount), "Word " & integer'image(wordCount) & " mismatch!");
               -- uncomment for debugging
               --printf(":: tbAxiCheckWrData(): wordCount: %d, wData: %h @\@", toSlv(wordCount, 32), axiWriteMaster_i.wdata(31 downto 0));
            end if;
            -- Increment word count and reset the timeout counter
            wordCount := wordCount + 1;
            timeoutCounter := 0;
         else
            -- Break out of loop in case of timeout
            if (timeoutCounter >= tValidTimeout_i) then
               printf(":: ERR: tbAxiCheckWrData(): Waiting for new data timed out at word " & integer'image(wordCount) &" \@");
               tbErrorCnt_v.inc;
               exit;
            else
               timeoutCounter := timeoutCounter + 1;
            end if;
         end if;
      end loop;

      axiWriteSlave_o.awready <= '0';
      axiWriteSlave_o.wready  <= '0';
      axiWriteSlave_o.bresp   <= "00";
      axiWriteSlave_o.bvalid  <= '1';

      tbClkPeriod(clk_i, 1, tpd_i);

      -- Set not ready on slave interface
      axiWriteSlave_o <= AXI_WRITE_SLAVE_INIT_C;

   end procedure;

   procedure tbAxiStateCheck (
      signal   val : in  AxiLiteReadSlaveType; -- observed value
      constant ref : in  AxiLiteReadSlaveType; -- reference value
      constant str : in  string := "";         -- error string
      constant dbg : in  boolean := false      -- optional debug
   ) is
   begin

      tbCheck(val.arready, ref.arready, "tbAxiStateCheck() - arready mismatch: " & str, dbg);
      tbCheck(val.rdata, ref.rdata, "tbAxiStateCheck() - rdata mismatch: " & str, dbg);
      tbCheck(val.rresp, ref.rresp, "tbAxiStateCheck() - rresp mismatch: " & str, dbg); 
      tbCheck(val.rvalid, ref.rvalid, "tbAxiStateCheck() - rvalid mismatch: " & str, dbg);

   end tbAxiStateCheck;

   procedure tbAxiStateCheck (
      signal   val : in  AxiLiteWriteSlaveType; -- observed value
      constant ref : in  AxiLiteWriteSlaveType; -- reference value
      constant str : in  string := "";         -- error string
      constant dbg : in  boolean := false      -- optional debug
   ) is
   begin

      tbCheck(val.awready, ref.awready, "tbAxiStateCheck() - awready mismatch: " & str, dbg);
      tbCheck(val.wready, ref.wready, "tbAxiStateCheck() - wready mismatch: " & str, dbg);
      tbCheck(val.bresp, ref.bresp, "tbAxiStateCheck() - bresp mismatch: " & str, dbg);
      tbCheck(val.bvalid, ref.bvalid, "tbAxiStateCheck() - bvalid mismatch: " & str, dbg);

   end tbAxiStateCheck;

   procedure tbAxiStateCheck (
      signal   val : in  AxiLiteWriteMasterType; -- observed value
      constant ref : in  AxiLiteWriteMasterType; -- reference value
      constant str : in  string := "";         -- error string
      constant dbg : in  boolean := false      -- optional debug
   ) is
   begin

      tbCheck(val.awaddr, ref.awaddr, "tbAxiStateCheck() - awaddr mismatch: " & str, dbg);
      tbCheck(val.awprot, ref.awprot, "tbAxiStateCheck() - awprot mismatch: " & str, dbg);
      tbCheck(val.awvalid, ref.awvalid, "tbAxiStateCheck() - awvalid mismatch: " & str, dbg);
      tbCheck(val.wdata, ref.wdata, "tbAxiStateCheck() - wdata mismatch: " & str, dbg);
      tbCheck(val.wstrb, ref.wstrb, "tbAxiStateCheck() - wstrb mismatch: " & str, dbg);
      tbCheck(val.wvalid, ref.wvalid, "tbAxiStateCheck() - wvalid mismatch: " & str, dbg);
      tbCheck(val.bready, ref.bready, "tbAxiStateCheck() - bready mismatch: " & str, dbg);

   end tbAxiStateCheck;

   procedure tbAxiStateCheck (
      signal   val : in  AxiReadMasterType; -- observed value
      constant ref : in  AxiReadMasterType; -- reference value
      constant str : in  string := "";      -- error string
      constant dbg : in  boolean := false   -- optional debug
   ) is
   begin

      tbCheck(val.arvalid, ref.arvalid, "tbAxiStateCheck() - arvalid mismatch: " & str, dbg);
      tbCheck(val.araddr, ref.araddr, "tbAxiStateCheck() - araddr mismatch: " & str, dbg);
      tbCheck(val.arid, ref.arid, "tbAxiStateCheck() - arid mismatch: " & str, dbg);
      tbCheck(val.arlen, ref.arlen, "tbAxiStateCheck() - arlen mismatch: " & str, dbg);
      tbCheck(val.arsize, ref.arsize, "tbAxiStateCheck() - arsize mismatch: " & str, dbg);
      tbCheck(val.arburst, ref.arburst, "tbAxiStateCheck() - arburst mismatch: " & str, dbg);
      tbCheck(val.arlock, ref.arlock, "tbAxiStateCheck() - arlock mismatch: " & str, dbg);
      tbCheck(val.arprot, ref.arprot, "tbAxiStateCheck() - arprot mismatch: " & str, dbg);
      tbCheck(val.arcache, ref.arcache, "tbAxiStateCheck() - arcache mismatch: " & str, dbg);
      tbCheck(val.arqos, ref.arqos, "tbAxiStateCheck() - arqos mismatch: " & str, dbg);
      tbCheck(val.arregion, ref.arregion, "tbAxiStateCheck() - arregion mismatch: " & str, dbg);
      tbCheck(val.rready, ref.rready, "tbAxiStateCheck() - rready mismatch: " & str, dbg);

   end tbAxiStateCheck;

   procedure tbAxiStateCheck (
      signal   val : in  AxiStreamMasterType; -- observed value
      constant ref : in  AxiStreamMasterType; -- reference value
      constant str : in  string := "";      -- error string
      constant dbg : in  boolean := false   -- optional debug
   ) is
   begin

      tbCheck(val.tvalid, ref.tvalid, "tbAxiStateCheck() - tvalid mismatch: " & str, dbg);
      tbCheck(val.tdata, ref.tdata, "tbAxiStateCheck() - tdata mismatch: " & str, dbg);
      tbCheck(val.tstrb, ref.tstrb, "tbAxiStateCheck() - tstrb mismatch: " & str, dbg);
      tbCheck(val.tkeep, ref.tkeep, "tbAxiStateCheck() - tkeep mismatch: " & str, dbg);
      tbCheck(val.tlast, ref.tlast, "tbAxiStateCheck() - tlast mismatch: " & str, dbg);
      tbCheck(val.tdest, ref.tdest, "tbAxiStateCheck() - tdest mismatch: " & str, dbg);
      tbCheck(val.tid, ref.tid, "tbAxiStateCheck() - tid mismatch: " & str, dbg);
      tbCheck(val.tuser, ref.tuser, "tbAxiStateCheck() - tuser mismatch: " & str, dbg);

   end tbAxiStateCheck;

   procedure tbAxiStateCheck (
      signal   val : in  AxiStreamSlaveType; -- observed value
      constant ref : in  AxiStreamSlaveType; -- reference value
      constant str : in  string := "";      -- error string
      constant dbg : in  boolean := false   -- optional debug
   ) is
   begin

      tbCheck(val.tReady, ref.tReady, "tbAxiStateCheck() - tReady mismatch: " & str, dbg);

   end tbAxiStateCheck;

   procedure tbAxiStateCheck (
      signal   val : in  AxiWriteMasterType; -- observed value
      constant ref : in  AxiWriteMasterType; -- reference value
      constant str : in  string := "";      -- error string
      constant dbg : in  boolean := false   -- optional debug
   ) is
   begin

      tbCheck(val.awvalid, ref.awvalid, "tbAxiStateCheck() - awvalid mismatch: " & str, dbg);
      tbCheck(val.awaddr, ref.awaddr, "tbAxiStateCheck() - awaddr mismatch: " & str, dbg);
      tbCheck(val.awid, ref.awid, "tbAxiStateCheck() - awid mismatch: " & str, dbg);
      tbCheck(val.awlen, ref.awlen, "tbAxiStateCheck() - awlen mismatch: " & str, dbg);
      tbCheck(val.awsize, ref.awsize, "tbAxiStateCheck() - awsize mismatch: " & str, dbg);
      tbCheck(val.awburst, ref.awburst, "tbAxiStateCheck() - awburst mismatch: " & str, dbg);
      tbCheck(val.awlock, ref.awlock, "tbAxiStateCheck() - awlock mismatch: " & str, dbg);
      tbCheck(val.awprot, ref.awprot, "tbAxiStateCheck() - awprot mismatch: " & str, dbg);
      tbCheck(val.awcache, ref.awcache, "tbAxiStateCheck() - awcache mismatch: " & str, dbg);
      tbCheck(val.awqos, ref.awqos, "tbAxiStateCheck() - awqos mismatch: " & str, dbg);
      tbCheck(val.awregion, ref.awregion, "tbAxiStateCheck() - awregion mismatch: " & str, dbg);
      tbCheck(val.wdata, ref.wdata, "tbAxiStateCheck() - wdata mismatch: " & str, dbg);
      tbCheck(val.wlast, ref.wlast, "tbAxiStateCheck() - wlast mismatch: " & str, dbg);
      tbCheck(val.wvalid, ref.wvalid, "tbAxiStateCheck() - wvalid mismatch: " & str, dbg);
      tbCheck(val.wid, ref.wid, "tbAxiStateCheck() - wid mismatch: " & str, dbg);
      tbCheck(val.wstrb, ref.wstrb, "tbAxiStateCheck() - wstrb mismatch: " & str, dbg);
      tbCheck(val.bready, ref.bready, "tbAxiStateCheck() - bready mismatch: " & str, dbg);

   end tbAxiStateCheck;

   procedure tbAxiStateCheck (
      signal   val : in  AxiWriteMasterArray; -- observed value
      constant ref : in  AxiWriteMasterArray; -- reference value
      constant str : in  string := "";      -- error string
      constant dbg : in  boolean := false   -- optional debug
   ) is
   begin

      if (ref'length /= val'length) then
         printf(":: ERR: tbAxiStateCheck(): val and ref array length mismatch:" & str & "\@");
         assert false severity failure;
      end if;

      for index in val'range loop
         tbAxiStateCheck(val(index), ref(index), "array element " & integer'image(index) & " mismatch.", dbg);
      end loop;

   end tbAxiStateCheck;

   procedure tbAxiStateCheck (
      signal   val : in  AxiStreamMasterArray; -- observed value
      constant ref : in  AxiStreamMasterArray; -- reference value
      constant str : in  string := "";      -- error string
      constant dbg : in  boolean := false   -- optional debug
   ) is
   begin

      if (ref'length /= val'length) then
         printf(":: ERR: tbAxiStateCheck(): val and ref array length mismatch:" & str & "\@");
         assert false severity failure;
      end if;

      for index in val'range loop
         tbAxiStateCheck(val(index), ref(index), "array element " & integer'image(index) & " mismatch.", dbg);
      end loop;

   end tbAxiStateCheck;

   procedure tbAxiStateCheck (
      signal   val : in  AxiStreamSlaveArray; -- observed value
      constant ref : in  AxiStreamSlaveArray; -- reference value
      constant str : in  string := "";        -- error string
      constant dbg : in  boolean := false     -- optional debug
   ) is
   begin

      if (ref'length /= val'length) then
         printf(":: ERR: tbAxiStateCheck(): val and ref array length mismatch:" & str & "\@");
         assert false severity failure;
      end if;

      for index in val'range loop
         tbAxiStateCheck(val(index), ref(index), "array element " & integer'image(index) & " mismatch.", dbg);
      end loop;

   end tbAxiStateCheck;

   -----------------------------------------------------------------------------
   -- File to interfaces procedures
   -----------------------------------------------------------------------------
   procedure tbFile2Axis(
      signal clk_i         : in  sl;
      signal mAxisMaster_o : out AxiStreamMasterType;
      signal mAxisSlave_i  : in AxiStreamSlaveType;
      constant type_i      : string;
      constant dataWidth_i : in  positive;
      constant tpd_i       : in  time := 1 ns;
      constant filePath_i  : in  string;
      constant waitN        : in positive := 1
   ) is
      file filePointer  : text;
      variable fileLine_v : line;
      variable dataSlv_v  : slv(dataWidth_i - 1 downto 0);
      variable dataInt_v  : integer;
      variable good_v     : boolean;
      variable fileStat_v : file_open_status;
   begin

      -- init
      file_open(fileStat_v, filePointer, filePath_i, READ_MODE);
      assert fileStat_v = OPEN_OK report "ERR: tbFile2Axis: File could not be opened." severity failure;

      mAxisMaster_o <= AXI_STREAM_MASTER_INIT_C;

      tbClkPeriod(clk_i, 1, tpd_i);

      while not endfile(filePointer) loop

         -- read line and store it to fileLine_v
         readline(filePointer, fileLine_v);
         -- if line is empty, dont' use it.
         next when fileLine_v'length = 0;

         -- read all data from single line
         if type_i = "hex" then
            hread(fileLine_v, dataSlv_v, good_v);
            mAxisMaster_o.tdata(dataSlv_v'range) <= dataSlv_v;
         elsif type_i = "dec" then
            read(fileLine_v, dataInt_v, good_v);
            mAxisMaster_o.tdata(dataSlv_v'range) <= toSlv(dataInt_v, dataSlv_v'length);
         end if;

         assert good_v report "ERR: tbFile2Axis: Wrong file format." severity failure;

         -- write data AXIS
         mAxisMaster_o.tvalid <= '1';
         wait for 1 ns;
         waitEqual(mAxisSlave_i.tReady, '1', 1 ms);
         tbClkPeriod(clk_i, 1, tpd_i);
         mAxisMaster_o.tvalid <= '0';
         tbClkPeriod(clk_i, waitN, tpd_i);

      end loop;

      mAxisMaster_o <= AXI_STREAM_MASTER_INIT_C;
      file_close(filePointer);

   end procedure tbFile2Axis;

end package body CslTbAxiPkg;
