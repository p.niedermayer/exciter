--------------------------------------------------------------------------------
-- Title       : <Title Block>
-- Project     : Default Project Name
--------------------------------------------------------------------------------
-- File        : PidFloatSequential_tb.vhd
-- Author      : User Name <user.email@user.company.com>
-- Company     : User Company Name
-- Created     : Tue Jun 27 17:55:56 2023
-- Last update : Fri Aug 11 13:42:44 2023

-- Platform    : Default Part Number
-- Standard    : <VHDL-2008 | VHDL-2002 | VHDL-1993 | VHDL-1987>
--------------------------------------------------------------------------------
-- Copyright (c) 2023 User Company Name
-------------------------------------------------------------------------------
-- Description: 
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;
use std.textio.all;
use ieee.math_real.all;

use work.CslStdRtlPkg.all;
use work.CslAxiPkg.all;
use work.CslTbAxiPkg.all;
use work.CslTbBasePkg.all;
use work.CslPrintfPkg.all;

use work.PidTestbenchPkg.all;
-----------------------------------------------------------

entity PidFloatCoreTb is

end entity PidFloatCoreTb;

-----------------------------------------------------------

architecture testbench of PidFloatCoreTb is

   -- Testbench DUT generics
   constant TPD_G          : time    := 1 ns;
   constant DATA_WIDTH_G   : natural := 16;
   constant TA_WIDTH_G     : natural := 16;
   constant OUTPUT_WIDTH_G : natural := 16;

   signal tbSlv0 : slv(3 downto 0) := (others => '0');
   signal tbSlv3 : slv(3 downto 0) := toSlv(3, 4);
   signal tbSl1  : sl              := '1';
   signal tbSl0  : sl              := '0';

   -- signal mAxisMasterFile_o : AxiStreamMasterType;
   -- signal mAxisSlaveFile_i  : AxiStreamSlaveType;

   signal readFile : sl := '0';

   constant FILE_PATH_C       : string  := "/home/fpga/exciter/rfnoc-beam_exciter/fpga/rfnoc_block_feedback_controller/tb/data/input.txt";
   constant FILE_DATA_WIDTH_C : integer := 32;
   --constant FILE_ROWS_C       : integer := 50000;
   constant FILE_ROWS_C : integer := 100; -- 50000; 3900000;

   --constant RESULT_FILE_PATH_C : string := "/samba/bkelbl/workspace/fpgaOutput.txt";
   constant RESULT_FILE_PATH_C        : string := "/home/fpga/exciter/rfnoc-beam_exciter/fpga/rfnoc_block_feedback_controller/tb/data/fpgaOutput.txt";
   constant RESULT_FILE_PATH_STREAM_C : string := "/home/fpga/exciter/rfnoc-beam_exciter/fpga/rfnoc_block_feedback_controller/tb/data/fpgaOutputStream.txt";

   -- Testbench DUT ports
   signal active_i              : sl;
   signal ta_i                  : slv(TA_WIDTH_G - 1 downto 0);
   signal kp_i                  : slv(31 downto 0);
   signal kiTimesTa_i           : slv(31 downto 0);
   signal kdOverTa_i            : slv(31 downto 0);
   signal target_i              : slv(31 downto 0);
   signal feedback_i            : sl;
   signal feedForward_i         : sl;
   signal feedForwardConstant_i : slv(31 downto 0);
   signal outMin_i              : slv(31 downto 0);
   signal outMax_i              : slv(31 downto 0);
   signal outScale_i            : slv(31 downto 0);
   signal repeat_i              : slv(7 downto 0);
   signal sampleInAxisMaster_i  : AxiStreamMasterType;
   signal sampleInAxisSlave_o   : AxiStreamSlaveType;
   signal sampleOutAxisMaster_o : AxiStreamMasterType;
   signal sampleOutAxisSlave_i  : AxiStreamSlaveType;
   signal floatOut_o            : slv(31 downto 0);
   signal floatValid_o          : sl;
   signal clk_i                 : sl;
   signal rst_i                 : sl;

   signal floatRealSig : real;

   signal cntRows : natural := 0;
   signal cntRowsStream : natural := 0;

   -- Other signals
   signal sampleRead : sl;
   -- Other constants
   constant CLK_PERIOD_C : time := 10 ns; -- NS

begin
   -----------------------------------------------------------
   -- Clocks and Reset
   -----------------------------------------------------------
   tbClock(clk_i, CLK_PERIOD_C);

   -----------------------------------------------------------
   -- Testbench Stimulus
   -----------------------------------------------------------
   sampleRead                  <= sampleInAxisMaster_i.tvalid or sampleInAxisSlave_o.tReady;
   sampleOutAxisSlave_i.tReady <= '1';
   p_Sim : process
   begin
      active_i <= '1';
      ta_i     <= slv(to_unsigned(200, ta_i'length));

      --kp_i                  <= real_to_flt(6.666666666666667E-09, normal, 32, 24);
      --kiTimesTa_i           <= real_to_flt(1.3333333333333335E-09, normal, 32, 24);


      kp_i        <= real_to_flt(0.0013333333333333334, normal, 32, 24);
      kiTimesTa_i <= real_to_flt(0.0002666666666666667, normal, 32, 24);
      -- kp_i                  <= real_to_flt(0.001333333333333333333333333333333333333, normal, 32, 24);
      -- kiTimesTa_i           <= real_to_flt(0.000266666666666666666666666666666666667, normal, 32, 24);
      --kp_i                  <= real_to_flt(0.12, normal, 32, 24);
      --kiTimesTa_i           <= real_to_flt(0.024, normal, 32, 24);
      kdOverTa_i            <= real_to_flt(0.0, zero_pos, 32, 24);
      target_i              <= real_to_flt(4.5, normal, 32, 24);
      feedback_i            <= '1';
      feedForward_i         <= '1';
      feedForwardConstant_i <= real_to_flt(0.0, zero_pos, 32, 24);
      outMin_i              <= real_to_flt(0.0, zero_pos, 32, 24);
      outMax_i              <= real_to_flt(0.2, normal, 32, 24);
      outScale_i            <= real_to_flt(3.0, normal, 32, 24);
      repeat_i              <= slv(to_unsigned(1,8));

      printf("-----------------------------------------------------------------------------------");
      printf("Simulation start @ \@");
      printf("Init \@");

      rst_i <= '0';

      tbReset(clk_i, rst_i, '1', 10);
      tbClkPeriod(clk_i, 1);
      printf("Reset done @ \@");

      printf("-----------------------------------------------------------------------------------");
      printf("Checks that pass. @ \@");

      tbCheck(tbSlv0, x"0", "tbSlv0 check failed. (0)");
      tbCheck(tbSlv0, 0, "tbSlv0 check failed. (1)");
      tbCheck(tbSlv3, 3, "tbSlv3 check failed. (0)");
      tbCheck(tbSlv3, x"3", "tbSlv3 check failed. (1)");
      tbCheck(tbSl0, 0, "tbSl0 check failed. (0)");
      tbCheck(tbSl0, '0', "tbSl0 check failed. (1)");
      tbCheck(tbSl1, 1, "tbSl1 check failed. (0)");
      tbCheck(tbSl1, '1', "tbSl1 check failed. (1)");

      printf("-----------------------------------------------------------------------------------");
      printf("Test tbFile2Axis @ \@");

      readFile <= '1';

      cntRows <= 0;
      LOOP_FILE : for I in 1 to FILE_ROWS_C loop
         wait until sampleOutAxisMaster_o.tValid = '1';
         wait for CLK_PERIOD_C/2;
         cntRows <= cntRows + 1;
         tbClkPeriod(clk_i, 1, TPD_G);
      end loop LOOP_FILE;

      tbClkPeriod(clk_i, 100000);


      tbReportErrorCnt;
      printf("-----------------------------------------------------------------------------------");
      printf("Simulation Finished @ \@");
      printf("-----------------------------------------------------------------------------------");
      finish;

   end process;

   p_AxisReadFile : process
   begin
      waitEqual(readFile, '1', 100 * CLK_PERIOD_C);
      if (readFile = '1') then
         printf("p_AxisReadFile: tbFile2Axis started. @ \@");
         tbFile2Axis(
            clk_i,
            sampleInAxisMaster_i,
            sampleInAxisSlave_o,
            "dec",
            FILE_DATA_WIDTH_C,
            TPD_G,
            FILE_PATH_C,
            5
         );
      end if;
      wait;
   end process p_AxisReadFile;


   p_AxisWriteFileValue : process
      file filePointer         : text;
      variable fileLine_v      : line;
      variable fileStat_v      : file_open_status;
      variable floatReal       : real;
      variable formattedString : string(1 to 19);
   begin
      file_open(fileStat_v, filePointer, RESULT_FILE_PATH_C, WRITE_MODE);
      assert fileStat_v = OPEN_OK report "ERR: tbFile2Axis: File could not be opened." severity failure;
      while true loop
         --         waitEqual(floatValid_o , '1', 1000 * CLK_PERIOD_C);
         wait until floatValid_o = '1';
         wait for CLK_PERIOD_C/2;
         floatReal := flt_to_real(floatOut_o, 32, 24);
         -- --flt_to_real(floatOut_o, 32, 24);
         formattedString := TO_STRING(floatReal,"%.17f");
         write(fileLine_v, formattedString);
         
         writeline(filePointer, fileLine_v);
         tbClkPeriod(clk_i, 1);
      end loop;
      wait;
   end process p_AxisWriteFileValue;

   p_AxisWriteFileStream : process
      file filePointer         : text;
      variable fileLine_v      : line;
      variable fileStat_v      : file_open_status;
      variable fixedReal       : real;
      variable formattedString : string(1 to 19);
   begin
      file_open(fileStat_v, filePointer, RESULT_FILE_PATH_STREAM_C, WRITE_MODE);
      cntRowsStream <= 0;
      assert fileStat_v = OPEN_OK report "ERR: tbFile2Axis: File could not be opened." severity failure;
      while true loop
         wait until sampleOutAxisMaster_o.tValid = '1';
         wait for CLK_PERIOD_C/2;
         fixedReal := fix_to_real(to_integer(signed(sampleOutAxisMaster_o.tdata(15 downto 0))), 15);
         formattedString := TO_STRING(fixedReal,"%.17f");
         write(fileLine_v, formattedString);
         cntRowsStream <= cntRowsStream + 1;

         writeline(filePointer, fileLine_v);
         tbClkPeriod(clk_i, 1, TPD_G);
      end loop;
      wait;
   end process p_AxisWriteFileStream;
   -----------------------------------------------------------
   -- Entity Under Test
   -----------------------------------------------------------
   DUT : entity work.PidFloatCore
      generic map (
         TPD_G          => TPD_G,
         DATA_WIDTH_G   => DATA_WIDTH_G,
         TA_WIDTH_G     => TA_WIDTH_G,
         OUTPUT_WIDTH_G => OUTPUT_WIDTH_G
      )
      port map (
         active_i              => active_i,
         ta_i                  => ta_i,
         kp_i                  => kp_i,
         kiTimesTa_i           => kiTimesTa_i,
         kdOverTa_i            => kdOverTa_i,
         target_i              => target_i,
         feedback_i            => feedback_i,
         feedForward_i         => feedForward_i,
         feedForwardConstant_i => feedForwardConstant_i,
         outMin_i              => outMin_i,
         outMax_i              => outMax_i,
         outScale_i            => outScale_i,
         repeat_i              => repeat_i,
         sampleInAxisMaster_i  => sampleInAxisMaster_i,
         sampleInAxisSlave_o   => sampleInAxisSlave_o,
         sampleOutAxisMaster_o => sampleOutAxisMaster_o,
         sampleOutAxisSlave_i  => sampleOutAxisSlave_i,
         floatOut_o            => floatOut_o,
         floatValid_o          => floatValid_o,
         clk_i                 => clk_i,
         rst_i                 => rst_i
      );

end architecture testbench;