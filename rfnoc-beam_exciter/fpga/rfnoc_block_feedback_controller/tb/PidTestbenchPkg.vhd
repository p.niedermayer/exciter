library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.CslStdRtlPkg.all;
use work.CslAxiPkg.all;
--use work.CslCommonTbPkg.all;
use work.CslTbBasePkg.all;

package PidTestbenchPkg is

   procedure tbAxisTx (
         signal clk        : in  sl;
         signal axisMaster : out AxiStreamMasterType;
         signal axisSlave  : in  AxiStreamSlaveType;
         constant data     : in  natural;
         constant last     : in  sl   := '1';
         constant tpd      : in  time := 1ns
      );

   procedure tbAxisTx (
         signal clk        : in  sl;
         signal axisMaster : out AxiStreamMasterType;
         signal axisSlave  : in  AxiStreamSlaveType;
         constant data     : in  slv(31 downto 0);
         constant last     : in  sl   := '1';
         constant tpd      : in  time := 1ns
      );

   -- Enumerated type to indicate special floating point values.
   -- Note: denormalized numbers are treated as zero, and signalling NaNs are treated as quiet NaNs.
   -- Out Of Range just means the value is larger than VHDL can support in a Real type
   type floating_point_special_t is (normal, zero_pos, zero_neg, inf_pos, inf_neg, nan, out_of_range);

   function flt_to_real(f : std_logic_vector; -- floating point number to convert
         w, fw : integer)                     -- total and fractional bit width of result
      return real;

   function flt_to_special(f : std_logic_vector; -- floating point number to convert
         w, fw : integer)                        -- total and fractional bit width of result
      return floating_point_special_t;

   function real_to_flt(x : real;          -- real number to convert
         s     : floating_point_special_t; -- indicates special values to convert (overrides x)
         w, fw : integer)                  -- total and fractional bit width of result
      return std_logic_vector;

   function width_limit(w : integer) return integer;

   function fix_to_real
      (
         fixed_point_number       : integer;
         number_of_fracional_bits : natural
      )
      return real;

end package PidTestbenchPkg;

package body PidTestbenchPkg is

   procedure tbAxisTx (
         signal clk        : in  sl;
         signal axisMaster : out AxiStreamMasterType;
         signal axisSlave  : in  AxiStreamSlaveType;
         constant data     : in  natural;
         constant last     : in  sl   := '1';
         constant tpd      : in  time := 1ns
      ) is
      variable dataVectUnsigned : unsigned(31 downto 0);
      variable dataVect         : std_logic_vector(31 downto 0);
   begin
      dataVectUnsigned := to_unsigned(data, dataVectUnsigned'length);
      dataVect         := std_logic_vector(dataVectUnsigned);
      -- set VALID
      axisMaster.tdata                 <= (others => '0') after tpd;
      axisMaster.tdata(dataVect'range) <= dataVect;
      axisMaster.tstrb                 <= (0 downto 0 => '1' , others => '0') after tpd;
      axisMaster.tkeep                 <= (0 downto 0 => '1' , others => '0') after tpd;
      axisMaster.tlast                 <= last after tpd;
      axisMaster.tdest                 <= (others => '0') after tpd;
      axisMaster.tid                   <= (others => '0') after tpd;
      axisMaster.tuser                 <= (others => '0') after tpd;
      axisMaster.tvalid                <= '1' after tpd;
      tbClkPeriod(clk);
      -- Wait for a response
      while (axisSlave.tready = '0') loop
         tbClkPeriod(clk);
      end loop;
      -- clear VALID
      --      axisMaster.tdata  <= (others => 'U') after tpd;
      axisMaster.tstrb  <= (others => 'U') after tpd;
      axisMaster.tkeep  <= (others => 'U') after tpd;
      axisMaster.tlast  <= 'U' after tpd;
      axisMaster.tdest  <= (others => 'U') after tpd;
      axisMaster.tid    <= (others => 'U') after tpd;
      axisMaster.tuser  <= (others => 'U') after tpd;
      axisMaster.tvalid <= '0' after tpd;
   end procedure tbAxisTx;

   procedure tbAxisTx (
         signal clk        : in  sl;
         signal axisMaster : out AxiStreamMasterType;
         signal axisSlave  : in  AxiStreamSlaveType;
         constant data     : in  slv(31 downto 0);
         constant last     : in  sl   := '1';
         constant tpd      : in  time := 1ns
      ) is
   begin

      -- set VALID
      axisMaster.tdata             <= (others => '0') after tpd;
      axisMaster.tdata(data'range) <= data;
      axisMaster.tstrb             <= (0 downto 0 => '1' , others => '0') after tpd;
      axisMaster.tkeep             <= (0 downto 0 => '1' , others => '0') after tpd;
      axisMaster.tlast             <= last after tpd;
      axisMaster.tdest             <= (others => '0') after tpd;
      axisMaster.tid               <= (others => '0') after tpd;
      axisMaster.tuser             <= (others => '0') after tpd;
      axisMaster.tvalid            <= '1' after tpd;
      tbClkPeriod(clk);
      -- Wait for a response
      while (axisSlave.tready = '0') loop
         tbClkPeriod(clk);
      end loop;
      -- clear VALID
      --      axisMaster.tdata  <= (others => 'U') after tpd;
      axisMaster.tstrb  <= (others => 'U') after tpd;
      axisMaster.tkeep  <= (others => 'U') after tpd;
      axisMaster.tlast  <= 'U' after tpd;
      axisMaster.tdest  <= (others => 'U') after tpd;
      axisMaster.tid    <= (others => 'U') after tpd;
      axisMaster.tuser  <= (others => 'U') after tpd;
      axisMaster.tvalid <= '0' after tpd;
   end procedure tbAxisTx;

   -----------------------------------------------------------------------
   -- Functions
   -----------------------------------------------------------------------

   -- Function to limit an integer bit width to a value for which functions can use VHDL's integer type for calculations
   function width_limit(w : integer) return integer is
      constant MAX_WIDTH : integer := 30; -- maximum width: allows 2**width to be stored in an integer variable
   begin
      if w < MAX_WIDTH then
         return w;
      else
         return MAX_WIDTH;
      end if;
   end function width_limit;

   -- Function to convert a real number to a std_logic_vector floating point representation
   function real_to_flt(x : real;          -- real number to convert
         s     : floating_point_special_t; -- indicates special values to convert (overrides x)
         w, fw : integer)                  -- total and fractional bit width of result
      return std_logic_vector is
      constant EW       : integer                        := w - fw;
      constant FW_LIM   : integer                        := width_limit(fw); -- limited internal value of fractional bit width
      constant E_BIAS   : integer                        := 2 ** (EW - 1) - 1;
      constant MANT_MAX : real                           := 2.0 - 1.0 / real(2 ** (FW_LIM - 1));
      variable result   : std_logic_vector(w-1 downto 0) := (others => '0');
      variable sign     : std_logic                      := '0';
      variable exp      : integer                        := 0;
      variable mant     : real;
      variable mant_int : integer;
   begin

      -- Handle special cases
      case s is
         when zero_pos =>                            -- plus zero
            result(w-1)          := '0';             -- sign bit clear
            result(w-2 downto 0) := (others => '0'); -- exponent and mantissa clear

         when zero_neg =>                            -- minus zero
            result(w-1)          := '1';             -- sign bit set
            result(w-2 downto 0) := (others => '0'); -- exponent and mantissa clear

         when inf_pos =>                                -- plus infinity
            result(w-1)             := '0';             -- sign bit clear
            result(w-2 downto fw-1) := (others => '1'); -- exponent
            result(fw-2 downto 0)   := (others => '0'); -- mantissa

         when inf_neg =>                                -- minus infinity
            result(w-1)             := '1';             -- sign bit set
            result(w-2 downto fw-1) := (others => '1'); -- exponent
            result(fw-2 downto 0)   := (others => '0'); -- mantissa

         when nan =>                                    -- Not a Number
            result(w-1)             := '0';             -- sign bit
            result(w-2 downto fw-1) := (others => '1'); -- exponent
            result(fw-2)            := '1';             -- MSB of mantissa set
            result(fw-3 downto 0)   := (others => '0'); -- rest of mantissa clear

         -- NOTE: out_of_range might be possible under some circumstances, but it can be represented
         when normal | out_of_range =>
            -- Handle normal numbers

            -- Zero must be requested using s = zero_pos or s = zero_neg, not s = normal and x = 0.0
            assert x /= 0.0
               report "ERROR: real_to_flt: zero must be requested using s = zero_pos or s = zero_neg, not s = normal and x = 0.0"
               severity failure;

            -- Get sign bit
            if x < 0.0 then
               sign := '1';
               mant := -x;
            else
               sign := '0';
               mant := x;
            end if;

            -- Normalize input to calculate exponent
            while mant < 1.0 loop
               exp  := exp - 1;
               mant := mant * 2.0;
            end loop;
            while mant > MANT_MAX loop
               exp  := exp + 1;
               mant := mant / 2.0;
            end loop;

            -- Remove hidden bit and convert to std_logic_vector
            -- To avoid VHDL's integer type overflowing, use at most 30 bits of the mantissa
            mant                              := mant - 1.0;
            mant_int                          := integer(mant * real(2 ** (FW_LIM - 1))); -- implicit round-to-nearest
            result(fw - 2 downto fw - FW_LIM) := std_logic_vector(to_unsigned(mant_int, FW_LIM - 1));

            -- Add exponent bias and convert to std_logic_vector
            exp                         := exp + E_BIAS;
            result(w - 2 downto fw - 1) := std_logic_vector(to_unsigned(exp, EW));

            -- Add sign bit
            result(w - 1) := sign;

      end case;

      return result;
   end function real_to_flt;


   -- Function to identify special numbers in a std_logic_vector floating point representation
   function flt_to_special(f : std_logic_vector; -- floating point number to convert
         w, fw : integer)                        -- total and fractional bit width of result
      return floating_point_special_t is
      constant EW     : integer                           := w - fw;
      constant ZEROS  : std_logic_vector(w-1 downto 0)    := (others => '0');
      constant ONES   : std_logic_vector(w-1 downto 0)    := (others => '1');
      variable f_int  : std_logic_vector(f'high downto 0) := f;                      -- fixed width version of f
      variable f_sign : std_logic                         := f_int(w-1);             -- sign bit of f
      variable f_exp  : std_logic_vector(EW-1 downto 0)   := f_int(w-2 downto fw-1); -- exponent of f
      variable f_mant : std_logic_vector(fw-2 downto 0)   := f_int(fw-2 downto 0);   -- mantissa of f
      variable result : floating_point_special_t;
      constant E_BIAS : integer := 2 ** (EW - 1) - 1;
   begin

      -- Check bit widths match
      assert f'high = w - 1
         report "ERROR: flt_to_special: input std_logic_vector f must have bit width = input integer w"
         severity failure;

      -- Check for special cases
      if f_exp = ZEROS(w-2 downto fw-1) then
         -- +/- zero (treat denormalized numbers as zero)
         if f_sign = '0' then
            result := zero_pos;
         else
            result := zero_neg;
         end if;

      elsif f_exp = ONES(w-2 downto fw-1) then
         if f_mant = ZEROS(fw-2 downto 0) then
            -- +/- infinity
            if f_sign = '0' then
               result := inf_pos;
            else
               result := inf_neg;
            end if;

         else
            -- NaN (not a number)
            result := nan;
         end if;
      elsif to_integer(unsigned(f_exp)) - E_BIAS > 1022 then
         -- If the exponent is too large then we end up with a number that VHDL can't represent in a REAL.
         -- This gives fatal runtime errors.  Note that 1022 is used instead of 1023 (the actual max) because
         -- that allows us to ignore the value of the mantissa

         result := out_of_range;
      elsif to_integer(unsigned(f_exp)) - E_BIAS < -1021 then
         result := out_of_range;

      else
         -- not a special case
         result := normal;
      end if;

      return result;
   end function flt_to_special;

   -- Function to convert a std_logic_vector floating point representation into a real number
   function flt_to_real(f : std_logic_vector; -- floating point number to convert
         w, fw : integer)                     -- total and fractional bit width of result
      return real is
      constant EW     : integer                           := w - fw;
      constant FW_LIM : integer                           := width_limit(fw); -- limited internal value of fractional bit width
      constant E_BIAS : integer                           := 2 ** (EW - 1) - 1;
      constant ZEROS  : std_logic_vector(w-1 downto 0)    := (others => '0');
      variable f_int  : std_logic_vector(f'high downto 0) := f;                      -- fixed width version of f
      variable f_sign : std_logic                         := f_int(w-1);             -- sign bit of f
      variable f_exp  : std_logic_vector(EW-1 downto 0)   := f_int(w-2 downto fw-1); -- exponent of f
      variable f_mant : std_logic_vector(fw-2 downto 0)   := f_int(fw-2 downto 0);   -- mantissa of f
      variable exp    : integer;
      variable result : real;
   begin

      -- Check bit widths match
      assert f'high = w - 1
         report "ERROR: flt_to_real: input std_logic_vector f must have bit width = input integer w"
         severity failure;

      -- Check for special cases: return zero if any are found
      if flt_to_special(f, w, fw) /= normal then
         result := 0.0;
      else

         -- Convert mantissa to real
         -- To avoid VHDL's integer type overflowing, consider at most 30 bits of the mantissa
         result := real(to_integer(unsigned(f_mant(fw - 2 downto fw - FW_LIM)))) / real(2 ** (FW_LIM - 1)) + 1.0;

         -- Apply exponent
         exp := to_integer(unsigned(f_exp)) - E_BIAS;
         if exp > 0 then
            result := result * (2.0 ** real(exp));
         elsif exp < 0 then
            --            result := result * (2.0 ** real(exp));
            result := result / (2.0 ** real(-exp));
         end if;

         -- Apply sign bit
         if f_sign = '1' then
            result := -result;
         end if;

      end if;

      return result;
   end function flt_to_real;

   function fix_to_real
      (
         fixed_point_number       : integer;
         number_of_fracional_bits : natural
      )
      return real
      is
   begin
      return real(fixed_point_number)/2.0**number_of_fracional_bits;
   end fix_to_real;

end package body PidTestbenchPkg;

