---------------------------------------------------------------------------------------------------
--! @brief Contains a procedure printf that is used for writing into console.
--! 
--! @details This is a VHDL equivalence of C printf function. Note that this procedure does not 
--! cover all the special characters and behaviour of a C printf function. 
--! printf(string, arg0, arg1, arg2, arg3, arg4)
--! The procedure can only accept max 5 arguments. An argument can only be a slv
--!
--! examples:
--! printf("%d", slv1) output: decimal value of slv
--! printf("%h", slv1) output: hexadecimal value of slv
--! printf("%b", slv1) output: binary value of slv
--! printf("\n") output: newline
--! printf("\t") output: tabulator (4x empty space)
--! printf("\@") output: current simulation time in ps
--! More examples can be found in testbench (printf_tb.vhd)
--!
--! Note that printf will automaticly add a newline at the end of a text. No, this cannot be 
--! disabled.
--!
--! @author Klemen Erjavec, Cosylab (klemen.erjavec@cosylab.com)
--!
--! @date <16.08.2015> created
--! @date <16.08.2015> last modify
--! 
--! @version <1.0> 
-- 
-- @par Modifications:
-- <author>, <date>: <what>
-- 
-- @todo Currently printf only accepts slvs, would be nice if there could be mixed type arguments.
-- @bug
--! @file
--!
--! Copyright (c) 2020 Cosylab d.d.
--! This software is distributed under the terms found
--! in file LICENSE.txt that is included with this distribution.
---------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library STD;
use STD.textio.all;

use work.CslStdRtlPkg.all;

package CslPrintfPkg is

   --! number of standard logic vectors, modify this accordingly if you intend to add more slv arguments
   constant NUM_OF_SLVS_C : integer := 5;

   --! main procedure
   procedure printf(str_i: string; slv0_i, slv1_i, slv2_i, slv3_i, slv4_i : slv; numOfSlvs_i : integer);
   
   --! hexadecimal write
   procedure cslHexWrite(lineBuff_i: inout line; value_i:in slv);

   -- std logic vector write
   procedure cslSlvWrite(lineBuff_i: inout line; value_i:in slv);

   --! procedures that will override the main procedure
   procedure printf(str_i: string; slv0_i, slv1_i, slv2_i, slv3_i, slv4_i : slv);
   procedure printf(str_i: string; slv0_i, slv1_i, slv2_i, slv3_i : slv);
   procedure printf(str_i: string; slv0_i, slv1_i, slv2_i : slv);
   procedure printf(str_i: string; slv0_i, slv1_i : slv);
   procedure printf(str_i: string; slv0_i : slv);
   procedure printf(str_i: string);
   
   --! slv size type 
   type t_slv_size is array (NUM_OF_SLVS_C - 1 downto 0) of integer;
   
end CslPrintfPkg;

package body CslPrintfPkg is

---------------------------------------------------------------------------------------------------
--! main procedure this procedure will be overridden by the other printfs with fewer arguments

   procedure cslHexWrite(lineBuff_i: inout line; value_i:in slv) is
      variable value    : slv(value_i'length-1 downto 0) := value_i;
      variable valueRem : integer := (value'left + 1) rem 4;
      variable resizedVal : slv(value'left + 4 downto 0) := x"0" & value;
   begin

      if (valueRem > 0) then
         l_HEX_RESIZED: for I in (resizedVal'left + 1)/4 downto 1 loop
            case resizedVal(4 * I - 1 downto 4 * (I - 1)) is
               when x"0" => write(lineBuff_i, string'("0"));
               when x"1" => write(lineBuff_i, string'("1"));
               when x"2" => write(lineBuff_i, string'("2"));
               when x"3" => write(lineBuff_i, string'("3"));
               when x"4" => write(lineBuff_i, string'("4"));
               when x"5" => write(lineBuff_i, string'("5"));
               when x"6" => write(lineBuff_i, string'("6"));
               when x"7" => write(lineBuff_i, string'("7"));
               when x"8" => write(lineBuff_i, string'("8"));
               when x"9" => write(lineBuff_i, string'("9"));
               when x"A" => write(lineBuff_i, string'("A"));
               when x"B" => write(lineBuff_i, string'("B"));
               when x"C" => write(lineBuff_i, string'("C"));
               when x"D" => write(lineBuff_i, string'("D"));
               when x"E" => write(lineBuff_i, string'("E"));
               when x"F" => write(lineBuff_i, string'("F"));
               when others => write(lineBuff_i, string'("X"));
            end case;
         end loop l_HEX_RESIZED;
      else
         l_HEX_NORMAL: for I in (value'left + 1)/4 downto 1 loop
            case value(4 * I - 1 downto 4 * (I - 1)) is
               when x"0" => write(lineBuff_i, string'("0"));
               when x"1" => write(lineBuff_i, string'("1"));
               when x"2" => write(lineBuff_i, string'("2"));
               when x"3" => write(lineBuff_i, string'("3"));
               when x"4" => write(lineBuff_i, string'("4"));
               when x"5" => write(lineBuff_i, string'("5"));
               when x"6" => write(lineBuff_i, string'("6"));
               when x"7" => write(lineBuff_i, string'("7"));
               when x"8" => write(lineBuff_i, string'("8"));
               when x"9" => write(lineBuff_i, string'("9"));
               when x"A" => write(lineBuff_i, string'("A"));
               when x"B" => write(lineBuff_i, string'("B"));
               when x"C" => write(lineBuff_i, string'("C"));
               when x"D" => write(lineBuff_i, string'("D"));
               when x"E" => write(lineBuff_i, string'("E"));
               when x"F" => write(lineBuff_i, string'("F"));
               when others => write(lineBuff_i, string'("X"));
            end case;
         end loop l_HEX_NORMAL;
      end if;

   end cslHexWrite;

   procedure cslSlvWrite(lineBuff_i: inout line; value_i:in slv) is
      variable value   : slv(value_i'length-1 downto 0) := value_i;
   begin

     l_HEX_NORMAL: for I in value'left downto 0 loop
        case value(I) is
           when '0' => write(lineBuff_i, string'("0"));
           when '1' => write(lineBuff_i, string'("1"));
           when 'U' => write(lineBuff_i, string'("U"));
           when 'X' => write(lineBuff_i, string'("X"));
           when 'Z' => write(lineBuff_i, string'("Z"));
        end case;
     end loop l_HEX_NORMAL;
   end cslSlvWrite;

   procedure printf(str_i: string; slv0_i, slv1_i, slv2_i, slv3_i, slv4_i : slv; numOfSlvs_i : integer) is
      --! needed for write();
      variable lineBuff      : line;
      
      --! is true if current character is the last character
      variable lastChar  : boolean;
      
      --! counter used for getting chars out of input string
      variable charIndex        : integer;
      
      --! counter that increments on every procent character (%)
      variable procentCnt : integer;

      --! error signal:
      --! 1 =  Special char received (eg "%d") but INTEGER is missing 
      --!     eg. printf("%d", INTEGER MISSING);
      variable errorId : integer;
      
   begin
   
     ---------------------------------------------------------------------------------------------------
     --  initialize variables
                       
      charIndex := 1;
      procentCnt := 0;
     ---------------------------------------------------------------------------------------------------
     --  main loop
            
      l_main :loop
         
         --! when this is true end loop (charIndex gets incremented everytime we process a new char
         --! from input string)
         if (charIndex > str_i'right) then
            exit;
         end if;
         
         --! use to determine if current char is the last char in string. This is used so we can
         --! to display an error if user writes in something like this "printf("ASD%)"
         if (charIndex = str_i'right) then
            lastChar := true;
         end if;
         
         --! if character in string is % and it is not the last character
         if(str_i(charIndex) = '%' and lastChar = false) then
            
            --! increment procent character counter, will be used to determine which
            --! slv input to display
            procentCnt := procentCnt + 1;
            
            --! increment charIndex to check the next character
            charIndex := charIndex + 1;
            
            --! numOfSlvs_i must not be smaller than procentCnt otherwise it means
            --! that user inputed something like printf("ASD %d") instead of printf("ASD %d",1)
            if (procentCnt <= numOfSlvs_i) then
               
               --! if next character is 'd' write decimal (charIndex was incremented)
               if (str_i(charIndex) = 'd') then
               
                  --! if procentCnt = 1 it will take the first slv argument, 
                  --! if 2 the second... 
                  case (procentCnt) is
                     when 1 => write(lineBuff, to_integer(unsigned(slv0_i)));
                     when 2 => write(lineBuff, to_integer(unsigned(slv1_i)));
                     when 3 => write(lineBuff, to_integer(unsigned(slv2_i)));
                     when 4 => write(lineBuff, to_integer(unsigned(slv3_i)));
                     when 5 => write(lineBuff, to_integer(unsigned(slv4_i)));
                     when others => report "Gratz you found a bug1" severity failure;
                  end case;
                  
               --! if next character is 'h' write hexadecimal (charIndex was incremented)
               elsif (str_i(charIndex) = 'h') then

                  case (procentCnt) is
                     when 1 => 
                        cslHexWrite(lineBuff, slv0_i);
                     when 2 =>
                        cslHexWrite(lineBuff, slv1_i);
                     when 3 => 
                        cslHexWrite(lineBuff, slv2_i);
                     when 4 => 
                        cslHexWrite(lineBuff, slv3_i);
                     when 5 => 
                        cslHexWrite(lineBuff, slv4_i);
                     when others => report "Error case unreachable" severity failure;
                  end case;
                  
               --! if next character is 'b' write hexadecimal (charIndex was incremented)
               elsif (str_i(charIndex) = 'b') then

                  case (procentCnt) is
                     when 1 => cslSlvWrite(lineBuff, slv0_i);
                     when 2 => cslSlvWrite(lineBuff, slv1_i);
                     when 3 => cslSlvWrite(lineBuff, slv2_i);
                     when 4 => cslSlvWrite(lineBuff, slv3_i);
                     when 5 => cslSlvWrite(lineBuff, slv4_i);
                     when others => report "Error case unreachable binary" severity failure;
                  end case;
                  
               --! if the character after the % is non of the valid chars ('b', 'd', 'h') than
               --! display an error.                  
               else
                  errorId := 2;
               end if;
            
            --! user has written % and a valid char ('b', 'd', 'h') but has forgot to add an
            --! argument (printf("asd %d"))
            else
               errorId := 1;
            end if;
            
         --! if slash char '\'   
         elsif(str_i(charIndex) = '\' and lastChar = false) then 
            
            --! increment charIndex to check the next character
            charIndex := charIndex + 1;
            
            --! if next character is 'n' write newline
            if (str_i(charIndex) = 'n') then
               writeline(OUTPUT, lineBuff);
               
            --! if next character is '@' write current time
            elsif(str_i(charIndex) = '@') then
               write(lineBuff, time'IMAGE(now));
               
            --! if next character is 't' write tabulator
            elsif(str_i(charIndex) = 't') then
               write(lineBuff, string'("   "));
            
            --! if no valid character after '\'               
            else
               errorId := 3;
            end if;
            
         --! if no special character write string
         else
            write(lineBuff, str_i(charIndex));
         end if;
         
         --! check for error
         if (errorId > 0) then
            
            --! display error ID
            writeline(OUTPUT, lineBuff);
            write(lineBuff, string'("ERROR ID :"));
            write(lineBuff, errorId);
            writeline(OUTPUT, lineBuff);
            
            --! display error message
            case(errorId) is
              when 1 => write(lineBuff, string'("Missing argument (printf(%d, MISSING_ARGUMENT))"));
              when 2 => write(lineBuff, string'("Non valid character after the %."));
              when 3 => write(lineBuff, string'("Non valid character after the /."));
              when others => report "Error case unreachable " severity failure;
            end case;
            
            exit;
         end if;
         
         --! increment string counter
         charIndex := charIndex + 1;

      end loop l_main;
      
      writeline(OUTPUT, lineBuff);
     
   end printf;

---------------------------------------------------------------------------------------------------
--  Overrides
    
   procedure printf(str_i: string; slv0_i, slv1_i, slv2_i, slv3_i, slv4_i : slv) is
   begin 
      printf(str_i, slv0_i, slv1_i, slv2_i, slv3_i, slv4_i, 5);
   end printf;
   
   procedure printf(str_i: string; slv0_i, slv1_i, slv2_i, slv3_i : slv) is
      variable v_std_dummy : slv(0 downto 0);
   begin 
      v_std_dummy := (others => '0');
      
      --! (others => '0') will crash compiler, that is why v_std_dummy
      printf(str_i, slv0_i, slv1_i, slv2_i, slv3_i, v_std_dummy, 4);
      
   end printf;
   
   procedure printf(str_i: string; slv0_i, slv1_i, slv2_i : slv) is
      variable v_std_dummy : slv(0 downto 0);
   begin 
      v_std_dummy := (others => '0');
      
      --! (others => '0') will crash compiler, that is why v_std_dummy
      printf(str_i, slv0_i, slv1_i, slv2_i, v_std_dummy, v_std_dummy, 3);
      
   end printf;
   
   procedure printf(str_i: string; slv0_i, slv1_i : slv) is
      variable v_std_dummy : slv(0 downto 0);
   begin 
      v_std_dummy := (others => '0');
      
      --! (others => '0') will crash compiler, that is why v_std_dummy
      printf(str_i, slv0_i, slv1_i, v_std_dummy, v_std_dummy, v_std_dummy, 2);
      
   end printf;
   
   procedure printf(str_i: string; slv0_i : slv) is
      variable v_std_dummy : slv(0 downto 0);
   begin 
      v_std_dummy := (others => '0');
      
      --! (others => '0') will crash compiler, that is why v_std_dummy
      printf(str_i, slv0_i, v_std_dummy, v_std_dummy, v_std_dummy, v_std_dummy, 1);
      
   end printf;
   
   procedure printf(str_i: string) is
      variable v_std_dummy : slv(0 downto 0);
   begin 
      v_std_dummy := (others => '0');
      
      --! (others => '0') will crash compiler, that is why v_std_dummy
      printf(str_i, v_std_dummy, v_std_dummy, v_std_dummy, v_std_dummy, v_std_dummy, 0);
      
   end printf;
   

end CslPrintfPkg;