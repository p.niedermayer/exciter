---------------------------------------------------------------------------------------------------
--! @brief  Core module, connecting averaging module, pid and repeater
--! @details 
--!   Structural module.
--!   
--!   
--! @author Blaz Kelbl, Cosylab (blaz.kelbl@cosylab.com)
--!
--! @date Jun 27 2023 created
--! @date Jun 27 2023 last modify
--!
--! @version v0.1
--!
--! @file PidFloatCore.vhd
---------------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.CslStdRtlPkg.all;
use work.CslAxiPkg.all;
---------------------------------------------------------------------------------------------------
entity PidFloatCore is
   generic (
      TPD_G          : time    := 1 ns; --! Simulation delta time offset
      DATA_WIDTH_G   : natural := 16;
      TA_WIDTH_G     : natural := 16;
      OUTPUT_WIDTH_G : natural := 16
   );
   port (
      active_i              : in  sl;
      ta_i                  : in  slv(TA_WIDTH_G - 1 downto 0);
      kp_i                  : in  slv(31 downto 0);
      kiTimesTa_i           : in  slv(31 downto 0);
      kdOverTa_i            : in  slv(31 downto 0);
      target_i              : in  slv(31 downto 0);
      feedback_i            : in  sl;
      feedForward_i         : in  sl;
      feedForwardConstant_i : in  slv(31 downto 0);
      outMin_i              : in  slv(31 downto 0);
      outMax_i              : in  slv(31 downto 0);
      outScale_i            : in  slv(31 downto 0);
      repeat_i              : in  slv(7 downto 0);
      sampleInAxisMaster_i  : in  AxiStreamMasterType;
      sampleInAxisSlave_o   : out AxiStreamSlaveType;
      sampleOutAxisMaster_o : out AxiStreamMasterType;
      sampleOutAxisSlave_i  : in  AxiStreamSlaveType;
      floatOut_o            : out slv(31 downto 0);
      floatValid_o          : out sl;
      clk_i                 : in  sl; --! Global clock  
      rst_i                 : in  sl  --! Global reset
   );
end PidFloatCore;

architecture rtl of PidFloatCore is

   signal avgInAxisMaster  : AxiStreamMasterType;
   signal avgInAxisSlave   : AxiStreamSlaveType;
   signal avgOutAxisMaster : AxiStreamMasterType;
   signal avgOutAxisSlave  : AxiStreamSlaveType;
   signal pidOutAxisMaster : AxiStreamMasterType;
   signal pidOutAxisSlave  : AxiStreamSlaveType;
   signal rptOutAxisMaster : AxiStreamMasterType;
   signal rptOutAxisSlave  : AxiStreamSlaveType;

begin

   i_avg : entity work.SampleAveragingFloat
      generic map (
         TPD_G        => TPD_G,
         DATA_WIDTH_G => DATA_WIDTH_G,
         TA_WIDTH_G   => TA_WIDTH_G
      )
      port map (
         ta_i                  => ta_i,
         sampleInAxisMaster_i  => avgInAxisMaster,
         sampleInAxisSlave_o   => avgInAxisSlave,
         sampleOutAxisMaster_o => avgOutAxisMaster,
         sampleOutAxisSlave_i  => avgOutAxisSlave,
         clk_i                 => clk_i,
         rst_i                 => rst_i
      );

   i_pid : entity work.PidFloatSequential
      generic map (
         TPD_G          => TPD_G,
         OUTPUT_WIDTH_G => OUTPUT_WIDTH_G
      )
      port map (
         active_i              => active_i,
         kp_i                  => kp_i,
         kiTimesTa_i           => kiTimesTa_i,
         kdOverTa_i            => kdOverTa_i,
         target_i              => target_i,
         feedback_i            => feedback_i,
         feedForward_i         => '0',
         feedForwardConstant_i => feedForwardConstant_i,
         outMin_i              => outMin_i,
         outMax_i              => outMax_i,
         outScale_i            => outScale_i,
         sampleInAxisMaster_i  => avgOutAxisMaster,
         sampleInAxisSlave_o   => avgOutAxisSlave,
         sampleOutAxisMaster_o => pidOutAxisMaster,
         sampleOutAxisSlave_i  => pidOutAxisSlave,
         floatOut_o            => open,
         floatValid_o          => open,
         clk_i                 => clk_i,
         rst_i                 => rst_i
      );

   i_ioh : entity work.InOutHandler
      generic map (
         TPD_G          => TPD_G,
         OUTPUT_WIDTH_G => OUTPUT_WIDTH_G,
         TA_WIDTH_G     => TA_WIDTH_G
      )
      port map (
         ta_i                  => ta_i,
         active_i              => active_i,
         kp_i                  => kp_i,
         kiTimesTa_i           => kiTimesTa_i,
         kdOverTa_i            => kdOverTa_i,
         target_i              => target_i,
         feedback_i            => feedback_i,
         feedForward_i         => feedForward_i,
         feedForwardConstant_i => feedForwardConstant_i,
         outMin_i              => outMin_i,
         outMax_i              => outMax_i,
         outScale_i            => outScale_i,
         sampleInAxisMaster_i  => sampleInAxisMaster_i,
         sampleInAxisSlave_o   => sampleInAxisSlave_o,
         pidInAxisMaster_i     => pidOutAxisMaster,
         pidInAxisSlave_o      => pidOutAxisSlave,
         avgOutAxisMaster_o    => avgInAxisMaster,
         avgOutAxisSlave_i     => avgInAxisSlave,
         sampleOutAxisMaster_o => rptOutAxisMaster,
         sampleOutAxisSlave_i  => rptOutAxisSlave,
         floatOut_o            => floatOut_o,
         floatValid_o          => floatValid_o,
         clk_i                 => clk_i,
         rst_i                 => rst_i
      );

   i_rpt : entity work.OutputRepeater
      generic map (
         TPD_G => TPD_G
      )
      port map (
         ta_i                  => slv(to_unsigned(1,16)),
         repeat_i              => repeat_i,
         sampleInAxisMaster_i  => rptOutAxisMaster,
         sampleInAxisSlave_o   => rptOutAxisSlave,
         sampleOutAxisMaster_o => sampleOutAxisMaster_o,
         sampleOutAxisSlave_i  => sampleOutAxisSlave_i,
         clk_i                 => clk_i,
         rst_i                 => rst_i
      );

end rtl;
