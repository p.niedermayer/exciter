---------------------------------------------------------------------------------------------------
--! @brief  Axis valid data repeater
--! @details 
--!   TODO
--!   
--!   
--! @author Jernej Kokalj, Cosylab (jernej.kokalj@cosylab.com)
--!
--! @date Jun 27 2023 created
--! @date Jun 27 2023 last modify
--!
--! @version v0.1
--!
--! @file OutputRepeater.vhd
---------------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.CslStdRtlPkg.all;
use work.CslAxiPkg.all;
---------------------------------------------------------------------------------------------------
entity OutputRepeater is
   generic (
      TPD_G : time := 1 ns --! Simulation delta time offset
   );
   port (
      ta_i                  : in  slv(15 downto 0);
      repeat_i              : in  slv(7 downto 0);
      sampleInAxisMaster_i  : in  AxiStreamMasterType;
      sampleInAxisSlave_o   : out AxiStreamSlaveType;
      sampleOutAxisMaster_o : out AxiStreamMasterType;
      sampleOutAxisSlave_i  : in  AxiStreamSlaveType;
      clk_i                 : in  sl; --! Global clock  
      rst_i                 : in  sl  --! Global reset
   );
end OutputRepeater;

architecture rtl of OutputRepeater is

   type StateType is
      (
         IDLE_S,
         REPEAT_S
      );

   type RegType is record
      state         : StateType;
      ta            : unsigned(15 downto 0);
      repeat        : unsigned(15 downto 0);
      data          : slv(15 downto 0);
      cnt           : unsigned(31 downto 0);
      repeatLimit   : unsigned(31 downto 0);
      inAxisSlave   : AxiStreamSlaveType;
      outAxisMaster : AxiStreamMasterType;
   end record RegType;

   constant REG_INIT_C : RegType := (
         state         => IDLE_S,
         ta            => (others => '0'),
         repeat        => (others => '0'),
         data          => (others => '0'),
         cnt           => (others => '0'),
         repeatLimit   => (others => '0'),
         inAxisSlave   => AXI_STREAM_SLAVE_INIT_C,
         outAxisMaster => AXI_STREAM_MASTER_INIT_C
      );

   signal r,rin : RegType;

begin

   comb : process(
      r,

      ta_i,
      repeat_i,

      sampleInAxisMaster_i,
      sampleOutAxisSlave_i,

      rst_i
      )
      variable v : RegType;
   begin
      v := r;

      v.outAxisMaster      := AXI_STREAM_MASTER_INIT_C;
      v.inAxisSlave.tReady := '0';

      case r.state is
         when IDLE_S =>
            v.inAxisSlave.tReady := '1';
            v.ta                 := unsigned(ta_i);
            v.repeat(7 downto 0) := unsigned(repeat_i);

            if repeat_i = slv(to_unsigned(0,8)) then
               v.repeatLimit := unsigned(resize(ta_i,32));
            else
               v.repeatLimit := unsigned(ta_i) * unsigned(resize(repeat_i,16));
            end if;

            v.data := sampleInAxisMaster_i.tData(15 downto 0);
            v.cnt  := (others => '0');

            if sampleInAxisMaster_i.tValid = '1' then
               v.state := REPEAT_S;
            end if;

         when REPEAT_S =>
            v.outAxisMaster.tValid             := '1';
            v.outAxisMaster.tdata(15 downto 0) := r.data;

            if sampleOutAxisSlave_i.tReady = '1' then
               v.cnt := r.cnt + 1;
            end if;

            if r.cnt >= r.repeatLimit - 1 or r.repeatLimit = to_unsigned(0,32) then
               v.outAxisMaster.tLast := '1';
               v.state               := IDLE_S;

            end if;
      end case;

      -- Reset
      if (rst_i = '1') then
         v := REG_INIT_C;
      end if;

      rin <= v;

      sampleInAxisSlave_o   <= v.inAxisSlave;
      sampleOutAxisMaster_o <= v.outAxisMaster;

   end process comb;

   --! @brief Sequential process
   --! @details Assign rin to r on rising edge of clk to create registers
   seq : process (clk_i) is
   begin
      if (rising_edge(clk_i)) then
         r <= rin after TPD_G;
      end if;
   end process seq;

end rtl;