---------------------------------------------------------------------------------------------------
--! @brief  PID by specification of dr. Philipp Niedermayer (v1)
--! @details 
--!   TODO
--!   
--!   
--! @author Blaz Kelbl, Cosylab (blaz.kelbl@cosylab.com)
--!
--! @date Jun 27 2023 created
--! @date Jun 27 2023 last modify
--!
--! @version v0.1
--!
--! @file InOutHandler.vhd
---------------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.CslStdRtlPkg.all;
use work.CslAxiPkg.all;
---------------------------------------------------------------------------------------------------
entity InOutHandler is
   generic (
      TPD_G          : time    := 1 ns; --! Simulation delta time offset
      OUTPUT_WIDTH_G : natural := 16;
      TA_WIDTH_G     : natural := 16
   );
   port (
      active_i              : in  sl;
      ta_i                  : in  slv(TA_WIDTH_G - 1 downto 0);
      kp_i                  : in  slv(31 downto 0);
      kiTimesTa_i           : in  slv(31 downto 0);
      kdOverTa_i            : in  slv(31 downto 0);
      target_i              : in  slv(31 downto 0);
      feedback_i            : in  sl;
      feedForward_i         : in  sl;
      feedForwardConstant_i : in  slv(31 downto 0);
      outMin_i              : in  slv(31 downto 0);
      outMax_i              : in  slv(31 downto 0);
      outScale_i            : in  slv(31 downto 0);
      sampleInAxisMaster_i  : in  AxiStreamMasterType;
      sampleInAxisSlave_o   : out AxiStreamSlaveType;
      pidInAxisMaster_i     : in  AxiStreamMasterType;
      pidInAxisSlave_o      : out AxiStreamSlaveType;
      avgOutAxisMaster_o    : out AxiStreamMasterType;
      avgOutAxisSlave_i     : in  AxiStreamSlaveType;
      sampleOutAxisMaster_o : out AxiStreamMasterType;
      sampleOutAxisSlave_i  : in  AxiStreamSlaveType;
      floatOut_o            : out slv(31 downto 0);
      floatValid_o          : out sl;
      clk_i                 : in  sl; --! Global clock  
      rst_i                 : in  sl  --! Global reset
   );
end InOutHandler;

architecture rtl of InOutHandler is

   COMPONENT FloatAdder
      PORT (
         aclk                    : IN  STD_LOGIC;
         s_axis_a_tvalid         : IN  STD_LOGIC;
         s_axis_a_tready         : OUT STD_LOGIC;
         s_axis_a_tdata          : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
         s_axis_b_tvalid         : IN  STD_LOGIC;
         s_axis_b_tready         : OUT STD_LOGIC;
         s_axis_b_tdata          : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
         s_axis_operation_tvalid : IN  STD_LOGIC;
         s_axis_operation_tready : OUT STD_LOGIC;
         s_axis_operation_tdata  : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         m_axis_result_tvalid    : OUT STD_LOGIC;
         m_axis_result_tready    : IN  STD_LOGIC;
         m_axis_result_tdata     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
   END COMPONENT;
   COMPONENT FloatMultiplier
      PORT (
         aclk                 : IN  STD_LOGIC;
         s_axis_a_tvalid      : IN  STD_LOGIC;
         s_axis_a_tready      : OUT STD_LOGIC;
         s_axis_a_tdata       : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
         s_axis_b_tvalid      : IN  STD_LOGIC;
         s_axis_b_tready      : OUT STD_LOGIC;
         s_axis_b_tdata       : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
         m_axis_result_tvalid : OUT STD_LOGIC;
         m_axis_result_tready : IN  STD_LOGIC;
         m_axis_result_tdata  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
   END COMPONENT;
   COMPONENT FloatCompareLessThan
      PORT (
         aclk                 : IN  STD_LOGIC;
         s_axis_a_tvalid      : IN  STD_LOGIC;
         s_axis_a_tready      : OUT STD_LOGIC;
         s_axis_a_tdata       : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
         s_axis_b_tvalid      : IN  STD_LOGIC;
         s_axis_b_tready      : OUT STD_LOGIC;
         s_axis_b_tdata       : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
         m_axis_result_tvalid : OUT STD_LOGIC;
         m_axis_result_tready : IN  STD_LOGIC;
         m_axis_result_tdata  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
      );
   END COMPONENT;
   COMPONENT FloatToFixed_S16_16
      PORT (
         aclk                 : IN  STD_LOGIC;
         s_axis_a_tvalid      : IN  STD_LOGIC;
         s_axis_a_tready      : OUT STD_LOGIC;
         s_axis_a_tdata       : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
         m_axis_result_tvalid : OUT STD_LOGIC;
         m_axis_result_tready : IN  STD_LOGIC;
         m_axis_result_tdata  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
      );
   END COMPONENT;
   function comparatorTrue(value : std_logic_vector) return boolean is
      variable returnValue : boolean;
   begin
      if value(5 downto 0) /= "000000" then
         returnValue := true;
      else
         returnValue := false;
      end if;
      return returnValue;
   end function comparatorTrue;

   type AxiSType32 is record
      valid : sl;
      data  : slv(31 downto 0);
   end record;

   type AxiSType16 is record
      valid : sl;
      data  : slv(15 downto 0);
   end record;

   type AxiSType8 is record
      valid : sl;
      data  : slv(7 downto 0);
   end record;

   signal adderA               : AxiSType32;
   signal adderA_ready         : sl;
   signal adderB               : AxiSType32;
   signal adderB_ready         : sl;
   signal adderOperation       : AxiSType8;
   signal adderOperation_ready : sl;
   signal adderResult          : AxiSType32;
   signal adderResult_ready    : sl;

   constant ADDER_OPERATION_ADD_C : slv(adderOperation.data'range) := X"00";
   constant ADDER_OPERATION_SUB_C : slv(adderOperation.data'range) := X"01";

   signal multiplierA            : AxiSType32;
   signal multiplierA_ready      : sl;
   signal multiplierB            : AxiSType32;
   signal multiplierB_ready      : sl;
   signal multiplierResult       : AxiSType32;
   signal multiplierResult_ready : sl;

   signal comparatorLessThanA            : AxiSType32;
   signal comparatorLessThanA_ready      : sl;
   signal comparatorLessThanB            : AxiSType32;
   signal comparatorLessThanB_ready      : sl;
   signal comparatorLessThanResult       : AxiSType8;
   signal comparatorLessThanResult_ready : sl;

   signal floatToFixed             : AxiSType32;
   signal floatToFixed_ready       : sl;
   signal floatToFixedResult       : AxiSType16;
   signal floatToFixedResult_ready : sl;


   subtype FloatType is std_logic_vector(31 downto 0);

   type StateType is
      (
         IDLE_S,
         FORWARD_SAMPLE_S,
         WAIT_PID_S,
         ADD_PID_FF_S,
         WAIT_ADD_PID_FF_S,
         COMPARE_TO_MIN_S,
         COMPARE_TO_MAX_S,
         WAIT_COMPARISON_TO_MIN_S,
         WAIT_COMPARISON_TO_MAX_S,
         SCALE_OUTPUT_S,
         WAIT_OUTPUT_SCALING_S,
         CONVERT_TO_FIXED_POINT_S,
         WAIT_CONVERSION_TO_FIXED_POINT_S,
         PUSH_RESULT_S
      );

   type RegType is record
      state            : StateType;
      actual           : FloatType;
      errorCurrent     : FloatType;
      errorDiff        : FloatType;
      errorPrevious    : FloatType;
      errorSum         : FloatType;
      errorSumPrevious : FloatType;
      pTerm            : FloatType;
      iTerm            : FloatType;
      dTerm            : FloatType;
      outCalc          : FloatType;
      tempFloat        : FloatType;
      pidData          : FloatType;
      count            : slv(31 downto 0);
      outInteger       : slv(OUTPUT_WIDTH_G - 1 downto 0);
   end record RegType;

   constant REG_INIT_C : RegType := (
         state            => IDLE_S,
         actual           => (others => '0'),
         errorCurrent     => (others => '0'),
         errorDiff        => (others => '0'),
         errorPrevious    => (others => '0'),
         errorSum         => (others => '0'),
         errorSumPrevious => (others => '0'),
         pTerm            => (others => '0'),
         iTerm            => (others => '0'),
         dTerm            => (others => '0'),
         outCalc          => (others => '0'),
         tempFloat        => (others => '0'),
         pidData          => (others => '0'),
         count            => (others => '0'),
         outInteger       => (others => '0')
      );


   signal r,rin : RegType;

begin
   adderResult_ready              <= '1';
   multiplierResult_ready         <= '1';
   comparatorLessThanResult_ready <= '1';
   floatToFixedResult_ready       <= '1';

   adderB.valid              <= adderA.valid;
   adderOperation.valid      <= adderA.valid;
   multiplierB.valid         <= multiplierA.valid;
   comparatorLessThanB.valid <= comparatorLessThanA.valid ;

   comb : process(
         r,

         adderA_ready,
         adderB_ready,
         adderOperation_ready,
         adderResult,
         multiplierA_ready,
         multiplierB_ready,
         multiplierResult,
         comparatorLessThanA_ready,
         comparatorLessThanB_ready,
         comparatorLessThanResult,
         floatToFixed_ready,
         floatToFixedResult,

         active_i,
         ta_i,
         kp_i,
         kiTimesTa_i,
         kdOverTa_i,
         target_i,
         feedback_i,
         feedForward_i,
         feedForwardConstant_i,
         outMin_i,
         outMax_i,
         outScale_i,
         sampleInAxisMaster_i,
         pidInAxisMaster_i,
         avgOutAxisSlave_i,
         sampleOutAxisSlave_i,

         rst_i
      )
      variable v : RegType;
   begin
      v := r;
      -- defaults
      sampleInAxisSlave_o.tReady <= '0';
      pidInAxisSlave_o.tReady    <= '0';

      avgOutAxisMaster_o       <= AXI_STREAM_MASTER_INIT_C;
      avgOutAxisMaster_o.tData <= sampleInAxisMaster_i.tdata;

      sampleOutAxisMaster_o                           <= AXI_STREAM_MASTER_INIT_C;
      sampleOutAxisMaster_o.tData(r.outInteger'range) <= r.outInteger;

      adderA.valid              <= '0';
      multiplierA.valid         <= '0';
      comparatorLessThanA.valid <= '0';
      floatToFixed.valid        <= '0';

      adderA.data              <= (others => 'U');
      adderB.data              <= (others => 'U');
      adderOperation.data      <= ADDER_OPERATION_ADD_C;
      multiplierA.data         <= (others => 'U');
      multiplierB.data         <= (others => 'U');
      comparatorLessThanA.data <= (others => 'U');
      comparatorLessThanB.data <= (others => 'U');
      floatToFixed.data        <= (others => 'U');
      floatOut_o               <= (others => '0');
      floatValid_o             <= '0';
      
      case r.state is

         when IDLE_S =>
            v.state   := FORWARD_SAMPLE_S;
            v.outCalc := (others => '0');

         when FORWARD_SAMPLE_S =>
            sampleInAxisSlave_o.tReady <= avgOutAxisSlave_i.tReady;
            avgOutAxisMaster_o.tValid  <= sampleInAxisMaster_i.tValid;

            if feedForward_i = '1' then
               v.outCalc := feedForwardConstant_i;
            end if;

            if sampleInAxisMaster_i.tValid = '1' and avgOutAxisSlave_i.tReady = '1' then
               v.count := slv(unsigned(r.count) + 1);
               v.state := ADD_PID_FF_S;
            end if;

         when WAIT_PID_S =>
            pidInAxisSlave_o.tReady <= '1';
            v.pidData               := pidInAxisMaster_i.tData(31 downto 0);
            if pidInAxisMaster_i.tValid = '1' then
               v.state := IDLE_S;
            end if;

         when ADD_PID_FF_S =>
            adderA.valid        <= '1';
            adderA.data         <= r.outCalc;
            adderB.data         <= r.pidData;
            adderOperation.data <= ADDER_OPERATION_ADD_C;

            if adderA_ready = '1' then
               v.state := WAIT_ADD_PID_FF_S;
            end if;

         when WAIT_ADD_PID_FF_S =>
            if adderResult.valid = '1' then
               v.outCalc := adderResult.data;
               v.state   := COMPARE_TO_MIN_S;
            end if;

         when COMPARE_TO_MIN_S =>
            comparatorLessThanA.data  <= r.outCalc;
            comparatorLessThanB.data  <= outMin_i;
            comparatorLessThanA.valid <= '1';
            if comparatorLessThanA_ready = '1' then
               v.state := COMPARE_TO_MAX_S;
            end if;

         when COMPARE_TO_MAX_S =>
            comparatorLessThanA.data  <= outMax_i;
            comparatorLessThanB.data  <= r.outCalc;
            comparatorLessThanA.valid <= '1';
            if comparatorLessThanA_ready = '1' then
               v.state := WAIT_COMPARISON_TO_MIN_S;
            end if;

         when WAIT_COMPARISON_TO_MIN_S =>
            if comparatorLessThanResult.valid = '1' then
               v.state := WAIT_COMPARISON_TO_MAX_S;
               if comparatorTrue(comparatorLessThanResult.data) then
                  v.outCalc  := outMin_i;
                  v.errorSum := r.errorSumPrevious;
               end if;
            end if;

         when WAIT_COMPARISON_TO_MAX_S =>
            if comparatorLessThanResult.valid = '1' then
               v.state := SCALE_OUTPUT_S;
               if comparatorTrue(comparatorLessThanResult.data) then
                  v.outCalc  := outMax_i;
                  v.errorSum := r.errorSumPrevious;
               end if;
            end if;

         when SCALE_OUTPUT_S =>
            multiplierA.valid <= '1';
            multiplierA.data  <= r.outCalc;
            multiplierB.data  <= outScale_i;
            if multiplierA_ready = '1' then
               v.state := WAIT_OUTPUT_SCALING_S;
            end if;

         when WAIT_OUTPUT_SCALING_S =>
            if multiplierResult.valid = '1' then
               v.outCalc    := multiplierResult.data;
               floatOut_o   <= multiplierResult.data;
               floatValid_o <= '1';
               v.state      := CONVERT_TO_FIXED_POINT_S;
            end if;

         when CONVERT_TO_FIXED_POINT_S =>
            floatToFixed.valid <= '1';
            floatToFixed.data  <= r.outCalc;
            if floatToFixed_ready = '1' then
               v.state := WAIT_CONVERSION_TO_FIXED_POINT_S;
            end if;

         when WAIT_CONVERSION_TO_FIXED_POINT_S =>
            if floatToFixedResult.valid = '1' then
               v.outInteger := floatToFixedResult.data(v.outInteger'range);
               v.state      := PUSH_RESULT_S;
            end if;

         when PUSH_RESULT_S =>
            sampleOutAxisMaster_o.tValid <= '1';
            if sampleOutAxisSlave_i.tReady = '1' then
               v.state := IDLE_S;
               if unsigned(r.count(ta_i'range)) >= unsigned(ta_i) then
                  v.state := WAIT_PID_S;
                  v.count := (others => '0');
               end if;
            end if;
      end case;

      if active_i = '0' then
         v.outInteger := (others => '0');
      end if;

      -- Reset
      if (rst_i = '1') then
         v := REG_INIT_C;
      end if;
      rin <= v;

   end process comb;

   --! @brief Sequential process
   --! @details Assign rin to r on rising edge of clk to create registers
   seq : process (clk_i) is
   begin
      if (rising_edge(clk_i)) then
         r <= rin after TPD_G;
      end if;
   end process seq;

   i_adder : FloatAdder
      PORT MAP (
         aclk                    => clk_i,
         s_axis_a_tvalid         => adderA.valid,
         s_axis_a_tready         => adderA_ready,
         s_axis_a_tdata          => adderA.data,
         s_axis_b_tvalid         => adderB.valid,
         s_axis_b_tready         => adderB_ready,
         s_axis_b_tdata          => adderB.data,
         s_axis_operation_tvalid => adderOperation.valid,
         s_axis_operation_tready => adderOperation_ready,
         s_axis_operation_tdata  => adderOperation.data,
         m_axis_result_tvalid    => adderResult.valid,
         m_axis_result_tready    => adderResult_ready,
         m_axis_result_tdata     => adderResult.data
      );

   i_multiplier : FloatMultiplier
      PORT MAP (
         aclk                 => clk_i,
         s_axis_a_tvalid      => multiplierA.valid,
         s_axis_a_tready      => multiplierA_ready,
         s_axis_a_tdata       => multiplierA.data,
         s_axis_b_tvalid      => multiplierB.valid,
         s_axis_b_tready      => multiplierB_ready,
         s_axis_b_tdata       => multiplierB.data,
         m_axis_result_tvalid => multiplierResult.valid,
         m_axis_result_tready => multiplierResult_ready,
         m_axis_result_tdata  => multiplierResult.data
      );

   i_comparatorLessThan : FloatCompareLessThan
      PORT MAP (
         aclk                 => clk_i,
         s_axis_a_tvalid      => comparatorLessThanA.valid,
         s_axis_a_tready      => comparatorLessThanA_ready,
         s_axis_a_tdata       => comparatorLessThanA.data,
         s_axis_b_tvalid      => comparatorLessThanB.valid,
         s_axis_b_tready      => comparatorLessThanB_ready,
         s_axis_b_tdata       => comparatorLessThanB.data,
         m_axis_result_tvalid => comparatorLessThanResult.valid,
         m_axis_result_tready => comparatorLessThanResult_ready,
         m_axis_result_tdata  => comparatorLessThanResult.data
      );

   i_floatToFixed : FloatToFixed_S16_16
      PORT MAP (
         aclk                 => clk_i,
         s_axis_a_tvalid      => floatToFixed.valid,
         s_axis_a_tready      => floatToFixed_ready,
         s_axis_a_tdata       => floatToFixed.data,
         m_axis_result_tvalid => floatToFixedResult.valid,
         m_axis_result_tready => floatToFixedResult_ready,
         m_axis_result_tdata  => floatToFixedResult.data
      );
end rtl;