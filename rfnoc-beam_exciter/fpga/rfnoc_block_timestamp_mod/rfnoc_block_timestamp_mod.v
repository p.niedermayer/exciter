//
// Copyright 2023 Ettus Research, a National Instruments Brand
//
// SPDX-License-Identifier: LGPL-3.0-or-later
//
// Module: rfnoc_block_timestamp_mod
//
// Description:
//
//   A block template
//
// Parameters:
//
//   NOC_ID      : The block's NOC ID
//   THIS_PORTID : Control crossbar port to which this block is connected
//   CHDR_W      : AXIS-CHDR data bus width
//   MTU         : Maximum transmission unit (i.e., maximum packet size in
//                 CHDR words is 2**MTU).
//

`default_nettype none

module rfnoc_block_timestamp_mod #(
  parameter [31:0] NOC_ID          = 32'h06428902,
  parameter [ 9:0] THIS_PORTID     = 10'd0,
  parameter        CHDR_W          = 64,
  parameter [ 5:0] MTU             = 10
)(
  // RFNoC Framework Clocks and Resets
  input  wire                   rfnoc_chdr_clk,
  input  wire                   rfnoc_ctrl_clk,
  // RFNoC Backend Interface
  input  wire [511:0]           rfnoc_core_config,
  output wire [511:0]           rfnoc_core_status,
  // AXIS-CHDR Input Ports (from framework)
  input  wire [(1)*CHDR_W-1:0] s_rfnoc_chdr_tdata,
  input  wire [(1)-1:0]        s_rfnoc_chdr_tlast,
  input  wire [(1)-1:0]        s_rfnoc_chdr_tvalid,
  output wire [(1)-1:0]        s_rfnoc_chdr_tready,
  // AXIS-CHDR Output Ports (to framework)
  output wire [(1)*CHDR_W-1:0] m_rfnoc_chdr_tdata,
  output wire [(1)-1:0]        m_rfnoc_chdr_tlast,
  output wire [(1)-1:0]        m_rfnoc_chdr_tvalid,
  input  wire [(1)-1:0]        m_rfnoc_chdr_tready,
  // AXIS-Ctrl Input Port (from framework)
  input  wire [31:0]            s_rfnoc_ctrl_tdata,
  input  wire                   s_rfnoc_ctrl_tlast,
  input  wire                   s_rfnoc_ctrl_tvalid,
  output wire                   s_rfnoc_ctrl_tready,
  // AXIS-Ctrl Output Port (to framework)
  output wire [31:0]            m_rfnoc_ctrl_tdata,
  output wire                   m_rfnoc_ctrl_tlast,
  output wire                   m_rfnoc_ctrl_tvalid,
  input  wire                   m_rfnoc_ctrl_tready
);

  //---------------------------------------------------------------------------
  // Signal Declarations
  //---------------------------------------------------------------------------

  // Clocks and Resets
  wire               ctrlport_clk;
  wire               ctrlport_rst;
  wire               axis_data_clk;
  wire               axis_data_rst;
  // CtrlPort Master
  wire               m_ctrlport_req_wr;
  wire               m_ctrlport_req_rd;
  wire [19:0]        m_ctrlport_req_addr;
  wire [31:0]        m_ctrlport_req_data;
  wire               m_ctrlport_resp_ack;
  wire [31:0]        m_ctrlport_resp_data;
  // Data Stream to User Logic: in
  wire [32*1-1:0]    m_in_axis_tdata;
  wire [1-1:0]       m_in_axis_tkeep;
  wire               m_in_axis_tlast;
  wire               m_in_axis_tvalid;
  wire               m_in_axis_tready;
  wire [63:0]        m_in_axis_ttimestamp;
  wire               m_in_axis_thas_time;
  wire [15:0]        m_in_axis_tlength;
  wire               m_in_axis_teov;
  wire               m_in_axis_teob;
  // Data Stream from User Logic: out
  wire [32*1-1:0]    s_out_axis_tdata;
  wire [0:0]         s_out_axis_tkeep;
  wire               s_out_axis_tlast;
  wire               s_out_axis_tvalid;
  wire               s_out_axis_tready;
  wire [63:0]        s_out_axis_ttimestamp;
  wire               s_out_axis_thas_time;
  wire [15:0]        s_out_axis_tlength;
  wire               s_out_axis_teov;
  wire               s_out_axis_teob;

  //---------------------------------------------------------------------------
  // NoC Shell
  //---------------------------------------------------------------------------

  noc_shell_timestamp_mod #(
    .NOC_ID              (NOC_ID),
    .CHDR_W              (CHDR_W),
    .THIS_PORTID         (THIS_PORTID),
    .MTU                 (MTU)
  ) noc_shell_timestamp_mod_i (
    //---------------------
    // Framework Interface
    //---------------------

    // Clock Inputs
    .rfnoc_chdr_clk      (rfnoc_chdr_clk),
    .rfnoc_ctrl_clk      (rfnoc_ctrl_clk),
    // Reset Outputs
    .rfnoc_chdr_rst      (),
    .rfnoc_ctrl_rst      (),
    // RFNoC Backend Interface
    .rfnoc_core_config   (rfnoc_core_config),
    .rfnoc_core_status   (rfnoc_core_status),
    // CHDR Input Ports  (from framework)
    .s_rfnoc_chdr_tdata  (s_rfnoc_chdr_tdata),
    .s_rfnoc_chdr_tlast  (s_rfnoc_chdr_tlast),
    .s_rfnoc_chdr_tvalid (s_rfnoc_chdr_tvalid),
    .s_rfnoc_chdr_tready (s_rfnoc_chdr_tready),
    // CHDR Output Ports (to framework)
    .m_rfnoc_chdr_tdata  (m_rfnoc_chdr_tdata),
    .m_rfnoc_chdr_tlast  (m_rfnoc_chdr_tlast),
    .m_rfnoc_chdr_tvalid (m_rfnoc_chdr_tvalid),
    .m_rfnoc_chdr_tready (m_rfnoc_chdr_tready),
    // AXIS-Ctrl Input Port (from framework)
    .s_rfnoc_ctrl_tdata  (s_rfnoc_ctrl_tdata),
    .s_rfnoc_ctrl_tlast  (s_rfnoc_ctrl_tlast),
    .s_rfnoc_ctrl_tvalid (s_rfnoc_ctrl_tvalid),
    .s_rfnoc_ctrl_tready (s_rfnoc_ctrl_tready),
    // AXIS-Ctrl Output Port (to framework)
    .m_rfnoc_ctrl_tdata  (m_rfnoc_ctrl_tdata),
    .m_rfnoc_ctrl_tlast  (m_rfnoc_ctrl_tlast),
    .m_rfnoc_ctrl_tvalid (m_rfnoc_ctrl_tvalid),
    .m_rfnoc_ctrl_tready (m_rfnoc_ctrl_tready),

    //---------------------
    // Client Interface
    //---------------------

    // CtrlPort Clock and Reset
    .ctrlport_clk              (ctrlport_clk),
    .ctrlport_rst              (ctrlport_rst),
    // CtrlPort Master
    .m_ctrlport_req_wr         (m_ctrlport_req_wr),
    .m_ctrlport_req_rd         (m_ctrlport_req_rd),
    .m_ctrlport_req_addr       (m_ctrlport_req_addr),
    .m_ctrlport_req_data       (m_ctrlport_req_data),
    .m_ctrlport_resp_ack       (m_ctrlport_resp_ack),
    .m_ctrlport_resp_data      (m_ctrlport_resp_data),

    // AXI-Stream Clock and Reset
    .axis_data_clk (axis_data_clk),
    .axis_data_rst (axis_data_rst),
    // Data Stream to User Logic: in
    .m_in_axis_tdata      (m_in_axis_tdata),
    .m_in_axis_tkeep      (m_in_axis_tkeep),
    .m_in_axis_tlast      (m_in_axis_tlast),
    .m_in_axis_tvalid     (m_in_axis_tvalid),
    .m_in_axis_tready     (m_in_axis_tready),
    .m_in_axis_ttimestamp (m_in_axis_ttimestamp),
    .m_in_axis_thas_time  (m_in_axis_thas_time),
    .m_in_axis_tlength    (m_in_axis_tlength),
    .m_in_axis_teov       (m_in_axis_teov),
    .m_in_axis_teob       (m_in_axis_teob),
    // Data Stream from User Logic: out
    .s_out_axis_tdata      (s_out_axis_tdata),
    .s_out_axis_tkeep      (s_out_axis_tkeep),
    .s_out_axis_tlast      (s_out_axis_tlast),
    .s_out_axis_tvalid     (s_out_axis_tvalid),
    .s_out_axis_tready     (s_out_axis_tready),
    .s_out_axis_ttimestamp (s_out_axis_ttimestamp),
    .s_out_axis_thas_time  (s_out_axis_thas_time),
    .s_out_axis_tlength    (s_out_axis_tlength),
    .s_out_axis_teov       (s_out_axis_teov),
    .s_out_axis_teob       (s_out_axis_teob)
  );




  //---------------------------------------------------------------------------
  // Registers (settings bus)
  //---------------------------------------------------------------------------
  
  wire [ 8-1:0] set_addr;
  wire [32-1:0] set_data;
  //wire [ 1-1:0] set_has_time;
  wire [ 1-1:0] set_stb;
  //wire [64-1:0] set_time;
  wire [ 8-1:0] rb_addr;
  reg  [64-1:0] rb_data;

  ctrlport_to_settings_bus # (
    .NUM_PORTS(1)
  ) ctrlport_to_settings_bus_i (
    .ctrlport_clk             (ctrlport_clk),
    .ctrlport_rst             (ctrlport_rst),
    .s_ctrlport_req_wr        (m_ctrlport_req_wr),
    .s_ctrlport_req_rd        (m_ctrlport_req_rd),
    .s_ctrlport_req_addr      (m_ctrlport_req_addr),
    .s_ctrlport_req_data      (m_ctrlport_req_data),
    //.s_ctrlport_req_has_time  (m_ctrlport_req_has_time),
    //.s_ctrlport_req_time      (m_ctrlport_req_time),
    .s_ctrlport_resp_ack      (m_ctrlport_resp_ack),
    .s_ctrlport_resp_data     (m_ctrlport_resp_data),
    .set_data                 (set_data),
    .set_addr                 (set_addr),
    .set_stb                  (set_stb),
    //.set_time                 (set_time),
    //.set_has_time             (set_has_time),
    .rb_stb                   (1'b1),
    .rb_addr                  (rb_addr),
    .rb_data                  (rb_data),
    .timestamp                (64'b0)
  );
  
  
  
  localparam SR_TS_REMOVE          =  0; // 1 bit boolean
  localparam SR_TS_OFFSET          =  1; // 32 bit  unsigned integer
  localparam SR_RESERVED           = 99; // reserved register
  
  localparam DEFAULT_TS_REMOVE     =  1'b1;
  localparam DEFAULT_TS_OFFSET     = 32'd0;
  
  
  // Settings registers
  wire [ 0:0] reg_ts_remove;
  setting_reg #(.my_addr(SR_TS_REMOVE), .width(1), .at_reset(DEFAULT_TS_REMOVE)
  ) set_ts_remove (.out(reg_ts_remove), .in(set_data), .addr(set_addr), .strobe(set_stb), .clk(ctrlport_clk), .rst(ctrlport_rst));
  
  wire [31:0] reg_ts_offset;
  setting_reg #(.my_addr(SR_TS_OFFSET), .width(32), .at_reset(DEFAULT_TS_OFFSET)
  ) set_ts_offset (.out(reg_ts_offset), .in(set_data), .addr(set_addr), .strobe(set_stb), .clk(ctrlport_clk), .rst(ctrlport_rst));


  // Readback registers
  // TODO
  always @(*) begin
	case(rb_addr)
	  //RB_XXXX         : rb_data <= XXXX;
	  default           : rb_data <= 64'h0BADC0DE0BADC0DE;
	endcase
  end
  
  
  
  
  
  

  //---------------------------------------------------------------------------
  // User Logic
  //---------------------------------------------------------------------------
  
  // wire-through data
  assign s_out_axis_tdata      = m_in_axis_tdata;
  assign s_out_axis_tlast      = m_in_axis_tlast;
  assign s_out_axis_tlength    = m_in_axis_tlength;
   
  assign s_out_axis_thas_time  = reg_ts_remove ?  1'b0 : m_in_axis_thas_time;
  assign s_out_axis_ttimestamp = reg_ts_remove ? 64'b0 : m_in_axis_ttimestamp + {32'b0, reg_ts_offset};
  
  assign s_out_axis_teov       = m_in_axis_teov;
  assign s_out_axis_teob       = m_in_axis_teob;
  assign s_out_axis_tvalid     = m_in_axis_tvalid;
  assign m_in_axis_tready      = s_out_axis_tready;

 
  // Only 1-sample per clock, so tkeep should always be asserted
  assign s_out_axis_tkeep = 1'b1;


endmodule


`default_nettype wire
