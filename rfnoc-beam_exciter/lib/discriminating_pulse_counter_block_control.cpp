//
// Copyright 2019 Ettus Research, a National Instruments Brand
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

// Include our own header:
#include <uhd/rfnoc/beam_exciter_rfnoc/discriminating_pulse_counter_block_control.hpp>
#include <uhd/rfnoc/beam_exciter_rfnoc/util.hpp>

#include <uhd/rfnoc/defaults.hpp>
#include <uhd/rfnoc/property.hpp>
#include <uhd/rfnoc/registry.hpp>


using namespace rfnoc::beam_exciter_rfnoc;
using namespace uhd::rfnoc;


class discriminating_pulse_counter_block_control_impl : public discriminating_pulse_counter_block_control {
public:

    const std::string PROP_THRESHOLD_LOW  = "threshold_low";
    const std::string PROP_THRESHOLD_HIGH = "threshold_high";
    const std::string PROP_DECIMATION     = "decimation";

    const uint32_t SR_THRESHOLD_LOW       = 0; // 16 bit fixed float
    const uint32_t SR_THRESHOLD_HIGH      = 1; // 16 bit fixed float
    const uint32_t SR_DECIMATION          = 2; // 16 bit unsigned integer

    const float DEFAULT_THRESHOLD_LOW     = -0.5;
    const float DEFAULT_THRESHOLD_HIGH    = -0.3;
    const uint16_t DEFAULT_DECIMATION     = 20;


    RFNOC_BLOCK_CONSTRUCTOR(discriminating_pulse_counter_block_control) {
        _register_props();

        // don't forward samp_rate since this block is decimating
        // UPDATE: property and samp_rate change is now handled correctly below
        //set_prop_forwarding_policy(forwarding_policy_t::DROP, PROP_KEY_SAMP_RATE);
    }


private:
    
    /* Property handling
     *
     * https://files.ettus.com/manual/page_properties.html
     */

    property_t<double> _prop_threshold_low  = property_t<double>(PROP_THRESHOLD_LOW, DEFAULT_THRESHOLD_LOW, {res_source_info::USER});
    property_t<double> _prop_threshold_high = property_t<double>(PROP_THRESHOLD_HIGH, DEFAULT_THRESHOLD_HIGH, {res_source_info::USER});
    property_t<int>    _prop_decimation     = property_t<int>(PROP_DECIMATION, DEFAULT_DECIMATION, {res_source_info::USER});
    property_t<double> _prop_samp_rate_in   = property_t<double>(PROP_KEY_SAMP_RATE, {res_source_info::INPUT_EDGE});
    property_t<double> _prop_samp_rate_out  = property_t<double>(PROP_KEY_SAMP_RATE, {res_source_info::OUTPUT_EDGE});
    
    
    void _register_props(){
        
        register_property(&_prop_threshold_low);
        register_property(&_prop_threshold_high);
        register_property(&_prop_decimation);
        register_property(&_prop_samp_rate_in);
        register_property(&_prop_samp_rate_out);
        
        
        add_property_resolver({&_prop_threshold_low}, {&_prop_threshold_low}, [this]() {
            regs().poke32(SR_THRESHOLD_LOW*8, float2i(this->_prop_threshold_low.get()));
        });
        add_property_resolver({&_prop_threshold_high}, {&_prop_threshold_high}, [this]() {
            regs().poke32(SR_THRESHOLD_HIGH*8, float2i(this->_prop_threshold_high.get()));
        });
        add_property_resolver({&_prop_decimation}, {&_prop_decimation, &_prop_samp_rate_out}, [this]() {
            regs().poke32(SR_DECIMATION*8, this->_prop_decimation.get());
            // decimation changed -> update output rate
            if (_prop_samp_rate_in.is_valid()){
                _prop_samp_rate_out = _prop_samp_rate_in.get() / _prop_decimation.get();
            };
        });
        add_property_resolver({&_prop_samp_rate_in}, {&_prop_samp_rate_out}, [this]() {
            // input rate changed -> update output rate
            if (_prop_samp_rate_in.is_valid() && _prop_decimation.is_valid()) {
                _prop_samp_rate_out = _prop_samp_rate_in.get() / _prop_decimation.get();
            }
        });
        add_property_resolver({&_prop_samp_rate_out}, {&_prop_samp_rate_in}, [this]() {
            // output rate changed -> update input rate
            if (_prop_samp_rate_out.is_valid() && _prop_decimation.is_valid()) {
                _prop_samp_rate_in = _prop_samp_rate_out.get() * _prop_decimation.get();
            }
        });
        
        
        
    }
};

UHD_RFNOC_BLOCK_REGISTER_DIRECT(
    discriminating_pulse_counter_block_control, 0x064289D2, "DiscriminatingPulseCounter", CLOCK_KEY_GRAPH, "bus_clk")
