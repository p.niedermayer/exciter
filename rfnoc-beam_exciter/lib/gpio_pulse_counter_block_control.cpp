//
// Copyright 2019 Ettus Research, a National Instruments Brand
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

// Include our own header:
#include <uhd/rfnoc/beam_exciter_rfnoc/gpio_pulse_counter_block_control.hpp>

#include <uhd/rfnoc/defaults.hpp>
#include <uhd/rfnoc/property.hpp>
#include <uhd/rfnoc/registry.hpp>

using namespace rfnoc::beam_exciter_rfnoc;
using namespace uhd::rfnoc;


const std::string PROP_DECIMATION = "decimation";
const int DEFAULT_DECIMATION = 1;
const uint32_t REG_DECIMATION = 0x00;

class gpio_pulse_counter_block_control_impl : public gpio_pulse_counter_block_control{
public:
    RFNOC_BLOCK_CONSTRUCTOR(gpio_pulse_counter_block_control) {
		_register_props();
	}

    void set_decimation_value(const int decimation){
		regs().poke32(REG_DECIMATION, (uint32_t) decimation);
    }

    int get_decimation_value(){
		return regs().peek32(REG_DECIMATION);
    }

private:
	
	/* Property handling
	 *
	 * https://files.ettus.com/manual/page_properties.html
	 */

	property_t<int> _prop_decimation = property_t<int>(PROP_DECIMATION, DEFAULT_DECIMATION, {res_source_info::USER});
	
    void _register_props(){

		register_property(&_prop_decimation);
		
		add_property_resolver({&_prop_decimation}, {&_prop_decimation}, [this]() {
			this->set_decimation_value(this->_prop_decimation.get());
		});
		
		
    }
};

UHD_RFNOC_BLOCK_REGISTER_DIRECT(
    gpio_pulse_counter_block_control, 0x06428900, "GpioPulseCounter", CLOCK_KEY_GRAPH, "bus_clk")
