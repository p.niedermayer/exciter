//
// Copyright 2019 Ettus Research, a National Instruments Brand
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

// Include our own header:
#include <uhd/rfnoc/beam_exciter_rfnoc/discriminator_block_control.hpp>
#include <uhd/rfnoc/beam_exciter_rfnoc/util.hpp>

#include <uhd/rfnoc/defaults.hpp>
#include <uhd/rfnoc/property.hpp>
#include <uhd/rfnoc/registry.hpp>


using namespace rfnoc::beam_exciter_rfnoc;
using namespace uhd::rfnoc;






const std::string PROP_THRESHOLD_LOW = "threshold_low";
const uint32_t REG_THRESHOLD_LOW = 0x00;
const float DEFAULT_THRESHOLD_LOW = -0.5;

const std::string PROP_THRESHOLD_HIGH = "threshold_high";
const uint32_t REG_THRESHOLD_HIGH = 0x04;
const float DEFAULT_THRESHOLD_HIGH = -0.3;

class discriminator_block_control_impl : public discriminator_block_control {
public:
    RFNOC_BLOCK_CONSTRUCTOR(discriminator_block_control) {
		_register_props();
	}

    void set_threshold_low(const float threshold){
		uint16_t value = float2i(threshold);
		regs().poke32(REG_THRESHOLD_LOW, (uint32_t) value);
    }

    float get_threshold_low(){
		uint32_t value = regs().peek32(REG_THRESHOLD_LOW);
		return i2float((uint16_t) (value & 0xffff));
    }

    void set_threshold_high(const float threshold){
		uint16_t value = float2i(threshold);
		regs().poke32(REG_THRESHOLD_HIGH, (uint32_t) value);
    }

    float get_threshold_high(){
		uint32_t value = regs().peek32(REG_THRESHOLD_HIGH);
		return i2float((uint16_t) (value & 0xffff));
    }

private:
	
	/* Property handling
	 *
	 * https://files.ettus.com/manual/page_properties.html
	 */

	property_t<double> _prop_threshold_low = property_t<double>(PROP_THRESHOLD_LOW, DEFAULT_THRESHOLD_LOW, {res_source_info::USER});
	property_t<double> _prop_threshold_high = property_t<double>(PROP_THRESHOLD_HIGH, DEFAULT_THRESHOLD_HIGH, {res_source_info::USER});
	
    void _register_props(){

		register_property(&_prop_threshold_low);
		register_property(&_prop_threshold_high);
		
		add_property_resolver({&_prop_threshold_low}, {&_prop_threshold_low}, [this]() {
			this->set_threshold_low(this->_prop_threshold_low.get());
		});
		add_property_resolver({&_prop_threshold_high}, {&_prop_threshold_high}, [this]() {
			this->set_threshold_high(this->_prop_threshold_high.get());
		});
		
		
    }
};

UHD_RFNOC_BLOCK_REGISTER_DIRECT(
    discriminator_block_control, 0x064289D1, "Discriminator", CLOCK_KEY_GRAPH, "bus_clk")
