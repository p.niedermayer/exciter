//
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: GPL-3.0-or-later
//
// Conversion helpers
// to convert numeric data between CPU and RFNoC domain
//
// 



#include <uhd/rfnoc/beam_exciter_rfnoc/util.hpp>


using namespace rfnoc::beam_exciter_rfnoc;




/**
 * Convert float [-1;+1] to uint16_t
 */
const uint16_t rfnoc::beam_exciter_rfnoc::float2i(const float f){
	const int16_t i = clamp<int16_t>(f * 32768.0);
	return (uint16_t) i;
}

/**
 * Convert uint16_t to float [-1;+1] 
 */
const float rfnoc::beam_exciter_rfnoc::i2float(const uint16_t ui){
	int16_t i = (int16_t) ui;
	float v = i;
	v = v / 32768.0;
	return v;
}

/**
 * Convert complex double to sc16
 */
const uint32_t rfnoc::beam_exciter_rfnoc::complex2iq(const std::complex<double> v){
	uint32_t i = float2i(v.real());
	uint32_t q = float2i(v.imag());
	return (i << 16) | (q & 0xffff);
}

/**
 * Convert float to unsigned fixed point representation
 * @param value floating point value
 * @param bits total number of bits of the fixed point
 * @param point_offset bit offset of the fixed point
 * @return binary
 *
 * Examples:
 *   0*2^point_offset --> 0x0
 *   1*2^point_offset --> 0x1
 *   2*2^point_offset --> 0x2
 */
const uint32_t rfnoc::beam_exciter_rfnoc::float2fixed(const float value, const int bits, const int point_offset){
    return clamp<uint32_t>(round(value / pow(2, point_offset)));
}

/**
 * Convert unsigned fixed point representation to float
 * @param value binary
 * @param bits total number of bits of the fixed point
 * @param point_offset bit offset of the fixed point
 * @return floating point value
 *
 * Examples:
 *   0x0 --> 0*2^point_offset
 *   0x1 --> 1*2^point_offset
 *   0x2 --> 2*2^point_offset
 */
const float rfnoc::beam_exciter_rfnoc::fixed2float(const uint32_t value, const int bits, const int point_offset){
    uint32_t masked_value = value & ((2 << bits) - 1); // mask bits
    return masked_value * pow(2, point_offset);
}


#ifndef __STDC_IEC_559__
#error "Requires IEEE 754 floating point!"
#endif

/**
 * Convert float to IEEE 754 representation
 * @param value floating point value
 * @return binary
 */
const uint32_t rfnoc::beam_exciter_rfnoc::float2ieee(const float value){
    uint32_t binary;
    std::memcpy(&binary, &value, 4);
    return binary;
}

/**
 * Convert IEEE 754 representation to float
 * @param value binary
 * @return floating point value
 */
const float rfnoc::beam_exciter_rfnoc::ieee2float(const uint32_t binary){
    float value;
    std::memcpy(&value, &binary, 4);
    return value;
}

