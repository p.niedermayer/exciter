//
// Copyright 2019 Ettus Research, a National Instruments Brand
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

// Include our own header:
#include <uhd/rfnoc/beam_exciter_rfnoc/gain_block_control.hpp>

#include <uhd/rfnoc/defaults.hpp>
#include <uhd/rfnoc/property.hpp>
#include <uhd/rfnoc/registry.hpp>

using namespace rfnoc::beam_exciter_rfnoc;
using namespace uhd::rfnoc;






const std::string PROP_GAIN = "gain";
const int DEFAULT_GAIN = 1;
const uint32_t REG_GAIN = 0x00;

class gain_block_control_impl : public gain_block_control{
public:
    RFNOC_BLOCK_CONSTRUCTOR(gain_block_control) {
		_register_props();
	}

    void set_gain_value(const int gain){
		// The gain block from the tutorial only supports integer gain values
		regs().poke32(REG_GAIN, (uint32_t) gain);
    }

    int get_gain_value(){
		return regs().peek32(REG_GAIN);
    }

private:
	
	/* Property handling
	 *
	 * https://files.ettus.com/manual/page_properties.html
	 */

	property_t<int> _prop_gain = property_t<int>(PROP_GAIN, DEFAULT_GAIN, {res_source_info::USER});
	
    void _register_props(){

		register_property(&_prop_gain);
		
		add_property_resolver({&_prop_gain}, {&_prop_gain}, [this]() {
			this->set_gain_value(this->_prop_gain.get());
		});
		
		
    }
};

UHD_RFNOC_BLOCK_REGISTER_DIRECT(
    gain_block_control, 0xb16, "Gain", CLOCK_KEY_GRAPH, "bus_clk")
