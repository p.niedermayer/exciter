//
// Copyright 2019 Ettus Research, a National Instruments Brand
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

// Include our own header:
#include <uhd/rfnoc/beam_exciter_rfnoc/multiply_block_control.hpp>

#include <uhd/rfnoc/defaults.hpp>
#include <uhd/rfnoc/property.hpp>
#include <uhd/rfnoc/registry.hpp>

using namespace rfnoc::beam_exciter_rfnoc;
using namespace uhd::rfnoc;






class multiply_block_control_impl : public multiply_block_control{
public:
    RFNOC_BLOCK_CONSTRUCTOR(multiply_block_control) {
		
	}

};

UHD_RFNOC_BLOCK_REGISTER_DIRECT(
    multiply_block_control, 0x06428901, "Multiply", CLOCK_KEY_GRAPH, "bus_clk")
