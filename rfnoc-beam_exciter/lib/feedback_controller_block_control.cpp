//
// Copyright 2019 Ettus Research, a National Instruments Brand
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

// Include our own header:
#include <uhd/rfnoc/beam_exciter_rfnoc/feedback_controller_block_control.hpp>
#include <uhd/rfnoc/beam_exciter_rfnoc/util.hpp>

#include <uhd/rfnoc/defaults.hpp>
#include <uhd/rfnoc/property.hpp>
#include <uhd/rfnoc/registry.hpp>


using namespace rfnoc::beam_exciter_rfnoc;
using namespace uhd::rfnoc;


class feedback_controller_block_control_impl : public feedback_controller_block_control {
public:
    
    const std::string PROP_FEEDFORWARD       = "feedforward";
    const std::string PROP_FEEDBACK          = "feedback";
    const std::string PROP_TA                = "ta";
    const std::string PROP_TARGET            = "target";
    const std::string PROP_KP                = "kp";
    const std::string PROP_KI_TIMES_TA       = "ki_times_ta";
    const std::string PROP_KD_OVER_TA        = "kd_over_ta";
    const std::string PROP_FEEDFORWARD_CONST = "feedforward_const";
    const std::string PROP_OUT_SCALE         = "out_scale";
    const std::string PROP_OUT_MIN           = "out_min";
    const std::string PROP_OUT_MAX           = "out_max";
    const std::string PROP_REPEAT            = "repeat";
    const std::string PROP_ACTIVE            = "active";
    
    const uint32_t SR_FEEDFORWARD            =  0; // feedforward (1 bit)
    const uint32_t SR_FEEDBACK               =  1; // feedback (1 bit)
    const uint32_t SR_TA                     =  2; // ta (16 bit unsigned integer)
    const uint32_t SR_TARGET                 =  3; // target (32 bit IEEE 754 float)
    const uint32_t SR_KP                     =  4; // kp (32 bit IEEE 754 float)
    const uint32_t SR_KI_TIMES_TA            =  5; // ki_times_ta (32 bit IEEE 754 float)
    const uint32_t SR_KD_OVER_TA             =  6; // kd_over_ta (32 bit IEEE 754 float)
    const uint32_t SR_FEEDFORWARD_CONST      =  7; // feedforward_const (32 bit IEEE 754 float)
    const uint32_t SR_OUT_SCALE              =  8; // out_scale (32 bit IEEE 754 float)
    const uint32_t SR_OUT_MIN                =  9; // out_min (32 bit IEEE 754 float)
    const uint32_t SR_OUT_MAX                = 10; // out_max (32 bit IEEE 754 float)
    const uint32_t SR_REPEAT                 = 11; // repeat (8 bit unsigned integer)
    const uint32_t SR_ACTIVE                 = 12; // active (1 bit)
    const uint32_t SR_RESERVED               = 99; // reserved register
  
    const bool     DEFAULT_FEEDFORWARD       = false;
    const bool     DEFAULT_FEEDBACK          = false;
    const uint16_t DEFAULT_TA                = 200;
    const float    DEFAULT_TARGET            = 4.5;
    const float    DEFAULT_KP                = 1.3333333e-3;
    const float    DEFAULT_KI_TIMES_TA       = 2.6666666e-4;
    const float    DEFAULT_KD_OVER_TA        = 0;
    const float    DEFAULT_FEEDFORWARD_CONST = 0.05;
    const float    DEFAULT_OUT_SCALE         = 3;
    const float    DEFAULT_OUT_MIN           = 0;
    const float    DEFAULT_OUT_MAX           = 10;
    const uint8_t  DEFAULT_REPEAT            = 1;  
    const bool     DEFAULT_ACTIVE            = false;


    RFNOC_BLOCK_CONSTRUCTOR(feedback_controller_block_control) {
        _register_props();
    }
    
    

private:

    /* Property handling
     * 
     * Supports: double, int, 
     * https://files.ettus.com/manual/page_properties.html
     */


    property_t<int> _prop_feedforward = property_t<int>(PROP_FEEDFORWARD, DEFAULT_FEEDFORWARD, {res_source_info::USER});
    property_t<int> _prop_feedback = property_t<int>(PROP_FEEDBACK, DEFAULT_FEEDBACK, {res_source_info::USER});
    property_t<int> _prop_ta = property_t<int>(PROP_TA, DEFAULT_TA, {res_source_info::USER});
    property_t<double> _prop_target = property_t<double>(PROP_TARGET, DEFAULT_TARGET, {res_source_info::USER});
    property_t<double> _prop_kp = property_t<double>(PROP_KP, DEFAULT_KP, {res_source_info::USER});
    property_t<double> _prop_ki_times_ta = property_t<double>(PROP_KI_TIMES_TA, DEFAULT_KI_TIMES_TA, {res_source_info::USER});
    property_t<double> _prop_kd_over_ta = property_t<double>(PROP_KD_OVER_TA, DEFAULT_KD_OVER_TA, {res_source_info::USER});
    property_t<double> _prop_feedforward_const = property_t<double>(PROP_FEEDFORWARD_CONST, DEFAULT_FEEDFORWARD_CONST, {res_source_info::USER});
    property_t<double> _prop_out_scale = property_t<double>(PROP_OUT_SCALE, DEFAULT_OUT_SCALE, {res_source_info::USER});
    property_t<double> _prop_out_min = property_t<double>(PROP_OUT_MIN, DEFAULT_OUT_MIN, {res_source_info::USER});
    property_t<double> _prop_out_max = property_t<double>(PROP_OUT_MAX, DEFAULT_OUT_MAX, {res_source_info::USER});    
    property_t<int> _prop_repeat = property_t<int>(PROP_REPEAT, DEFAULT_REPEAT, {res_source_info::USER});
    property_t<int> _prop_active = property_t<int>(PROP_ACTIVE, DEFAULT_ACTIVE, {res_source_info::USER});
    property_t<double> _prop_samp_rate_in   = property_t<double>(PROP_KEY_SAMP_RATE, {res_source_info::INPUT_EDGE});
    property_t<double> _prop_samp_rate_out  = property_t<double>(PROP_KEY_SAMP_RATE, {res_source_info::OUTPUT_EDGE});
    
    
    void _register_props(){

        register_property(&_prop_feedforward);
        register_property(&_prop_feedback);
        register_property(&_prop_ta);
        register_property(&_prop_target);
        register_property(&_prop_kp);
        register_property(&_prop_ki_times_ta);
        register_property(&_prop_kd_over_ta);
        register_property(&_prop_feedforward_const);
        register_property(&_prop_out_scale);
        register_property(&_prop_out_min);
        register_property(&_prop_out_max);
        register_property(&_prop_repeat);
        register_property(&_prop_active);
        register_property(&_prop_samp_rate_in);
        register_property(&_prop_samp_rate_out);
        
        
        add_property_resolver({&_prop_feedforward}, {&_prop_feedforward}, [this]() {
            regs().poke32(SR_FEEDFORWARD*8, this->_prop_feedforward.get());
        });
        add_property_resolver({&_prop_feedback}, {&_prop_feedback}, [this]() {
            regs().poke32(SR_FEEDBACK*8, this->_prop_feedback.get());
        });
        add_property_resolver({&_prop_ta}, {&_prop_ta}, [this]() {
            regs().poke32(SR_TA*8, this->_prop_ta.get());
        });
        add_property_resolver({&_prop_target}, {&_prop_target}, [this]() {
            regs().poke32(SR_TARGET*8, float2ieee(this->_prop_target.get()));
        });
        add_property_resolver({&_prop_kp}, {&_prop_kp}, [this]() {
            regs().poke32(SR_KP*8, float2ieee(this->_prop_kp.get()));
        });
        add_property_resolver({&_prop_ki_times_ta}, {&_prop_ki_times_ta}, [this]() {
            regs().poke32(SR_KI_TIMES_TA*8, float2ieee(this->_prop_ki_times_ta.get()));
        });
        add_property_resolver({&_prop_kd_over_ta}, {&_prop_kd_over_ta}, [this]() {
            regs().poke32(SR_KD_OVER_TA*8, float2ieee(this->_prop_kd_over_ta.get()));
        });
        add_property_resolver({&_prop_feedforward_const}, {&_prop_feedforward_const}, [this]() {
            regs().poke32(SR_FEEDFORWARD_CONST*8, float2ieee(this->_prop_feedforward_const.get()));
        });
        add_property_resolver({&_prop_out_scale}, {&_prop_out_scale}, [this]() {
            regs().poke32(SR_OUT_SCALE*8, float2ieee(this->_prop_out_scale.get()));
        });
        add_property_resolver({&_prop_out_min}, {&_prop_out_min}, [this]() {
            regs().poke32(SR_OUT_MIN*8, float2ieee(this->_prop_out_min.get()));
        });
        add_property_resolver({&_prop_out_max}, {&_prop_out_max}, [this]() {
            regs().poke32(SR_OUT_MAX*8, float2ieee(this->_prop_out_max.get()));
        });        
        add_property_resolver({&_prop_repeat}, {&_prop_repeat}, [this]() {
            regs().poke32(SR_REPEAT*8, this->_prop_repeat.get());
            // repeat changed -> update output rate
            if (_prop_samp_rate_in.is_valid()){
                _prop_samp_rate_out = _prop_samp_rate_in.get() * _prop_repeat.get();
            };
        });
        add_property_resolver({&_prop_active}, {&_prop_active}, [this]() {
            regs().poke32(SR_ACTIVE*8, this->_prop_active.get());
        });
        add_property_resolver({&_prop_samp_rate_in}, {&_prop_samp_rate_out}, [this]() {
            // input rate changed -> update output rate
            if (_prop_samp_rate_in.is_valid() && _prop_repeat.is_valid()) {
                _prop_samp_rate_out = _prop_samp_rate_in.get() * _prop_repeat.get();
            }
        });
        add_property_resolver({&_prop_samp_rate_out}, {&_prop_samp_rate_in}, [this]() {
            // output rate changed -> update input rate
            if (_prop_samp_rate_out.is_valid() && _prop_repeat.is_valid()) {
                _prop_samp_rate_in = _prop_samp_rate_out.get() / _prop_repeat.get();
            }
        });


    }
};

UHD_RFNOC_BLOCK_REGISTER_DIRECT(
    feedback_controller_block_control, 0x064289FC, "FeedbackController", CLOCK_KEY_GRAPH, "bus_clk")
