//
// Copyright 2019 Ettus Research, a National Instruments Brand
// Copyright 2023 Philipp Niedermayer
//
// SPDX-License-Identifier: GPL-3.0-or-later
//

// Include our own header:
#include <uhd/rfnoc/beam_exciter_rfnoc/timestamp_mod_block_control.hpp>

#include <uhd/rfnoc/defaults.hpp>
#include <uhd/rfnoc/property.hpp>
#include <uhd/rfnoc/registry.hpp>

using namespace rfnoc::beam_exciter_rfnoc;
using namespace uhd::rfnoc;







class timestamp_mod_block_control_impl : public timestamp_mod_block_control{
public:
    RFNOC_BLOCK_CONSTRUCTOR(timestamp_mod_block_control) {
        _register_props();
    }

    
    const std::string PROP_TS_REMOVE      = "ts_remove";
    const std::string PROP_TS_OFFSET      = "ts_offset";

    const uint32_t SR_TS_REMOVE           = 0; // 1 bit boolean
    const uint32_t SR_TS_OFFSET           = 1; // 32 bit unsigned integer

    const bool DEFAULT_TS_REMOVE          = true;
    const uint32_t DEFAULT_TS_OFFSET      = 0;


private:
    
    /* Property handling
     *
     * https://files.ettus.com/manual/page_properties.html
     */

    property_t<int> _prop_ts_remove = property_t<int>(PROP_TS_REMOVE, DEFAULT_TS_REMOVE, {res_source_info::USER});
    property_t<int> _prop_ts_offset = property_t<int>(PROP_TS_OFFSET, DEFAULT_TS_OFFSET, {res_source_info::USER});
    
    void _register_props(){

        register_property(&_prop_ts_remove);
        register_property(&_prop_ts_offset);
        
        add_property_resolver({&_prop_ts_remove}, {&_prop_ts_remove}, [this]() {
            regs().poke32(SR_TS_REMOVE*8, this->_prop_ts_remove.get());
        });
        add_property_resolver({&_prop_ts_offset}, {&_prop_ts_offset}, [this]() {
            regs().poke32(SR_TS_OFFSET*8, this->_prop_ts_offset.get());
        });
        
        
    }
};

UHD_RFNOC_BLOCK_REGISTER_DIRECT(
    timestamp_mod_block_control, 0x06428902, "TimestampMod", CLOCK_KEY_GRAPH, "bus_clk")
