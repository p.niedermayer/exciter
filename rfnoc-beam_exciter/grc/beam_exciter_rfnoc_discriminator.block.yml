# 
# Copyright 2023 Philipp Niedermayer.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


id: beam_exciter_rfnoc_discriminator
label: RFNoC Discriminator
category: '[Beam exciter]/RFNoC'

documentation: |-
    Discriminate input signal with two tresholds
    - If input < threshold_low -> output = 0
    - If input > threshold_high -> output = 1
    - Else: output remains unchanged
    
    The block only acts on the real (in-phase) part. The imaginary
    (quadrature) component is passed through unchanged.
    In- and outputs are defined as sc16 for compatibility reasons.
    This means that you have to multiply the real output by 32767
    to get the float values 0 and 1.
    
    (c) 2023 Philipp Niedermayer

templates:
  imports: |-
    from gnuradio import uhd
  make: |-
    uhd.rfnoc_block_generic(
        self.rfnoc_graph,
        uhd.device_addr(""),
        "Discriminator",
        -1, # device_select
        -1, # instance_index
    )
    self.${id}.set_property('threshold_low', ${threshold_low})
    self.${id}.set_property('threshold_high', ${threshold_high})
  callbacks:
    - set_property('threshold_low', ${threshold_low})
    - set_property('threshold_high', ${threshold_high})
    

parameters:
- id: threshold_low
  label: Threshold low
  dtype: float
  default: '-0.5'
- id: threshold_high
  label: Threshold high
  dtype: float
  default: '-0.3'

asserts:
- ${ threshold_low < threshold_high }


# RFNoC inputs and outputs
inputs:
- domain: rfnoc
  dtype: 'sc16'
  vlen: 1

outputs:
- domain: rfnoc
  dtype: 'sc16'
  #dtype: 'byte'
  vlen: 1

# GRC YML format: https://wiki.gnuradio.org/index.php/YAML_GRC
file_format: 1
