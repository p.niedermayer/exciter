#!/usr/bin/env python3

#
# Data File Viewer for RFKO Extraction with Feedback
# 
# Allows for online and offline plotting of data from
# *.200kSps.complex64 files where it is understood
# that the real part is the spill rate and the complex
# part is the controller level.
# 
# Author: Philipp Niedermayer (p.niedermayer@gsi.de)
# Date: 17.04.2023
#



#################################################
# Data handler
#################################################

from dataclasses import dataclass
import numpy as np
import re

FNAME_PATTERN = re.compile(r'((?P<path>.*)/)?(?P<name>.+)\.(?P<rate>\d+)kSps.(?P<dtype>[^.]+)')

@dataclass
class Data:
    """Class for keeping track of an item in inventory."""
    name: str
    dt: float
    rate: np.array
    controller: np.array
    fname: str
    t0: float = 0
    
    @property
    def time(self):
        return self.t0 + self.dt*np.arange(0, self.rate.size)
    
    @property
    def duration(self):
        return self.dt*self.rate.size

    def crop(self, t_start, t_stop):
        """Crop data to time range"""
        irange = slice(*[None if t is None or self.time.size == 0 else int(np.argmin(np.abs(self.time-t))) for t in (t_start, t_stop)])
        return Data(
            name = self.name,
            dt = self.dt,
            rate = self.rate[irange],
            controller = self.controller[irange],
            fname = self.fname,
            t0 = self.time[irange][0] if self.time[irange].size > 0 else np.nan,
        )
        
    def resample(self, dt):
        """Resample data to reduced time resolution
        If dt is not a multiple of the data time resolution, it is rounded
        """
        if np.isnan(self.dt): return self
        
        n = int(max(1, round(dt/self.dt)))
        size = n*int(self.rate.size/n)
        return Data(
            name = self.name,
            dt = n*self.dt,
            rate = np.reshape(self.rate[:size], (-1, n)).mean(axis=1),
            controller = np.reshape(self.controller[:size], (-1, n)).mean(axis=1),
            fname = self.fname,
            t0 = self.t0,
        )

    @staticmethod
    def from_file(fname):
        """Load spill data from a file
        Expects a filename simmilar to `xxx.200kSps.complex64`
        """
        m = FNAME_PATTERN.match(fname)
        if m is None:
            return NO_DATA
        rate = float(m.group("rate"))*1e3
        #d = np.memmap(fname, dtype=m.group("dtype"), mode="r")
        d = np.fromfile(fname, dtype=m.group("dtype"))
        return Data(
            name = m.group("name"),
            dt = 1/rate,
            rate = d.real,
            controller = d.imag,
            fname = fname,
        )

NO_DATA = Data("", np.nan, np.empty(0), np.empty(0), "", np.nan)


#################################################
# Plot
#################################################

import pint
import matplotlib as mpl
from matplotlib import pyplot as plt

# Colors
# https://arxiv.org/abs/2107.02270
petroff_colors = ["#3f90da", "#ffa90e", "#bd1f01", "#94a4a2", "#832db6", "#a96b59", "#e76300", "#b9ac70", "#717581", "#92dadd"]
cmap_petroff_10 = mpl.colors.ListedColormap(petroff_colors, 'Petroff 10')
mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=petroff_colors)

# Matplotlib options
mpl.rcParams.update({
    # Improve layout
    'figure.constrained_layout.use': True,
    'legend.fontsize': 'x-small',
    'legend.title_fontsize': 'small',
    'grid.color': '#DDD',
    # Improve rendering time
    'path.simplify': True,
    'path.simplify_threshold': 1.0,
    'agg.path.chunksize': 16384,
    # ... and don't use markers
})


class TwinFunctionMajorLocator(mpl.ticker.Locator):
    def __init__(self, twin_axis, function_twin_to_this, function_this_to_twin, granularity=1):
        """A major locator for twin axes
        
        Finds nice tick locations close to the twin, but uses a custom function to place ticks at integer multiples of granularity.
        This is useful for twin axes which share the same limits, but are formatted with values based on different functions.
        
        Args:
            twin_axis (mpl.axis.Axis): The other axis to align tick locations with
            function_twin_to_this (callable): Function to calculate tick values of this axis given the tick values of the other axis.
            function_this_to_twin (callable): Function to calculate tick values of the other axis given the tick values of this axis.
            granularity (float): Base at multiples of which to locate ticks.
        """
        self.twin = twin_axis
        self.twin2this = function_twin_to_this
        self.this2twin = function_this_to_twin
        self.granularity = granularity
    def __call__(self):
        """Return the locations of the ticks."""
        return self.tick_values(*self.axis.get_view_interval())
    def tick_values(self, vmin, vmax):
        twin_values = np.array(self.twin.get_major_locator().tick_values(vmin, vmax))
        this_values = self.twin2this(twin_values)
        this_values = np.round(this_values/self.granularity)*self.granularity
        return self.this2twin(this_values)


class SpillPlot:
    
    def __init__(self, nrefs=3, figsize=(8,6), dpi=80, mosaic=None, **kwargs):
        mosaic = mosaic or (("rate", "fft"),("controller", "quality"))
        
        self.fig, self.ax = fig, ax = plt.subplot_mosaic(mosaic, constrained_layout=True, figsize=figsize, dpi=dpi, **kwargs)
        self.artist = {}
        self.annotation = {}
        self.nrefs = nrefs
        
        def traces_with_refs(a, **kwargs):
            art = {
                i: a.plot([], [], **kwargs, zorder=2-i/nrefs/2, color=petroff_colors[i] if i < len(petroff_colors) else None)[0]
                for i in range(nrefs+1)
            }
            return art
        
        # Spill rate
        self.axis_rate = a = self.ax.get("rate")
        if a:
            a.set(title="Spill rate", ylabel="Particles/s", xlabel="Extraction time / s")
            #plt.setp(self.axis_rate.get_xticklabels(), visible=False)
            self.artist_rate = self.artist["rate"] = traces_with_refs(a, lw=1)
            self.annotation["rate"] = a.text(0, 0, "Loading...", ha="left", va="top", c="gray", linespacing=1, clip_on=False,
                                             transform=mpl.transforms.offset_copy(a.transAxes, fig, -0.5, -0.5))

        # Controller
        self.axis_controller = a = self.ax.get("controller")
        if a:
            a.set(title="Controller output", ylabel="Level", xlabel="Extraction time / s")
            if self.axis_rate is not None: self.axis_controller.sharex(self.axis_rate)
            self.artist_controller = self.artist["controller"] = traces_with_refs(a, lw=1)
        
        # FFT
        self.axis_fft = a = self.ax.get("fft")
        if a:
            a.set(title="Spill spectrum", ylabel="Power spectrum / a.u.", yscale='log', xlabel=f'Frequency / Hz', xscale='log')
            self.artist_fft = self.artist["fft"] = traces_with_refs(a, lw=1)
        
        # Quality metric
        self.axis_quality = a = self.ax.get("quality")
        self.axis_quality_twin = None
        if a:
            a.set(title="Spill quality", ylabel="...", xlabel=f'Time resolution $\\Delta t_\\mathrm{{count}}$ / s', xscale='log')
            self.artist_quality = self.artist["quality"] = traces_with_refs(a)
            self.artist_quality_limit = self.artist["quality_limit"] = traces_with_refs(a, ls=(0, (1,5)), lw=1)
            self.annotation["quality"] = a.text(0, 0, "Loading...", ha="left", va="top", c="gray", linespacing=1, clip_on=False,
                                                transform=mpl.transforms.offset_copy(a.transAxes, fig, -0.5, -0.5))
            
            # Twin axes: duty factor
            self.axis_quality_twin = at = self.axis_quality.twinx()
            e = 1 # minor ticks every (spill duty factor percent value)
            cv2duty = lambda cv: 100/(1+cv**2)
            duty2cv = lambda du: (100/du-1)**.5
            at.set(ylabel="Spill duty factor   $F=\\langle N \\rangle^2/\\langle N^2 \\rangle$")
            at.yaxis.set_major_locator(TwinFunctionMajorLocator(self.axis_quality.yaxis, cv2duty, duty2cv, e))
            at.yaxis.set_major_formatter(mpl.ticker.FuncFormatter(lambda cv, i: f'${cv2duty(cv):.0f}\,$%'))
            at.yaxis.set_minor_locator(mpl.ticker.FixedLocator(duty2cv(np.arange(e, 101, e))))
            # handle axis limits updates
            self.axis_quality.callbacks.connect("ylim_changed", lambda a: self.axis_quality_twin.set(ylim=a.get_ylim()))
            self.axis_quality_twin.set_navigate(False) # we maintain limits ourselves
            self.axis_quality.set_zorder(self.axis_quality_twin.get_zorder()+1)
            #self.axis_quality.set_frame_on(False)
            self.axis_quality_twin.axis("off")  # only show on request
        
        # Grid
        for a in fig.axes:
            a.grid(c="#ddd")
    
    
    def autoscale(self, enabled=True, *, tight=True):
        for a in self.fig.axes:
            if a == self.axis_quality_twin: continue
            a.autoscale(enabled, tight=tight)
            if enabled:
                a.relim()
                a.autoscale_view()
    
    def update(self, trace_data, ref=0):
        for key in self.artist:
            self.artist[key][ref].set_data(*getattr(trace_data, f"trace_{key}"))
            if not key.endswith("_limit"):
                self.artist[key][ref].set_label(trace_data.label)
        
        if "rate" in self.ax:
            self.ax["rate"].set(
                title="Spill integral" if trace_data.cumulative else "Spill rate",
                ylabel="Particles" if trace_data.cumulative else "Particles/s"
            )
        if "quality" in self.ax:
            self.ax["quality"].set(
                ylabel={"cv": "Coefficient of variation $c_v=\\sigma/\\mu$", "maxmean": "Max-mean ratio"}[trace_data.quality_metric],
            )
            if trace_data.quality_metric != "cv":
                self.axis_quality_twin.axis("off")
                self.axis_quality_twin.set(ylabel="ERROR: Spill duty factor axis requires quality_metric='cv'!")
            
        if ref == 0:
            if "rate" in self.annotation: self.annotation["rate"].set_text(
                f"$\\Delta t_\\mathrm{{count}} = {pint.Quantity(trace_data.dt, 's').to_compact():~gL}$")
            if "quality" in self.annotation: self.annotation["quality"].set_text(
                f"$\\Delta t_\\mathrm{{evaluate}} = {trace_data.trace_quality_counting_bins_per_evaluation:g}\\,\\Delta t_\\mathrm{{count}}$")
            


    def copy(self, source, dest):
        for artists in self.artist.values():
            artists[dest].set_data(*(artists[source].get_data() if source in artists else ([], [])))
            artists[dest].set_label(artists[source].get_label() if source in artists else None)
    
    def draw(self):        
        #self.fig.canvas.draw()
        self.fig.canvas.draw_idle()


class SpillPlotTraceData:
    def __init__(self, data, dt=None, t_start=None, t_stop=None, cumulative=False, fft=True, fmax=None, quality=True, quality_res=50, label=None, counting_bins_per_evaluation = 50, quality_metric="cv"):
        self.dt = np.nan
        self.label = label or data.name
        self.cumulative = cumulative
        self.trace_rate = [], []
        self.trace_controller = [], []
        self.trace_fft = [], []
        self.trace_quality = [], []
        self.trace_quality_limit = [], []
        self.trace_quality_counting_bins_per_evaluation = counting_bins_per_evaluation
        self.quality_metric = quality_metric
        
        data = data.crop(t_start, t_stop)
        if dt is None:
            dt = data.dt
        
        if dt <= 0 or not np.isfinite(data.dt) or data.time.size < 2:
            # invalid data
            return
        
        d = data.resample(dt)
        self.dt = dt = d.dt
        self.trace_rate = d.time, (np.cumsum(d.rate)*d.dt) if cumulative else d.rate
        self.trace_controller = d.time, d.controller        
        
        if fft: # FFT
            if fmax is not None:
                d = data.resample(1/fmax/2)
            fmax = 1/2/d.dt
            freq = np.fft.rfftfreq(len(d.rate), d=d.dt)[1:]
            mag = np.abs(np.fft.rfft(d.rate))[1:]
            mag *= 2/len(d.rate) # amplitude
            mag = mag**2 # power
            self.trace_fft = freq, mag
        
        if quality: # Coefficient of variation
            plot = []
            
            max_rebin = int(data.duration/data.dt/counting_bins_per_evaluation)
            if max_rebin > 0:
                rebins = np.unique(np.geomspace(1, max_rebin, quality_res, dtype=int))
                ncbins = int(counting_bins_per_evaluation)
                for rebin in rebins:
                    # rebin into counting bins
                    new_dt = data.dt*rebin
                    size = rebin*int(data.rate.size/rebin)
                    N = new_dt*np.reshape(data.rate[:size], (-1, rebin)).mean(axis=1)

                    # reshape into evaluation bins
                    N = N[:int(N.size/ncbins)*ncbins].reshape((-1, ncbins))

                    # calculate metric
                    with np.errstate(invalid='ignore', divide='ignore'):
                        mean, std, max = np.mean(N, axis=1), np.std(N, axis=1), np.max(N, axis=1)
                        if self.quality_metric == "cv":
                            val = Cv = std/mean
                            val_limit = Cv_poisson = 1/mean**.5
                            # Cv_poisson = 1/np.mean(N / 9428.6, axis=1)**.5 # scaling factor for IC for elog 844
                        elif self.quality_metric == "maxmean":
                            val = max/mean
                            val_limit = np.NaN * mean
                        else:
                            raise ValueError(f"Quality metric '{self.quality_metric}' not implemented. Valid choices are 'cv' or 'maxmean'.")

                        val = val[np.isfinite(val)]
                        val_limit = val_limit[np.isfinite(val_limit)]
                    
                    plot.append((new_dt, np.nanmean(val), np.nanstd(val), np.mean(val_limit)))
                    
                new_dt, val_mean, val_std, val_lim = np.array(plot).T
                self.trace_quality = new_dt, val_mean
                self.trace_quality_limit = new_dt, val_lim
                self.trace_quality_uncertainty = new_dt, val_mean-val_std, val_mean+val_std


#################################################
# GUI
#################################################

class AttrDict(dict):
    def __getattr__(self, key):
        return self.get(key)

if __name__ == "__main__":

    import time
    import glob
    import os
    import subprocess
    import threading
    import traceback
    import queue
    import PySimpleGUI as sg
    from matplotlib import pyplot as plt
    from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
    
    #print = sg.cprint
    
    searchpath = os.path.abspath(".")

    
    # Global variables
    data = {}    
    op = AttrDict(
        dt = 5e-3,
        cumsum = False,
        autoscale = True,
        autoref = True,
        auto_update = os.name == 'posix',
        wait_complete = True,
        plot_fft = True,
        plot_quality = True,
    )
    

    # Layout
    ####################
    
    sg.theme('Default1')

    # Workflow options
    layout_data = [[
        #sg.Text("..."), sg.Input(key='frev', size=(10,1)),
        sg.Checkbox("Auto update data", default=op.auto_update, key='auto_update', enable_events=True, tooltip="Monitors files in the directory\nand updates data to the most recent.\nWorks only on Linux systems."),
        sg.Checkbox("Only when complete", default=op.wait_complete, disabled=not op.auto_update, key='wait_complete', enable_events=True),
        sg.Checkbox("Set ref-3 to previous data", default=op.autoref, disabled=not op.auto_update, key='autoref', enable_events=True),
    ]]
    # Trace management
    nrefs = 3
    for ref in range(1+nrefs):
        line = [
            sg.Button("set", key=f"ref:{ref}:set:0", pad=((5,0),0), disabled=(ref==0),
                tooltip="Click to set reference from data\nRight click for more options", 
                right_click_menu=["", [f"Clear::ref:{ref}:set:-1"] + [f"Copy from {i}::ref:{ref}:set:{i}" for i in range(1, 1+nrefs) if i!=ref]]),
            sg.Input(key=f"ref:{ref}:open", enable_events=True, visible=False),
            sg.FileBrowse(f"Ref-{ref}" if ref > 0 else "Data", target=f"ref:{ref}:open",
                tooltip="Click to open from file", file_types=(('Spill data', '*.complex64'),('Any', '*'),),
                button_color=("white", petroff_colors[ref]), size=(5,1), pad=0,
            ),#, 
            sg.Text(key=f"ref:{ref}:desc", expand_x=True),
        ]
        layout_data.append(line)
    # Plot control
    layout_plot = [
        [sg.Text("Data time range:", size=(15,1), right_click_menu=["", ["Set from plot::t_xlim", "Clear::t_clear"]]), sg.Input("", key='t_start', size=(6,1)), sg.Text("to", pad=0), sg.Input("", key='t_stop', size=(6,1)), sg.Text("s", pad=0)],
        [sg.Text("Spill rate resolution:", size=(15,1)), sg.Input("5e-3", key='dt', size=(6,1)), sg.Text("s", pad=0), sg.Checkbox('Cumulative', default=op.cumsum, key='cumsum', enable_events=True, tooltip="Plot spill integral instead of spill rate ")],
        [sg.Text("Spill spectrum:", size=(15,1)), sg.Input("1e3", key='fmax', size=(6,1)), sg.Text("Hz", pad=0), sg.Combo(['Logscale', 'Semi-log', 'Linear'], default_value='Logscale', key='fftlogscale', enable_events=True, readonly=True, tooltip="Logarithmic axis scaling for spill spectrum")],
        [sg.Text("Spill quality:", size=(15,1)),sg.Checkbox('Duty factor', default=False, key='qualitytwinax', enable_events=True, tooltip="Show second axis with spill duty factor")],
        [sg.Checkbox('Autoscale', default=op.autoscale, key='autoscale', enable_events=True)],
    ]

    layout = [
        [sg.Column([[sg.Frame('Data sources', layout_data, expand_x=True, expand_y=True), sg.Frame('Plotting options', layout_plot, expand_y=True)]], expand_x=True)],
        [sg.pin(sg.Canvas(key='plot_toolbar'), expand_x=True), sg.Push()],
        [sg.Canvas(key='plot', expand_x=True, expand_y=True)],
        [sg.Multiline(size=(40,6), expand_x=True, autoscroll=True, reroute_stdout=True, write_only=True, reroute_cprint=True)],
    ]
    window = sg.Window('RFKO Extraction with Feedback - Data File Viewer', layout, finalize=True, resizable=True)
    for key in ('dt', 'fmax', 't_start', 't_stop'):
        window[key].bind("<Return>", "")
    #window.keep_on_top_set()


    # Plot
    ####################
    
    plot = SpillPlot()
    plot.autoscale(op.autoscale)
    fig_canvas_agg = FigureCanvasTkAgg(plot.fig, master=window['plot'].TKCanvas)
    fig_canvas_agg.draw()
    fig_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    fig_toolbar = NavigationToolbar2Tk(fig_canvas_agg, window['plot_toolbar'].TKCanvas)
    fig_toolbar.update()


    # Thread functions
    ####################


    def filewatch(window):
        fname = None
        mtime_l1 = mtime_l2 = "INIT"
        while True:
            time.sleep(0.5)
            if not op.auto_update: continue
            
            try:
                if fname is None:
                    # find most recently updated file
                    fname = subprocess.run("find -L "+searchpath+" -type f -name '*.complex64' -printf '%T@\t%p\n' | sort | tail -1", shell=True, capture_output=True).stdout.split(b"\n")[0].split(b"\t")[1].decode()
                    if not fname: continue
                
                mtime = os.path.getmtime(fname)
                if op.wait_complete:
                    if mtime == mtime_l1:
                        # file is complete ...
                        if mtime_l1 != mtime_l2 and mtime_l2 is not None:
                            # ... but was not before
                            window.write_event_value('filename', fname)
                        fname = mtime_l1 = mtime_l2 = None
                else:
                    if mtime != mtime_l1:
                        window.write_event_value('filename', fname)
                    fname = None
                
                mtime_l2, mtime_l1 = mtime_l1, mtime

            except Exception as e:
                print("Error in filewatcher:", e)
    
    threading.Thread(target=filewatch, args=(window,), daemon=True).start()
    
    
    def load_and_prepare_data(fname, ref=0, *, draw=True):
        r = window[f"ref:{ref}:desc"];r.update("Loading...");window.refresh()
        data[ref] = Data.from_file(fname)
        x = np.sum(data[ref].rate)
        prepare_data(ref, draw=draw)
        print(f"Loaded trace {ref} from {fname}")
        
    def prepare_data(ref=0, *, draw=True):
        d = data.get(ref, NO_DATA)
        trace_data = SpillPlotTraceData(d, op.dt, op.t_start, op.t_stop, fmax=op.fmax, cumulative=op.cumsum, fft=op.plot_fft, quality=op.plot_quality)
        
        # update plots
        plot.update(trace_data, ref)
        r = window[f"ref:{ref}:desc"];r.update(d.name);r.set_tooltip(d.fname)
        if draw:        
            plot.autoscale(op.autoscale)
            plot.draw()
        
    #worker_queue = queue.Queue(maxsize=10)
    #def worker():
    #    while True:
    #        fun, args, kwargs = worker_queue.get()
    #        try:
    #            fun(*args, **kwargs)
    #        except:
    #            print("Worker error", fun, args)
    #            traceback.print_exc()
    #
    #threading.Thread(target=worker, args=(), daemon=True).start()
    

    # Event Loop
    ####################
    
    def copy_data(src, ref):
        data[ref] = data.get(src, NO_DATA)
        # the following is faster but equivalent to prepare_data:
        r = window[f"ref:{ref}:desc"];r.update(data[ref].name);r.set_tooltip(data[ref].fname)
        plot.copy(src, ref)
        plot.draw()
    
    while True:
        event, values = window.read()
        if event == sg.WINDOW_CLOSED or event == 'Quit': break
        if '::' in event:  # Label::Key from right_click_menu
            event = event.split('::')[1]
        
        ## Global options
        for key in ('cumsum', 'autoscale', 'autoref', 'wait_complete', 'auto_update'):
            op[key] = values[key]
        for key in ('t_start', 't_stop', 'fmax', 'dt'):
            window[key].update(background_color="white")
            try:
                op[key] = float(values[key])
            except:
                # leave unchanged or set to None
                if key in ('t_start', 't_stop') and not values[key].strip():
                    op[key] = None
                else:
                    # error: do not proceed
                    window[key].update(background_color="red")
                    if event == key:
                        event = ""
        window['wait_complete'].update(disabled=not op.auto_update)
        window['autoref'].update(disabled=not op.auto_update)


        ## Event handling
        
        if event == "filename":
            fname = values[event]
            if op.autoref and fname != data.get(0, NO_DATA).fname:
                copy_data(0, 3)
            load_and_prepare_data(fname)
            #worker_queue.put((load_and_prepare_data, (fname,), {}), timeout=5)
            
        elif event == "autoscale":
            plot.autoscale(op.autoscale)
            plot.draw()
            
        elif event == "fftlogscale":
            xscale = "log" if "log" in values[event].lower() and not "semi" in values[event].lower() else "linear"
            yscale = "log" if "log" in values[event].lower() else "linear"
            plot.axis_fft.set(xscale=xscale, yscale=yscale)
            plot.draw()
            
        elif event == "qualitytwinax":
            plot.axis_quality_twin.axis("on" if values[event] else "off")
            plot.draw()
            
        elif event in ("dt", "fmax", "t_start", "t_stop", "cumsum"):
            for ref in data:
                prepare_data(ref, draw=False)
                #worker_queue.put((prepare_data, (ref,), {}), timeout=5)
            plot.autoscale(op.autoscale)
            plot.draw()
        
        elif event in ('t_xlim', 't_clear'):
            t_start, t_stop = plot.axis_rate.get_xlim() if event == 't_xlim' else ('', '')
            window['t_start'].update(value=f"{t_start:.3g}" if t_start else '')
            window['t_stop'].update(value=f"{t_stop:.3g}" if t_stop else '')
            window.write_event_value('t_start', str(t_start))
            
        elif event.startswith('ref:'):
            ref = int(event.split(':')[1])
            cmd = event.split(':')[2]
            if cmd == 'open':
                fname = values[event]
                load_and_prepare_data(fname, ref)
                #worker_queue.put((load_and_prepare_data, (fname, ref), {}), timeout=5)

            elif cmd == 'set':   
                src = int(event.split(':')[3])
                copy_data(src, ref)


    # Cleanup
    ####################
    
    window.close()



    
