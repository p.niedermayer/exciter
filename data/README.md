# Data directory

Subfolders per day in the form `YYMMDD`

## RFKO Extraction with Feedback

Filenames in the form `HHMMSS_xxx.200kSps.complex64`
where
- `200kSps` is the sampling rate (Sps = Samples per second)
- `complex64` is the datatype
- the real part is the detector input in particles/s
- the imaginary part is the controller output (relative gain factor)

The script `plot_rfko_feedback_data.py` is provided for data analysis.

