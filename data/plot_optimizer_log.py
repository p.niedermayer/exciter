#!/usr/bin/env python3

#
# Log File Viewer for Optimizer
# 
# Allows for online and offline plotting of data from
# *.log files containing data in the following format:
# step, var1, var2, ..., objective
# 
# Author: Philipp Niedermayer (p.niedermayer@gsi.de)
# Date: 04.10.2023
#



#################################################
# Data handler
#################################################

from dataclasses import dataclass
import numpy as np
import re

FNAME_PATTERN = re.compile(r'((?P<path>.*)/)?(?P<name>.+)\.log')

@dataclass
class Data:
    name: str
    steps: np.array
    variables: np.array
    objective: np.array
    fname: str
    varnames: list = ()

    @staticmethod
    def from_file(fname):
        """Load log data from a *.log file
        """
        m = FNAME_PATTERN.match(fname)
        if m is None:
            return NO_DATA
        
        # try capture variables
        varnames = ()
        with open(fname, "r") as f:
            VAR_PATTERN = re.compile
            for line in f:
                if line.startswith("# Variables:"):
                    # Variables: ['var1', 'var2', 'var3']
                    varnames = line.split("# Variables:")[1].strip("()[] \n")
                    varnames = [v.strip("'\" ") for v in varnames.split(",")]
                    break
        
        # load data
        d = np.loadtxt(fname, delimiter=",") # , ndim=2
        if d.ndim == 1:
            d = d[np.newaxis, :]
        if d.ndim != 2 or d.size == 0:
            return NO_DATA
        
        return Data(
            name = m.group("name"),
            steps = d[:,0],
            variables = d[:,1:-1],
            objective = d[:,-1],
            fname = fname,
            varnames = varnames,
        )

NO_DATA = Data("NO DATA", np.empty(0), np.empty(0), np.empty(0), "")




#################################################
# Plot
#################################################

import pint
import matplotlib as mpl
from matplotlib import pyplot as plt
from scipy.interpolate import griddata

# Colors
# https://arxiv.org/abs/2107.02270
petroff_colors = ["#3f90da", "#ffa90e", "#bd1f01", "#94a4a2", "#832db6", "#a96b59", "#e76300", "#b9ac70", "#717581", "#92dadd"]
cmap_petroff_10 = mpl.colors.ListedColormap(petroff_colors, 'Petroff 10')
mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=petroff_colors)
cmap_petroff_bipolar = mpl.colors.LinearSegmentedColormap.from_list('Petroff bipolar', [petroff_colors[i] for i in reversed((2,6,1,3,9,0,4))])
cmap_petroff_bipolar.set_under(petroff_colors[8])
cmap_petroff_bipolar.set_over(petroff_colors[5])

# Matplotlib options
mpl.rcParams.update({
    # Improve layout
    'figure.constrained_layout.use': True,
    'legend.fontsize': 'x-small',
    'legend.title_fontsize': 'small',
    'grid.color': '#DDD',
    # Improve rendering time
    'path.simplify': True,
    'path.simplify_threshold': 1.0,
    'agg.path.chunksize': 16384,
    # ... and don't use markers
})



class OptimizerPlot:
    
    def __init__(self, nrefs=3, figsize=(10,5), dpi=80, mosaic=None):
        mosaic = mosaic or (("history","heatmap"),)
        
        self.fig, self.ax = fig, ax = plt.subplot_mosaic(mosaic, constrained_layout=True, figsize=figsize, dpi=dpi)
        self.artist = {}
        self.annotation = {}
        self.nrefs = nrefs
        
        def traces_with_refs(a, **kwargs):
            art = {
                i: a.plot([], [], **kwargs, zorder=2-i/nrefs/2, color=petroff_colors[i] if i < len(petroff_colors) else None)[0]
                for i in range(nrefs+1)
            }
            return art
        
        # History
        a = self.ax.get("history")
        if a:
            a.set(title="Optimisation history", ylabel="Objective", xlabel="Step")
            self.artist["history"] = traces_with_refs(a, ls="", marker=".")
            self.artist["history_best"] = traces_with_refs(a, ls="", marker="o")
            a.grid()
        
        # Heatmap (only for primary dataset)
        a = self.ax.get("heatmap")
        if a:
            a.set(title="Heatmap", ylabel="...", xlabel="...")
            self.artist["heatmap"] = {0: None}
            self.artist["heatmap_dots"] = {0: a.plot([], [], 'wx')[0]}
        
    
    def autoscale(self, enabled=True):
        for a in self.fig.axes:
            a.autoscale(enabled)
            if enabled:
                a.relim()
                a.autoscale_view()
        
    
    def scale_tight(self):
        lim = lambda a: (min(a), max(a))
        for key in self.ax:
            self.ax[key].set(xlim=lim(self.artist[key][0].get_xdata()))
    
    def update(self, data, ref=0, heatvar=[None, None]):
        
        # History
        a = self.artist.get("history")
        if a:
            a[ref].set_data(data.steps, data.objective)
            a[ref].set_label(data.name)
            best = ([data.steps[np.argmin(data.objective)]], [np.min(data.objective)]) if data.objective.size > 0 else ([],[])
            self.artist["history_best"][ref].set_data(*best)
        
        # Heatmap
        a = self.ax.get("heatmap")
        if a and ref in self.artist["heatmap"]: # only for primary dataset!
            if self.artist["heatmap"][ref]:
                self.artist["heatmap"][ref].remove()
                self.artist["heatmap"][ref] = None
                a.set(xlabel="...", ylabel="...")
            i, j = [data.varnames.index(h) if h in data.varnames else i for i,h in enumerate(heatvar)]
            if data.variables.ndim == 2 and max(i,j) < data.variables.shape[1]:
                x, y, z = data.variables[:,i], data.variables[:,j], data.objective
                # round coordinates to merge nearby (x,y) points
                epsilon_x = (np.max(x)-np.min(x))/1e3
                x = np.round(x/epsilon_x)*epsilon_x
                epsilon_y = (np.max(y)-np.min(y))/1e3
                y = np.round(y/epsilon_y)*epsilon_y
                # average z for identical (x,y) points
                xy = np.transpose([x,y])
                xy_unique, indices, frequency = np.unique(xy, axis=0, return_inverse=True, return_counts=True)
                z_sum = np.zeros(len(xy_unique))
                np.add.at(z_sum, indices, z) # sum z for all duplicates
                (x, y), z = xy_unique.T, z_sum/frequency
                # plot it
                self.artist["heatmap_dots"][ref].set_data(x, y)
                mi, ma = self.ax.get("history").get_ylim()
                mask = (z>mi) & (z<ma)
                x, y, z = x[mask], y[mask], z[mask]
                if i != j and np.unique(x).size >= 3 and np.unique(y).size >= 3: # need at least 3 data points
                    self.artist["heatmap"][ref] = a.tricontourf(x, y, z, cmap=cmap_petroff_bipolar, levels=20)
                    a.set(xlabel=data.varnames[i], ylabel=data.varnames[j])
        

    def copy(self, source, dest):
        for key, artists in self.artist.items():
            if source in artists and artists[source] is None: continue
            if dest not in artists or artists[dest] is None: continue
            artists[dest].set_data(*(artists[source].get_data() if source in artists else ([], [])))
            artists[dest].set_label(artists[source].get_label() if source in artists else None)
    
    def draw(self):        
        self.fig.canvas.draw()






#################################################
# GUI
#################################################

if __name__ == "__main__":

    import time
    import glob
    import os
    import threading
    import traceback
    import queue
    import PySimpleGUI as sg
    from matplotlib import pyplot as plt
    from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
    
    #print = sg.cprint

    
    # Global variables
    data = {}
    op_autoscale = True
    op_autoref = True
    op_heatvar = [None, None]
    

    # Layout
    ####################
    
    sg.theme('Default1')

    # Workflow options
    layout = [[
        sg.Checkbox("Autoset Ref 3 from previous data", default=op_autoref, key='autoref'),
    ]]
    # Trace management
    nrefs = 3
    for ref in range(1+nrefs):
        line = [
            sg.Input(key=f"ref:{ref}:open", enable_events=True, visible=False),
            sg.FileBrowse(f"Ref {ref}" if ref > 0 else "Data", target=f"ref:{ref}:open", pad=0,
                tooltip="Click to load from file", button_color=("white", petroff_colors[ref]), size=(7,1), file_types=(('LOG', '*.log'),)),
        ]
        if ref > 0:
            line.extend([
                sg.Button('SET', key=f"ref:{ref}:set:0", pad=0, tooltip="Set from data\nRight click for more options", right_click_menu=["setter",
                    [f"Clear::ref:{ref}:set:-1"] + [f"Copy from {i}::ref:{ref}:set:{i}" for i in range(1, 1+nrefs) if i!=ref]]),
            ])
        line.extend([
            sg.Text(key=f"ref:{ref}:desc", expand_x=True),
        ])
        layout.append(line)
    # Plot control
    layout.extend([
        [sg.Text("Heatmap vars:"), sg.Combo(["automatic"], key="heatvar_x", enable_events=True), sg.Combo(["automatic"], key="heatvar_y", enable_events=True),
        ],
        [sg.pin(sg.Canvas(key='plot_toolbar'), expand_x=True), sg.Push(),
         sg.Checkbox('Autoscale', default=op_autoscale, key='autoscale', enable_events=True),
        ],
        [sg.Canvas(key='plot', expand_x=True, expand_y=True)],
        [sg.Multiline(size=(40,6), expand_x=True, autoscroll=True, reroute_stdout=True, write_only=True, reroute_cprint=True)],
    ])

    window = sg.Window('RFKO Extraction with Feedback - Optimizer Log Viewer', layout, finalize=True, resizable=True)
    for key in ("heatvar_x", "heatvar_y"):
        window[key].bind("<Return>", "")
        

    # Plot
    ####################
    
    plot = OptimizerPlot()
    plot.autoscale(op_autoscale)
    fig_canvas_agg = FigureCanvasTkAgg(plot.fig, master=window['plot'].TKCanvas)
    fig_canvas_agg.draw()
    fig_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    fig_toolbar = NavigationToolbar2Tk(fig_canvas_agg, window['plot_toolbar'].TKCanvas)
    fig_toolbar.update()


    # Thread functions
    ####################


    def filewatch(window):
        fname = None
        mtime_l1 = mtime_l2 = "INIT"
        while True:
            try:
                if fname is None:
                    # most recent asuming incremental filenames
                    fname = sorted(glob.glob("**/*.log", recursive=True))[-1]
            
                mtime = os.path.getmtime(fname)
                if mtime != mtime_l1:
                    window.write_event_value('filename', fname)
                fname = None
                
                mtime_l2, mtime_l1 = mtime_l1, mtime

            except:
                print("Error filewatcher")

            time.sleep(1)
    
    threading.Thread(target=filewatch, args=(window,), daemon=True).start()
    
    
    def load_and_prepare_data(fname, ref=0):
        data[ref] = Data.from_file(fname)
        if data[ref] == NO_DATA:
            print(f"Warning: file {fname} ignored - it contains no data!")
        else:
            prepare_data(ref)
        
    def prepare_data(ref=0):
        d = data.get(ref, NO_DATA)
        window.write_event_value("THREAD_PREPARED_DATA", (d, ref))
        

    # Event Loop
    ####################
    
    def copy_data(src, ref):
        data[ref] = data.get(src, NO_DATA)
        # the following is faster but equivalent to prepare_data:
        r = window[f"ref:{ref}:desc"];r.update(data[ref].name);r.set_tooltip(data[ref].fname)
        plot.copy(src, ref)
        plot.draw()
    
    while True:
        event, values = window.read()
        if event == sg.WINDOW_CLOSED or event == 'Quit': break
        if '::' in event:  # Label::Key from right_click_menu
            event = event.split('::')[1]
        
        ## Global options
        
        op_autoscale = values['autoscale']
        op_autoref = values['autoref']
        op_heatvar = [values["heatvar_x"], values["heatvar_y"]]
        
        ## Event handling
        
        if event == "filename":
            fname = values[event]
            if op_autoref and fname != data.get(0, NO_DATA).fname:
                copy_data(0, 3)
            load_and_prepare_data(fname)
            
        elif event == "autoscale":
            plot.autoscale(op_autoscale)
            plot.draw()
        
        elif event.startswith("heatvar"):
            prepare_data(0)
            
        elif event.startswith('ref:'):
            ref = int(event.split(':')[1])
            cmd = event.split(':')[2]
            if cmd == 'open':
                fname = values[event]
                load_and_prepare_data(fname, ref)

            elif cmd == 'set':   
                src = int(event.split(':')[3])
                copy_data(src, ref)
        
        elif event == "THREAD_PREPARED_DATA":
            d, ref = values[event]
            print(f"Loaded trace {ref} from {d.fname}")
            window["heatvar_x"].update(values=d.varnames, value=op_heatvar[0])
            window["heatvar_y"].update(values=d.varnames, value=op_heatvar[1])
            r = window[f"ref:{ref}:desc"];r.update(d.name);r.set_tooltip(d.fname)
            plot.update(d, ref, heatvar=op_heatvar)
            plot.autoscale(op_autoscale)
            plot.draw()


    # Cleanup
    ####################
    
    window.close()



    