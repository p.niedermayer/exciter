#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Test RX & TX
# Author: Philipp Niedermayer
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from usrp_sink import usrp_sink  # grc-generated hier_block
from usrp_source import usrp_source  # grc-generated hier_block
import sip



class test_rx_tx(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Test RX & TX", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Test RX & TX")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "test_rx_tx")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 10e6
        self.f2 = f2 = 1e6
        self.f1 = f1 = 1e6
        self.a2 = a2 = 1
        self.a1 = a1 = 1

        ##################################################
        # Blocks
        ##################################################

        self.tab_tx = Qt.QTabWidget()
        self.tab_tx_widget_0 = Qt.QWidget()
        self.tab_tx_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_tx_widget_0)
        self.tab_tx_grid_layout_0 = Qt.QGridLayout()
        self.tab_tx_layout_0.addLayout(self.tab_tx_grid_layout_0)
        self.tab_tx.addTab(self.tab_tx_widget_0, 'Signal output (TX)')
        self.top_grid_layout.addWidget(self.tab_tx, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.tab_rx = Qt.QTabWidget()
        self.tab_rx_widget_0 = Qt.QWidget()
        self.tab_rx_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_rx_widget_0)
        self.tab_rx_grid_layout_0 = Qt.QGridLayout()
        self.tab_rx_layout_0.addLayout(self.tab_rx_grid_layout_0)
        self.tab_rx.addTab(self.tab_rx_widget_0, 'Signal input (RX)')
        self.top_grid_layout.addWidget(self.tab_rx, 1, 0, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._f2_tool_bar = Qt.QToolBar(self)
        self._f2_tool_bar.addWidget(Qt.QLabel("TX 2: Frequency [Hz]" + ": "))
        self._f2_line_edit = Qt.QLineEdit(str(self.f2))
        self._f2_tool_bar.addWidget(self._f2_line_edit)
        self._f2_line_edit.editingFinished.connect(
            lambda: self.set_f2(eval(str(self._f2_line_edit.text()))))
        self.tab_tx_grid_layout_0.addWidget(self._f2_tool_bar, 0, 2, 1, 1)
        for r in range(0, 1):
            self.tab_tx_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_tx_grid_layout_0.setColumnStretch(c, 1)
        self._f1_tool_bar = Qt.QToolBar(self)
        self._f1_tool_bar.addWidget(Qt.QLabel("TX 1: Frequency [Hz]" + ": "))
        self._f1_line_edit = Qt.QLineEdit(str(self.f1))
        self._f1_tool_bar.addWidget(self._f1_line_edit)
        self._f1_line_edit.editingFinished.connect(
            lambda: self.set_f1(eval(str(self._f1_line_edit.text()))))
        self.tab_tx_grid_layout_0.addWidget(self._f1_tool_bar, 0, 1, 1, 1)
        for r in range(0, 1):
            self.tab_tx_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_tx_grid_layout_0.setColumnStretch(c, 1)
        self._a2_tool_bar = Qt.QToolBar(self)
        self._a2_tool_bar.addWidget(Qt.QLabel("TX 2: Amplitude [V]" + ": "))
        self._a2_line_edit = Qt.QLineEdit(str(self.a2))
        self._a2_tool_bar.addWidget(self._a2_line_edit)
        self._a2_line_edit.editingFinished.connect(
            lambda: self.set_a2(eval(str(self._a2_line_edit.text()))))
        self.tab_tx_grid_layout_0.addWidget(self._a2_tool_bar, 1, 2, 1, 1)
        for r in range(1, 2):
            self.tab_tx_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_tx_grid_layout_0.setColumnStretch(c, 1)
        self._a1_tool_bar = Qt.QToolBar(self)
        self._a1_tool_bar.addWidget(Qt.QLabel("TX 1: Amplitude [V]" + ": "))
        self._a1_line_edit = Qt.QLineEdit(str(self.a1))
        self._a1_tool_bar.addWidget(self._a1_line_edit)
        self._a1_line_edit.editingFinished.connect(
            lambda: self.set_a1(eval(str(self._a1_line_edit.text()))))
        self.tab_tx_grid_layout_0.addWidget(self._a1_tool_bar, 1, 1, 1, 1)
        for r in range(1, 2):
            self.tab_tx_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_tx_grid_layout_0.setColumnStretch(c, 1)
        self.usrp_source_0 = usrp_source(
            fs_signal=samp_rate,
        )
        self.usrp_sink_0 = usrp_sink(
            fs_signal=samp_rate,
        )
        self.qtgui_sink_x_0_0 = qtgui.sink_c(
            4096, #fftsize
            window.WIN_BLACKMAN_hARRIS, #wintype
            0, #fc
            samp_rate, #bw
            "RX 2", #name
            True, #plotfreq
            True, #plotwaterfall
            True, #plottime
            False, #plotconst
            None # parent
        )
        self.qtgui_sink_x_0_0.set_update_time(1.0/10)
        self._qtgui_sink_x_0_0_win = sip.wrapinstance(self.qtgui_sink_x_0_0.qwidget(), Qt.QWidget)

        self.qtgui_sink_x_0_0.enable_rf_freq(False)

        self.tab_rx_grid_layout_0.addWidget(self._qtgui_sink_x_0_0_win, 0, 2, 1, 1)
        for r in range(0, 1):
            self.tab_rx_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.tab_rx_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_sink_x_0 = qtgui.sink_c(
            4096, #fftsize
            window.WIN_BLACKMAN_hARRIS, #wintype
            0, #fc
            samp_rate, #bw
            "RX 1", #name
            True, #plotfreq
            True, #plotwaterfall
            True, #plottime
            False, #plotconst
            None # parent
        )
        self.qtgui_sink_x_0.set_update_time(1.0/10)
        self._qtgui_sink_x_0_win = sip.wrapinstance(self.qtgui_sink_x_0.qwidget(), Qt.QWidget)

        self.qtgui_sink_x_0.enable_rf_freq(False)

        self.tab_rx_grid_layout_0.addWidget(self._qtgui_sink_x_0_win, 0, 1, 1, 1)
        for r in range(0, 1):
            self.tab_rx_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.tab_rx_grid_layout_0.setColumnStretch(c, 1)
        self.blocks_null_source_0 = blocks.null_source(gr.sizeof_float*1)
        self.blocks_float_to_complex_1_0 = blocks.float_to_complex(1)
        self.blocks_float_to_complex_1 = blocks.float_to_complex(1)
        self.blocks_float_to_complex_0 = blocks.float_to_complex(1)
        self.blocks_complex_to_float_0 = blocks.complex_to_float(1)
        self.analog_sig_source_x_0_0 = analog.sig_source_f(samp_rate, analog.GR_COS_WAVE, f2, a2, 0, 0)
        self.analog_sig_source_x_0 = analog.sig_source_f(samp_rate, analog.GR_COS_WAVE, f1, a1, 0, 0)


        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_float_to_complex_0, 0))
        self.connect((self.analog_sig_source_x_0_0, 0), (self.blocks_float_to_complex_0, 1))
        self.connect((self.blocks_complex_to_float_0, 0), (self.blocks_float_to_complex_1, 0))
        self.connect((self.blocks_complex_to_float_0, 1), (self.blocks_float_to_complex_1_0, 0))
        self.connect((self.blocks_float_to_complex_0, 0), (self.usrp_sink_0, 0))
        self.connect((self.blocks_float_to_complex_1, 0), (self.qtgui_sink_x_0, 0))
        self.connect((self.blocks_float_to_complex_1_0, 0), (self.qtgui_sink_x_0_0, 0))
        self.connect((self.blocks_null_source_0, 0), (self.blocks_float_to_complex_1, 1))
        self.connect((self.blocks_null_source_0, 0), (self.blocks_float_to_complex_1_0, 1))
        self.connect((self.usrp_source_0, 0), (self.blocks_complex_to_float_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "test_rx_tx")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.analog_sig_source_x_0_0.set_sampling_freq(self.samp_rate)
        self.qtgui_sink_x_0.set_frequency_range(0, self.samp_rate)
        self.qtgui_sink_x_0_0.set_frequency_range(0, self.samp_rate)
        self.usrp_sink_0.set_fs_signal(self.samp_rate)
        self.usrp_source_0.set_fs_signal(self.samp_rate)

    def get_f2(self):
        return self.f2

    def set_f2(self, f2):
        self.f2 = f2
        Qt.QMetaObject.invokeMethod(self._f2_line_edit, "setText", Qt.Q_ARG("QString", repr(self.f2)))
        self.analog_sig_source_x_0_0.set_frequency(self.f2)

    def get_f1(self):
        return self.f1

    def set_f1(self, f1):
        self.f1 = f1
        Qt.QMetaObject.invokeMethod(self._f1_line_edit, "setText", Qt.Q_ARG("QString", repr(self.f1)))
        self.analog_sig_source_x_0.set_frequency(self.f1)

    def get_a2(self):
        return self.a2

    def set_a2(self, a2):
        self.a2 = a2
        Qt.QMetaObject.invokeMethod(self._a2_line_edit, "setText", Qt.Q_ARG("QString", repr(self.a2)))
        self.analog_sig_source_x_0_0.set_amplitude(self.a2)

    def get_a1(self):
        return self.a1

    def set_a1(self, a1):
        self.a1 = a1
        Qt.QMetaObject.invokeMethod(self._a1_line_edit, "setText", Qt.Q_ARG("QString", repr(self.a1)))
        self.analog_sig_source_x_0.set_amplitude(self.a1)




def main(top_block_cls=test_rx_tx, options=None):

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
