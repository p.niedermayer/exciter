# Ramps definition files

Ramps can be used for amplitude control with the `exciter_extraction` flowgraph.

Ramps are defined by linearly interpolated points from a CSV file (columns separated by `,` and decimal separator `.`):
- the first column is offset (typically as relative fraction between 0 and 1, so the ramp length can be changed dynamically at runtime)
- the second column is the value
- outside the defined ramp, the first or last value is used respectively

