#!/usr/bin/env python3

import numpy as np
from scipy.special import erf, owens_t
import time
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import pickle
import PySimpleGUI as sg

# https://arxiv.org/abs/2107.02270
petroff_colors = ["#3f90da", "#ffa90e", "#bd1f01", "#94a4a2", "#832db6", "#a96b59", "#e76300", "#b9ac70", "#717581", "#92dadd"]
cmap_petroff_10 = mpl.colors.ListedColormap(petroff_colors, 'Petroff 10')
mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=petroff_colors)

cmap_petroff_gradient = mpl.colors.LinearSegmentedColormap.from_list('Petroff gradient', [petroff_colors[i] for i in (9,0,4,2,6,1)])
cmap_petroff_gradient.set_under(petroff_colors[3])
cmap_petroff_gradient.set_over(petroff_colors[7])
mpl.rcParams['image.cmap'] = cmap_petroff_gradient

cmap_petroff_bipolar = mpl.colors.LinearSegmentedColormap.from_list('Petroff bipolar', [petroff_colors[i] for i in (2,6,1,3,9,0,4)])
cmap_petroff_bipolar.set_under(petroff_colors[5])
cmap_petroff_bipolar.set_over(petroff_colors[8])

def furukawa_tub(r=0.5, o=0, s=1, *, n=101):
    # T. Furukawa et al. (2004): Global spill control in RF-knockout slow-extraction. DOI: 10.1016/j.nima.2003.11.395.
    n = np.linspace(0, 1, n-2)
    a = np.exp(-(1/(1-r))**2)
    sigma2 = -10/np.log(0.9*n*(1-a)+a)
    x = n[:-1]
    y = o+s*np.diff(sigma2)
    return np.concatenate([[0], x, [1]]), np.concatenate([[0], y, [0]])

def poly_tub(a0=0.566, am=0.564, m=0.18, a1=0.61, *, n=101):
    x = [1/n, m-1e-6, m+1e-6, 1-1/n]
    y = [a0, am, am, a1]
    p = np.polynomial.Polynomial.fit(x, y, 3, domain=[x[0], x[-1]], window=[0, 1])
    x = np.linspace(*p.domain, n-2)
    y = p(x)
    return np.concatenate([[0], x, [1]]), np.concatenate([[0], y, [0]])

def double_poly_tub(a0=0.566, am=0.564, m=0.18, a1=0.61, *, n=101):
    p1 = np.polynomial.Polynomial.fit([1/n, m-1e-6, m+1e-6], [a0, am, am], 2)
    p2 = np.polynomial.Polynomial.fit([m-1e-6, m+1e-6, 1-1/n], [am, am, a1], 2)
    x = np.linspace(1/n, 1-1/n, n-2)
    y = np.where(x < m, p1(x), p2(x))
    return np.concatenate([[0], x, [1]]), np.concatenate([[0], y, [0]])

def linear(l0=0, l1=1, d=0, t=1):
    return [0, d, d+t, 1], [l0, l0, l1, l1]

def smooth(y0=0, y1=1, t0=0.1, dt=0.8, alpha=0, *, n=101):
    x = np.linspace(0, 1, n-2)
    w = 0.15 * 2/(1+np.exp(-np.abs(alpha/2)))
    xx = (x-t0)/dt-1/2
    xx = xx + 3*w*(1/(1+np.exp(-1.5*alpha/2))-1/2)
    y = y0 + (y1-y0) * ((1+erf(xx/w/2**.5))/2 - 2*owens_t(xx/w, 1.5*alpha/2))
    return x, y

def detune(q0, q1, delay, duration, reverse=False, *, n=101):
    """Average detuning as function of normalized amplitude"""
    Hn = np.linspace(0, 1, n)[:,None]
    #Hn = np.linspace(0, 1**4, n)[:,None]**.25
    A = np.linspace(0, 2*np.pi, 100 * 3*2*2, endpoint=False)[None, :]
    R = 27 - 36*Hn*np.cos(3*A)**2 + 4*Hn**2*np.cos(3*A)**3 * ( 2*np.cos(3*A) - np.emath.sqrt(2*np.cos(6*A) + 2 - 4/Hn) )  # note the use of emath.sqrt !
    Jn = ( 6  +  R**(1/3) * (np.sqrt(3)*1j - 1)  +  R**(-1/3) * (np.sqrt(3)*1j + 1) * (8*Hn*np.cos(3*A)**2 - 9) )  /  ( 16 * np.cos(3*A)**2 )
    Jn = np.abs(Jn)
    Jn = np.where(Hn == 0, 0, Jn)
    Jn = np.where(np.logical_and(Hn > 0, np.abs((3*A)%np.pi - np.pi/2) < 1e-12), Hn/6, Jn)
    Jn = np.where(np.logical_and(Hn == 1, (3*A)%(2*np.pi) == 0), Hn/2, Jn)
    mu3 = 3 * (Hn/2/Jn - 1)/2
    mu3 = np.where(Jn == 0, 3, mu3)
    mu3_avg = 2*np.pi / np.trapz(1/mu3, A, axis=1)
    x, y = np.sqrt(Hn[:,0]), mu3_avg/3
    x, y = np.concatenate([[0], delay + duration*x, [1]]), np.concatenate([[q0], q1 + (q0-q1)*y, [q1]])
    if reverse: y = y[::-1]
    return x, y

#%matplotlib widget
#plt.figure()
##print(np.loadtxt('ramp_ko_sis.csv', delimiter=','))
#plt.plot(*np.loadtxt('ramp_ko_sis.csv', delimiter=',').T, 'k')
#plt.plot(*furukawa_tub(o=0.56, r=0.1, s=0.3))
#plt.plot(*poly_tub())
#plt.ylim(0.56, 0.62)
#plt.grid()
##plt.plot(*furukawa_tub_mod(), '--')
#None


# GUI

function = None
functions = {
    #'Simple poly tub': {
    #    'f': poly_tub,
    #    'param': ['a0','am','m','a1'],
    #    'layout': [[sg.Text('Initial level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.566, key='param:a0', size=(5,1), enable_events=True)],
    #               [sg.Text('Level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.564, key='param:am', size=(5,1), enable_events=True), 
    #                sg.Text('at:'), sg.Spin(list(np.round(np.arange(0, 1, 0.01), 2)), 0.18, key='param:m', size=(5,1), enable_events=True)],
    #               [sg.Text('Final level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.61, key='param:a1', size=(5,1), enable_events=True)],
    #              ],
    #},
    'Double poly tub': {
        'f': double_poly_tub,
        'param': ['a0','am','m','a1'],
        'layout': [[sg.Text('Initial level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.566, key='param:a0', size=(5,1), enable_events=True)],
                   [sg.Text('Level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.564, key='param:am', size=(5,1), enable_events=True), 
                    sg.Text('at:'), sg.Spin(list(np.round(np.arange(0, 1, 0.01), 2)), 0.18, key='param:m', size=(5,1), enable_events=True)],
                   [sg.Text('Final level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.61, key='param:a1', size=(5,1), enable_events=True)],
                  ],
    },
    'Furukawa tub': {
        'f': furukawa_tub,
        'param': ['o','s','r'],
        'layout': [[sg.Text('Offset:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.555, key='param:o', size=(5,1), enable_events=True)],
                   [sg.Text('Scale:'), sg.Spin(list(np.round(np.arange(0, 1, 0.01), 2)), 0.05, key='param:s', size=(5,1), enable_events=True)], 
                   [sg.Text('Initial:'), sg.Spin(list(np.round(np.arange(0, 1, 0.01), 2)), 0.5, key='param:r', size=(5,1), enable_events=True)],
                  ],
    },
    'Linear': {
        'f': linear,
        'param': ['l0','l1','d','t'],
        'layout': [[sg.Text('Initial level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.1, key='param:l0', size=(5,1), enable_events=True)],
                   [sg.Text('Final level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.8, key='param:l1', size=(5,1), enable_events=True)],
                   [sg.Text('Delay:'), sg.Spin(list(np.round(np.arange(0, 1, 0.01), 2)), 0.1, key='param:d', size=(5,1), enable_events=True)], 
                   [sg.Text('Duration:'), sg.Spin(list(np.round(np.arange(0, 1, 0.01), 2)), 0.8, key='param:t', size=(5,1), enable_events=True)],
                  ],
    },
    'Smooth': {
        'f': smooth,
        'param': ['y0','y1','alpha','t0', 'dt'],
        'layout': [[sg.Text('Initial level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.1, key='param:y0', size=(5,1), enable_events=True)],
                   [sg.Text('Final level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.8, key='param:y1', size=(5,1), enable_events=True)],
                   [sg.Text('Skew:'), sg.Spin(list(np.round(np.arange(-10, 11, 1), 0)), 0, key='param:alpha', size=(5,1), enable_events=True)],
                   [sg.Text('Delay:'), sg.Spin(list(np.round(np.arange(0, 1, 0.01), 2)), 0.1, key='param:t0', size=(5,1), enable_events=True)], 
                   [sg.Text('Duration:'), sg.Spin(list(np.round(np.arange(0, 1, 0.01), 2)), 0.8, key='param:dt', size=(5,1), enable_events=True)],
                  ],
    },
    'Detune': {
        'f': detune,
        'param': ['q0','q1','delay', 'duration', 'reverse'],
        'layout': [[sg.Text('Initial level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.270, key='param:q0', size=(5,1), enable_events=True)],
                   [sg.Text('Final level:'), sg.Spin(list(np.round(np.arange(0, 1, 0.001), 3)), 0.333, key='param:q1', size=(5,1), enable_events=True)],
                   [sg.Text('Delay:'), sg.Spin(list(np.round(np.arange(0, 1, 0.01), 2)), 0.0, key='param:delay', size=(5,1), enable_events=True)], 
                   [sg.Text('Duration:'), sg.Spin(list(np.round(np.arange(0, 1, 0.01), 2)), 1.0, key='param:duration', size=(5,1), enable_events=True), sg.Checkbox('Reverse', key='param:reverse', enable_events=True)],
                  ],
    },
}

refs = {i: dict(data=[[],[]], f=None, params=None) for i in range(1,5)}
layout_refs = [[sg.Text('Ref %i'%i), sg.Button('Store', key='ref:%i:set'%i), 
                sg.Button('Load', key='ref:%i:apply'%i, disabled=True),
                sg.Input(key='ref:%i:open'%i, enable_events=True, visible=False), sg.FileBrowse('Browse', target='ref:%i:open'%i, file_types=(('CSV', '*.csv'),)),
                sg.Button('X', key='ref:%i:clear'%i, disabled=True),
               ] for i in refs]

layout = [[sg.Text('Function:'), sg.Combo(list(functions.keys()), function, enable_events=True, readonly=True, key='function')], []]
for fun in functions:
    layout[-1].append(sg.Column(functions[fun]['layout'], key='function:'+fun, visible=fun==function))
layout.extend([[sg.Text('Autosave to:'), sg.Input(key='filename', size=(20,1)), sg.FileSaveAs('Browse', target='filename', file_types=(('CSV', '*.csv'),)), sg.Text('...', key='saved')],
               [sg.Text('...', background_color='darkgreen', key='status')],
               [sg.Canvas(key='plot', expand_x=True, expand_y=True)],
               [sg.pin(sg.Canvas(key='plot_toolbar')), sg.Text('Show value span:'), sg.Input(0, key='width', size=(20,1), enable_events=True)],
               [sg.Column(layout_refs, key='refs')]])
window = sg.Window('CSV function Helper', layout, finalize=True)
for key in window.key_dict:
    if key.startswith('param:'):
        window[key].bind("<Return>", "_Enter")

# Plot
fig = plt.figure(constrained_layout=True, dpi=110)
data = np.array([]),np.array([])
fig_plot,  = plt.plot(*data, 'k-', zorder=100, label='Function')
fig_span = None
fig_refs = {i: plt.plot([],[], ':', zorder=50, label='Ref %i'%i)[0] for i in refs}
plt.xlim(-0.03,1.03)
plt.ylim(0,1)
plt.grid()
plt.legend()
fig_canvas_agg = FigureCanvasTkAgg(fig, master=window['plot'].TKCanvas)
fig_canvas_agg.draw()
fig_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
fig_toolbar = NavigationToolbar2Tk(fig_canvas_agg, window['plot_toolbar'].TKCanvas)
fig_toolbar.update()



# Event Loop
while True:
    event, values = window.read()
    if event == sg.WINDOW_CLOSED or event == 'Quit':
        break

    print(event)

    try:
        if event.startswith('ref:'):
            i = int(event.split(':')[1])
            cmd = event.split(':')[2]
            if cmd == 'set':
                refs[i].update(dict(data=data, f=function, params={key:values['param:'+key] for key in functions[function]['param']}))
                window['ref:%i:apply'%i].update(disabled=False)
                window['ref:%i:clear'%i].update(disabled=False)
            elif cmd == 'open':
                refs[i].update(dict(data=np.loadtxt(values[event], delimiter=',').T, f=None, params=None))
                try:
                    with open(values[event]+'.param', 'rb') as file:
                        refs[i].update(pickle.load(file))
                except:
                    pass
                window['ref:%i:apply'%i].update(disabled=refs[i]['f'] is None)
                window['ref:%i:clear'%i].update(disabled=False)
            elif cmd == 'apply':
                window['function'].update(refs[i]['f'])
                values['function'] = refs[i]['f']
                for key, value in refs[i]['params'].items():
                    window['param:'+key].update(value)
                    values['param:'+key] = value
                event = 'function'
            elif cmd == 'clear':
                refs[i].update(dict(data=[[],[]], f=None, params=None))
                window['ref:%i:apply'%i].update(disabled=True)
                window['ref:%i:clear'%i].update(disabled=True)
                
            fig_refs[i].set_data(*refs[i]['data'])
            fig.canvas.draw()
        
        if event == 'function':
            function = values['function']
            for fun in functions:
                window['function:'+fun].update(visible=fun==function)
        
        if event == 'function' or event.startswith('param:'):
            # plot and save
            params = {key:float(values['param:'+key]) for key in functions[function]['param']}
            data = functions[function]['f'](**params)
            fig_plot.set_data(*data)
            fig.canvas.draw()
            if values['filename']:
                assert np.all(np.isfinite(data)), 'Non-finite values in function!'
                np.savetxt(values['filename'], np.transpose(data), fmt='%.6f', delimiter=',')
                with open(values['filename']+'.param', 'wb') as file:
                    pickle.dump(dict(f=function, params=params), file)
                window['saved'].update('Saved on ' + time.asctime() + ' to\n' + values['filename'])
            else:
                window['saved'].update('...')
        
        if event == 'width' or event == 'function' or event.startswith('param:'):
            if fig_span: fig_span.remove()
            fig_span = plt.fill_between(data[0], data[1]-float(values['width'])/2, data[1]+float(values['width'])/2, color='k', zorder=10, alpha=0.5, lw=0)
            fig.canvas.draw()
            
            
    except Exception as e:
        window['status'].update('Error: ' + str(e), background_color='red')
        #raise
    else:
        window['status'].update('OK', background_color='darkgreen')
    
# Cleanup
window.close()


