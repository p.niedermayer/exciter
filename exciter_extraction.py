#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Excitation for RF KO extraction
# Author: Philipp Niedermayer
# Copyright: Copyright 2023 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from exciter_signals import exciter_signals  # grc-generated hier_block
from gnuradio import beam_exciter
import pmt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from triggered_output import triggered_output  # grc-generated hier_block
from usrp_sink import usrp_sink  # grc-generated hier_block


def snipfcn_snippet_1(self):
    # Handle an uncought exception
    def handle_exception(exc_type, exc_value, exc_traceback):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        print("Troubleshooting help at https://git.gsi.de/p.niedermayer/exciter/-/wikis/Troubleshooting")
        # Inform user by flashing window in red
        w = self # the top block, i.e. main qt window
        w.setAttribute(Qt.Qt.WA_StyledBackground, True)
        w.setStyleSheet('background-color: red;')
        from PyQt5 import QtCore
        QtCore.QTimer.singleShot(200, lambda: w.setStyleSheet(""))

    sys.excepthook = handle_exception


def snippets_init_before_blocks(tb):
    snipfcn_snippet_1(tb)

class exciter_extraction(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Excitation for RF KO extraction", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Excitation for RF KO extraction")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "exciter_extraction")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.upsample = upsample = 20
        self.fs_signal = fs_signal = 5e6
        self.maxoutbuf = maxoutbuf = 64
        self.length = length = 1
        self.fs_signal_baseband = fs_signal_baseband = fs_signal/upsample
        self.fs_gpio = fs_gpio = 1000
        self.filename_amplitude = filename_amplitude = 'ramps/const_1.csv'
        self.delay = delay = 1e-3

        ##################################################
        # Blocks
        ##################################################
        snippets_init_before_blocks(self)
        self.tab_ramp = Qt.QTabWidget()
        self.tab_ramp_widget_0 = Qt.QWidget()
        self.tab_ramp_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_ramp_widget_0)
        self.tab_ramp_grid_layout_0 = Qt.QGridLayout()
        self.tab_ramp_layout_0.addLayout(self.tab_ramp_grid_layout_0)
        self.tab_ramp.addTab(self.tab_ramp_widget_0, 'Amplitude ramp')
        self.top_grid_layout.addWidget(self.tab_ramp, 4, 0, 1, 1)
        for r in range(4, 5):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.tab_output = Qt.QTabWidget()
        self.tab_output_widget_0 = Qt.QWidget()
        self.tab_output_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_output_widget_0)
        self.tab_output_grid_layout_0 = Qt.QGridLayout()
        self.tab_output_layout_0.addLayout(self.tab_output_grid_layout_0)
        self.tab_output.addTab(self.tab_output_widget_0, 'Output')
        self.top_grid_layout.addWidget(self.tab_output, 10, 0, 1, 1)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.tab_excitation = Qt.QTabWidget()
        self.tab_excitation_widget_0 = Qt.QWidget()
        self.tab_excitation_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_excitation_widget_0)
        self.tab_excitation_grid_layout_0 = Qt.QGridLayout()
        self.tab_excitation_layout_0.addLayout(self.tab_excitation_grid_layout_0)
        self.tab_excitation.addTab(self.tab_excitation_widget_0, 'Excitation Signal')
        self.top_grid_layout.addWidget(self.tab_excitation, 2, 0, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._length_tool_bar = Qt.QToolBar(self)
        self._length_tool_bar.addWidget(Qt.QLabel("Length [s]" + ": "))
        self._length_line_edit = Qt.QLineEdit(str(self.length))
        self._length_tool_bar.addWidget(self._length_line_edit)
        self._length_line_edit.editingFinished.connect(
            lambda: self.set_length(eng_notation.str_to_num(str(self._length_line_edit.text()))))
        self.tab_ramp_grid_layout_0.addWidget(self._length_tool_bar, 1, 0, 1, 1)
        for r in range(1, 2):
            self.tab_ramp_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_ramp_grid_layout_0.setColumnStretch(c, 1)
        self._filename_amplitude_tool_bar = Qt.QToolBar(self)
        self._filename_amplitude_tool_bar.addWidget(Qt.QLabel("Amplitude ramp filename" + ": "))
        self._filename_amplitude_line_edit = Qt.QLineEdit(str(self.filename_amplitude))
        self._filename_amplitude_tool_bar.addWidget(self._filename_amplitude_line_edit)
        self._filename_amplitude_line_edit.editingFinished.connect(
            lambda: self.set_filename_amplitude(str(str(self._filename_amplitude_line_edit.text()))))
        self.tab_ramp_grid_layout_0.addWidget(self._filename_amplitude_tool_bar, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_ramp_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_ramp_grid_layout_0.setColumnStretch(c, 1)
        self.usrp_sink_0 = usrp_sink(
            fs_signal=fs_signal,
        )
        self.sig = exciter_signals(
            samp_rate=fs_signal,
            upsample=upsample,
        )

        self.tab_excitation_grid_layout_0.addWidget(self.sig, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_excitation_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_excitation_grid_layout_0.setColumnStretch(c, 1)
        self.output = triggered_output(
            delay=0,
            fs_gpio=fs_gpio,
            fs_signal=fs_signal,
            open_mode=True,
            rfnoc_graph=None,
            show_output_real_imag='real',
        )

        self.tab_output_grid_layout_0.addWidget(self.output, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.blocks_multiply_xx_0 = blocks.multiply_vff(1)
        self.blocks_float_to_complex_0 = blocks.float_to_complex(1)
        self.blocks_float_to_complex_0.set_max_output_buffer(maxoutbuf)
        self.beam_exciter_rampArbitrary_0 = beam_exciter.rampArbitrary(filename_amplitude, length*fs_signal, pmt.PMT_NIL, pmt.intern("trigger_switch_block"))


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.output, 'reset'), (self.beam_exciter_rampArbitrary_0, 'reset'))
        self.connect((self.beam_exciter_rampArbitrary_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.blocks_float_to_complex_0, 0), (self.output, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.blocks_float_to_complex_0, 0))
        self.connect((self.output, 0), (self.usrp_sink_0, 0))
        self.connect((self.sig, 0), (self.beam_exciter_rampArbitrary_0, 0))
        self.connect((self.sig, 0), (self.blocks_multiply_xx_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "exciter_extraction")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_upsample(self):
        return self.upsample

    def set_upsample(self, upsample):
        self.upsample = upsample
        self.set_fs_signal_baseband(self.fs_signal/self.upsample)
        self.sig.set_upsample(self.upsample)

    def get_fs_signal(self):
        return self.fs_signal

    def set_fs_signal(self, fs_signal):
        self.fs_signal = fs_signal
        self.set_fs_signal_baseband(self.fs_signal/self.upsample)
        self.beam_exciter_rampArbitrary_0.set_offset_scale(self.length*self.fs_signal)
        self.output.set_fs_signal(self.fs_signal)
        self.sig.set_samp_rate(self.fs_signal)
        self.usrp_sink_0.set_fs_signal(self.fs_signal)

    def get_maxoutbuf(self):
        return self.maxoutbuf

    def set_maxoutbuf(self, maxoutbuf):
        self.maxoutbuf = maxoutbuf

    def get_length(self):
        return self.length

    def set_length(self, length):
        self.length = length
        Qt.QMetaObject.invokeMethod(self._length_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.length)))
        self.beam_exciter_rampArbitrary_0.set_offset_scale(self.length*self.fs_signal)

    def get_fs_signal_baseband(self):
        return self.fs_signal_baseband

    def set_fs_signal_baseband(self, fs_signal_baseband):
        self.fs_signal_baseband = fs_signal_baseband

    def get_fs_gpio(self):
        return self.fs_gpio

    def set_fs_gpio(self, fs_gpio):
        self.fs_gpio = fs_gpio
        self.output.set_fs_gpio(self.fs_gpio)

    def get_filename_amplitude(self):
        return self.filename_amplitude

    def set_filename_amplitude(self, filename_amplitude):
        self.filename_amplitude = filename_amplitude
        Qt.QMetaObject.invokeMethod(self._filename_amplitude_line_edit, "setText", Qt.Q_ARG("QString", str(self.filename_amplitude)))
        self.beam_exciter_rampArbitrary_0.set_filename(self.filename_amplitude)

    def get_delay(self):
        return self.delay

    def set_delay(self, delay):
        self.delay = delay




def main(top_block_cls=exciter_extraction, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        gr.logger("realtime").warning("Error: failed to enable real-time scheduling.")

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
