#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Save Excitation Signals to File
# Author: Philipp Niedermayer
# Copyright: Copyright 2022 Philipp Niedermayer, GSI Helmholtzzentrum für Schwerionenforschung
# GNU Radio version: 3.10.8.0

from PyQt5 import Qt
from gnuradio import qtgui
import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from exciter_signals import exciter_signals  # grc-generated hier_block
from gnuradio import analog
from gnuradio import beam_exciter
from gnuradio.beam_exciter import ensure_pmt
import pmt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio.qtgui import Range, RangeWidget
from PyQt5 import QtCore
import sip



class exciter_to_file(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Save Excitation Signals to File", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Save Excitation Signals to File")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "exciter_to_file")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Variables
        ##################################################
        self.upsample = upsample = 20
        self.output_duration = output_duration = 3
        self.manual_trigger = manual_trigger = 0
        self.fs_signal = fs_signal = 10e6
        self.daq_filename = daq_filename = ""+f"signals/%y%m%d_%H%M%S_tmp.{fs_signal/1e6:g}MSps.float32"

        ##################################################
        # Blocks
        ##################################################

        self.tab_output = Qt.QTabWidget()
        self.tab_output_widget_0 = Qt.QWidget()
        self.tab_output_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_output_widget_0)
        self.tab_output_grid_layout_0 = Qt.QGridLayout()
        self.tab_output_layout_0.addLayout(self.tab_output_grid_layout_0)
        self.tab_output.addTab(self.tab_output_widget_0, 'Output')
        self.top_grid_layout.addWidget(self.tab_output, 30, 0, 1, 1)
        for r in range(30, 31):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.tab_excitation = Qt.QTabWidget()
        self.tab_excitation_widget_0 = Qt.QWidget()
        self.tab_excitation_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_excitation_widget_0)
        self.tab_excitation_grid_layout_0 = Qt.QGridLayout()
        self.tab_excitation_layout_0.addLayout(self.tab_excitation_grid_layout_0)
        self.tab_excitation.addTab(self.tab_excitation_widget_0, 'Excitation Signal')
        self.top_grid_layout.addWidget(self.tab_excitation, 2, 0, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._output_duration_range = Range(0, int((2**31-1)/fs_signal), 0.1, 3, 200)
        self._output_duration_win = RangeWidget(self._output_duration_range, self.set_output_duration, "Burst duration [s]", "counter", float, QtCore.Qt.Horizontal)
        self.tab_output_grid_layout_0.addWidget(self._output_duration_win, 1, 0, 1, 2)
        for r in range(1, 2):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 2):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        _manual_trigger_push_button = Qt.QPushButton('Save')
        _manual_trigger_push_button = Qt.QPushButton('Save')
        self._manual_trigger_choices = {'Pressed': 1, 'Released': 0}
        _manual_trigger_push_button.pressed.connect(lambda: self.set_manual_trigger(self._manual_trigger_choices['Pressed']))
        _manual_trigger_push_button.released.connect(lambda: self.set_manual_trigger(self._manual_trigger_choices['Released']))
        self.tab_output_grid_layout_0.addWidget(_manual_trigger_push_button, 1, 2, 1, 2)
        for r in range(1, 2):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 4):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self._daq_filename_tool_bar = Qt.QToolBar(self)
        self._daq_filename_tool_bar.addWidget(Qt.QLabel("Filename" + ": "))
        self._daq_filename_line_edit = Qt.QLineEdit(str(self.daq_filename))
        self._daq_filename_tool_bar.addWidget(self._daq_filename_line_edit)
        self._daq_filename_line_edit.editingFinished.connect(
            lambda: self.set_daq_filename(str(str(self._daq_filename_line_edit.text()))))
        self.tab_output_grid_layout_0.addWidget(self._daq_filename_tool_bar, 0, 0, 1, 4)
        for r in range(0, 1):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 4):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.sig = exciter_signals(
            samp_rate=fs_signal,
            upsample=upsample,
        )

        self.tab_excitation_grid_layout_0.addWidget(self.sig, 0, 0, 1, 1)
        for r in range(0, 1):
            self.tab_excitation_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.tab_excitation_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_waterfall_sink_x_0_0_0 = qtgui.waterfall_sink_f(
            16384, #size
            window.WIN_BLACKMAN_hARRIS, #wintype
            0, #fc
            fs_signal, #bw
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_waterfall_sink_x_0_0_0.set_update_time(0.1)
        self.qtgui_waterfall_sink_x_0_0_0.enable_grid(True)
        self.qtgui_waterfall_sink_x_0_0_0.enable_axis_labels(True)


        self.qtgui_waterfall_sink_x_0_0_0.set_plot_pos_half(not False)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        colors = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_waterfall_sink_x_0_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_waterfall_sink_x_0_0_0.set_line_label(i, labels[i])
            self.qtgui_waterfall_sink_x_0_0_0.set_color_map(i, colors[i])
            self.qtgui_waterfall_sink_x_0_0_0.set_line_alpha(i, alphas[i])

        self.qtgui_waterfall_sink_x_0_0_0.set_intensity_range(-80, 10)

        self._qtgui_waterfall_sink_x_0_0_0_win = sip.wrapinstance(self.qtgui_waterfall_sink_x_0_0_0.qwidget(), Qt.QWidget)

        self.top_grid_layout.addWidget(self._qtgui_waterfall_sink_x_0_0_0_win, 50, 0, 1, 1)
        for r in range(50, 51):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_number_sink_0 = qtgui.number_sink(
            gr.sizeof_float,
            0.01,
            qtgui.NUM_GRAPH_HORIZ,
            1,
            None # parent
        )
        self.qtgui_number_sink_0.set_update_time(0.10)
        self.qtgui_number_sink_0.set_title("")

        labels = ['RMS Signal level:', '', '', '', '',
            '', '', '', '', '']
        units = ['V', '', '', '', '',
            '', '', '', '', '']
        colors = [("black", "red"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
            ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]

        for i in range(1):
            self.qtgui_number_sink_0.set_min(i, 0)
            self.qtgui_number_sink_0.set_max(i, 1)
            self.qtgui_number_sink_0.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_number_sink_0.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_number_sink_0.set_label(i, labels[i])
            self.qtgui_number_sink_0.set_unit(i, units[i])
            self.qtgui_number_sink_0.set_factor(i, factor[i])

        self.qtgui_number_sink_0.enable_autoscale(False)
        self._qtgui_number_sink_0_win = sip.wrapinstance(self.qtgui_number_sink_0.qwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_number_sink_0_win, 40, 0, 1, 1)
        for r in range(40, 41):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_ledindicator_0_0_0_0 = self._qtgui_ledindicator_0_0_0_0_win = qtgui.GrLEDIndicator('Saving in progress', "lime", "black", 0, 35, 1, 1, 1, self)
        self.qtgui_ledindicator_0_0_0_0 = self._qtgui_ledindicator_0_0_0_0_win
        self.tab_output_grid_layout_0.addWidget(self._qtgui_ledindicator_0_0_0_0_win, 0, 4, 2, 1)
        for r in range(0, 2):
            self.tab_output_grid_layout_0.setRowStretch(r, 1)
        for c in range(4, 5):
            self.tab_output_grid_layout_0.setColumnStretch(c, 1)
        self.blocks_throttle2_0 = blocks.throttle( gr.sizeof_float*1, fs_signal, True, 0 if "auto" == "auto" else max( int(float(0.1) * fs_signal) if "auto" == "time" else int(0.1), 1) )
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_burst_tagger_0 = blocks.burst_tagger(gr.sizeof_float)
        self.blocks_burst_tagger_0.set_true_tag('SOB',True)
        self.blocks_burst_tagger_0.set_false_tag('NA',False)
        self.beam_exciter_burstFileSink_0_0 = beam_exciter.burstFileSink(gr.sizeof_float, daq_filename, ensure_pmt(pmt.intern("SOB")), ensure_pmt(pmt.intern("EOB")), (int(output_duration*fs_signal)), True)
        self.beam_exciter_LevelProbe_0 = beam_exciter.LevelProbe(1, 10000)
        self.analog_const_source_x_0 = analog.sig_source_s(0, analog.GR_CONST_WAVE, 0, 0, manual_trigger)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.beam_exciter_burstFileSink_0_0, 'write'), (self.qtgui_ledindicator_0_0_0_0, 'state'))
        self.connect((self.analog_const_source_x_0, 0), (self.blocks_burst_tagger_0, 1))
        self.connect((self.beam_exciter_LevelProbe_0, 1), (self.blocks_null_sink_0, 0))
        self.connect((self.beam_exciter_LevelProbe_0, 0), (self.qtgui_number_sink_0, 0))
        self.connect((self.blocks_burst_tagger_0, 0), (self.blocks_throttle2_0, 0))
        self.connect((self.blocks_throttle2_0, 0), (self.beam_exciter_burstFileSink_0_0, 0))
        self.connect((self.sig, 0), (self.beam_exciter_LevelProbe_0, 0))
        self.connect((self.sig, 0), (self.blocks_burst_tagger_0, 0))
        self.connect((self.sig, 0), (self.qtgui_waterfall_sink_x_0_0_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "exciter_to_file")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_upsample(self):
        return self.upsample

    def set_upsample(self, upsample):
        self.upsample = upsample
        self.sig.set_upsample(self.upsample)

    def get_output_duration(self):
        return self.output_duration

    def set_output_duration(self, output_duration):
        self.output_duration = output_duration
        self.beam_exciter_burstFileSink_0_0.set_length((int(self.output_duration*self.fs_signal)))

    def get_manual_trigger(self):
        return self.manual_trigger

    def set_manual_trigger(self, manual_trigger):
        self.manual_trigger = manual_trigger
        self.analog_const_source_x_0.set_offset(self.manual_trigger)

    def get_fs_signal(self):
        return self.fs_signal

    def set_fs_signal(self, fs_signal):
        self.fs_signal = fs_signal
        self.beam_exciter_burstFileSink_0_0.set_length((int(self.output_duration*self.fs_signal)))
        self.blocks_throttle2_0.set_sample_rate(self.fs_signal)
        self.qtgui_waterfall_sink_x_0_0_0.set_frequency_range(0, self.fs_signal)
        self.sig.set_samp_rate(self.fs_signal)

    def get_daq_filename(self):
        return self.daq_filename

    def set_daq_filename(self, daq_filename):
        self.daq_filename = daq_filename
        Qt.QMetaObject.invokeMethod(self._daq_filename_line_edit, "setText", Qt.Q_ARG("QString", str(self.daq_filename)))
        self.beam_exciter_burstFileSink_0_0.set_filename(self.daq_filename)




def main(top_block_cls=exciter_to_file, options=None):

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
